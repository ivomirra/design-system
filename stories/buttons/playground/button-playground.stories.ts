import { storiesOf } from '@storybook/angular';
import { withKnobs, select, boolean, text } from '@storybook/addon-knobs/angular';

storiesOf('Components|Buttons', module)
    .addDecorator(withKnobs)
    .addParameters({ options: { showPanel: true } })
    .add('Playground', () => ({
        templateUrl: 'button-playground.html',
        styleUrls: ['../buttons.css'],
        props: {
            text: text('Text', 'Button'),
            class: select(
                'Classes',
                [
                    '',
                    'green',
                    'green--active',
                    'red',
                    'red--active',
                    'blue',
                    'blue--active',
                    'tertiary--active',
                    'grey',
                    'secondary--active',
                ],
                'green'
            ),
            type: select(
                'Type',
                ['', 'secondary', 'tertiary'],
                ''
            ),
            disabled: boolean('disabled', false),
            role: select(
                'Role',
                ['', 'icon'],
                ''
            ),
            color: select(
                'Color',
                ['', 'grey'],
                ''
            ),
        },
    }))
