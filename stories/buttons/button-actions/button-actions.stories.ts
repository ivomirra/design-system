import { storiesOf, moduleMetadata } from '@storybook/angular';
import { actions } from '@storybook/addon-actions';

import { UiButtonActionsModule } from '../../../src/components/button-actions';

storiesOf('Components|Buttons', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiButtonActionsModule],
        })
    )
    .addParameters({ options: { showPanel: true } })
    .add('Button Actions', () => ({
        templateUrl: 'button-actions-example.html',
        styleUrls: ['../buttons.css'],
        props: {
            urlsList: {
                items: [
                    {
                        text: 'See Details',
                        callback: function() { actions('"See Details" was clicked') }
                    },
                    {
                        text: 'Copy Link',
                        callback: function() { actions('"Copy Link" was clicked') }
                    }
                ]
            },
        }
    }))
