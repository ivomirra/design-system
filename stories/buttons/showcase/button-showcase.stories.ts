import { storiesOf } from '@storybook/angular';

storiesOf('Components|Buttons', module)
    .addParameters({ options: { showPanel: false } })
    .add('Showcase', () => ({
        templateUrl: 'button-showcase.html',
        styleUrls: ['../buttons.css'],
        props: {}
    }))
