import { storiesOf } from '@storybook/angular';

storiesOf('Components|Messaging', module)
    .addParameters({ options: { showPanel: true } })
    .add('Messages', () => ({
        templateUrl: 'messages-example.html',
        styleUrls: ['./messages.css'],
        props: {}
    }));
