import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiTooltipsModule } from '../../../../src/components/messaging/tooltips';
import { TooltipModule } from '../../../../src/components/messaging/tooltip';

storiesOf('Components|Messaging', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiTooltipsModule, TooltipModule]
        })
    )
    .addParameters({ options: { showPanel: true } })
    .add('Tooltips', () => ({
        templateUrl: 'tooltips-example.html',
        styleUrls: ['./tooltips.css'],
        props: {
            tooltipViewModel: {
                title: 'So...',
                text: "Your bones don't break, mine do. That's clear."
            },
            redErrorTooltipViewModel: {
                title: 'So...',
                text: "Your bones don't break, mine do. That's clear.",
                icon: 'error',
                color: 'red'
            },
            tooltipWithNoTitleViewModel: {
                text: "This is a tooltip without title. Your bones don't break, mine do. That's clear.",
                icon: 'info',
                color: 'blue-grey-dark'
            },
            tooltipSmallTextViewModel: {
                text: 'Small text.',
                icon: 'info',
                color: 'blue-grey-dark'
            }
        }
    }));
