import { Component, Input } from '@angular/core';
import { SandboxUiStepperDummyComponent } from './ui-stepper-dummy.component';
@Component({
    selector: 'sandbox-ui-stepper-example',
    templateUrl: './ui-stepper-example.component.html'
})
export class SandboxUiStepperComponent {
    @Input()
    public context;
    public stepperViewModel = {
        items: [
            {
                title: 'First Step',
                component: SandboxUiStepperDummyComponent,
                context: {
                    text: 'This text comes from context for component 1'
                }
            },
            {
                title: 'Second Step',
                component: SandboxUiStepperDummyComponent,
                context: {
                    text: 'This text comes from context for component 2'
                }
            },
            {
                title: 'Third Step',
                component: SandboxUiStepperDummyComponent, context: {
                    text: 'This text comes from context for component 3'
                }
            }
        ],
        selectedIndex: 0
    };
    constructor() {}

    public resetStepper() {
        this.stepperViewModel = {
            items: [
                {
                    title: 'First Step',
                    component: SandboxUiStepperDummyComponent,
                    context: {
                        text: 'This text comes from context for component 1'
                    }
                },
                {
                    title: 'Second Step',
                    component: SandboxUiStepperDummyComponent,
                    context: {
                        text: 'This text comes from context for component 2'
                    }
                },
                {
                    title: 'Third Step',
                    component: SandboxUiStepperDummyComponent, context: {
                        text: 'This text comes from context for component 3'
                    }
                }
            ],
            selectedIndex: 0
        }
    }

}
