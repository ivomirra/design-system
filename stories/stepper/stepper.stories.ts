import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiStepperModule } from '../../src/components/stepper';

import { SandboxUiStepperComponent } from './ui-stepper-example.component';
import { SandboxUiStepperDummyComponent } from './ui-stepper-dummy.component';

storiesOf('Components|Stepper', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiStepperModule],
            entryComponents: [SandboxUiStepperDummyComponent],
            declarations: [SandboxUiStepperComponent, SandboxUiStepperDummyComponent],
        })
    )
    .addParameters({ options: { showPanel: false } })
    .add('Stepper', () => ({
        component: SandboxUiStepperComponent,
        props: {}
    }));
