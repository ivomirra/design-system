import { storiesOf } from '@storybook/angular';

storiesOf('Styleguide|Typography', module)
    .addParameters({ options: { showPanel: false } })
    .add('Typography', () => ({
        templateUrl: 'typography.html',
        styleUrls: ['typography.css'],
        props: {}
    }));
