import { storiesOf } from '@storybook/angular';

storiesOf('Styleguide|Iconography', module)
    .addParameters({ options: { showPanel: false } })
    .add('Iconography', () => ({
        templateUrl: 'iconography.html',
        styleUrls: ['iconography.css'],
        props: {}
    }));
