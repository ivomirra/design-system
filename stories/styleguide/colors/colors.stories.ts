import { storiesOf } from '@storybook/angular';

const getAllVariables = () =>
    Array.from(document.styleSheets)
        .filter((sheet) => sheet.href === null || sheet.href.startsWith(window.location.origin))
        .reduce(
            (acc, sheet) =>
                (acc = [
                    ...acc,
                    ...Array.from(sheet.cssRules).reduce(
                        (def, rule) =>
                            (def =
                                rule.selectorText === ':root'
                                    ? [...def, ...Array.from(rule.style).filter((name) => name.startsWith('--'))]
                                    : def),
                        []
                    )
                ]),
            []
        )
        .sort();

const variables = getAllVariables()
    .filter((variable) => variable.indexOf('--font') < 0 && variable.indexOf('bar-height') < 0)
    .map((variable) => {
        const isRGB = variable.indexOf('-rgb') > -1;
        const isBoxShadow = variable.indexOf('box-shadow') > -1;
        const isGradient = variable.indexOf('-gradient') > -1;
        const isRegularColor = !isBoxShadow && !isGradient;

        const colorVar = `var(${variable})`;
        const finalColorVar = isRGB ? `rgb(${colorVar}, 1)` : `${colorVar}`;
        const colorValue = getComputedStyle(document.documentElement).getPropertyValue(variable);

        return {
            variable: finalColorVar,
            name: variable,
            value: colorValue,
            isBoxShadow,
            isGradient,
            isRegularColor,
        };
    });

storiesOf('Styleguide|Colors', module)
    .addParameters({ options: { showPanel: false } })
    .add('Colors', () => ({
        templateUrl: 'colors.html',
        styleUrls: ['colors.css'],
        props: {
            variables
        }
    }));
