import { Component, OnInit } from '@angular/core';
import { UiPaginationViewModelHelper } from './../../../src/components/pagination/ui-pagination.view-model.helper'
import { PaginationViewModelInterface } from './../../../src/components/pagination/ui-pagination.interface';
import { UI_PAGINATION_CONSTANT } from './../../../src/components/pagination/ui-pagination.constant';


@Component({
    selector: 'sandbox-pagination-example',
    templateUrl: './pagination-example.html'
})
export class SandboxUiPaginationComponent implements OnInit {
    public paginationViewModel: PaginationViewModelInterface;
    private TOTAL_ITEMS_PER_PAGE = 10;
    private NUMBER_OF_PAGES = 9;
    private TOTAL_NUMBER_OF_ITEMS = 85;

    constructor(private paginationViewModelHelper: UiPaginationViewModelHelper) {}
    ngOnInit() {
         this.paginationViewModel = this.paginationViewModelHelper.getGenericPaginationViewModel(
             this.TOTAL_ITEMS_PER_PAGE,
             null,
             UI_PAGINATION_CONSTANT.DIRECTIONS.NEXT,
             1,
             this.NUMBER_OF_PAGES,
             this.TOTAL_NUMBER_OF_ITEMS);
    }

    public onPaginationChanged(pageDirection) {
        this.paginationViewModel = this.paginationViewModelHelper.getUpdatedPaginationViewModel(
            this.paginationViewModel,
            pageDirection);

        const previousLink =
            this.paginationViewModelHelper.getPreviousLinkOfflinePagination(this.paginationViewModel.currentPage);
        const nextLink =
            this.paginationViewModelHelper.getNextLinkOfflinePagination(this.paginationViewModel.currentPage,
                this.NUMBER_OF_PAGES);

        this.paginationViewModel = this.paginationViewModelHelper.getGenericPaginationViewModel(
            this.TOTAL_ITEMS_PER_PAGE,
            previousLink,
            nextLink,
            this.paginationViewModel.currentPage,
            this.NUMBER_OF_PAGES,
            this.TOTAL_NUMBER_OF_ITEMS);
    }
}
