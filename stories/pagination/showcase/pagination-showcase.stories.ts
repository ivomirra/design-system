import { storiesOf, moduleMetadata } from '@storybook/angular';

import { SandboxUiPaginationComponent } from './ui-pagination-example.component';
import { UiPaginationModule } from '../../../src/components/pagination';

storiesOf('Components|Pagination', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiPaginationModule],
            declarations: [SandboxUiPaginationComponent],
        })
    )
    .addParameters({ options: { showPanel: false } })
    .add('Pagination', () => ({
        component: SandboxUiPaginationComponent,
        props: {}
    }));
