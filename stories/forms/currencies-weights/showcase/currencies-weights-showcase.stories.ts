import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiCurrenciesDropdownModule } from '../../../../src/components/forms/currency-dropdown';
import { UiCurrencyInputModule } from '../../../../src/components/forms/currency-input';
import { UiWeightUnitsDropdownModule } from '../../../../src/components/forms/weight-units-dropdown';

storiesOf('Components|Forms', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiCurrenciesDropdownModule, UiCurrencyInputModule, UiWeightUnitsDropdownModule]
        })
    )
    .addParameters({ options: { showPanel: false } })
    .add('Currencies & Weights', () => ({
        templateUrl: 'currencies-weights-example.html',
        props: {
            onChange: (value) => {
                console.log(value);
            },
            onInputChange: (value) => {
                console.log(value);
            }
        }
    }));
