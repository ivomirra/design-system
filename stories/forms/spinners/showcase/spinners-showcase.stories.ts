import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiSpinnerModule } from '../../../../src/components/forms/spinner';

storiesOf('Components|Forms', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiSpinnerModule]
        })
    )
    .addParameters({ options: { showPanel: false } })
    .add('Spinners', () => ({
        templateUrl: 'spinners-example.html',
        props: {}
    }));
