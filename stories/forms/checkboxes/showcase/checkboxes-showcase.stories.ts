import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiCheckboxIndeterminateModule } from '../../../../src/components/forms/checkbox-indeterminate';

storiesOf('Components|Forms', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiCheckboxIndeterminateModule]
        })
    )
    .addParameters({ options: { showPanel: false } })
    .add('Checkboxes', () => ({
        templateUrl: 'checkboxes-example.html',
        props: {}
    }));
