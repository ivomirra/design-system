import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiInputSearchModule } from '../../../../src/components/forms/search';

storiesOf('Components|Forms', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiInputSearchModule]
        })
    )
    .addParameters({ options: { showPanel: false } })
    .add('Search Input', () => ({
        templateUrl: 'search-input-example.html',
        props: {
            inputSearchViewModel: {
                placeholder: 'Type something to search',
                value: ''
            },
            onFormSubmited: function($event) {
                console.log($event, '<-- search input SUBMITED');
            },
            onInputChange: function($event) {
                console.log($event, '<-- search input CHANGED');
            }
        }
    }));
