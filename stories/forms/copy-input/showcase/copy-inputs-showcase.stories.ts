import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiCopyInputModule } from '../../../../src/components/forms/copy-input';

storiesOf('Components|Forms', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiCopyInputModule]
        })
    )
    .addParameters({ options: { showPanel: false } })
    .add('Copy Inputs', () => ({
        templateUrl: 'copy-inputs-example.html',
        props: {
            onSubmit: (value) => {
                console.log(value);
            },
            currencyLabel: {
                value: `Carriage value with tooltip`,
                tooltipViewModel: {
                    text: 'Tooltip test example.',
                    icon: 'info',
                    color: 'blue-grey-dark',
                    size: 'extra-extra-small'
                }
            },
            uiCopyViewModel: {
                editText: 'Editar',
                tooltipTextBeforeCopy: 'Clique para copiar o texto',
                tooltipTextAfterCopy: 'Texto copiado com sucesso'
            }
        }
    }));
