import { Component } from '@angular/core';

@Component({
    selector: 'sandbox-forms-example',
    templateUrl: './forms-example.html'
})
export class SandboxFormsComponent {
    public currencyLabel = {value: `Carriage value with tooltip`, tooltipViewModel: {
        text: 'Tooltip test example.',
        icon: 'info',
        color: 'blue-grey-dark',
        size: 'extra-extra-small'
    }};

    public uploadFilesViewModel = {
        isUploading: false,
        isUploaded: false,
        fileUploadPercentage: 0,
        fileName: ''
    };

    constructor() {
        console.log(this.currencyLabel);
    }

    public uploadFileOnSelectedFile(payload) {
        setInterval(() => {
            this.uploadFilesViewModel.fileUploadPercentage++;
        }, 1000);
        console.log(payload);
    }

    public uploadFileReplace(payload) {
        console.log(payload);
    }

    public uploadFileOnRemovedFile(payload) {
        console.log(payload);
    }

    public onFormSubmited($event) {
        console.log($event, '<-- search input SUBMITED');
    }

    public onInputChange($event) {
        console.log($event, '<-- search input CHANGED');
    }

    public onSubmit(value) {
        console.log(value);
    }

    public onChange(value) {
        console.log(value);
    }
}
