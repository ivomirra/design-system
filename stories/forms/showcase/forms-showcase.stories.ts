import { storiesOf, moduleMetadata } from '@storybook/angular';

import { SandboxFormsComponent } from './forms-example.component';
import { UiCheckboxIndeterminateModule } from './../../../src/components/forms/checkbox-indeterminate';
import { UiCopyInputModule } from './../../../src/components/forms/copy-input';
import { UiCurrenciesDropdownModule } from '../../../src/components/forms/currency-dropdown';
import { UiCurrencyInputModule } from './../../../src/components/forms/currency-input';
import { UiDateInputModule } from '../../../src/components/forms/date-input';
import { UiInputSearchModule } from '../../../src/components/forms/search';
import { UiProgressBarModule } from '../../../src/components/progress-bar';
import { UiSpinnerModule } from '../../../src/components/forms/spinner';
import { UiTooltipsModule } from './../../../src/components/messaging/tooltips';
import { UiUploadFileModule } from '../../../src/components/forms/upload-files';
import { UiWeightUnitsDropdownModule } from '../../../src/components/forms/weight-units-dropdown';

storiesOf('Components|Forms', module)
    .addDecorator(
        moduleMetadata({
            imports: [
                UiDateInputModule,
                UiInputSearchModule,
                UiSpinnerModule,
                UiUploadFileModule,
                UiProgressBarModule,
                UiCurrencyInputModule,
                UiCopyInputModule,
                UiTooltipsModule,
                UiCheckboxIndeterminateModule,
                UiWeightUnitsDropdownModule,
                UiCurrenciesDropdownModule
            ]
        })
    )
    .addParameters({ options: { showPanel: false } })
    .add('EVERYTHING', () => ({
        component: SandboxFormsComponent,
        props: {
            inputSearchViewModel: {
                placeholder: 'Type something to search',
                value: ''
            },
            tooltipViewModel: {
                text: 'Tooltip test example.',
                icon: 'info',
                color: 'blue-grey-dark'
            },
            currencyLabel: {
                value: `Carriage value with tooltip`,
                tooltipViewModel: {
                    text: 'Tooltip test example.',
                    icon: 'info',
                    color: 'blue-grey-dark',
                    size: 'extra-extra-small'
                }
            },
            monthNames: [
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
            ],
            datesDisabled: [new Date('2018-12-31'), new Date('2018-12-30')],
            inputDateValue: new Date('2018-12-12'),
            datesWithBullets: [
                {
                    date: new Date('2018-12-12'),
                    tooltip: 'Delivery 1'
                },
                {
                    date: new Date('2018-12-17'),
                    tooltip: 'Delivery 2'
                },
                {
                    date: new Date('2018-12-18'),
                    tooltip: 'Delivery 3'
                }
            ],
            uploadFilesViewModel: {
                isUploading: false,
                isUploaded: false,
                fileUploadPercentage: 0,
                fileName: ''
            },
            uiCopyViewModel: {
                editText: 'Editar',
                tooltipTextBeforeCopy: 'Clique para copiar o texto',
                tooltipTextAfterCopy: 'Texto copiado com sucesso'
            },
            weekDays: ['S', 'M', 'T', 'W', 'T', 'F', 'S']
        }
    }));
