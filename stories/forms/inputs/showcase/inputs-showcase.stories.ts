import { storiesOf } from '@storybook/angular';

storiesOf('Components|Forms', module)
    .addParameters({ options: { showPanel: false } })
    .add('Inputs', () => ({
        templateUrl: 'inputs-example.html',
        props: {}
    }))
