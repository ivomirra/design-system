import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiDateInputModule } from '../../../../src/components/forms/date-input';

storiesOf('Components|Forms', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiDateInputModule]
        })
    )
    .addParameters({ options: { showPanel: false } })
    .add('Date Input', () => ({
        templateUrl: 'date-input-example.html',
        props: {
            monthNames: [
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
            ],
            datesDisabled: [new Date('2018-12-31'), new Date('2018-12-30')],
            inputDateValue: new Date('2018-12-12'),
            datesWithBullets: [
                {
                    date: new Date('2018-12-12'),
                    tooltip: 'Delivery 1'
                },
                {
                    date: new Date('2018-12-17'),
                    tooltip: 'Delivery 2'
                },
                {
                    date: new Date('2018-12-18'),
                    tooltip: 'Delivery 3'
                }
            ],
            weekDays: ['S', 'M', 'T', 'W', 'T', 'F', 'S']
        }
    }));
