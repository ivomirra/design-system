import { storiesOf } from '@storybook/angular';

storiesOf('Components|Forms', module)
    .addParameters({ options: { showPanel: false } })
    .add('Select Box', () => ({
        templateUrl: 'select-example.html',
        props: {}
    }));
