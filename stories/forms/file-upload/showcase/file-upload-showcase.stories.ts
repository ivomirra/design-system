import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiUploadFileModule } from '../../../../src/components/forms/upload-files';

storiesOf('Components|Forms', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiUploadFileModule]
        })
    )
    .addParameters({ options: { showPanel: false } })
    .add('File Upload', () => ({
        templateUrl: 'file-upload-example.html',
        props: {
            uploadFilesViewModel: {
                isUploading: false,
                isUploaded: false,
                fileUploadPercentage: 0,
                fileName: ''
            },
            uploadFileOnSelectedFile: function(payload) {
                setInterval(() => {
                    this.uploadFilesViewModel.fileUploadPercentage++;
                }, 1000);
                console.log(payload);
            },
            uploadFileReplace: (payload) => {
                console.log(payload);
            },
            uploadFileOnRemovedFile: (payload) => {
                console.log(payload);
            }
        }
    }));
