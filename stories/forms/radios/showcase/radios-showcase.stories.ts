import { storiesOf } from '@storybook/angular';

storiesOf('Components|Forms', module)
    .addParameters({ options: { showPanel: false } })
    .add('Radios', () => ({
        templateUrl: 'radios-example.html',
        props: {}
    }));
