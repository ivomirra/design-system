import { storiesOf } from '@storybook/angular';

storiesOf('Components|Forms', module)
    .addParameters({ options: { showPanel: false } })
    .add('Textareas', () => ({
        templateUrl: 'textareas-example.html',
        props: {}
    }));
