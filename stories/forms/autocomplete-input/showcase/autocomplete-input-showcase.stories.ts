import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiAutoCompleteModule } from '../../../../src/components/auto-complete';

storiesOf('Components|Forms', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiAutoCompleteModule]
        })
    )
    .addParameters({ options: { showPanel: false } })
    .add('Autocomplete Input', () => ({
        templateUrl: 'autocomplete-input-example.html',
        props: {
            optionsList: [
                { text: 'Afghanistan' },
                { text: 'Albania' },
                { text: 'Algeria' },
                { text: 'Andorra' },
                { text: 'Angola' },
                { text: 'Anguilla' },
                { text: 'Antigua & Barbuda' },
                { text: 'Argentina' },
                { text: 'Armenia' },
                { text: 'Aruba' },
                { text: 'Australia' },
                { text: 'Austria' },
                { text: 'Azerbaijan' },
                { text: 'Bahamas' },
                { text: 'Bahrain' },
                { text: 'Bangladesh' },
                { text: 'Barbados' },
                { text: 'Belarus' },
                { text: 'Belgium' },
                { text: 'Belize' },
                { text: 'Benin' },
                { text: 'Bermuda' },
                { text: 'Bhutan' },
                { text: 'Bolivia' },
                { text: 'Bosnia & Herzegovina' },
                { text: 'Botswana' },
                { text: 'Brazil' },
                { text: 'British Virgin Islands' },
                { text: 'Brunei' },
                { text: 'Bulgaria' },
                { text: 'Burkina Faso' },
                { text: 'Burundi' },
                { text: 'Cambodia' },
                { text: 'Cameroon' },
                { text: 'Canada' },
                { text: 'Cape Verde' },
                { text: 'Cayman Islands' },
                { text: 'Central Arfrican Republic' },
                { text: 'Chad' },
                { text: 'Chile' },
                { text: 'China' }
            ],
            onItemsChanged: function(itemsConfig) {
                console.log(itemsConfig);
            }
        }
    }));
