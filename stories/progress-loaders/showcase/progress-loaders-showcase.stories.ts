import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiProgressBarModule } from '../../../src/components/progress-bar';

storiesOf('Components|Progress & Loaders', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiProgressBarModule]
        })
    )
    .addParameters({ options: { showPanel: false } })
    .add('Progress Bars', () => ({
        templateUrl: './progress-loaders-example.html',
        props: {}
    }));
