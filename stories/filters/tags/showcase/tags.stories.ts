import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiFilterTagsModule } from '../../../../src/components/filter-tags';

storiesOf('Components|Filters', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiFilterTagsModule],
        })
    )
    .addParameters({ options: { showPanel: true } })
    .add('Tags', () => ({
        templateUrl: 'tags-example.html',
        props: {
            filterTagsViewModel: {
                items: [
                    {
                        text: 'Filter - 1'
                    },
                    {
                        text: 'Filter - 2'
                    }
                ]
            }
        }
    }))
