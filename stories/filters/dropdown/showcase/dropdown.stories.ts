import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiFilterModule } from '../../../../src/components/filter';

storiesOf('Components|Filters', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiFilterModule],
        })
    )
    .addParameters({ options: { showPanel: true } })
    .add('Dropdown', () => ({
        templateUrl: 'dropdown-example.html',
        props: {
            filterViewModel: {
                filterText: 'Filter',
                buttonText: 'Add',
                selectBox: [
                    {
                        text: 'Select'
                    },
                    {
                        text: 'Account',
                        value: 'value - 1'
                    },
                    {
                        text: 'Following',
                        value: 'value - 2',
                        type: 'following'
                    },
                    {
                        text: 'Status',
                        value: 'value - 3',
                        type: 'status'
                    }
                ],
                selectOptions: {
                    account: [
                        {
                            text: 'Primary',
                            value: true
                        },
                        {
                            text: 'Secondary',
                            value: false
                        }
                    ],

                    following: [
                        {
                            text: 'Following',
                            field: 'is_following',
                            value: true
                        },
                        {
                            text: 'Not Following',
                            field: 'is_following',
                            value: false
                        }
                    ],

                    status: [
                        {
                            text: 'Active',
                            value: true
                        },
                        {
                            text: 'Not Active',
                            value: false
                        }
                    ]
                }
            },
            onFilterAdded: (filterInfo) => {
                console.log(filterInfo);
            }
        }
    }))
