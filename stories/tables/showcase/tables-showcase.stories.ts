import { storiesOf } from '@storybook/angular';

storiesOf('Components|Tables', module)
    .addParameters({ options: { showPanel: false } })
    .add('Tables', () => ({
        templateUrl: './tables-example.html',
        props: {
            tableViewModel: [
                {
                    id: 0,
                    name: 'Name 01',
                    creationDate: Date.now(),
                    status: 'Processed'
                },
                {
                    id: 1,
                    name: 'Name 02',
                    creationDate: Date.now(),
                    status: 'Waiting'
                },
                {
                    id: 2,
                    name: 'Name 03',
                    creationDate: Date.now(),
                    status: 'Processed'
                },
                {
                    id: 3,
                    name: 'Name 04',
                    creationDate: Date.now(),
                    status: 'Processed'
                }
            ]
        }
    }));
