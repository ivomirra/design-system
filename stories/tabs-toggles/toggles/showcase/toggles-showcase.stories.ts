import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiToggleButtonsModule } from '../../../../src/components/toggle-buttons';

storiesOf('Components|Tabs & Toggles', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiToggleButtonsModule]
        })
    )
    .addParameters({ options: { showPanel: false } })
    .add('Toggles', () => ({
        templateUrl: './toggles-example.html',
        props: {
            sandBoxViewModel: {
                items: [
                    {
                        text: 'Item - 1'
                    },
                    {
                        text: 'Item - 2',
                        disabled: true
                    },
                    {
                        text: 'Item - 3'
                    }
                ],
                selectedIndex: 0
            },
            sandBoxFlatViewModel: {
                items: [
                    {
                        text: 'Wholesale (81)'
                    },
                    {
                        text: 'E-commerce (15)'
                    }
                ],
                selectedIndex: 0
            }
        }
    }));
