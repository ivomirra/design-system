import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiTabsModule } from '../../../../src/components/tabs/index';

storiesOf('Components|Tabs & Toggles', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiTabsModule]
        })
    )
    .addParameters({ options: { showPanel: false } })
    .add('Tabs', () => ({
        templateUrl: './tabs-example.html',
        props: {
            sandBoxViewModel: {
                items: [
                    {
                        text: 'Item - 1'
                    },
                    {
                        text: 'Item - 2',
                        disabled: true
                    },
                    {
                        text: 'Item - 3'
                    }
                ],
                selectedIndex: 0
            },
            sandBoxViewModelWithNotification: {
                items: [
                    {
                        text: 'Item - 1 <span class="ui-notification-counter" color="red">16</span>'
                    },
                    {
                        text: 'Item - 2 <span class="ui-notification-counter">16</span>'
                    },
                    {
                        text: 'Item - 3 <span class="ui-notification-counter" color="blue">16</span>'
                    },
                    {
                        text: 'Item - 4 <span class="ui-notification-counter" color="green">16</span>'
                    }
                ],
                selectedIndex: 0
            }
        }
    }));
