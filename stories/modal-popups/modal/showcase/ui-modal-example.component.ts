import { Component , OnInit, Inject } from '@angular/core';

import { UiModalService } from '../../../../src/components/modal/ui-modal.service';

@Component({
    selector: 'ui-modal-example',
    templateUrl: './modal-template.html'
})
export class SandboxUiModalComponent implements OnInit {
    public viewModel = {};

    constructor(
        private modal: UiModalService,
        @Inject('MODAL_DATA') private componentViewModel
    ) {}

    ngOnInit() {
        console.log(this.componentViewModel);
    }

    public onSubmitClicked() {
        this.modal.close({
            cenas: 'cenas'
        });
    }
}
