import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiModalModule } from '../../../../src/components/modal';
import { SandboxUiModalComponent } from './ui-modal-example.component';
import { SandboxUiModalIntegrationComponent } from './ui-modal-integration-example.component';

storiesOf('Components|Modals & Popups', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiModalModule],
            entryComponents: [SandboxUiModalComponent],
            declarations: [SandboxUiModalComponent, SandboxUiModalIntegrationComponent]
        })
    )
    .addParameters({ options: { showPanel: false } })
    .add('Modal', () => ({
        component: SandboxUiModalIntegrationComponent,
        props: {}
    }));
