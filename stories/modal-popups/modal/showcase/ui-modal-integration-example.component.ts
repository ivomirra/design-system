import { Component } from '@angular/core';
import { SandboxUiModalComponent } from './ui-modal-example.component';
import { UiModalComponent } from '../../../../src/components/modal/ui-modal.component';
import { UiModalService } from '../../../../src/components/modal/ui-modal.service';

@Component({
    selector: 'sandbox-ui-modal-example',
    templateUrl: './modal-example.html'
})
export class SandboxUiModalIntegrationComponent {
    constructor(private uiModal: UiModalService) {}

    public openModal(): void {
        this.uiModal
            .openModal(UiModalComponent, [SandboxUiModalComponent], {
                title: 'O Meu modal',
                formViewModel: {
                    userName: 'stuff'
                }
            })
            .afterClose()
            .subscribe(data => {
                console.log(data);
            });
    }
}
