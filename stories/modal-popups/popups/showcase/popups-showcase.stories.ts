import { storiesOf, moduleMetadata } from '@storybook/angular';

import { UiPopupMessageModule } from '../../../../src/components/popup-message';

storiesOf('Components|Modals & Popups', module)
    .addDecorator(
        moduleMetadata({
            imports: [UiPopupMessageModule]
        })
    )
    .addParameters({ options: { showPanel: false } })
    .add('Popups', () => ({
        templateUrl: './popups-example.html',
        props: {
            type: undefined,
            sandboxSuccessModalViewModel: {
                isVisible: false,
                title: 'Success Header',
                message: `You will have enough stock to fulfill this delivery.
                    Are you sure you want to realocate the products?`,
                submit: {
                    button_text: 'Save',
                    callback: function() {
                        this.testFunction();
                    }
                },
                cancel: {
                    button_text: 'Cancel',
                    callback: function() {}
                }
            },
            sandboxErrorModalViewModel: {
                isVisible: false,
                title: 'Error Header',
                message: `You will have enough stock to fulfill this delivery.
                    Are you sure you want to realocate the products?`,
                submit: {
                    button_text: 'Save',
                    callback: function() {
                        this.testFunction();
                    }
                },
                cancel: {
                    button_text: 'Cancel',
                    callback: function() {}
                }
            },
            sandboxNeutralModalViewModel: {
                isVisible: false,
                title: 'Neutral Header',
                message: `You will have enough stock to fulfill this delivery.
                    Are you sure you want to realocate the products?`,
                submit: {
                    button_text: 'Save',
                    callback: function() {
                        this.testFunction();
                    }
                },
                cancel: {
                    button_text: 'Cancel',
                    callback: function() {}
                }
            },
            popupSuccessVisibility: function() {
                this.sandboxSuccessModalViewModel.isVisible = !this.sandboxSuccessModalViewModel.isVisible;
            },
            popupErrorVisibility: function() {
                this.sandboxErrorModalViewModel.isVisible = !this.sandboxErrorModalViewModel.isVisible;
            },
            popupNeutralVisibility: function() {
                this.sandboxNeutralModalViewModel.isVisible = !this.sandboxNeutralModalViewModel.isVisible;
            },
            testFunction: function() {
                alert('works');
            }
        }
    }));
