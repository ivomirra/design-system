# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)

## [Released]


## [5.5.2] - 2019-10-03

### Feat

- Adds a new color variable `--white-light: #fefefe`

## [Released]


## [5.5.1] - 2019-10-03

### Fix

- Removed the background of the calendar icon when the date input field is disabled

## [Released]


## [5.5.0] - 2019-10-01

### Added

- Husky as a dependency
- Update colors

## [Released]


## [5.4.2] - 2019-09-23

### Added

- Added new `merge` icon

## [Released]


## [5.4.1] - 2019-09-09

### Added

- Update ui-upload-file element click

## [Released]


## [5.4.0] - 2019-09-03

### Added

- Added filter funcionality to currencies dropdown
- Added filter funcionality to weight-units dropdown


## [Released]


## [5.3.0] - 2019-08-29

### Added

- Added weight unit dropdown
- Added currencies  dropdown


## [Released]


## [5.2.1] - 2019-08-29

### Fix

- Fix tooltip directive public methods

## [Released]


## [5.2.0] - 2019-08-28

### Added

- New tooltip directive component
- Select box disable status

## [Released]


## [5.1.0] - 2019-08-26

### Added

- Sandbox for icons

### Updated

- Update icon fonts

### Removed

- Removed ui-upload-images component

## [Released]


## [5.0.9] - 2019-08-20

### Update

- Bump angular version to 8.2.0

## [Released]


## [4.9.6] - 2019-07-02

### Fix

- Minor fix in `checkbox-indeterminate` component

## [4.9.5] - 2019-06-26

### Added

- Added `checkbox-indeterminate` component

## [4.9.4] - 2019-05-09

### Fixed

- Fixed the way variable are being passed to components in `ui-currency-input`

## [4.9.3] - 2019-05-08

### Added

- Added `editable` and `disabled` input for `ui-copy-input` and `ui-currency-input`

## [Released]

## [4.9.2] - 2019-05-07

### Fixed

- Improved `ui-copy-input` component

## [Released]

## [4.9.1] - 2019-05-02

### Fixed

- Improved `ui-currency-input` component
- Improved `ui-copy-input` component

## [Released]

## [4.9.0] - 2019-05-02

### Added

- Added `ui-copy-input` component

### Fixed

- Improved `ui-currency-input` component
- Improved `ui-stepper` component

## [Released]

## [4.8.0] - 2019-04-05

### Added

- Added `ui-stepper` component
- Added `ui-currency-input` component
- Added new colors

### Fixed

- Lib is now exporting the pagination viewModel helper

## [Released]

## [4.7.0] - 2019-03-29

### Added

- Added `ui-file-upload` component
- Added themes in `ui-tabs`
- Added `ui-notification-counter` class
- Added `ui-tables` class
- Added `ui-pagination` component

### Fixed

- Tooltip styling

### Removed

- Removed previous version of table.

## [Released]

## [4.6.2] - 2019-03-18

### Removed

- Removed border radius from input search

## [Released]

## [4.6.1] - 2019-03-18

### Fixed

- Fix some weight font

## [Released]

## [4.6.0] - 2019-03-13

### Fixed

- Only closes `ui-input-date` when exists a click outside the component

## [Released]

## [4.5.0] - 2019-03-05

### Added

- onChange event to `ui-input-search`
- clear icon to reset the searched string in `ui-input-search`
- on `ui-filter` component when user saves the filter the component should toggle his calendarToggleVisibility
- implement validation to show save button into `ui-filter` component

## [Released]

## [4.4.0] - 2019-03-04

### Added

- Export fonts variables.

### Fixed

- Filter icon size.
- Buttons disable state

## [Released]

## [4.3.0] - 2019-02-28

### Fixed

- Modal component to have dynamic content

## [4.2.0] - 2019-02-27

### Added

- Implement isInvalidDate as a mutable property
- Implement the possibility of disable weekend days

### Fixed

- Selected dates in the past should be visible in the calendar

## [4.1.0] - 2019-02-12

### Fixed

- Fixed ui-filter in firefox

## [Released]

## [4.0.0] - 2019-02-12

### Added

- New avenir fonts

## [Released]

## [3.6.0] - 2019-01-16

### Added

- Add rgb colors into variables

## [3.5.1] - 2019-01-15

### Changed

- Update font weights

## [Released]

## [3.5.0] - 2019-01-10

### Changed

- Add new icon font version

## [Released]

## [3.4.1] - 2019-01-02

### Changed

- Export popup message component class

## [Released]

## [3.4.0] - 2018-12-20

### Added

- Added new funcionalities to ui-data-input

### Changed

- Fix ui-filter object binding

## [Released]

## [3.3.0] - 2018-11-23

### Added

- New input type toggle
- New color

## [Released]

## [3.2.0] - 2018-11-19

### Added

- New object merge function to helper class

## [Released]

## [3.1.1] - 2018-11-16

### Changed

- Update ui-filter-tags styles

## [Released]

## [3.1.0] - 2018-11-16

### Added

- Added parent selected option in ui-filter component

## [Released]

## [3.0.1] - 2018-11-16

### Changed

- Fixed array reference assignment in ui-filter

## [Released]

## [3.0.0] - 2018-11-15

### Removed

- Activity timeline component
- Menu bar component
- Remove circular loader
- Lodash dependency
- Material design icons dependency

### Changed

- Filter component icons
- Add new icon font version
- Update toggle button current types to themes

### Added
- New toggle button type

## [Released]

## [2.9.2] - 2018-11-09

### Changed

- Updated forms padding (except text area)

## [Released]

## [2.9.1] - 2018-10-01

### Changed

- Update calendar container size

## [Released]

## [2.9.0] - 2018-10-01

### Changed

- Add black theme to tooltip component

## [Released]

## [2.8.1] - 2018-09-28

### Changed

- Fix icon fonts

## [Released]

## [2.8.0] - 2018-09-20

### Changed

- Add the possibility to render HTML in tooltip component

## [Released]

## [2.7.1] - 2018-09-18

### Changed

- Fix icon font files

## [Released]

## [2.7.0] - 2018-09-18

### Changed

- Add icons sizes
- Add arrow up and arrow down icons

## [Released]

## [2.6.0] - 2018-09-18

### Changed

- Tooltip can now have its size, color and icon customized

## [Released]

## [2.5.0] - 2018-09-17

### Changed

- Add error icon
- Add red color to icons

## [Released]

## [2.4.1] - 2018-09-13

### Changed

- Fix red secondary button background hover color

## [Released]

## [2.4.0] - 2018-09-07

### Changed

- Add red secondary button

## [Released]

## [2.3.0] - 2018-09-05

### Changed

- Icon font files with more icons
- Used new font files wherever possible

## [Released]

## [2.2.1] - 2018-08-31

### Changed

- Hot fix into lib export

## [Released]

## [2.2.0] - 2018-08-31

### Changed

- Add in popup component the possibility to render HTML

## [Released]

## [2.1.4] - 2018-08-29

### Changed

- Added new icon font

## [Released]

## [2.1.3] - 2018-08-28

### Changed

- Fix buttons style
- Fix pop up style
- Fix tabs style
- Fix filter tags style

## [Released]

## [2.1.2] - 2018-07-30

### Changed

- Add white spaces to sandbox templates
- Fix package.json version
- Fix progress bar background color

## [Released]

## [2.1.1] - 2018-07-27

### Changed

- Add new class `blue-grey` for `small` tag
- Reset all margins and paddings

## [Released]

## [2.1.0] - 2018-07-26

### Changed

- Fix dependency problems

## [Released]

## [2.0.0] - 2018-07-20

### Added

- Neutral popup styling.
- Error and neutral popups in the sandbox.
- Progress bar component

### Changed

- Typography styling rules.
- Search input box style.
- Checkbox style.
- Radio button style.
- Filter Tags style.
- Update colors.
- Move variables to variables file

### Removed

#### Available global css classes
- `hb-bg__white`
- `hb-bg__azure`
- `hb-bg__green`
- `hb-bg__green-active`
- `hb-bg__light-azure`
- `hb-bg__yellow`
- `hb-bg__yellow-active`
- `hb-bg__red`
- `hb-bg__red-active`
- `hb-bg__red-light`
- `hb-bg__grey`
- `hb-bg__light-grey`
- `hb-bg__dark-grey`
- `hb-cl__white`
- `hb-cl__azure`
- `hb-cl__light-azure`
- `hb-cl__green`
- `hb-cl__green-active`
- `hb-cl__yellow`
- `hb-cl__yellow-active`
- `hb-cl__red`
- `hb-cl__red-active`
- `hb-cl__red-light`
- `hb-cl__grey`
- `hb-cl__light-grey`
- `hb-cl__dark-grey`
- `hb-bd__white`
- `hb-bd__azure`
- `hb-bd__light-azure`
- `hb-bd__green`
- `hb-bd__green-active`
- `hb-bd__yellow`
- `hb-bd__yellow-active`
- `hb-bd__red`
- `hb-bd__red-active`
- `hb-bd__red-light`
- `hb-bd__grey`
- `hb-bd__light-grey`
- `hb-bd__dark-grey`

## [Released]

## [1.0.0] - 2018-07-02
### Available components
- `ui-activity-timeline`
- `ui-auto-complete`
- `ui-button-actions`
- `ui-filter`
- `ui-filter-tags`
- `ui-date-input`
- `ui-input-search`
- `ui-spinner`
- `ui-upload-image`
- `ui-loader`
- `ui-menu-bar`
- `ui-tooltips`
- `ui-popup-message`
- `ui-tables`
- `ui-tabs`
- `ui-toggle-buttons`

### Available helpers
- `MaterialHelper`
- `UiModalService`

### Available global css classes
- `hb-bg__white`
- `hb-bg__azure`
- `hb-bg__green`
- `hb-bg__green-active`
- `hb-bg__light-azure`
- `hb-bg__yellow`
- `hb-bg__yellow-active`
- `hb-bg__red`
- `hb-bg__red-active`
- `hb-bg__red-light`
- `hb-bg__grey`
- `hb-bg__light-grey`
- `hb-bg__dark-grey`
- `hb-cl__white`
- `hb-cl__azure`
- `hb-cl__light-azure`
- `hb-cl__green`
- `hb-cl__green-active`
- `hb-cl__yellow`
- `hb-cl__yellow-active`
- `hb-cl__red`
- `hb-cl__red-active`
- `hb-cl__red-light`
- `hb-cl__grey`
- `hb-cl__light-grey`
- `hb-cl__dark-grey`
- `hb-bd__white`
- `hb-bd__azure`
- `hb-bd__light-azure`
- `hb-bd__green`
- `hb-bd__green-active`
- `hb-bd__yellow`
- `hb-bd__yellow-active`
- `hb-bd__red`
- `hb-bd__red-active`
- `hb-bd__red-light`
- `hb-bd__grey`
- `hb-bd__light-grey`
- `hb-bd__dark-grey`

### Added
- Unit tests to all components
- Create one `ngModule` per component
- Update sandbox to include `ngModule` instead components

## [Unreleased]

## [beta-0.3] - 2018-06-05
### Added
- Changelog.md file

### Changed
- Include `allowSyntheticDefaultImports` in tsconfig file

### Removed
- Nothing
