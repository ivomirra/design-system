import { __decorate, __metadata, __param } from 'tslib';
import { Subject } from 'rxjs';
import { Injector, Injectable, ViewChild, ViewContainerRef, Component, Inject, ComponentFactoryResolver, Input, NgModule, EventEmitter, Output, HostListener, ElementRef, Directive, Pipe } from '@angular/core';
import { OverlayConfig, Overlay, OverlayModule, OverlayPositionBuilder } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { HttpEventType } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import * as moment_ from 'moment-mini-ts';
import { CurrenciesListHelper, CurrenciesListHelperModule, CopyToClipboardHelper, CopyToClipboardHelperModule, WeightUnitsListHelper, WeightUnitsListHelperModule } from 'huub-utils/lib';

let UiModalService = class UiModalService {
    constructor(overlay) {
        this.overlay = overlay;
    }
    disposeModal(context) {
        this.overlayRef.dispose();
        this.modalSubject.next(context || null);
        this.modalSubject.unsubscribe();
    }
    openModal(componentParent, childComponents, modalData = {}, backDropCallCallback) {
        const positionStrategy = this.overlay
            .position()
            .global()
            .centerHorizontally()
            .centerVertically();
        this.modalSubject = new Subject();
        /** Creates overlay */
        this.overlayRef = this.overlay.create(new OverlayConfig({
            hasBackdrop: true,
            scrollStrategy: this.overlay.scrollStrategies.block(),
            positionStrategy
        }));
        /** Appends component to overlay */
        this.overlayRef.attach(new ComponentPortal(componentParent, null, Injector.create({
            providers: [
                {
                    provide: 'MODAL_DATA',
                    useValue: Object.assign({}, modalData, { childComponents })
                }
            ]
        })));
        /** Appends subscribes overlay close */
        this.overlayRef.backdropClick().subscribe(() => {
            if (backDropCallCallback) {
                backDropCallCallback();
                return;
            }
            this.disposeModal();
        });
        return {
            afterClose: () => {
                return this.modalSubject.asObservable();
            }
        };
    }
    close(data) {
        this.disposeModal(data);
    }
};
UiModalService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Overlay])
], UiModalService);

let UiModalComponent = class UiModalComponent {
    constructor(modal, componentFactoryResolver, componentData) {
        this.modal = modal;
        this.componentFactoryResolver = componentFactoryResolver;
        this.componentData = componentData;
        this.viewModel = {
            title: this.componentData.title
        };
    }
    ngOnInit() {
        this.componentData.childComponents.forEach((component) => {
            const componentCreated = this.viewContainerRef.createComponent(this.componentFactoryResolver.resolveComponentFactory(component));
            componentCreated.changeDetectorRef.detectChanges();
        });
    }
    closeModal() {
        this.modal.close();
    }
};
__decorate([
    ViewChild('childrenContainer', { read: ViewContainerRef, static: true }),
    __metadata("design:type", ViewContainerRef)
], UiModalComponent.prototype, "viewContainerRef", void 0);
UiModalComponent = __decorate([
    Component({
        selector: 'ui-modal',
        template: "<div class=\"ui-modal\">\n    <div class=\"ui-modal__content\">\n        <div class=\"ui-modal__header\">\n            <h2 class=\"ui-modal__header-title\">{{viewModel.title}}</h2>\n            <button class=\"ui-modal__header-button\" (click)=\"closeModal()\">\n                <i class=\"huub-material-icon\" icon=\"close\"></i>\n           </button>\n        </div>\n        <div class=\"ui-modal__body\">\n            <ng-template #childrenContainer></ng-template>\n        </div>\n    </div>\n</div>\n"
    }),
    __param(2, Inject('MODAL_DATA')),
    __metadata("design:paramtypes", [UiModalService,
        ComponentFactoryResolver, Object])
], UiModalComponent);

let MaterialHelper = class MaterialHelper {
    constructor() { }
    getAbsolutePositionRenderConfig(elementOffSet, elementSize) {
        return {
            canRenderToRight: window.innerWidth > elementOffSet + elementSize,
            canRenderToLeft: elementOffSet > elementSize
        };
    }
    merge(...objects) {
        const isObject = (obj) => obj && typeof obj === 'object';
        const newObject = {};
        objects.forEach((obj) => {
            Object.keys(obj).forEach((key) => {
                const pVal = newObject[key];
                const oVal = obj[key];
                if (Array.isArray(oVal)) {
                    newObject[key] = new Array().concat(oVal);
                }
                else if (isObject(oVal)) {
                    newObject[key] = this.merge(pVal || {}, oVal);
                }
                else {
                    newObject[key] = oVal;
                }
            });
        });
        return newObject;
    }
};
MaterialHelper = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [])
], MaterialHelper);

let UiPopupMessageComponent = class UiPopupMessageComponent {
    constructor() { }
    hidePopup() {
        this.viewModel.isVisible = false;
    }
    onButtonClicked(option) {
        if (option && option.callback) {
            option.callback(option);
        }
        this.hidePopup();
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiPopupMessageComponent.prototype, "viewModel", void 0);
UiPopupMessageComponent = __decorate([
    Component({
        selector: 'ui-popup-message',
        template: "<div class=\"ui-popup\" *ngIf=\"viewModel.isVisible\">\n    <div class=\"ui-popup__overlay ui-popup__overlay--visible\"\n         (click)=\"hidePopup()\">\n    </div>\n    <div class=\"ui-popup__content\">\n        <div class=\"ui-popup__header\">\n            <h2 class=\"ui-popup__header-title\">{{viewModel.title}}</h2>\n        </div>\n        <div class=\"ui-popup__body\" [innerHTML]=\"viewModel.message\"></div>\n        <div class=\"ui-popup__footer\">\n            <button *ngIf=\"viewModel.cancel.button_text!== ''\"\n                    type=\"tertiary\"\n                    class=\"ui-popup__cancel\"\n                    (click)=\"onButtonClicked(viewModel.cancel)\">\n                {{viewModel.cancel.button_text}}\n            </button>\n            <button  *ngIf=\"viewModel.submit.button_text!== ''\"\n                     type=\"secondary\"\n                     (click)=\"onButtonClicked(viewModel.submit)\">\n                {{viewModel.submit.button_text}}\n            </button>\n        </div>\n    </div>\n</div>\n"
    }),
    __metadata("design:paramtypes", [])
], UiPopupMessageComponent);

let UiUploadFileEventsHelper = class UiUploadFileEventsHelper {
    constructor() { }
    getAbstractedEventsDataModel(event) {
        switch (event.type) {
            case HttpEventType.Sent:
                return {
                    isUploaded: false,
                    isUploading: false,
                    fileUploadPercentage: 0
                };
            case HttpEventType.UploadProgress:
                return {
                    isUploaded: false,
                    isUploading: true,
                    fileUploadPercentage: Math.round(100 * event.loaded / event.total)
                };
            case HttpEventType.Response:
                return Object.assign({}, event, { isUploaded: true, isUploading: false, fileUploadPercentage: 100 });
        }
    }
};
UiUploadFileEventsHelper = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [])
], UiUploadFileEventsHelper);

const UI_PAGINATION_CONSTANT = {
    DIRECTIONS: {
        PREVIOUS: 'previous',
        NEXT: 'next'
    },
    DEFAULT_BUTTON_TEXT: {
        PREVIOUS: 'Back',
        NEXT: 'Next'
    },
    SHOWING_ITEMS_TOTAL_STRING: 'Showing {firstItem} - {lastItem} of {totalItems}'
};

let UiPaginationViewModelHelper = class UiPaginationViewModelHelper {
    constructor() { }
    getGenericPaginationViewModel(totalItemsPerPage, previousApiUrl, nextApiUrl, currentPageNumber, totalPages, totalItems) {
        let firstItem = totalItemsPerPage * (currentPageNumber - 1) + 1;
        const possibleLastItem = totalItemsPerPage * currentPageNumber;
        const lastItem = possibleLastItem > totalItems ? totalItems : possibleLastItem;
        if (firstItem > lastItem) {
            firstItem = lastItem;
        }
        return {
            currentPage: currentPageNumber,
            isPreviousDisabled: !previousApiUrl,
            isNextDisabled: !nextApiUrl,
            canShowPagination: totalPages > 1,
            totalPages,
            nextText: UI_PAGINATION_CONSTANT.DEFAULT_BUTTON_TEXT.NEXT,
            previousText: UI_PAGINATION_CONSTANT.DEFAULT_BUTTON_TEXT.PREVIOUS,
            showingItemsOfTotalString: UI_PAGINATION_CONSTANT.SHOWING_ITEMS_TOTAL_STRING.replace('{firstItem}', firstItem)
                .replace('{lastItem}', lastItem)
                .replace('{totalItems}', totalItems)
        };
    }
    getUpdatedPaginationViewModel(viewModel, direction) {
        let currentPage = viewModel.currentPage;
        switch (direction) {
            case UI_PAGINATION_CONSTANT.DIRECTIONS.PREVIOUS:
                if (viewModel.currentPage > 1) {
                    currentPage--;
                }
                break;
            case UI_PAGINATION_CONSTANT.DIRECTIONS.NEXT:
                if (viewModel.currentPage < viewModel.totalPages) {
                    currentPage++;
                }
                break;
        }
        return Object.assign({}, viewModel, { currentPage });
    }
    getInitialPositionOfflinePagination(currentPage, totalItemsPerPage) {
        return (currentPage - 1) * totalItemsPerPage;
    }
    getFinalPositionOfflinePagination(currentPage, totalItemsPerPage, totalNumberOfItems) {
        return currentPage * totalItemsPerPage < totalNumberOfItems
            ? currentPage * totalItemsPerPage
            : totalNumberOfItems;
    }
    getNextLinkOfflinePagination(currentPage, totalNumberOfPages) {
        return currentPage !== totalNumberOfPages ? UI_PAGINATION_CONSTANT.DIRECTIONS.NEXT : null;
    }
    getPreviousLinkOfflinePagination(currentPage) {
        return currentPage === 1 ? null : UI_PAGINATION_CONSTANT.DIRECTIONS.PREVIOUS;
    }
};
UiPaginationViewModelHelper = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [])
], UiPaginationViewModelHelper);

let MaterialHelpersModule = class MaterialHelpersModule {
};
MaterialHelpersModule = __decorate([
    NgModule({
        providers: [MaterialHelper]
    })
], MaterialHelpersModule);

let UiModalModule = class UiModalModule {
};
UiModalModule = __decorate([
    NgModule({
        imports: [OverlayModule, CommonModule],
        entryComponents: [UiModalComponent],
        declarations: [UiModalComponent],
        providers: [UiModalService],
        exports: [UiModalComponent]
    })
], UiModalModule);

let UiToggleButtonsComponent = class UiToggleButtonsComponent {
    constructor() {
        /** Property that sould be used for binding a callback to receive the
         button change event **/
        this.onButtonChanged = new EventEmitter();
    }
    ngOnInit() {
        this.viewModel.selectedIndex = this.viewModel.selectedIndex || 0;
    }
    /**
     * Method binded for the button toggle change action.
     * toggleButtonIndex Current tab index clicked
     */
    onToggleButtonClicked(toggleButtonIndex) {
        if (!this.viewModel.items[toggleButtonIndex]) {
            return;
        }
        this.viewModel.selectedIndex = toggleButtonIndex;
        this.onButtonChanged.emit({
            toggleButtonSelected: this.viewModel.items[toggleButtonIndex],
            toggleButtonIndexSelected: toggleButtonIndex
        });
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiToggleButtonsComponent.prototype, "viewModel", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], UiToggleButtonsComponent.prototype, "onButtonChanged", void 0);
UiToggleButtonsComponent = __decorate([
    Component({
        selector: 'ui-toggle-buttons',
        template: "<nav class=\"ui-toggle-buttons__nav\">\n    <button *ngFor=\"let toggleButton of viewModel.items; let toggleIndex = index;\"\n            [ngClass]=\"{\n                'ui-toggle-buttons__button': !(viewModel.selectedIndex === toggleIndex),\n                'ui-toggle-buttons__button--active': viewModel.selectedIndex === toggleIndex\n            }\"\n            [attr.disabled]=\"toggleButton.disabled\"\n            (click)=\"onToggleButtonClicked(toggleIndex)\">{{toggleButton.text}}</button>\n</nav>\n"
    }),
    __metadata("design:paramtypes", [])
], UiToggleButtonsComponent);

let UiToggleButtonsModule = class UiToggleButtonsModule {
};
UiToggleButtonsModule = __decorate([
    NgModule({
        imports: [CommonModule],
        declarations: [UiToggleButtonsComponent],
        exports: [UiToggleButtonsComponent]
    })
], UiToggleButtonsModule);

let UiTabsComponent = class UiTabsComponent {
    constructor() {
        /** Property that sould be used for binding a callback to receive the tab clicked **/
        this.onTabChanged = new EventEmitter();
    }
    ngOnInit() {
        this.viewModel.selectedIndex = this.viewModel.selectedIndex || 0;
    }
    /**
     * Method used to update the selected tab. This method also triggers
     * the eventEmitter for onTabChanged callback that the parent may have
     * configured.
     * selectedTab Current tab clicked
     * selectedTabIndex Current tab index clicked
     */
    onTabClicked(selectedTab, selectedTabIndex) {
        if (selectedTab.disabled) {
            return;
        }
        this.onTabChanged.emit({
            selectedTab,
            selectedTabIndex
        });
        this.viewModel.selectedIndex = selectedTabIndex;
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiTabsComponent.prototype, "viewModel", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], UiTabsComponent.prototype, "onTabChanged", void 0);
UiTabsComponent = __decorate([
    Component({
        selector: 'ui-tabs',
        template: "<nav class=\"ui-tabs__nav\">\n    <ul class=\"ui-tabs__nav-items\">        \n        <li *ngFor=\"let item of viewModel.items; let itemIndex = index;\"\n            [ngClass]=\"{\n                'ui-tabs__nav-item': !(itemIndex === viewModel.selectedIndex) && !item.disabled,\n                'ui-tabs__nav-item--active': itemIndex === viewModel.selectedIndex,\n                'ui-tabs__nav-item--disabled': item.disabled\n            }\">\n            <a (click)=\"onTabClicked(item, itemIndex)\" class=\"ui-tabs__nav-link\" [innerHTML]=\"item.text\"></a>\n        </li>\n    </ul>\n</nav>\n"
    }),
    __metadata("design:paramtypes", [])
], UiTabsComponent);

let UiTabsModule = class UiTabsModule {
};
UiTabsModule = __decorate([
    NgModule({
        imports: [CommonModule],
        declarations: [UiTabsComponent],
        exports: [UiTabsComponent]
    })
], UiTabsModule);

let UiFilterTagsComponent = class UiFilterTagsComponent {
    constructor() {
        /** Property that should be used for binding a callback to receive the
         input submission **/
        this.onLabelRemoved = new EventEmitter();
    }
    /**
     * Method bound to the form submit event.
     */
    onLabelRemovedClick(labelIndex) {
        if (!this.viewModel.items[labelIndex]) {
            return;
        }
        const removedLabel = this.viewModel.items[labelIndex];
        this.viewModel.items.splice(labelIndex, 1);
        this.onLabelRemoved.emit({
            removedLabel,
            removedIndex: labelIndex,
            items: this.viewModel.items
        });
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiFilterTagsComponent.prototype, "viewModel", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], UiFilterTagsComponent.prototype, "onLabelRemoved", void 0);
UiFilterTagsComponent = __decorate([
    Component({
        selector: 'ui-filter-tags',
        template: "<label *ngFor=\"let label of viewModel.items; let labelIndex = index;\"\n        class=\"ui-filter-tags__label\">\n        <span class=\"ui-filter-tags__span\">{{label.text}}</span>\n        <i class=\"huub-material-icon ui-filter-tags__icon\"\n            icon=\"close\"\n            size=\"extra-extra-small\"\n            (click)=\"onLabelRemovedClick(labelIndex)\"></i>\n</label>\n"
    }),
    __metadata("design:paramtypes", [])
], UiFilterTagsComponent);

let UiFilterTagsModule = class UiFilterTagsModule {
};
UiFilterTagsModule = __decorate([
    NgModule({
        imports: [CommonModule],
        declarations: [UiFilterTagsComponent],
        exports: [UiFilterTagsComponent]
    })
], UiFilterTagsModule);

let UiFilterComponent = class UiFilterComponent {
    constructor(element, helper) {
        this.element = element;
        this.helper = helper;
        this.ELEMENT_SIZE = 350;
        this.alignRight = false;
        this.alignLeft = false;
        this.selectedOptions = [];
        this.optionContainerIsVisible = false;
        /** Property that should be used for binding a callback to receive the save action **/
        this.onSaveAction = new EventEmitter();
    }
    trackByFn(index, item) {
        return index;
    }
    onDomClicked(event) {
        if (!this.element.nativeElement.contains(event.target) && this.optionContainerIsVisible) {
            this.toggleOptionContainerVisibility();
        }
    }
    toggleOptionContainerVisibility() {
        const absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, this.ELEMENT_SIZE);
        this.optionContainerIsVisible = !this.optionContainerIsVisible;
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
        this.selectedOptions.length = 0;
        this.selectedOptionParent = {};
    }
    onSaveButtonClick() {
        this.onSaveAction.emit({
            selectedOptionParent: this.selectedOptionParent,
            selectedOption: this.selectedOptions.find((item) => item.selected === true) || this.selectedOptions[0]
        });
        this.toggleOptionContainerVisibility();
    }
    setSelectedOption(options, optionIndex) {
        options.forEach((item) => {
            item.selected = false;
        });
        options[optionIndex].selected = true;
    }
    onSelectChange(options, indexOption) {
        this.setSelectedOption(options, indexOption);
        this.selectedOptions = [];
        this.selectedOptionParent = this.helper.merge(this.viewModel.selectBox[indexOption]);
        if (this.viewModel.selectOptions[this.viewModel.selectBox[indexOption].type]) {
            this.selectedOptions = new Array().concat(this.viewModel.selectOptions[this.viewModel.selectBox[indexOption].type].map((value) => {
                return this.helper.merge(value);
            }));
        }
    }
    shouldShowSaveButton() {
        return !!(this.selectedOptionParent &&
            (this.selectedOptionParent.hasOwnProperty('value') || this.selectedOptions.length > 0));
    }
    onSecondarySelectChange(options, indexOption) {
        this.setSelectedOption(options, indexOption);
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiFilterComponent.prototype, "viewModel", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], UiFilterComponent.prototype, "onSaveAction", void 0);
__decorate([
    HostListener('document:click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], UiFilterComponent.prototype, "onDomClicked", null);
UiFilterComponent = __decorate([
    Component({
        selector: 'ui-filter',
        template: "<div class=\"ui-filter__container\">\n    <i class=\"huub-material-icon ui-filter__icon\" size=\"extra-extra-small\" icon=\"filter\"\n       (click)=\"toggleOptionContainerVisibility()\"></i>\n    <span class=\"ui-filter__text\"\n          (click)=\"toggleOptionContainerVisibility()\">{{viewModel.filterText}}</span>\n    <i class=\"huub-material-icon ui-filter__icon\"\n        icon=\"chevron-down\"\n        size=\"extra-extra-small\"\n        [ngClass]=\"{\n            'ui-filter__icon__tune': optionContainerIsVisible\n        }\"\n       (click)=\"toggleOptionContainerVisibility()\"></i>\n\n    <div class=\"ui-filter__options-container\"\n        [ngClass]=\"{\n            'ui-filter__options-container--visible': optionContainerIsVisible,\n            'ui-filter__options-container--right': alignRight,\n            'ui-filter__options-container--left': alignLeft\n        }\">\n        <ng-container *ngIf=\"optionContainerIsVisible\">\n            <select class=\"ui-filter__select\"\n                    (change)=\"onSelectChange(viewModel.selectBox, $event.target.value)\">\n                <option *ngFor=\"let option of viewModel.selectBox; let index = index; trackBy: trackByFn\"\n                        value=\"{{index}}\">{{option.text}}</option>\n            </select>\n\n            <select *ngIf=\"selectedOptions.length > 0\"\n                    class=\"ui-filter__select\"\n                    (change)=\"onSecondarySelectChange(selectedOptions, $event.target.value)\">\n                <option *ngFor=\"let option of selectedOptions; let index = index; trackBy: trackByFn\"\n                        value=\"{{index}}\">{{option.text}}</option>\n            </select>\n        </ng-container>\n\n        <button *ngIf=\"shouldShowSaveButton()\"\n                type=\"secondary\" class=\"ui-filter__button\"\n                (click)=\"onSaveButtonClick()\">{{viewModel.buttonText}}</button>\n    </div>\n</div>\n"
    }),
    __metadata("design:paramtypes", [ElementRef, MaterialHelper])
], UiFilterComponent);

let UiFilterModule = class UiFilterModule {
};
UiFilterModule = __decorate([
    NgModule({
        imports: [CommonModule, MaterialHelpersModule],
        declarations: [UiFilterComponent],
        exports: [UiFilterComponent]
    })
], UiFilterModule);

let UiSpinnerComponent = class UiSpinnerComponent {
    constructor() {
        /** Property that defines the input value **/
        this.allowNegatives = true;
        /** Property that sould be used for binding a callback to receive the current input value
         everytime it changes **/
        this.onValueChanged = new EventEmitter();
    }
    ngOnInit() {
        if (isNaN(this.value)) {
            throw new TypeError('The value defined should be a number');
        }
    }
    onKeyUp() {
        this.onValueChanged.emit(this.value);
    }
    onPlusButtonPressed() {
        this.value += 1;
        this.onValueChanged.emit(this.value);
    }
    onLessButtonPressed() {
        this.value -= 1;
        if (!this.allowNegatives && this.value < 0) {
            this.value = 0;
            return;
        }
        this.onValueChanged.emit(this.value);
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiSpinnerComponent.prototype, "allowNegatives", void 0);
__decorate([
    Input(),
    __metadata("design:type", Number)
], UiSpinnerComponent.prototype, "value", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], UiSpinnerComponent.prototype, "hasError", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], UiSpinnerComponent.prototype, "disabled", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], UiSpinnerComponent.prototype, "onValueChanged", void 0);
UiSpinnerComponent = __decorate([
    Component({
        selector: 'ui-spinner',
        template: "<input class=\"ui-spinner__input\"\n       #ctrl=\"ngModel\"\n       [(ngModel)]=\"value\"\n       [ngClass]=\"{'ui-spinner__input--error': hasError}\"\n       (keyup)=\"onKeyUp()\"\n       type=\"number\"\n       [attr.disabled]=\"disabled\">\n<button class=\"huub-material-icon huub-material-icon--small ui-spinner__button ui-spinner__button-less\"\n        icon=\"subtract\"\n        (click)=\"onLessButtonPressed()\"\n        [attr.disabled]=\"disabled\"></button>\n<button class=\"huub-material-icon huub-material-icon--small ui-spinner__button ui-spinner__button-plus\"\n        icon=\"add\"\n        (click)=\"onPlusButtonPressed()\"\n        [attr.disabled]=\"disabled\"></button>\n"
    }),
    __metadata("design:paramtypes", [])
], UiSpinnerComponent);

let UiSpinnerModule = class UiSpinnerModule {
};
UiSpinnerModule = __decorate([
    NgModule({
        imports: [CommonModule, FormsModule],
        declarations: [UiSpinnerComponent],
        exports: [UiSpinnerComponent]
    })
], UiSpinnerModule);

let UiInputSearchComponent = class UiInputSearchComponent {
    constructor() {
        this.viewModel = {
            placeholder: '',
            value: ''
        };
        /** Property that sould be used for binding a callback to receive the
         input submission **/
        this.onSubmit = new EventEmitter();
        /** Property that sould be used for binding a callback to receive the
         input change state **/
        this.onChange = new EventEmitter();
    }
    /**
     * Method binded to the form submit event.
     */
    onSubmitForm() {
        this.onSubmit.emit(this.viewModel.value);
    }
    /**
     * Method binded to the input change event.
     */
    onInputChange() {
        this.onChange.emit(this.viewModel.value);
    }
    onClearIconClicked() {
        this.viewModel.value = '';
        this.onSubmitForm();
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiInputSearchComponent.prototype, "viewModel", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], UiInputSearchComponent.prototype, "onSubmit", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], UiInputSearchComponent.prototype, "onChange", void 0);
UiInputSearchComponent = __decorate([
    Component({
        selector: 'ui-input-search',
        template: "<form (submit)=\"onSubmitForm()\" class=\"ui-input-search__form\">\n    <input type=\"text\"\n       class=\"ui-input-search\"\n       [attr.placeholder]=\"viewModel.placeholder\"\n       (input)=\"onInputChange()\"\n       [(ngModel)]=\"viewModel.value\"\n       [ngModelOptions]=\"{standalone: true}\">\n    <i class=\"huub-material-icon ui-input-search__icon\" icon=\"search\"></i>\n    <i *ngIf=\"viewModel.value && viewModel.value !== ''\"\n        (click)=\"onClearIconClicked()\"\n        class=\"huub-material-icon ui-input-clear__icon\"\n        icon=\"close\"></i>\n</form>\n"
    }),
    __metadata("design:paramtypes", [])
], UiInputSearchComponent);

let UiInputSearchModule = class UiInputSearchModule {
};
UiInputSearchModule = __decorate([
    NgModule({
        imports: [CommonModule, FormsModule],
        declarations: [UiInputSearchComponent],
        exports: [UiInputSearchComponent]
    })
], UiInputSearchModule);

const UI_TOOLTIP_CONFIG = {
    defaultIcon: 'info',
    defaultColor: 'blue'
};

let UiTooltipsComponent = class UiTooltipsComponent {
    constructor(element, helper) {
        this.element = element;
        this.helper = helper;
        this.alignLeft = false;
        this.alignRight = false;
    }
    ngOnInit() {
        this.viewModel.icon = !!this.viewModel.icon ? this.viewModel.icon : UI_TOOLTIP_CONFIG.defaultIcon;
        this.viewModel.color = !!this.viewModel.color ? this.viewModel.color : UI_TOOLTIP_CONFIG.defaultColor;
    }
    onTooltipShow() {
        const absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, this.element.nativeElement.querySelector('.ui-tooltip__container').offsetWidth);
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiTooltipsComponent.prototype, "viewModel", void 0);
UiTooltipsComponent = __decorate([
    Component({
        selector: 'ui-tooltips',
        template: "<div class=\"ui-tooltip\" (mouseover)=\"onTooltipShow()\">\n    <i class=\"huub-material-icon\"\n       [attr.size]=\"viewModel.size\"\n       [attr.icon]=\"viewModel.icon\"\n       [attr.color]=\"viewModel.color\"></i>\n    <div class=\"ui-tooltip__arrow\" [ngClass]=\"{ 'ui-tooltip__arrow-left': alignLeft && !alignRight }\"></div>\n    <div class=\"ui-tooltip__container\"\n         [ngClass]=\"{\n            'ui-tooltip__container--left': alignLeft && !alignRight,\n            'ui-tooltip__container--right': alignRight && !alignLeft,\n            'ui-tooltip__container--center': alignLeft && alignRight\n        }\">\n        <div *ngIf=\"viewModel.title\" class=\"ui-tooltip__title\">\n            {{viewModel.title}}\n        </div>\n        <div class=\"ui-tooltip__message\" [innerHTML]=\"viewModel.text\"></div>\n    </div>\n</div>\n"
    }),
    __metadata("design:paramtypes", [ElementRef, MaterialHelper])
], UiTooltipsComponent);

let UiTooltipsModule = class UiTooltipsModule {
};
UiTooltipsModule = __decorate([
    NgModule({
        imports: [CommonModule, MaterialHelpersModule],
        declarations: [UiTooltipsComponent],
        exports: [UiTooltipsComponent]
    })
], UiTooltipsModule);

let UiButtonActionsComponent = class UiButtonActionsComponent {
    constructor(element, helper) {
        this.element = element;
        this.helper = helper;
        this.DEFAULT_TYPE = 'tertiary';
        this.ELEMENT_SIZE = 200;
        this.alignRight = false;
        this.alignLeft = false;
        this.type = this.DEFAULT_TYPE;
        this.optionContainerIsVisible = false;
    }
    showOptionContainerVisibility() {
        const absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, this.ELEMENT_SIZE);
        this.optionContainerIsVisible = true;
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
    }
    hideOptionContainerVisibility() {
        this.optionContainerIsVisible = false;
    }
    onLinkClicked(link) {
        if (!link.callback || typeof link.callback !== 'function') {
            return;
        }
        link.callback(link);
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiButtonActionsComponent.prototype, "viewModel", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiButtonActionsComponent.prototype, "type", void 0);
UiButtonActionsComponent = __decorate([
    Component({
        selector: 'ui-button-actions',
        template: "<div class=\"ui-button-actions\"\n    (mouseleave)=\"hideOptionContainerVisibility()\">\n    <button [attr.type]=\"type\"\n            role=\"icon\"\n            (click)=\"showOptionContainerVisibility()\"></button>\n    <div class=\"ui-button-actions__actions-list\"\n        [ngClass]=\"{\n            'ui-button-actions__actions-list--visible': optionContainerIsVisible,\n            'ui-button-actions__actions-list--right': alignRight,\n            'ui-button-actions__actions-list--left': alignLeft\n        }\"\n        (mouseleave)=\"hideOptionContainerVisibility()\">\n            <a *ngFor=\"let link of viewModel.items\"\n                class=\"ui-button-actions__actions-list-link\"\n                (click)=\"onLinkClicked(link)\">\n                {{link.text}}\n            </a>\n    </div>\n</div>\n"
    }),
    __metadata("design:paramtypes", [ElementRef, MaterialHelper])
], UiButtonActionsComponent);

let UiButtonActionsModule = class UiButtonActionsModule {
};
UiButtonActionsModule = __decorate([
    NgModule({
        imports: [CommonModule, MaterialHelpersModule],
        declarations: [UiButtonActionsComponent],
        exports: [UiButtonActionsComponent]
    })
], UiButtonActionsModule);

let UiPopupMessageModule = class UiPopupMessageModule {
};
UiPopupMessageModule = __decorate([
    NgModule({
        imports: [CommonModule],
        declarations: [UiPopupMessageComponent],
        exports: [UiPopupMessageComponent]
    })
], UiPopupMessageModule);

let UiAutoCompleteComponent = class UiAutoCompleteComponent {
    constructor() {
        this.searchString = '';
        this.selectedOptions = [];
        this.processedOptionsList = [];
        this.showOptionsList = false;
        this.filterTagsViewModel = {
            items: []
        };
        this.optionsList = [];
        this.multiple = true;
        this.defaultValues = [];
        this.hasError = false;
        this.disabled = false;
        this.onItemsChanged = new EventEmitter();
    }
    emitUpdates() {
        this.onItemsChanged.emit({
            selectedItems: this.selectedOptions
        });
    }
    ngOnInit() {
        this.processedOptionsList = this.optionsList;
        this.selectedOptions = this.defaultValues;
        if (this.multiple) {
            this.filterTagsViewModel.items = new Array().concat(this.selectedOptions);
        }
        if (!this.multiple) {
            this.searchString = this.defaultValues.length === 1 ? this.defaultValues[0].text : '';
        }
    }
    showOptionsMenu() {
        this.showOptionsList = true;
    }
    hideOptionsMenu() {
        this.showOptionsList = false;
    }
    searchValue(searchString) {
        this.processedOptionsList = this.optionsList.filter((item) => {
            return item.text.toLowerCase().includes(searchString.toLowerCase());
        });
    }
    clearSearch() {
        this.searchString = '';
        if (!this.multiple) {
            this.selectedOptions.length = 0;
            this.emitUpdates();
        }
    }
    onFilterTagRemoved(filterData) {
        this.selectedOptions.splice(filterData.removedIndex, 1);
        this.emitUpdates();
    }
    onOptionSelected(optionSelected) {
        this.hideOptionsMenu();
        if (this.multiple) {
            this.clearSearch();
            this.ngOnInit();
            this.selectedOptions.push(optionSelected);
            this.filterTagsViewModel.items.push(optionSelected);
        }
        else {
            this.searchString = optionSelected.text;
            this.selectedOptions[0] = optionSelected;
        }
        this.emitUpdates();
    }
};
__decorate([
    Input(),
    __metadata("design:type", Array)
], UiAutoCompleteComponent.prototype, "optionsList", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiAutoCompleteComponent.prototype, "multiple", void 0);
__decorate([
    Input(),
    __metadata("design:type", Array)
], UiAutoCompleteComponent.prototype, "defaultValues", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], UiAutoCompleteComponent.prototype, "placeholder", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiAutoCompleteComponent.prototype, "hasError", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiAutoCompleteComponent.prototype, "disabled", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], UiAutoCompleteComponent.prototype, "onItemsChanged", void 0);
UiAutoCompleteComponent = __decorate([
    Component({
        selector: 'ui-auto-complete',
        template: "<div class=\"ui-autocomplete\"\n    (mouseleave)=\"hideOptionsMenu()\">\n    <input type=\"text\"\n           class=\"ui-autocomplete__textbox\"\n           [attr.placeholder]=\"placeholder\"\n           (keyup)=\"searchValue(searchString)\"\n           (focus)=\"showOptionsMenu()\"\n           [(ngModel)]=\"searchString\"\n           [ngClass]=\"{'form-field__input--error': hasError}\"\n           [disabled]=\"disabled\">\n    <button *ngIf=\"searchString !== ''\"\n            class=\"ui-autocomplete__clear\"\n            (click)=\"clearSearch()\">\n        <i class=\"huub-material-icon\"\n            size=\"small\"\n            icon=\"close\"></i>\n    </button>\n    <ui-filter-tags *ngIf=\"selectedOptions.length > 0\"\n                    class=\"ui-autocomplete__filter-tags\"\n                    (onLabelRemoved)=\"onFilterTagRemoved($event)\"\n                    [viewModel]=\"filterTagsViewModel\"></ui-filter-tags>\n    <div class=\"ui-autocomplete__selectbox\"\n         [ngClass]=\"{'ui-autocomplete__selectbox--visible': showOptionsList}\">\n        <a class=\"ui-autocomplete__option\"\n             *ngFor=\"let option of processedOptionsList\"\n             (click)=\"onOptionSelected(option)\">{{option.text}}</a>\n    </div>\n</div>\n"
    }),
    __metadata("design:paramtypes", [])
], UiAutoCompleteComponent);

let UiAutoCompleteModule = class UiAutoCompleteModule {
};
UiAutoCompleteModule = __decorate([
    NgModule({
        imports: [CommonModule, FormsModule, UiFilterTagsModule],
        declarations: [UiAutoCompleteComponent],
        exports: [UiAutoCompleteComponent]
    })
], UiAutoCompleteModule);

const moment = moment_;
const DEFAULT_CALENDAR_WIDTH = 327;
const COMPONENT_CONFIG = {
    WEEKEND_ISO_CODES: [7, 6]
};
let UiDateInputComponent = class UiDateInputComponent {
    constructor(helper, element) {
        this.helper = helper;
        this.element = element;
        this.placeholder = '';
        this.disablePastDates = false;
        this.disableWeekendDays = false;
        this.dateFormat = '';
        this.datesWithBullets = [];
        this.datesDisabled = [];
        this.isInvalidDate = false;
        this.onDateSelected = new EventEmitter();
        this.daysOfMonth = [];
        this.selectedDate = '';
        this.calendarIsVisible = false;
        this.alignRight = false;
        this.alignLeft = false;
    }
    hideCalendar() {
        this.calendarIsVisible = false;
    }
    onDomClicked(event) {
        if (!this.element.nativeElement.contains(event.target) && this.calendarIsVisible) {
            this.hideCalendar();
        }
    }
    formatDate(clickedDay) {
        const selectedDate = new Date(this.year, this.monthIndexPosition, clickedDay);
        return moment(selectedDate).format(this.dateFormat);
    }
    emitChanges() {
        this.onDateSelected.emit(this.selectedDate);
    }
    getDateWithBulletsIndexOfByDayNumber(dayNumber) {
        const dateToBeCompared = this.formatDate(dayNumber);
        return this.datesWithBullets.findIndex((dateConfig) => {
            return dateToBeCompared === moment(dateConfig.date).format(this.dateFormat);
        });
    }
    buildCalendarDays() {
        // because date = 0, you should pass the month instead month index
        const numberOfDays = new Date(this.year, this.month, 0).getDate();
        const weekDayStartingMonth = new Date(this.year + '-' + this.month + '-01').getDay();
        let daysToShow = [];
        this.daysOfMonth = [];
        for (let w = 1; w <= weekDayStartingMonth; w++) {
            daysToShow.push('');
        }
        for (let i = 1; i <= numberOfDays; i++) {
            if (daysToShow.length < 7) {
                daysToShow.push(i);
            }
            else {
                this.daysOfMonth.push(daysToShow);
                daysToShow = [i];
            }
            if (i === numberOfDays) {
                this.daysOfMonth.push(daysToShow);
            }
        }
    }
    checkIfDateIsValid() {
        this.isInvalidDate = false;
        if (!moment(this.selectedDate, this.dateFormat, true).isValid()) {
            this.isInvalidDate = true;
            return false;
        }
        return true;
    }
    calculateCurrentDate() {
        let currentDate = new Date();
        if (this.selectedDate !== '') {
            currentDate = moment(this.selectedDate, this.dateFormat, true);
        }
        this.year = currentDate.getFullYear ? currentDate.getFullYear() : currentDate.year();
        this.month = currentDate.getMonth ? currentDate.getMonth() + 1 : currentDate.month() + 1;
        this.monthIndexPosition = currentDate.getMonth ? currentDate.getMonth() : currentDate.month();
    }
    ngOnInit() {
        this.dateFormat = this.dateFormat.toUpperCase();
        this.selectedDate = this.value ? moment(this.value).format(this.dateFormat) : '';
        this.calculateCurrentDate();
        this.buildCalendarDays();
    }
    subtractMonth() {
        this.month = this.month - 1;
        this.monthIndexPosition = this.monthIndexPosition - 1;
        if (this.month <= 0) {
            this.year = this.year - 1;
            this.month = 12;
            this.monthIndexPosition = 11;
        }
        this.buildCalendarDays();
    }
    addMonth() {
        this.month = this.month + 1;
        this.monthIndexPosition = this.monthIndexPosition + 1;
        if (this.month >= 13) {
            this.year = this.year + 1;
            this.month = 1;
            this.monthIndexPosition = 0;
        }
        this.buildCalendarDays();
    }
    onInputChanged() {
        if (!this.checkIfDateIsValid()) {
            return;
        }
        this.calculateCurrentDate();
        this.buildCalendarDays();
        this.emitChanges();
    }
    onSelectedDate(clickedDay) {
        this.selectedDate = this.formatDate(clickedDay);
        this.checkIfDateIsValid();
        this.calendarToggleVisibility();
        this.emitChanges();
    }
    isSelectedDate(dayNumber) {
        if (this.selectedDate === '') {
            return false;
        }
        return this.selectedDate === this.formatDate(dayNumber);
    }
    dateHasBullet(dayNumber) {
        return this.getDateWithBulletsIndexOfByDayNumber(dayNumber) > -1;
    }
    getBulletTooltip(dayNumber) {
        const indexFound = this.getDateWithBulletsIndexOfByDayNumber(dayNumber);
        return indexFound > -1 ? this.datesWithBullets[indexFound].tooltip : '';
    }
    isDateDisabled(dayNumber) {
        const dateToBeCompared = this.formatDate(dayNumber);
        const momentDateToBeCompared = new Date(this.year, this.monthIndexPosition, dayNumber);
        if (this.disableWeekendDays &&
            COMPONENT_CONFIG.WEEKEND_ISO_CODES.indexOf(moment(momentDateToBeCompared).isoWeekday()) > -1) {
            return true;
        }
        if (this.disablePastDates && moment(momentDateToBeCompared).isBefore(new Date())) {
            return true;
        }
        return (this.datesDisabled.findIndex((date) => {
            return dateToBeCompared === moment(date).format(this.dateFormat);
        }) > -1);
    }
    calendarToggleVisibility() {
        const calendarWith = this.element.nativeElement.firstElementChild.clientWidth < DEFAULT_CALENDAR_WIDTH
            ? DEFAULT_CALENDAR_WIDTH
            : this.element.nativeElement.firstElementChild.clientWidth;
        const absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, calendarWith);
        this.calculateCurrentDate();
        this.buildCalendarDays();
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
        this.calendarIsVisible = !this.calendarIsVisible;
    }
};
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], UiDateInputComponent.prototype, "disable", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiDateInputComponent.prototype, "placeholder", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiDateInputComponent.prototype, "monthNames", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiDateInputComponent.prototype, "weekDays", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], UiDateInputComponent.prototype, "bottomLabel", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiDateInputComponent.prototype, "disablePastDates", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiDateInputComponent.prototype, "disableWeekendDays", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiDateInputComponent.prototype, "dateFormat", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], UiDateInputComponent.prototype, "value", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiDateInputComponent.prototype, "datesWithBullets", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiDateInputComponent.prototype, "datesDisabled", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiDateInputComponent.prototype, "isInvalidDate", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], UiDateInputComponent.prototype, "onDateSelected", void 0);
__decorate([
    HostListener('document:click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], UiDateInputComponent.prototype, "onDomClicked", null);
UiDateInputComponent = __decorate([
    Component({
        selector: 'ui-date-input',
        template: "<div class=\"ui-date-picker\"\n     [ngClass]=\"{\n         'ui-date-picker__calendar-visible': calendarIsVisible\n     }\">\n    <input class=\"ui-date-picker__input\"\n           type=\"text\"\n           placeholder=\"{{placeholder}}\"\n           [disabled]=\"disable\"\n           [(ngModel)]=\"selectedDate\"\n           (keyup)=\"onInputChanged()\"\n           [ngClass]=\"{\n                'form-field__input--error': isInvalidDate\n            }\">\n    <button class=\"ui-date-picker__button\"\n            (click)=\"calendarToggleVisibility()\"\n            [disabled]=\"disable\">\n        <i class=\"huub-material-icon\" icon=\"calendar\"></i>\n    </button>\n\n    <div class=\"ui-date-picker__hideborders\"\n         *ngIf=\"calendarIsVisible\"\n         (click)=\"calendarToggleVisibility()\">\n    </div>\n\n    <div class=\"ui-date-picker__calendar\"\n        *ngIf=\"calendarIsVisible\"\n        [ngClass]=\"{\n            'ui-date-picker__calendar--right': alignRight,\n            'ui-date-picker__calendar--left': alignLeft\n        }\">\n\n        <div class=\"ui-date-picker__month-year\">\n            <button (click)=\"subtractMonth()\">\n                <i class=\"huub-material-icon ui-date-picker__backward\" icon=\"chevron-left\" size=\"extra-small\"></i>\n            </button>\n            <div class=\"ui-date-picker__actual-month-year\">\n                {{monthNames[month - 1]}} {{year}}\n            </div>\n            <button (click)=\"addMonth()\">\n                <i class=\"huub-material-icon ui-date-picker__forward\" icon=\"chevron-right\" size=\"extra-small\"></i>\n            </button>\n        </div>\n\n        <div class=\"ui-date-picker__days-list-container\">\n            <table class=\"ui-date-picker__days-list\">\n                <thead class=\"ui-date-picker__week-days\">\n                    <tr>\n                        <td *ngFor=\"let weekday of weekDays\">\n                            {{weekday}}\n                        </td>\n                    </tr>\n                </thead>\n                <tbody class=\"ui-date-picker__days\">\n                    <tr class=\"ui-date-picker__days-line\"\n                        *ngFor=\"let dayOfMonthSet of daysOfMonth; let i = index\">\n                       <td *ngFor=\"let dayNumber of daysOfMonth[i]\">\n                            <button class=\"ui-date-picker__day\"\n                                 *ngIf=\"dayNumber !== ''\"\n                                 [disabled]=\"isDateDisabled(dayNumber)\"\n                                 [ngClass]=\"{\n                                    'ui-date-picker__day--selected': isSelectedDate(dayNumber)\n                                  }\"\n                                 (click)=\"onSelectedDate(dayNumber)\">\n                             {{dayNumber}}\n                                <ng-container *ngIf=\"dateHasBullet(dayNumber)\">\n                                    <i class=\"ui-date-picker__day-bullet\"></i>\n\n                                    <span class=\"ui-date-picker__day-bullet__tooltip\"\n                                        *ngIf=\"getBulletTooltip(dayNumber) != ''\">\n                                        {{getBulletTooltip(dayNumber)}}</span>\n                                </ng-container>\n                            </button>\n                       </td>\n                    </tr>\n                    <tr *ngIf=\"bottomLabel !== ''\">\n                        <td [attr.colspan]=\"weekDays.length\" class=\"ui-date-picker__bottom-message\">\n                            {{bottomLabel}}\n                        </td>\n                    </tr>\n                </tbody>\n            </table>\n        </div>\n    </div>\n</div>\n"
    }),
    __metadata("design:paramtypes", [MaterialHelper, ElementRef])
], UiDateInputComponent);

let UiDateInputModule = class UiDateInputModule {
};
UiDateInputModule = __decorate([
    NgModule({
        imports: [CommonModule, FormsModule],
        declarations: [UiDateInputComponent],
        exports: [UiDateInputComponent]
    })
], UiDateInputModule);

let UiProgressBarComponent = class UiProgressBarComponent {
    constructor() {
        this.MAX_PERCENTAGE = 100;
        this.isCompleted = false;
    }
    ngOnChanges(changes) {
        this.percentage =
            changes.percentage.currentValue > this.MAX_PERCENTAGE
                ? this.MAX_PERCENTAGE
                : changes.percentage.currentValue;
        this.isCompleted = this.percentage === this.MAX_PERCENTAGE;
    }
};
__decorate([
    Input(),
    __metadata("design:type", Number)
], UiProgressBarComponent.prototype, "percentage", void 0);
UiProgressBarComponent = __decorate([
    Component({
        selector: 'ui-progress-bar',
        template: "<div class=\"ui-progress-bar\"\n     [ngClass]=\"{\n        'ui-progress-bar--complete': isCompleted\n     }\">\n    <div class=\"ui-progress-bar__foreground\"\n         [ngStyle]=\"{\n             'width': percentage + '%'\n         }\"></div>\n</div>\n"
    }),
    __metadata("design:paramtypes", [])
], UiProgressBarComponent);

let UiProgressBarModule = class UiProgressBarModule {
};
UiProgressBarModule = __decorate([
    NgModule({
        imports: [CommonModule],
        declarations: [UiProgressBarComponent],
        exports: [UiProgressBarComponent]
    })
], UiProgressBarModule);

let UiUploadFileComponent = class UiUploadFileComponent {
    constructor() {
        this.viewModel = {};
        this.onFileSelected = new EventEmitter();
        this.onFileRemoved = new EventEmitter();
        this.onFileUploadCanceled = new EventEmitter();
        this.onFileViewClicked = new EventEmitter();
        this.componentActionsModel = {
            isBeingDragged: false
        };
    }
    handleSelectedFile(selectedFile) {
        this.viewModel.fileName = selectedFile.name;
        this.viewModel.fileUploadPercentage = 0;
        this.viewModel.isUploading = true;
    }
    onDragOver($event) {
        $event.preventDefault();
        this.componentActionsModel.isBeingDragged = true;
    }
    onDragLeave($event) {
        $event.preventDefault();
        this.componentActionsModel.isBeingDragged = false;
    }
    onDragDrop($event) {
        $event.preventDefault();
        this.componentActionsModel.isBeingDragged = false;
        const selectedFile = $event.dataTransfer.files[0];
        this.handleSelectedFile(selectedFile);
        this.onFileSelected.emit({
            selectedFile,
            viewModel: this.viewModel
        });
    }
    onChange($event) {
        $event.preventDefault();
        const selectedFile = $event.target.files[0];
        this.handleSelectedFile(selectedFile);
        this.onFileSelected.emit({
            selectedFile,
            viewModel: this.viewModel
        });
    }
    onRemoveFileClicked($event) {
        $event.preventDefault();
        this.onFileRemoved.emit({
            viewModel: this.viewModel
        });
    }
    onUploadCancelClicked() {
        this.viewModel.isUploading = false;
        this.viewModel.fileUploadPercentage = 0;
        this.onFileUploadCanceled.emit({
            viewModel: this.viewModel
        });
    }
    onReplaceDocumentClicked($event) {
        $event.preventDefault();
        this.uploadInput.nativeElement.click();
    }
    onViewFileClicked($event) {
        $event.preventDefault();
        this.onFileViewClicked.emit({
            viewModel: this.viewModel
        });
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiUploadFileComponent.prototype, "viewModel", void 0);
__decorate([
    Output(),
    __metadata("design:type", Object)
], UiUploadFileComponent.prototype, "onFileSelected", void 0);
__decorate([
    Output(),
    __metadata("design:type", Object)
], UiUploadFileComponent.prototype, "onFileRemoved", void 0);
__decorate([
    Output(),
    __metadata("design:type", Object)
], UiUploadFileComponent.prototype, "onFileUploadCanceled", void 0);
__decorate([
    Output(),
    __metadata("design:type", Object)
], UiUploadFileComponent.prototype, "onFileViewClicked", void 0);
__decorate([
    ViewChild('uploadInput', { read: ElementRef, static: false }),
    __metadata("design:type", ElementRef)
], UiUploadFileComponent.prototype, "uploadInput", void 0);
UiUploadFileComponent = __decorate([
    Component({
        selector: 'ui-upload-file',
        template: "<div class=\"ui-upload-file__container--upload\"\n    [ngClass]=\"{\n        'ui-upload-file__container--dragover': componentActionsModel.isBeingDragged\n    }\"\n    *ngIf=\"!viewModel.isUploaded && !viewModel.isUploading\">\n        Drag and drop a file here to upload, or&nbsp;<span class=\"ui-upload-file__message-link\">Select a file</span>\n        <input type=\"file\"\n                class=\"ui-upload-file__upload-input\"\n                (dragover)=\"onDragOver($event)\"\n\n                (dragleave)=\"onDragLeave($event)\"\n                (dragend)=\"onDragLeave($event)\"\n\n                (drop)=\"onDragDrop($event)\"\n                (change)=\"onChange($event)\">\n</div>\n\n<div class=\"ui-upload-file__container--uploading\" *ngIf=\"viewModel.isUploading\">\n    <div class=\"ui-upload-file__p-bar__container\">\n        <div class=\"ui-upload-file__p-bar__name\">\n            <div>{{viewModel.fileName}}</div>\n            <div>{{viewModel.fileUploadPercentage}}%</div>\n        </div>\n        <ui-progress-bar class=\"ui-upload-file__p-bar\"\n                         size=\"small\"\n                         [percentage]=\"viewModel.fileUploadPercentage\"></ui-progress-bar>\n        <i class=\"huub-material-icon ui-upload-file__p-bar__cancel\"\n            (click)=\"onUploadCancelClicked()\"\n            icon=\"close\"></i>\n    </div>\n</div>\n\n<span class=\"ui-upload-file__container--uploaded\"\n    *ngIf=\"viewModel.isUploaded && !viewModel.isUploading\">\n    <input type=\"file\"\n            #uploadInput\n            class=\"ui-upload-file__dummy-input\"\n            (change)=\"onChange($event)\">\n    <div class=\"ui-upload-file__doc-name\">\n        <i class=\"huub-material-icon\" icon=\"document\"></i>\n        <span class=\"ui-upload-file__doc-name-text\">{{viewModel.fileName}}</span>\n    </div>\n    <div class=\"ui-upload-file__doc-action-container\">\n        <a href=\"\"\n            (click)=\"onViewFileClicked($event)\"\n            class=\"ui-upload-file__doc-action\">View</a>\n        <a href=\"\"\n            (click)=\"onReplaceDocumentClicked($event)\"\n            class=\"ui-upload-file__doc-action\">Replace</a>\n        <a href=\"\"\n            (click)=\"onRemoveFileClicked($event)\"\n            class=\"ui-upload-file__doc-action\">Remove</a>\n    </div>\n</span>\n"
    }),
    __metadata("design:paramtypes", [])
], UiUploadFileComponent);

let UiUploadFileModule = class UiUploadFileModule {
};
UiUploadFileModule = __decorate([
    NgModule({
        imports: [CommonModule, FormsModule, UiProgressBarModule],
        providers: [UiUploadFileEventsHelper],
        declarations: [UiUploadFileComponent],
        exports: [UiUploadFileComponent]
    })
], UiUploadFileModule);

let UiPaginationComponent = class UiPaginationComponent {
    constructor() {
        this.paginationDirections = UI_PAGINATION_CONSTANT.DIRECTIONS;
        this.viewModel = {
            currentPage: 0,
            totalPages: 0,
            isPreviousDisabled: true,
            isNextDisabled: true,
            nextText: UI_PAGINATION_CONSTANT.DEFAULT_BUTTON_TEXT.NEXT,
            previousText: UI_PAGINATION_CONSTANT.DEFAULT_BUTTON_TEXT.PREVIOUS,
            canShowPagination: false,
            showingItemsOfTotalString: ''
        };
        this.onPageLinkClicked = new EventEmitter();
    }
    onPageLinkClick(pageDirection) {
        this.onPageLinkClicked.emit(pageDirection);
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiPaginationComponent.prototype, "viewModel", void 0);
__decorate([
    Output(),
    __metadata("design:type", Object)
], UiPaginationComponent.prototype, "onPageLinkClicked", void 0);
UiPaginationComponent = __decorate([
    Component({
        selector: 'ui-pagination',
        template: "<div class=\"ui-pagination-container\" *ngIf=\"viewModel.canShowPagination\">\n    <div class=\"ui-pagination-totals\">{{viewModel.showingItemsOfTotalString}}</div>\n    <div class=\"ui-pagination-links-container\">\n        <button class=\"ui-pagination-links\"\n            [disabled]=\"viewModel.isPreviousDisabled\"\n            (click)=\"onPageLinkClick(paginationDirections.PREVIOUS)\"\n            type=\"tertiary\"><i class=\"huub-material-icon ui-pagination-icon-left\" icon=\"chevron-left\"></i>{{viewModel.previousText}}</button>\n        <button class=\"ui-pagination-links\"\n            [disabled]=\"viewModel.isNextDisabled\"\n            (click)=\"onPageLinkClick(paginationDirections.NEXT)\"\n            type=\"tertiary\">{{viewModel.nextText}}<i class=\"huub-material-icon ui-pagination-icon-right\" icon=\"chevron-right\"></i></button>\n    </div>\n</div>\n",
        providers: []
    }),
    __metadata("design:paramtypes", [])
], UiPaginationComponent);

let UiPaginationModule = class UiPaginationModule {
};
UiPaginationModule = __decorate([
    NgModule({
        imports: [CommonModule],
        declarations: [UiPaginationComponent],
        providers: [UiPaginationViewModelHelper],
        exports: [UiPaginationComponent]
    })
], UiPaginationModule);

let UiStepperComponent = class UiStepperComponent {
    constructor(componentFactoryResolver) {
        this.componentFactoryResolver = componentFactoryResolver;
        this.viewModel = {
            items: [],
            selectedIndex: 0
        };
    }
    updateCurrentStep() {
        const selectedItem = this.viewModel.items[this.viewModel.selectedIndex];
        this.viewContainerRef.clear();
        this.currentComponent = this.viewContainerRef.createComponent(this.componentFactoryResolver.resolveComponentFactory(selectedItem.component));
        this.currentComponent.instance['stepContext'] = {
            goToNextStep: this.goToNextStep.bind(this),
            goToPreviousStep: this.goToPreviousStep.bind(this),
            componentContext: selectedItem.context
        };
        this.currentComponent.changeDetectorRef.detectChanges();
    }
    ngOnInit() {
        this.updateCurrentStep();
    }
    ngOnChanges() {
        this.updateCurrentStep();
    }
    goToNextStep() {
        if (this.viewModel.selectedIndex === this.viewModel.items.length - 1) {
            throw new TypeError('You are trying to go to the next step on the last available step.');
        }
        this.viewModel.selectedIndex++;
        this.updateCurrentStep();
    }
    goToPreviousStep() {
        if (this.viewModel.selectedIndex === 0) {
            throw new TypeError('You are trying to go to the previous step on the first available step.');
        }
        this.viewModel.selectedIndex--;
        this.updateCurrentStep();
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiStepperComponent.prototype, "viewModel", void 0);
__decorate([
    ViewChild('currentStep', { read: ViewContainerRef, static: true }),
    __metadata("design:type", ViewContainerRef)
], UiStepperComponent.prototype, "viewContainerRef", void 0);
UiStepperComponent = __decorate([
    Component({
        selector: 'ui-stepper',
        template: "<div class=\"ui-stepper\">\n    <div class=\"ui-stepper__content\">\n        <div class=\"ui-stepper__header\">\n            <div class=\"ui-stepper__header__item\" *ngFor=\"let item of viewModel.items; let index = index;\" [ngClass]=\"{'ui-stepper__header__item--active': index === viewModel.selectedIndex}\">\n                <div class=\"ui-stepper__header__item__content\" >\n                    <span class=\"ui-stepper__header__item__number\">{{index + 1}}</span> {{item.title}}</div>\n                <div class=\"ui-stepper__header__item__arrow\">\n                    <div class=\"ui-stepper__header__arrow-icon\"></div>\n                </div>\n            </div>\n        </div>\n        <div class=\"ui-stepper__body\">            \n            <ng-template #currentStep></ng-template>\n        </div>\n    </div>\n</div>"
    }),
    __metadata("design:paramtypes", [ComponentFactoryResolver])
], UiStepperComponent);

let UiStepperModule = class UiStepperModule {
};
UiStepperModule = __decorate([
    NgModule({
        imports: [CommonModule],
        declarations: [UiStepperComponent],
        exports: [UiStepperComponent]
    })
], UiStepperModule);

let UiCurrencyInputComponent = class UiCurrencyInputComponent {
    constructor(currenciesListHelper) {
        this.currenciesListHelper = currenciesListHelper;
        this.onChange = new EventEmitter();
        this.isDropdownOpened = false;
        this.currencySymbol = '';
        this.currenciesList = this.currenciesListHelper.getCurrenciesList();
    }
    ngOnInit() {
        this.currencySymbol = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).symbol;
        this.disabled = this.disabled !== undefined ? this.disabled : false;
    }
    onInputChange() {
        this.onChange.emit({ currency: this.currency, value: this.value });
    }
    openCloseDropdown() {
        if (!this.disabled) {
            this.isDropdownOpened = !this.isDropdownOpened;
        }
    }
    changeCurrency(isoCode) {
        this.currency = isoCode;
        this.onChange.emit({ currency: this.currency, value: this.value });
        this.currencySymbol = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).symbol;
        this.openCloseDropdown();
    }
};
__decorate([
    Input(),
    __metadata("design:type", String)
], UiCurrencyInputComponent.prototype, "currency", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], UiCurrencyInputComponent.prototype, "language", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], UiCurrencyInputComponent.prototype, "value", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], UiCurrencyInputComponent.prototype, "placeholder", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], UiCurrencyInputComponent.prototype, "disabled", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], UiCurrencyInputComponent.prototype, "onChange", void 0);
UiCurrencyInputComponent = __decorate([
    Component({
        selector: 'ui-currency-input',
        providers: [CurrenciesListHelper],
        template: "<div class=\"ui-currency-input\">\n    <span class=\"currency-char\" (click)=\"openCloseDropdown()\">{{currencySymbol}}</span>\n    <input type=\"number\" class=\"currency-char__input\" [attr.placeholder]=\"placeholder\" (input)=\"onInputChange()\"\n        [(ngModel)]=\"value\" [ngModelOptions]=\"{standalone: true}\" [disabled]=\"disabled\">\n    <div *ngIf=\"isDropdownOpened\" class=\"currency-input__dropdown\" (mouseleave)=\"openCloseDropdown()\">\n        <div *ngFor=\"let currency of currenciesList\" class=\"currency-input__dropdown-item\" (click)=\"changeCurrency(currency.iso3code)\">\n            <span class=\"currency-input__dropdown-char\">{{currency.symbol}}</span>\n            <span class=\"currency-input__dropdown-iso-code\">{{currency.iso3code}}</span>\n            <span class=\"currency-input__dropdown-name\">{{currency.name}}</span>\n        </div>\n    </div>\n</div>"
    }),
    __metadata("design:paramtypes", [CurrenciesListHelper])
], UiCurrencyInputComponent);

let UiCurrencyInputModule = class UiCurrencyInputModule {
};
UiCurrencyInputModule = __decorate([
    NgModule({
        imports: [CommonModule, FormsModule, CurrenciesListHelperModule],
        declarations: [UiCurrencyInputComponent],
        exports: [UiCurrencyInputComponent]
    })
], UiCurrencyInputModule);

const UI_COPY_INPUT_CONSTANT = {
    EDIT_TEXT: 'Edit',
    TOOLTIP_TEXT_BEFORE_COPY: 'Click to copy',
    TOOLTIP_TEXT_AFTER_COPY: 'Copied to clipboard!'
};

let UiCopyInputComponent = class UiCopyInputComponent {
    constructor(copyToClipboardHelper) {
        this.copyToClipboardHelper = copyToClipboardHelper;
        this.onChange = new EventEmitter();
        this.onSubmit = new EventEmitter();
        this.isTooltipActive = false;
        this.isCopied = false;
        this.isEditing = false;
        this.isTypeCurrency = false;
        this.field = {
            left: '0',
            top: '0'
        };
        this.tooltipViewModel = {
            text: 'Tooltip test example.',
            icon: 'info',
            color: 'blue-grey-dark'
        };
        this.backupValue = '';
    }
    ngOnInit() {
        this.viewModel = this.viewModel ? this.viewModel : {};
        this.type = this.type ? this.type : 'text';
        this.isTypeCurrency = this.type === 'currency';
        this.editable = this.editable !== undefined ? this.editable : true;
        this.disabled = this.disabled !== undefined ? this.disabled : false;
        this.viewModel.editText = this.viewModel.editText ? this.viewModel.editText : UI_COPY_INPUT_CONSTANT.EDIT_TEXT;
        this.label = this.label && typeof this.label === 'string' ? { value: this.label } : this.label;
        this.viewModel.tooltipTextBeforeCopy = this.viewModel.tooltipTextBeforeCopy
            ? this.viewModel.tooltipTextBeforeCopy
            : UI_COPY_INPUT_CONSTANT.TOOLTIP_TEXT_BEFORE_COPY;
        this.viewModel.tooltipTextAfterCopy = this.viewModel.tooltipTextAfterCopy
            ? this.viewModel.tooltipTextAfterCopy
            : UI_COPY_INPUT_CONSTANT.TOOLTIP_TEXT_AFTER_COPY;
        if (this.type === 'currency') {
            this.currencyValue = {
                currency: this.currency,
                value: this.value
            };
        }
    }
    onInputChange(currencyValue) {
        this.currencyValue = currencyValue;
        const value = this.type === 'currency' ? this.currencyValue : this.value;
        this.onChange.emit(value);
    }
    copy(event) {
        if (event && !this.isEditing) {
            event.target.blur();
            const value = this.type === 'currency' ? `${this.currencyValue.value} ${this.currencyValue.currency}` : this.value;
            this.copyToClipboardHelper.copyToClipboard(value);
            this.isCopied = true;
        }
    }
    edit() {
        this.isEditing = true;
        this.backupValue = this.value;
    }
    discard() {
        this.isEditing = false;
        this.value = this.backupValue;
        this.backupValue = '';
    }
    save() {
        const value = this.type === 'currency' ? this.currencyValue : this.value;
        this.onSubmit.emit(value);
        this.isEditing = false;
        this.backupValue = '';
    }
    showTooltip(event) {
        if (event) {
            this.field.left = `${event.pageX}px`;
            this.field.top = `${event.pageY}px`;
            this.isTooltipActive = true;
        }
        else {
            this.isTooltipActive = false;
            this.isCopied = false;
        }
    }
};
__decorate([
    Input(),
    __metadata("design:type", String)
], UiCopyInputComponent.prototype, "value", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], UiCopyInputComponent.prototype, "placeholder", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiCopyInputComponent.prototype, "label", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], UiCopyInputComponent.prototype, "type", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], UiCopyInputComponent.prototype, "currency", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], UiCopyInputComponent.prototype, "viewModel", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], UiCopyInputComponent.prototype, "disabled", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], UiCopyInputComponent.prototype, "editable", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], UiCopyInputComponent.prototype, "onChange", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], UiCopyInputComponent.prototype, "onSubmit", void 0);
UiCopyInputComponent = __decorate([
    Component({
        selector: 'ui-copy-input',
        providers: [CopyToClipboardHelper],
        template: "<div class=\"ui-copy-input\">\n    <span class=\"ui-copy-input__label\" *ngIf=\"label\">\n        {{label.value}} <ui-tooltips *ngIf=\"label.tooltipViewModel\" [viewModel]=\"label.tooltipViewModel\"></ui-tooltips>\n    </span>\n    <span class=\"ui-copy-input__edit\" *ngIf=\"editable\" (click)=\"edit()\">{{viewModel.editText}}</span>\n    <div class=\"ui-copy-input__wrapper\" (mouseenter)=\"showTooltip($event)\" (mousemove)=\"showTooltip($event)\"\n        (mouseleave)=\"showTooltip()\" (click)=\"copy($event)\">\n        <div *ngIf=\"!isEditing\" class=\"ui-copy-input__blocker\"></div>\n        <input *ngIf=\"!isTypeCurrency\" type=\"text\" class=\"ui-copy-input-area\"\n            [ngClass]=\"{'ui-copy-input-area__editing': isEditing}\" [attr.placeholder]=\"placeholder\"\n            (input)=\"onInputChange()\" [(ngModel)]=\"value\" [ngModelOptions]=\"{standalone: true}\" [disabled]=\"disabled\">\n        <ui-currency-input *ngIf=\"isTypeCurrency\" [currency]=\"currency\" [placeholder]=\"placeholder\"\n            (onChange)=\"onInputChange($event)\" [value]=\"value\" [disabled]=\"disabled\">\n        </ui-currency-input>\n    </div>\n    <div *ngIf=\"isEditing\" class=\"ui-copy-input__edit-buttons\">\n        <div class=\"ui-copy-input__edit-button\" (click)=\"save()\"><i class=\"huub-material-icon\" icon=\"check\"\n                size=\"extra-small\" color=\"green\"></i></div>\n        <div class=\"ui-copy-input__edit-button\" (click)=\"discard()\"><i class=\"huub-material-icon\" icon=\"close\"\n                size=\"extra-small\" color=\"red\"></i></div>\n    </div>\n    <div *ngIf=\"!isEditing && isTooltipActive\" class=\"ui-copy-input-tooltip\"\n        [ngStyle]=\"{left:field.left, top:field.top}\">\n        <div *ngIf=\"!isCopied\">{{viewModel.tooltipTextBeforeCopy}}</div>\n        <div *ngIf=\"isCopied\"><i class=\"huub-material-icon\" icon=\"check\" size=\"extra-extra-small\"></i>\n            {{viewModel.tooltipTextAfterCopy}}\n        </div>\n    </div>\n</div>"
    }),
    __metadata("design:paramtypes", [CopyToClipboardHelper])
], UiCopyInputComponent);

let UiCopyInputModule = class UiCopyInputModule {
};
UiCopyInputModule = __decorate([
    NgModule({
        imports: [CommonModule, FormsModule, CopyToClipboardHelperModule, UiCurrencyInputModule, UiTooltipsModule],
        declarations: [UiCopyInputComponent],
        exports: [UiCopyInputComponent]
    })
], UiCopyInputModule);

let UiCheckboxIndeterminateComponent = class UiCheckboxIndeterminateComponent {
    constructor() {
        this.onChange = new EventEmitter();
        this.statusValues = {
            unchecked: 'unchecked',
            indeterminate: 'indeterminate',
            checked: 'checked'
        };
    }
    getNextStatus(currentStatus) {
        switch (currentStatus) {
            case this.statusValues.unchecked:
                return this.statusValues.indeterminate;
            case this.statusValues.indeterminate:
                return this.statusValues.checked;
            case this.statusValues.checked:
            default:
                return this.statusValues.unchecked;
        }
    }
    ngOnInit() {
        this.status = this.statusValues[this.status] || this.statusValues.unchecked;
    }
    onCheckboxClick() {
        this.status = this.getNextStatus(this.status);
        this.onChange.emit(this.status);
    }
};
__decorate([
    Input(),
    __metadata("design:type", String)
], UiCheckboxIndeterminateComponent.prototype, "status", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], UiCheckboxIndeterminateComponent.prototype, "disabled", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], UiCheckboxIndeterminateComponent.prototype, "onChange", void 0);
UiCheckboxIndeterminateComponent = __decorate([
    Component({
        selector: 'ui-checkbox-indeterminate',
        template: "<input type=\"checkbox\"\n       class=\"huub-material-icon\"\n       [ngClass]=\"{\n           'indeterminate': status === statusValues.indeterminate\n       }\"\n       [checked]=\"status === statusValues.checked || status === statusValues.indeterminate\"\n       (change)=\"onCheckboxClick()\"\n       [disabled]=\"disabled\">\n"
    }),
    __metadata("design:paramtypes", [])
], UiCheckboxIndeterminateComponent);

let UiCheckboxIndeterminateModule = class UiCheckboxIndeterminateModule {
};
UiCheckboxIndeterminateModule = __decorate([
    NgModule({
        imports: [CommonModule, FormsModule],
        declarations: [UiCheckboxIndeterminateComponent],
        exports: [UiCheckboxIndeterminateComponent]
    })
], UiCheckboxIndeterminateModule);

let TooltipComponent = class TooltipComponent {
    constructor() {
        this.text = '';
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], TooltipComponent.prototype, "text", void 0);
TooltipComponent = __decorate([
    Component({
        selector: 'overlay-tooltip',
        template: "<div class=\"tooltip\">{{ text }}</div>\n"
    })
], TooltipComponent);

const TOOLTIP_CONFIG = {
    originX: 'center',
    originY: 'bottom',
    overlayX: 'center',
    overlayY: 'bottom',
    offsetY: 24
};

let TooltipDirective = class TooltipDirective {
    constructor(overlay, overlayPositionBuilder, elementRef) {
        this.overlay = overlay;
        this.overlayPositionBuilder = overlayPositionBuilder;
        this.elementRef = elementRef;
        this.text = '';
    }
    ngOnInit() {
        const positionStrategy = this.overlayPositionBuilder.flexibleConnectedTo(this.elementRef).withPositions([
            {
                originX: TOOLTIP_CONFIG.originX,
                originY: TOOLTIP_CONFIG.originY,
                overlayX: TOOLTIP_CONFIG.overlayX,
                overlayY: TOOLTIP_CONFIG.overlayY,
                offsetY: TOOLTIP_CONFIG.offsetY
            }
        ]);
        this.overlayRef = this.overlay.create({ positionStrategy });
        this.componentPortal = new ComponentPortal(TooltipComponent);
    }
    show() {
        const tooltipRef = this.overlayRef.attach(this.componentPortal);
        tooltipRef.instance.text = this.text;
    }
    hide() {
        this.overlayRef.detach();
    }
};
__decorate([
    Input('overlayTooltip'),
    __metadata("design:type", Object)
], TooltipDirective.prototype, "text", void 0);
__decorate([
    HostListener('mouseenter'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], TooltipDirective.prototype, "show", null);
__decorate([
    HostListener('mouseleave'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], TooltipDirective.prototype, "hide", null);
TooltipDirective = __decorate([
    Directive({ selector: '[overlayTooltip]' }),
    __metadata("design:paramtypes", [Overlay,
        OverlayPositionBuilder,
        ElementRef])
], TooltipDirective);

let TooltipModule = class TooltipModule {
};
TooltipModule = __decorate([
    NgModule({
        imports: [CommonModule],
        declarations: [TooltipComponent, TooltipDirective],
        entryComponents: [TooltipComponent],
        exports: [TooltipComponent, TooltipDirective]
    })
], TooltipModule);

const UI_WEIGHT_UNITS_DROPDOWN_CONSTANTS = {
    POPULAR_WEIGHT_UNITS: 'Popular weight units',
    ALL_WEIGHT_UNITS: 'All weight units',
    SEARCH_WEIGHT_UNIT: 'Search for weight unit'
};

let UiWeightUnitsDropdownComponent = class UiWeightUnitsDropdownComponent {
    constructor(weightUnitsHelper) {
        this.weightUnitsHelper = weightUnitsHelper;
        this.inputSearchViewModel = {
            placeholder: UI_WEIGHT_UNITS_DROPDOWN_CONSTANTS.SEARCH_WEIGHT_UNIT,
            value: ''
        };
        this.onChange = new EventEmitter();
        this.isDropdownOpened = false;
        this.weightUnitsList = this.weightUnitsHelper.getWeightUnitListOrderByName();
        this.popularWeightUnitsList = this.weightUnitsHelper.getPopularWeightUnitsListOrderByName();
        this.popularWeightUnitsTitle = UI_WEIGHT_UNITS_DROPDOWN_CONSTANTS.POPULAR_WEIGHT_UNITS;
        this.allWeightUnitsTitle = UI_WEIGHT_UNITS_DROPDOWN_CONSTANTS.ALL_WEIGHT_UNITS;
    }
    ngOnInit() {
        this.weightUnitName = this.weightUnitsHelper.getWeightUnitBySymbol(this.weightUnitSymbol).name;
    }
    onInputChange() {
        this.onChange.emit({ weightUnitSymbol: this.weightUnitSymbol });
    }
    openCloseDropdown() {
        this.isDropdownOpened = !this.isDropdownOpened;
        this.weightUnitsList = this.weightUnitsHelper.getWeightUnitListOrderByName();
    }
    changeWeight(symbol) {
        this.weightUnitSymbol = symbol;
        this.weightUnitName = this.weightUnitsHelper.getWeightUnitBySymbol(this.weightUnitSymbol).name;
        this.onChange.emit({ weightUnitSymbol: this.weightUnitSymbol });
        this.openCloseDropdown();
        this.onClearSearchInput();
    }
    onClearSearchInput() {
        this.inputSearchViewModel.value = '';
    }
};
__decorate([
    Input(),
    __metadata("design:type", String)
], UiWeightUnitsDropdownComponent.prototype, "weightUnitSymbol", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], UiWeightUnitsDropdownComponent.prototype, "onChange", void 0);
UiWeightUnitsDropdownComponent = __decorate([
    Component({
        selector: 'ui-weight-units-dropdown',
        template: "<div class=\"ui-weight-input\">\n    <span class=\"weight-char\" (click)=\"openCloseDropdown()\">{{weightUnitSymbol}}</span>\n    <input (click)=\"openCloseDropdown()\" type=\"text\" class=\"weight-char__input\" (input)=\"onInputChange()\" [(ngModel)]=\"weightUnitName\"\n        [ngModelOptions]=\"{standalone: true}\" readonly>\n    <div *ngIf=\"isDropdownOpened\" style=\"min-width: 300px;\" class=\"weight-input__dropdown\" (mouseleave)=\"openCloseDropdown()\">\n        <div class=\"ui-weight-input__search--style\">\n            <ui-input-search (onClearIconClicked)=\"onClearSearchInput()\" [viewModel]=\"inputSearchViewModel\" #searchInput ngDefaultControl\n                [(ngModel)]=\"inputSearchViewModel.value\"></ui-input-search>\n        </div>\n        <br />\n        <div *ngIf=\"!inputSearchViewModel.value\">\n            <p class=\"ui-weight-input__dropdown--title\">{{popularWeightUnitsTitle}}</p>\n            <div *ngFor=\"let weight of popularWeightUnitsList\" class=\"weight-input__dropdown-item\" (click)=\"changeWeight(weight.symbol)\">\n                <span class=\"weight-input__dropdown-char\">{{weight.symbol}}</span>\n                <span class=\"weight-input__dropdown-name\">{{weight.name}}</span>\n            </div>\n            <br />\n            <hr>\n            <br />\n            <p class=\"ui-weight-input__dropdown--title\">{{allWeightUnitsTitle}}</p>\n        </div>\n        <div *ngFor=\"let weight of weightUnitsList | searchPipe:'name,symbol':inputSearchViewModel.value\" class=\"weight-input__dropdown-item\"\n            (click)=\"changeWeight(weight.symbol)\">\n            <span class=\"weight-input__dropdown-char\">{{weight.symbol}}</span>\n            <span class=\"weight-input__dropdown-name\">{{weight.name}}</span>\n        </div>\n    </div>\n</div>\n"
    }),
    __metadata("design:paramtypes", [WeightUnitsListHelper])
], UiWeightUnitsDropdownComponent);

let SearchPipe = class SearchPipe {
    transform(value = [], keys, term) {
        if (!term) {
            return value;
        }
        return value.filter((item) => keys.split(',').some((key) => item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key])));
    }
};
SearchPipe = __decorate([
    Pipe({
        name: 'searchPipe'
    })
], SearchPipe);

let SearchPipeModule = class SearchPipeModule {
};
SearchPipeModule = __decorate([
    NgModule({
        declarations: [SearchPipe],
        exports: [SearchPipe]
    })
], SearchPipeModule);

let UiWeightUnitsDropdownModule = class UiWeightUnitsDropdownModule {
};
UiWeightUnitsDropdownModule = __decorate([
    NgModule({
        imports: [CommonModule, FormsModule, WeightUnitsListHelperModule, UiInputSearchModule, SearchPipeModule],
        declarations: [UiWeightUnitsDropdownComponent],
        exports: [UiWeightUnitsDropdownComponent]
    })
], UiWeightUnitsDropdownModule);

const UI_CURRENCIES_DROPDOWN_CONSTANTS = {
    POPULAR_CURRENCIES: 'Popular currencies',
    ALL_CURRENCIES: 'All currencies',
    SEARCH_CURRENCY: 'Type a currency'
};

let UiCurrenciesDropdownComponent = class UiCurrenciesDropdownComponent {
    constructor(currenciesListHelper) {
        this.currenciesListHelper = currenciesListHelper;
        this.onChange = new EventEmitter();
        this.inputSearchViewModel = {
            placeholder: UI_CURRENCIES_DROPDOWN_CONSTANTS.SEARCH_CURRENCY,
            value: ''
        };
        this.isDropdownOpened = false;
        this.currencySymbol = '';
        this.popularCurrencyTitle = UI_CURRENCIES_DROPDOWN_CONSTANTS.POPULAR_CURRENCIES;
        this.allCurrencyTitle = UI_CURRENCIES_DROPDOWN_CONSTANTS.ALL_CURRENCIES;
        this.currenciesList = this.currenciesListHelper.getCurrenciesList();
        this.popularCurrenciesList = this.currenciesListHelper.getPopularCurrenciesList();
    }
    ngOnInit() {
        this.currencySymbol = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).symbol;
        this.disabled = this.disabled !== undefined ? this.disabled : false;
    }
    onInputChange() {
        this.onChange.emit({ currency: this.currency, value: this.value });
    }
    openCloseDropdown() {
        if (!this.disabled) {
            this.isDropdownOpened = !this.isDropdownOpened;
        }
    }
    changeCurrency(isoCode) {
        this.currency = isoCode;
        this.currencySymbol = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).symbol;
        this.value = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).name;
        this.onChange.emit({ currency: this.currency, value: this.value });
        this.openCloseDropdown();
        this.onClearSearchInput();
    }
    onClearSearchInput() {
        this.inputSearchViewModel.value = '';
    }
};
__decorate([
    Input(),
    __metadata("design:type", String)
], UiCurrenciesDropdownComponent.prototype, "currency", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], UiCurrenciesDropdownComponent.prototype, "language", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], UiCurrenciesDropdownComponent.prototype, "value", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], UiCurrenciesDropdownComponent.prototype, "placeholder", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], UiCurrenciesDropdownComponent.prototype, "disabled", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], UiCurrenciesDropdownComponent.prototype, "onChange", void 0);
UiCurrenciesDropdownComponent = __decorate([
    Component({
        selector: 'ui-currencies-dropdown',
        template: "<div class=\"ui-currency-input\">\n    <span class=\"currency-char\" (click)=\"openCloseDropdown()\">{{currencySymbol}}</span>\n    <input (click)=\"openCloseDropdown()\" type=\"text\" class=\"currency-char__input\" (input)=\"onInputChange()\" [(ngModel)]=\"value\"\n        [ngModelOptions]=\"{standalone: true}\" readonly>\n    <div *ngIf=\"isDropdownOpened\" class=\"currency-input__dropdown\" (mouseleave)=\"openCloseDropdown()\">\n        <div class=\"ui-currency-input__search--style\">\n            <ui-input-search (onClearIconClicked)=\"onClearSearchInput()\" [viewModel]=\"inputSearchViewModel\" #searchInput ngDefaultControl\n                [(ngModel)]=\"inputSearchViewModel.value\"></ui-input-search>\n        </div>\n        <br />\n        <div *ngIf=\"!inputSearchViewModel.value\">\n            <p class=\"ui-currency-input__dropdown--title\">{{popularCurrencyTitle}}</p>\n            <div *ngFor=\"let currency of popularCurrenciesList\" class=\"currency-input__dropdown-item\" (click)=\"changeCurrency(currency.iso3code)\">\n                <span class=\"currency-input__dropdown-char\">{{currency.symbol}}</span>\n                <span class=\"currency-input__dropdown-iso-code\">{{currency.iso3code}}</span>\n                <span class=\"currency-input__dropdown-name\">{{currency.name}}</span>\n            </div>\n            <br />\n            <hr>\n            <br />\n            <p class=\"ui-currency-input__dropdown--title\">{{allCurrencyTitle}}</p>\n        </div>\n        <div *ngFor=\"let currency of currenciesList | searchPipe:'name,symbol,iso3code':inputSearchViewModel.value\" class=\"currency-input__dropdown-item\" (click)=\"changeCurrency(currency.iso3code)\">\n            <span class=\"currency-input__dropdown-char\">{{currency.symbol}}</span>\n            <span class=\"currency-input__dropdown-iso-code\">{{currency.iso3code}}</span>\n            <span class=\"currency-input__dropdown-name\">{{currency.name}}</span>\n        </div>\n    </div>\n</div>\n"
    }),
    __metadata("design:paramtypes", [CurrenciesListHelper])
], UiCurrenciesDropdownComponent);

let UiCurrenciesDropdownModule = class UiCurrenciesDropdownModule {
};
UiCurrenciesDropdownModule = __decorate([
    NgModule({
        imports: [CommonModule, FormsModule, CurrenciesListHelperModule, UiInputSearchModule, SearchPipeModule],
        declarations: [UiCurrenciesDropdownComponent],
        exports: [UiCurrenciesDropdownComponent]
    })
], UiCurrenciesDropdownModule);

let HuubMaterialLibModule = class HuubMaterialLibModule {
};
HuubMaterialLibModule = __decorate([
    NgModule({
        imports: [
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
            OverlayModule,
            MaterialHelpersModule,
            UiModalModule,
            UiToggleButtonsModule,
            UiTabsModule,
            UiFilterTagsModule,
            UiFilterModule,
            UiSpinnerModule,
            UiInputSearchModule,
            UiTooltipsModule,
            UiButtonActionsModule,
            UiPopupMessageModule,
            UiAutoCompleteModule,
            UiDateInputModule,
            UiProgressBarModule,
            UiPaginationModule,
            UiUploadFileModule,
            UiStepperModule,
            UiCurrencyInputModule,
            UiCopyInputModule,
            UiCheckboxIndeterminateModule,
            TooltipModule,
            UiWeightUnitsDropdownModule,
            UiCurrenciesDropdownModule,
            SearchPipeModule
        ],
        exports: [
            CommonModule,
            FormsModule,
            OverlayModule,
            MaterialHelpersModule,
            UiModalModule,
            UiToggleButtonsModule,
            UiTabsModule,
            UiFilterTagsModule,
            UiFilterModule,
            UiSpinnerModule,
            UiInputSearchModule,
            UiTooltipsModule,
            UiButtonActionsModule,
            UiPopupMessageModule,
            UiAutoCompleteModule,
            UiDateInputModule,
            UiProgressBarModule,
            UiPaginationModule,
            UiUploadFileModule,
            UiStepperModule,
            UiCurrencyInputModule,
            UiCopyInputModule,
            UiCheckboxIndeterminateModule,
            TooltipModule,
            UiWeightUnitsDropdownModule,
            UiCurrenciesDropdownModule,
            SearchPipeModule,
            ReactiveFormsModule
        ]
    })
], HuubMaterialLibModule);

/**
 * Generated bundle index. Do not edit.
 */

export { HuubMaterialLibModule, MaterialHelper, UiModalComponent, UiModalService, UiPaginationViewModelHelper, UiPopupMessageComponent, UiUploadFileEventsHelper, MaterialHelpersModule as ɵa, UiModalModule as ɵb, UiPaginationComponent as ɵba, UiUploadFileModule as ɵbb, UiUploadFileComponent as ɵbc, UiStepperModule as ɵbd, UiStepperComponent as ɵbe, UiCurrencyInputModule as ɵbf, UiCurrencyInputComponent as ɵbg, UiCopyInputModule as ɵbh, UiCopyInputComponent as ɵbi, UiCheckboxIndeterminateModule as ɵbj, UiCheckboxIndeterminateComponent as ɵbk, TooltipModule as ɵbl, TooltipComponent as ɵbm, TooltipDirective as ɵbn, UiWeightUnitsDropdownModule as ɵbo, SearchPipeModule as ɵbp, SearchPipe as ɵbq, UiWeightUnitsDropdownComponent as ɵbr, UiCurrenciesDropdownModule as ɵbs, UiCurrenciesDropdownComponent as ɵbt, UiToggleButtonsModule as ɵc, UiToggleButtonsComponent as ɵd, UiTabsModule as ɵe, UiTabsComponent as ɵf, UiFilterTagsModule as ɵg, UiFilterTagsComponent as ɵh, UiFilterModule as ɵi, UiFilterComponent as ɵj, UiSpinnerModule as ɵk, UiSpinnerComponent as ɵl, UiInputSearchModule as ɵm, UiInputSearchComponent as ɵn, UiTooltipsModule as ɵo, UiTooltipsComponent as ɵp, UiButtonActionsModule as ɵq, UiButtonActionsComponent as ɵr, UiPopupMessageModule as ɵs, UiAutoCompleteModule as ɵt, UiAutoCompleteComponent as ɵu, UiDateInputModule as ɵv, UiDateInputComponent as ɵw, UiProgressBarModule as ɵx, UiProgressBarComponent as ɵy, UiPaginationModule as ɵz };
//# sourceMappingURL=huub-material-lib.js.map
