import { EventEmitter, OnInit } from '@angular/core';
export declare class UiTabsComponent implements OnInit {
    /** Property that defines the viewModel to be rendered by ui-tabs **/
    viewModel: {
        items: object[];
        selectedIndex: number;
    };
    /** Property that sould be used for binding a callback to receive the tab clicked **/
    onTabChanged: EventEmitter<{
        selectedTab: object;
        selectedTabIndex: number;
    }>;
    constructor();
    ngOnInit(): void;
    /**
     * Method used to update the selected tab. This method also triggers
     * the eventEmitter for onTabChanged callback that the parent may have
     * configured.
     * selectedTab Current tab clicked
     * selectedTabIndex Current tab index clicked
     */
    onTabClicked(selectedTab: any, selectedTabIndex: number): void;
}
