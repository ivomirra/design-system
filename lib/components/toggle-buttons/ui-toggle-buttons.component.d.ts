import { EventEmitter, OnInit } from '@angular/core';
export declare class UiToggleButtonsComponent implements OnInit {
    /** Property that defines the viewModel to be rendered by ui-toggle-buttons **/
    viewModel: {
        items: object[];
        selectedIndex: number;
    };
    /** Property that sould be used for binding a callback to receive the
     button change event **/
    onButtonChanged: EventEmitter<{
        toggleButtonSelected: object;
        toggleButtonIndexSelected: number;
    }>;
    constructor();
    ngOnInit(): void;
    /**
     * Method binded for the button toggle change action.
     * toggleButtonIndex Current tab index clicked
     */
    onToggleButtonClicked(toggleButtonIndex: number): void;
}
