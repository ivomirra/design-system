import { EventEmitter, ElementRef } from '@angular/core';
import { MaterialHelper } from '../helpers/material.helpers';
export interface OptionsInterface {
    value: string;
    text: string;
    selected?: boolean;
    type?: string;
}
export declare class UiFilterComponent {
    private element;
    private helper;
    private readonly ELEMENT_SIZE;
    private selectedOptionParent;
    alignRight: boolean;
    alignLeft: boolean;
    selectedOptions: any[];
    optionContainerIsVisible: boolean;
    viewModel: {
        filterText: string;
        buttonText: string;
        selectBox: [OptionsInterface];
        selectOptions: any;
    };
    /** Property that should be used for binding a callback to receive the save action **/
    onSaveAction: EventEmitter<{
        selectedOption: any;
        selectedOptionParent: any;
    }>;
    constructor(element: ElementRef, helper: MaterialHelper);
    trackByFn(index: any, item: any): number;
    onDomClicked(event: any): void;
    toggleOptionContainerVisibility(): void;
    onSaveButtonClick(): void;
    private setSelectedOption;
    onSelectChange(options: any, indexOption: number): void;
    shouldShowSaveButton(): boolean;
    onSecondarySelectChange(options: any, indexOption: number): void;
}
