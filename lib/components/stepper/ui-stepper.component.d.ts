import { OnInit, ComponentFactoryResolver, OnChanges } from '@angular/core';
export declare class UiStepperComponent implements OnInit, OnChanges {
    private componentFactoryResolver;
    viewModel: {
        items: Array<{
            title: string;
            component: any;
            context?: any;
        }>;
        selectedIndex: 0;
    };
    private currentComponent;
    private viewContainerRef;
    constructor(componentFactoryResolver: ComponentFactoryResolver);
    private updateCurrentStep;
    ngOnInit(): void;
    ngOnChanges(): void;
    private goToNextStep;
    private goToPreviousStep;
}
