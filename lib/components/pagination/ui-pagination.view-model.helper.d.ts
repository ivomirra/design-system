export declare class UiPaginationViewModelHelper {
    constructor();
    getGenericPaginationViewModel(totalItemsPerPage: any, previousApiUrl: any, nextApiUrl: any, currentPageNumber: any, totalPages: any, totalItems: any): {
        currentPage: any;
        isPreviousDisabled: boolean;
        isNextDisabled: boolean;
        canShowPagination: boolean;
        totalPages: any;
        nextText: string;
        previousText: string;
        showingItemsOfTotalString: string;
    };
    getUpdatedPaginationViewModel(viewModel: any, direction: string): any;
    getInitialPositionOfflinePagination(currentPage: any, totalItemsPerPage: any): number;
    getFinalPositionOfflinePagination(currentPage: any, totalItemsPerPage: any, totalNumberOfItems: any): any;
    getNextLinkOfflinePagination(currentPage: any, totalNumberOfPages: any): string;
    getPreviousLinkOfflinePagination(currentPage: any): string;
}
