export declare const UI_PAGINATION_CONSTANT: {
    DIRECTIONS: {
        PREVIOUS: string;
        NEXT: string;
    };
    DEFAULT_BUTTON_TEXT: {
        PREVIOUS: string;
        NEXT: string;
    };
    SHOWING_ITEMS_TOTAL_STRING: string;
};
