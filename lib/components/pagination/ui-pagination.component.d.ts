import { EventEmitter } from '@angular/core';
import { PaginationViewModelInterface } from './ui-pagination.interface';
export declare class UiPaginationComponent {
    constructor();
    paginationDirections: {
        PREVIOUS: string;
        NEXT: string;
    };
    viewModel: PaginationViewModelInterface;
    onPageLinkClicked: EventEmitter<any>;
    onPageLinkClick(pageDirection: any): void;
}
