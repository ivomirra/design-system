import { PipeTransform } from '@angular/core';
export declare class SearchPipe implements PipeTransform {
    transform(value: any[], keys: string, term: string): any[];
}
