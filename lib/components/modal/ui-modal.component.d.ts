import { OnInit, ComponentFactoryResolver } from '@angular/core';
import { UiModalService } from './ui-modal.service';
export declare class UiModalComponent implements OnInit {
    private modal;
    private componentFactoryResolver;
    private componentData;
    viewModel: {
        title: string;
    };
    private viewContainerRef;
    constructor(modal: UiModalService, componentFactoryResolver: ComponentFactoryResolver, componentData: any);
    ngOnInit(): void;
    closeModal(): void;
}
