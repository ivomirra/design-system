import { EventEmitter } from '@angular/core';
export declare class UiFilterTagsComponent {
    /** Property that defines the viewModel to be rendered by ui-filter-tags **/
    viewModel: {
        items: object[];
    };
    /** Property that should be used for binding a callback to receive the
     input submission **/
    onLabelRemoved: EventEmitter<{
        removedLabel: object;
        removedIndex: number;
        items: object[];
    }>;
    constructor();
    /**
     * Method bound to the form submit event.
     */
    onLabelRemovedClick(labelIndex: number): void;
}
