import { ElementRef, OnInit } from '@angular/core';
import { Overlay, OverlayPositionBuilder } from '@angular/cdk/overlay';
export declare class TooltipDirective implements OnInit {
    private overlay;
    private overlayPositionBuilder;
    private elementRef;
    text: string;
    private overlayRef;
    private componentPortal;
    constructor(overlay: Overlay, overlayPositionBuilder: OverlayPositionBuilder, elementRef: ElementRef);
    ngOnInit(): void;
    show(): void;
    hide(): void;
}
