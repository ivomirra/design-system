import { ElementRef, OnInit } from '@angular/core';
import { MaterialHelper } from '../../helpers/material.helpers';
export declare class UiTooltipsComponent implements OnInit {
    private element;
    private helper;
    viewModel: {
        title?: string;
        text?: string;
        icon?: string;
        color?: string;
        size?: string;
    };
    alignLeft: boolean;
    alignRight: boolean;
    constructor(element: ElementRef, helper: MaterialHelper);
    ngOnInit(): void;
    onTooltipShow(): void;
}
