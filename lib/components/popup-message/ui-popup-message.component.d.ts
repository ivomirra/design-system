export declare class UiPopupMessageComponent {
    viewModel: {
        isVisible: boolean;
        title: string;
        message: string;
        submit: {
            button_text: string;
            callback: Function;
        };
        cancel: {
            button_text: string;
            callback: Function;
        };
    };
    constructor();
    hidePopup(): void;
    onButtonClicked(option?: {
        callback: Function;
    }): void;
}
