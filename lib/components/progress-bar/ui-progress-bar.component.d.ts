import { OnChanges, SimpleChanges } from '@angular/core';
export declare class UiProgressBarComponent implements OnChanges {
    percentage: number;
    private MAX_PERCENTAGE;
    isCompleted: boolean;
    constructor();
    ngOnChanges(changes: SimpleChanges): void;
}
