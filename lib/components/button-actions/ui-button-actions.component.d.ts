import { ElementRef } from '@angular/core';
import { MaterialHelper } from '../helpers/material.helpers';
export declare class UiButtonActionsComponent {
    private element;
    private helper;
    private DEFAULT_TYPE;
    private ELEMENT_SIZE;
    alignRight: boolean;
    alignLeft: boolean;
    viewModel: {
        items: [{
            text: string;
            callback?: Function;
        }];
    };
    type: string;
    optionContainerIsVisible: boolean;
    constructor(element: ElementRef, helper: MaterialHelper);
    showOptionContainerVisibility(): void;
    hideOptionContainerVisibility(): void;
    onLinkClicked(link: any): void;
}
