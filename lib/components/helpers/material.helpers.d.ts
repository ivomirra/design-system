export declare class MaterialHelper {
    constructor();
    getAbsolutePositionRenderConfig(elementOffSet: number, elementSize: number): any;
    merge(...objects: any[]): any;
}
