export declare const UI_WEIGHT_UNITS_DROPDOWN_CONSTANTS: {
    POPULAR_WEIGHT_UNITS: string;
    ALL_WEIGHT_UNITS: string;
    SEARCH_WEIGHT_UNIT: string;
};
