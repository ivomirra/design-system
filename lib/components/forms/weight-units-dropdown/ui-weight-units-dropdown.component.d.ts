import { OnInit, EventEmitter } from '@angular/core';
import { WeightUnitsListHelper } from 'huub-utils/lib';
export declare class UiWeightUnitsDropdownComponent implements OnInit {
    private weightUnitsHelper;
    weightUnitSymbol?: string;
    weightUnitName?: string;
    inputSearchViewModel: {
        placeholder: string;
        value: string;
    };
    onChange: EventEmitter<object>;
    isDropdownOpened: boolean;
    weightUnitsList: any[];
    popularWeightUnitsList: ({
        id: number;
        symbol: string;
        name: string;
        isPopular?: undefined;
    } | {
        id: number;
        symbol: string;
        name: string;
        isPopular: boolean;
    })[];
    popularWeightUnitsTitle: string;
    allWeightUnitsTitle: string;
    constructor(weightUnitsHelper: WeightUnitsListHelper);
    ngOnInit(): void;
    onInputChange(): void;
    openCloseDropdown(): void;
    changeWeight(symbol: any): void;
    onClearSearchInput(): void;
}
