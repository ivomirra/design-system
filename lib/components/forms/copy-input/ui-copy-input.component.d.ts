import { EventEmitter, OnInit } from '@angular/core';
import { CopyToClipboardHelper } from 'huub-utils/lib';
export declare class UiCopyInputComponent implements OnInit {
    private copyToClipboardHelper;
    value?: string;
    placeholder?: string;
    label?: any;
    type?: string;
    currency?: string;
    viewModel?: any;
    disabled?: boolean;
    editable?: boolean;
    onChange: EventEmitter<any>;
    onSubmit: EventEmitter<any>;
    isTooltipActive: boolean;
    isCopied: boolean;
    isEditing: boolean;
    isTypeCurrency: boolean;
    field: {
        left: string;
        top: string;
    };
    tooltipViewModel: {
        text: string;
        icon: string;
        color: string;
    };
    private backupValue;
    private currencyValue;
    constructor(copyToClipboardHelper: CopyToClipboardHelper);
    ngOnInit(): void;
    onInputChange(currencyValue?: any): void;
    copy(event: any): void;
    edit(): void;
    discard(): void;
    save(): void;
    showTooltip(event?: any): void;
}
