export declare const UI_COPY_INPUT_CONSTANT: {
    EDIT_TEXT: string;
    TOOLTIP_TEXT_BEFORE_COPY: string;
    TOOLTIP_TEXT_AFTER_COPY: string;
};
