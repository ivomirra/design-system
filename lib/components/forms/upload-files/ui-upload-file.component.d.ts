export declare class UiUploadFileComponent {
    viewModel: {
        isUploading?: boolean;
        isUploaded?: boolean;
        fileName?: string;
        fileUploadPercentage?: number | string;
    };
    private onFileSelected;
    private onFileRemoved;
    private onFileUploadCanceled;
    private onFileViewClicked;
    private uploadInput;
    constructor();
    componentActionsModel: {
        isBeingDragged: boolean;
    };
    private handleSelectedFile;
    onDragOver($event: any): void;
    onDragLeave($event: any): void;
    onDragDrop($event: any): void;
    onChange($event: any): void;
    onRemoveFileClicked($event: any): void;
    onUploadCancelClicked(): void;
    onReplaceDocumentClicked($event: any): void;
    onViewFileClicked($event: any): void;
}
