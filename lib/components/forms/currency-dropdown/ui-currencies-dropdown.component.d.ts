import { OnInit, EventEmitter } from '@angular/core';
import { CurrenciesListHelper } from 'huub-utils/lib';
export declare class UiCurrenciesDropdownComponent implements OnInit {
    private currenciesListHelper;
    currency?: string;
    language?: string;
    value?: string;
    placeholder?: string;
    disabled?: boolean;
    onChange: EventEmitter<object>;
    inputSearchViewModel: {
        placeholder: string;
        value: string;
    };
    isDropdownOpened: boolean;
    currencySymbol: string;
    popularCurrencyTitle: string;
    allCurrencyTitle: string;
    currenciesList: ({
        name: any;
        id: number;
        symbol: string;
        iso3code: string;
        isPopular?: undefined;
    } | {
        name: any;
        id: number;
        symbol: string;
        iso3code: string;
        isPopular: boolean;
    })[];
    popularCurrenciesList: ({
        name: any;
        id: number;
        symbol: string;
        iso3code: string;
        isPopular?: undefined;
    } | {
        name: any;
        id: number;
        symbol: string;
        iso3code: string;
        isPopular: boolean;
    })[];
    constructor(currenciesListHelper: CurrenciesListHelper);
    ngOnInit(): void;
    onInputChange(): void;
    openCloseDropdown(): void;
    changeCurrency(isoCode: string): void;
    onClearSearchInput(): void;
}
