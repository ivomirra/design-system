export declare const UI_CURRENCIES_DROPDOWN_CONSTANTS: {
    POPULAR_CURRENCIES: string;
    ALL_CURRENCIES: string;
    SEARCH_CURRENCY: string;
};
