import { EventEmitter, OnInit } from '@angular/core';
import { CurrenciesListHelper } from 'huub-utils/lib';
export declare class UiCurrencyInputComponent implements OnInit {
    private currenciesListHelper;
    currency?: string;
    language?: string;
    value?: string;
    placeholder?: string;
    disabled?: boolean;
    onChange: EventEmitter<object>;
    isDropdownOpened: boolean;
    currencySymbol: string;
    currenciesList: ({
        name: any;
        id: number;
        symbol: string;
        iso3code: string;
        isPopular?: undefined;
    } | {
        name: any;
        id: number;
        symbol: string;
        iso3code: string;
        isPopular: boolean;
    })[];
    constructor(currenciesListHelper: CurrenciesListHelper);
    ngOnInit(): void;
    onInputChange(): void;
    openCloseDropdown(): void;
    changeCurrency(isoCode: any): void;
}
