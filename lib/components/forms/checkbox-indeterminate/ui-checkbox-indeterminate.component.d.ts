import { EventEmitter, OnInit } from '@angular/core';
export declare class UiCheckboxIndeterminateComponent implements OnInit {
    status?: string;
    disabled?: boolean;
    onChange: EventEmitter<any>;
    statusValues: {
        unchecked: string;
        indeterminate: string;
        checked: string;
    };
    constructor();
    private getNextStatus;
    ngOnInit(): void;
    onCheckboxClick(): void;
}
