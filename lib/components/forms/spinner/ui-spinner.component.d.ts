import { EventEmitter, OnInit } from '@angular/core';
export declare class UiSpinnerComponent implements OnInit {
    /** Property that defines the input value **/
    allowNegatives: boolean;
    /** Property that defines the input value **/
    value: number;
    /** Property that defines if the input has any error, when a true is provided the
     input has a diferent styling beahvior **/
    hasError: boolean;
    /** Property that sets all the component as disabled **/
    disabled: boolean;
    /** Property that sould be used for binding a callback to receive the current input value
     everytime it changes **/
    onValueChanged: EventEmitter<any>;
    constructor();
    ngOnInit(): void;
    onKeyUp(): void;
    onPlusButtonPressed(): void;
    onLessButtonPressed(): void;
}
