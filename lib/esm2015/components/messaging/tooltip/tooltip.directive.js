import * as tslib_1 from "tslib";
import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { Overlay, OverlayPositionBuilder } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { TooltipComponent } from './tooltip.component';
import { TOOLTIP_CONFIG } from './tooltip.config';
let TooltipDirective = class TooltipDirective {
    constructor(overlay, overlayPositionBuilder, elementRef) {
        this.overlay = overlay;
        this.overlayPositionBuilder = overlayPositionBuilder;
        this.elementRef = elementRef;
        this.text = '';
    }
    ngOnInit() {
        const positionStrategy = this.overlayPositionBuilder.flexibleConnectedTo(this.elementRef).withPositions([
            {
                originX: TOOLTIP_CONFIG.originX,
                originY: TOOLTIP_CONFIG.originY,
                overlayX: TOOLTIP_CONFIG.overlayX,
                overlayY: TOOLTIP_CONFIG.overlayY,
                offsetY: TOOLTIP_CONFIG.offsetY
            }
        ]);
        this.overlayRef = this.overlay.create({ positionStrategy });
        this.componentPortal = new ComponentPortal(TooltipComponent);
    }
    show() {
        const tooltipRef = this.overlayRef.attach(this.componentPortal);
        tooltipRef.instance.text = this.text;
    }
    hide() {
        this.overlayRef.detach();
    }
};
tslib_1.__decorate([
    Input('overlayTooltip'),
    tslib_1.__metadata("design:type", Object)
], TooltipDirective.prototype, "text", void 0);
tslib_1.__decorate([
    HostListener('mouseenter'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], TooltipDirective.prototype, "show", null);
tslib_1.__decorate([
    HostListener('mouseleave'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], TooltipDirective.prototype, "hide", null);
TooltipDirective = tslib_1.__decorate([
    Directive({ selector: '[overlayTooltip]' }),
    tslib_1.__metadata("design:paramtypes", [Overlay,
        OverlayPositionBuilder,
        ElementRef])
], TooltipDirective);
export { TooltipDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9vbHRpcC5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvbWVzc2FnaW5nL3Rvb2x0aXAvdG9vbHRpcC5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBZ0IsU0FBUyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ2pHLE9BQU8sRUFBRSxPQUFPLEVBQUUsc0JBQXNCLEVBQWMsTUFBTSxzQkFBc0IsQ0FBQztBQUNuRixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFdEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFdkQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBR2xELElBQWEsZ0JBQWdCLEdBQTdCLE1BQWEsZ0JBQWdCO0lBTXpCLFlBQ1ksT0FBZ0IsRUFDaEIsc0JBQThDLEVBQzlDLFVBQXNCO1FBRnRCLFlBQU8sR0FBUCxPQUFPLENBQVM7UUFDaEIsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF3QjtRQUM5QyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBUlQsU0FBSSxHQUFHLEVBQUUsQ0FBQztJQVNoQyxDQUFDO0lBRUcsUUFBUTtRQUNYLE1BQU0sZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxhQUFhLENBQUM7WUFDcEc7Z0JBQ0ksT0FBTyxFQUFFLGNBQWMsQ0FBQyxPQUFPO2dCQUMvQixPQUFPLEVBQUUsY0FBYyxDQUFDLE9BQU87Z0JBQy9CLFFBQVEsRUFBRSxjQUFjLENBQUMsUUFBUTtnQkFDakMsUUFBUSxFQUFFLGNBQWMsQ0FBQyxRQUFRO2dCQUNqQyxPQUFPLEVBQUUsY0FBYyxDQUFDLE9BQU87YUFDbEM7U0FDSixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxlQUFlLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBR00sSUFBSTtRQUNQLE1BQU0sVUFBVSxHQUFtQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDaEcsVUFBVSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztJQUN6QyxDQUFDO0lBR00sSUFBSTtRQUNQLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDN0IsQ0FBQztDQUNKLENBQUE7QUFwQzRCO0lBQXhCLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQzs7OENBQVc7QUEyQm5DO0lBREMsWUFBWSxDQUFDLFlBQVksQ0FBQzs7Ozs0Q0FJMUI7QUFHRDtJQURDLFlBQVksQ0FBQyxZQUFZLENBQUM7Ozs7NENBRzFCO0FBcENRLGdCQUFnQjtJQUQ1QixTQUFTLENBQUMsRUFBRSxRQUFRLEVBQUUsa0JBQWtCLEVBQUUsQ0FBQzs2Q0FRbkIsT0FBTztRQUNRLHNCQUFzQjtRQUNsQyxVQUFVO0dBVHpCLGdCQUFnQixDQXFDNUI7U0FyQ1ksZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50UmVmLCBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIEhvc3RMaXN0ZW5lciwgSW5wdXQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT3ZlcmxheSwgT3ZlcmxheVBvc2l0aW9uQnVpbGRlciwgT3ZlcmxheVJlZiB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9vdmVybGF5JztcbmltcG9ydCB7IENvbXBvbmVudFBvcnRhbCB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9wb3J0YWwnO1xuXG5pbXBvcnQgeyBUb29sdGlwQ29tcG9uZW50IH0gZnJvbSAnLi90b29sdGlwLmNvbXBvbmVudCc7XG5cbmltcG9ydCB7IFRPT0xUSVBfQ09ORklHIH0gZnJvbSAnLi90b29sdGlwLmNvbmZpZyc7XG5cbkBEaXJlY3RpdmUoeyBzZWxlY3RvcjogJ1tvdmVybGF5VG9vbHRpcF0nIH0pXG5leHBvcnQgY2xhc3MgVG9vbHRpcERpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgQElucHV0KCdvdmVybGF5VG9vbHRpcCcpIHRleHQgPSAnJztcblxuICAgIHByaXZhdGUgb3ZlcmxheVJlZjogT3ZlcmxheVJlZjtcbiAgICBwcml2YXRlIGNvbXBvbmVudFBvcnRhbDogQ29tcG9uZW50UG9ydGFsPFRvb2x0aXBDb21wb25lbnQ+O1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgb3ZlcmxheTogT3ZlcmxheSxcbiAgICAgICAgcHJpdmF0ZSBvdmVybGF5UG9zaXRpb25CdWlsZGVyOiBPdmVybGF5UG9zaXRpb25CdWlsZGVyLFxuICAgICAgICBwcml2YXRlIGVsZW1lbnRSZWY6IEVsZW1lbnRSZWZcbiAgICApIHt9XG5cbiAgICBwdWJsaWMgbmdPbkluaXQoKSB7XG4gICAgICAgIGNvbnN0IHBvc2l0aW9uU3RyYXRlZ3kgPSB0aGlzLm92ZXJsYXlQb3NpdGlvbkJ1aWxkZXIuZmxleGlibGVDb25uZWN0ZWRUbyh0aGlzLmVsZW1lbnRSZWYpLndpdGhQb3NpdGlvbnMoW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIG9yaWdpblg6IFRPT0xUSVBfQ09ORklHLm9yaWdpblgsXG4gICAgICAgICAgICAgICAgb3JpZ2luWTogVE9PTFRJUF9DT05GSUcub3JpZ2luWSxcbiAgICAgICAgICAgICAgICBvdmVybGF5WDogVE9PTFRJUF9DT05GSUcub3ZlcmxheVgsXG4gICAgICAgICAgICAgICAgb3ZlcmxheVk6IFRPT0xUSVBfQ09ORklHLm92ZXJsYXlZLFxuICAgICAgICAgICAgICAgIG9mZnNldFk6IFRPT0xUSVBfQ09ORklHLm9mZnNldFlcbiAgICAgICAgICAgIH1cbiAgICAgICAgXSk7XG5cbiAgICAgICAgdGhpcy5vdmVybGF5UmVmID0gdGhpcy5vdmVybGF5LmNyZWF0ZSh7IHBvc2l0aW9uU3RyYXRlZ3kgfSk7XG4gICAgICAgIHRoaXMuY29tcG9uZW50UG9ydGFsID0gbmV3IENvbXBvbmVudFBvcnRhbChUb29sdGlwQ29tcG9uZW50KTtcbiAgICB9XG5cbiAgICBASG9zdExpc3RlbmVyKCdtb3VzZWVudGVyJylcbiAgICBwdWJsaWMgc2hvdygpIHtcbiAgICAgICAgY29uc3QgdG9vbHRpcFJlZjogQ29tcG9uZW50UmVmPFRvb2x0aXBDb21wb25lbnQ+ID0gdGhpcy5vdmVybGF5UmVmLmF0dGFjaCh0aGlzLmNvbXBvbmVudFBvcnRhbCk7XG4gICAgICAgIHRvb2x0aXBSZWYuaW5zdGFuY2UudGV4dCA9IHRoaXMudGV4dDtcbiAgICB9XG5cbiAgICBASG9zdExpc3RlbmVyKCdtb3VzZWxlYXZlJylcbiAgICBwdWJsaWMgaGlkZSgpIHtcbiAgICAgICAgdGhpcy5vdmVybGF5UmVmLmRldGFjaCgpO1xuICAgIH1cbn1cbiJdfQ==