import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
let TooltipComponent = class TooltipComponent {
    constructor() {
        this.text = '';
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], TooltipComponent.prototype, "text", void 0);
TooltipComponent = tslib_1.__decorate([
    Component({
        selector: 'overlay-tooltip',
        template: "<div class=\"tooltip\">{{ text }}</div>\n"
    })
], TooltipComponent);
export { TooltipComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9vbHRpcC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvbWVzc2FnaW5nL3Rvb2x0aXAvdG9vbHRpcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBTWpELElBQWEsZ0JBQWdCLEdBQTdCLE1BQWEsZ0JBQWdCO0lBSjdCO1FBS2EsU0FBSSxHQUFHLEVBQUUsQ0FBQztJQUN2QixDQUFDO0NBQUEsQ0FBQTtBQURZO0lBQVIsS0FBSyxFQUFFOzs4Q0FBVztBQURWLGdCQUFnQjtJQUo1QixTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsaUJBQWlCO1FBQzNCLHFEQUF1QztLQUMxQyxDQUFDO0dBQ1csZ0JBQWdCLENBRTVCO1NBRlksZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ292ZXJsYXktdG9vbHRpcCcsXG4gICAgdGVtcGxhdGVVcmw6ICcuL3Rvb2x0aXAuY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFRvb2x0aXBDb21wb25lbnQge1xuICAgIEBJbnB1dCgpIHRleHQgPSAnJztcbn1cbiJdfQ==