import * as tslib_1 from "tslib";
import { Component, Input, ElementRef } from '@angular/core';
import { MaterialHelper } from '../../helpers/material.helpers';
import { UI_TOOLTIP_CONFIG } from './ui-tooltips.config';
let UiTooltipsComponent = class UiTooltipsComponent {
    constructor(element, helper) {
        this.element = element;
        this.helper = helper;
        this.alignLeft = false;
        this.alignRight = false;
    }
    ngOnInit() {
        this.viewModel.icon = !!this.viewModel.icon ? this.viewModel.icon : UI_TOOLTIP_CONFIG.defaultIcon;
        this.viewModel.color = !!this.viewModel.color ? this.viewModel.color : UI_TOOLTIP_CONFIG.defaultColor;
    }
    onTooltipShow() {
        const absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, this.element.nativeElement.querySelector('.ui-tooltip__container').offsetWidth);
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiTooltipsComponent.prototype, "viewModel", void 0);
UiTooltipsComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-tooltips',
        template: "<div class=\"ui-tooltip\" (mouseover)=\"onTooltipShow()\">\n    <i class=\"huub-material-icon\"\n       [attr.size]=\"viewModel.size\"\n       [attr.icon]=\"viewModel.icon\"\n       [attr.color]=\"viewModel.color\"></i>\n    <div class=\"ui-tooltip__arrow\" [ngClass]=\"{ 'ui-tooltip__arrow-left': alignLeft && !alignRight }\"></div>\n    <div class=\"ui-tooltip__container\"\n         [ngClass]=\"{\n            'ui-tooltip__container--left': alignLeft && !alignRight,\n            'ui-tooltip__container--right': alignRight && !alignLeft,\n            'ui-tooltip__container--center': alignLeft && alignRight\n        }\">\n        <div *ngIf=\"viewModel.title\" class=\"ui-tooltip__title\">\n            {{viewModel.title}}\n        </div>\n        <div class=\"ui-tooltip__message\" [innerHTML]=\"viewModel.text\"></div>\n    </div>\n</div>\n"
    }),
    tslib_1.__metadata("design:paramtypes", [ElementRef, MaterialHelper])
], UiTooltipsComponent);
export { UiTooltipsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktdG9vbHRpcHMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaHV1Yi1tYXRlcmlhbC9saWIvIiwic291cmNlcyI6WyJjb21wb25lbnRzL21lc3NhZ2luZy90b29sdGlwcy91aS10b29sdGlwcy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUNyRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDaEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFNekQsSUFBYSxtQkFBbUIsR0FBaEMsTUFBYSxtQkFBbUI7SUFhNUIsWUFBb0IsT0FBbUIsRUFBVSxNQUFzQjtRQUFuRCxZQUFPLEdBQVAsT0FBTyxDQUFZO1FBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFIaEUsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNsQixlQUFVLEdBQUcsS0FBSyxDQUFDO0lBRWdELENBQUM7SUFFcEUsUUFBUTtRQUNYLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQztRQUNsRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLENBQUM7SUFDMUcsQ0FBQztJQUVNLGFBQWE7UUFDaEIsTUFBTSxvQkFBb0IsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLCtCQUErQixDQUNwRSxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLElBQUksRUFDdkQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLHdCQUF3QixDQUFDLENBQUMsV0FBVyxDQUNqRixDQUFDO1FBRUYsSUFBSSxDQUFDLFNBQVMsR0FBRyxvQkFBb0IsQ0FBQyxlQUFlLENBQUM7UUFDdEQsSUFBSSxDQUFDLFVBQVUsR0FBRyxvQkFBb0IsQ0FBQyxnQkFBZ0IsQ0FBQztJQUM1RCxDQUFDO0NBQ0osQ0FBQTtBQTNCRztJQURDLEtBQUssRUFBRTs7c0RBT047QUFSTyxtQkFBbUI7SUFKL0IsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLGFBQWE7UUFDdkIsMDFCQUEyQztLQUM5QyxDQUFDOzZDQWMrQixVQUFVLEVBQWtCLGNBQWM7R0FiOUQsbUJBQW1CLENBNkIvQjtTQTdCWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBFbGVtZW50UmVmLCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1hdGVyaWFsSGVscGVyIH0gZnJvbSAnLi4vLi4vaGVscGVycy9tYXRlcmlhbC5oZWxwZXJzJztcbmltcG9ydCB7IFVJX1RPT0xUSVBfQ09ORklHIH0gZnJvbSAnLi91aS10b29sdGlwcy5jb25maWcnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ3VpLXRvb2x0aXBzJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vdWktdG9vbHRpcHMuY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFVpVG9vbHRpcHNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIEBJbnB1dCgpXG4gICAgcHVibGljIHZpZXdNb2RlbDoge1xuICAgICAgICB0aXRsZT86IHN0cmluZztcbiAgICAgICAgdGV4dD86IHN0cmluZztcbiAgICAgICAgaWNvbj86IHN0cmluZztcbiAgICAgICAgY29sb3I/OiBzdHJpbmc7XG4gICAgICAgIHNpemU/OiBzdHJpbmc7XG4gICAgfTtcblxuICAgIHB1YmxpYyBhbGlnbkxlZnQgPSBmYWxzZTtcbiAgICBwdWJsaWMgYWxpZ25SaWdodCA9IGZhbHNlO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBlbGVtZW50OiBFbGVtZW50UmVmLCBwcml2YXRlIGhlbHBlcjogTWF0ZXJpYWxIZWxwZXIpIHt9XG5cbiAgICBwdWJsaWMgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMudmlld01vZGVsLmljb24gPSAhIXRoaXMudmlld01vZGVsLmljb24gPyB0aGlzLnZpZXdNb2RlbC5pY29uIDogVUlfVE9PTFRJUF9DT05GSUcuZGVmYXVsdEljb247XG4gICAgICAgIHRoaXMudmlld01vZGVsLmNvbG9yID0gISF0aGlzLnZpZXdNb2RlbC5jb2xvciA/IHRoaXMudmlld01vZGVsLmNvbG9yIDogVUlfVE9PTFRJUF9DT05GSUcuZGVmYXVsdENvbG9yO1xuICAgIH1cblxuICAgIHB1YmxpYyBvblRvb2x0aXBTaG93KCkge1xuICAgICAgICBjb25zdCBhYnNvbHV0ZVJlbmRlckNvbmZpZyA9IHRoaXMuaGVscGVyLmdldEFic29sdXRlUG9zaXRpb25SZW5kZXJDb25maWcoXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQubmF0aXZlRWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5sZWZ0LFxuICAgICAgICAgICAgdGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignLnVpLXRvb2x0aXBfX2NvbnRhaW5lcicpLm9mZnNldFdpZHRoXG4gICAgICAgICk7XG5cbiAgICAgICAgdGhpcy5hbGlnbkxlZnQgPSBhYnNvbHV0ZVJlbmRlckNvbmZpZy5jYW5SZW5kZXJUb0xlZnQ7XG4gICAgICAgIHRoaXMuYWxpZ25SaWdodCA9IGFic29sdXRlUmVuZGVyQ29uZmlnLmNhblJlbmRlclRvUmlnaHQ7XG4gICAgfVxufVxuIl19