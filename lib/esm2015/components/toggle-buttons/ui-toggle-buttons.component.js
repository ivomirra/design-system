import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
let UiToggleButtonsComponent = class UiToggleButtonsComponent {
    constructor() {
        /** Property that sould be used for binding a callback to receive the
         button change event **/
        this.onButtonChanged = new EventEmitter();
    }
    ngOnInit() {
        this.viewModel.selectedIndex = this.viewModel.selectedIndex || 0;
    }
    /**
     * Method binded for the button toggle change action.
     * toggleButtonIndex Current tab index clicked
     */
    onToggleButtonClicked(toggleButtonIndex) {
        if (!this.viewModel.items[toggleButtonIndex]) {
            return;
        }
        this.viewModel.selectedIndex = toggleButtonIndex;
        this.onButtonChanged.emit({
            toggleButtonSelected: this.viewModel.items[toggleButtonIndex],
            toggleButtonIndexSelected: toggleButtonIndex
        });
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiToggleButtonsComponent.prototype, "viewModel", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], UiToggleButtonsComponent.prototype, "onButtonChanged", void 0);
UiToggleButtonsComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-toggle-buttons',
        template: "<nav class=\"ui-toggle-buttons__nav\">\n    <button *ngFor=\"let toggleButton of viewModel.items; let toggleIndex = index;\"\n            [ngClass]=\"{\n                'ui-toggle-buttons__button': !(viewModel.selectedIndex === toggleIndex),\n                'ui-toggle-buttons__button--active': viewModel.selectedIndex === toggleIndex\n            }\"\n            [attr.disabled]=\"toggleButton.disabled\"\n            (click)=\"onToggleButtonClicked(toggleIndex)\">{{toggleButton.text}}</button>\n</nav>\n"
    }),
    tslib_1.__metadata("design:paramtypes", [])
], UiToggleButtonsComponent);
export { UiToggleButtonsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktdG9nZ2xlLWJ1dHRvbnMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaHV1Yi1tYXRlcmlhbC9saWIvIiwic291cmNlcyI6WyJjb21wb25lbnRzL3RvZ2dsZS1idXR0b25zL3VpLXRvZ2dsZS1idXR0b25zLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBVSxNQUFNLGVBQWUsQ0FBQztBQU0vRSxJQUFhLHdCQUF3QixHQUFyQyxNQUFhLHdCQUF3QjtJQWdCakM7UUFSQTtnQ0FDd0I7UUFFakIsb0JBQWUsR0FHakIsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUVULENBQUM7SUFFVCxRQUFRO1FBQ1gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLElBQUksQ0FBQyxDQUFDO0lBQ3JFLENBQUM7SUFFRDs7O09BR0c7SUFDSSxxQkFBcUIsQ0FBQyxpQkFBeUI7UUFDbEQsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLEVBQUU7WUFDMUMsT0FBTztTQUNWO1FBRUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEdBQUcsaUJBQWlCLENBQUM7UUFFakQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUM7WUFDdEIsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUM7WUFDN0QseUJBQXlCLEVBQUUsaUJBQWlCO1NBQy9DLENBQUMsQ0FBQztJQUNQLENBQUM7Q0FDSixDQUFBO0FBbkNHO0lBREMsS0FBSyxFQUFFOzsyREFJTjtBQUtGO0lBREMsTUFBTSxFQUFFO3NDQUNlLFlBQVk7aUVBR1o7QUFkZix3QkFBd0I7SUFKcEMsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLG1CQUFtQjtRQUM3Qix3Z0JBQWlEO0tBQ3BELENBQUM7O0dBQ1csd0JBQXdCLENBc0NwQztTQXRDWSx3QkFBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAndWktdG9nZ2xlLWJ1dHRvbnMnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi91aS10b2dnbGUtYnV0dG9ucy5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgVWlUb2dnbGVCdXR0b25zQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICAvKiogUHJvcGVydHkgdGhhdCBkZWZpbmVzIHRoZSB2aWV3TW9kZWwgdG8gYmUgcmVuZGVyZWQgYnkgdWktdG9nZ2xlLWJ1dHRvbnMgKiovXG4gICAgQElucHV0KClcbiAgICBwdWJsaWMgdmlld01vZGVsOiB7XG4gICAgICAgIGl0ZW1zOiBvYmplY3RbXTtcbiAgICAgICAgc2VsZWN0ZWRJbmRleDogbnVtYmVyO1xuICAgIH07XG5cbiAgICAvKiogUHJvcGVydHkgdGhhdCBzb3VsZCBiZSB1c2VkIGZvciBiaW5kaW5nIGEgY2FsbGJhY2sgdG8gcmVjZWl2ZSB0aGVcbiAgICAgYnV0dG9uIGNoYW5nZSBldmVudCAqKi9cbiAgICBAT3V0cHV0KClcbiAgICBwdWJsaWMgb25CdXR0b25DaGFuZ2VkOiBFdmVudEVtaXR0ZXI8e1xuICAgICAgICB0b2dnbGVCdXR0b25TZWxlY3RlZDogb2JqZWN0O1xuICAgICAgICB0b2dnbGVCdXR0b25JbmRleFNlbGVjdGVkOiBudW1iZXI7XG4gICAgfT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgICBwdWJsaWMgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMudmlld01vZGVsLnNlbGVjdGVkSW5kZXggPSB0aGlzLnZpZXdNb2RlbC5zZWxlY3RlZEluZGV4IHx8IDA7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogTWV0aG9kIGJpbmRlZCBmb3IgdGhlIGJ1dHRvbiB0b2dnbGUgY2hhbmdlIGFjdGlvbi5cbiAgICAgKiB0b2dnbGVCdXR0b25JbmRleCBDdXJyZW50IHRhYiBpbmRleCBjbGlja2VkXG4gICAgICovXG4gICAgcHVibGljIG9uVG9nZ2xlQnV0dG9uQ2xpY2tlZCh0b2dnbGVCdXR0b25JbmRleDogbnVtYmVyKTogdm9pZCB7XG4gICAgICAgIGlmICghdGhpcy52aWV3TW9kZWwuaXRlbXNbdG9nZ2xlQnV0dG9uSW5kZXhdKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnZpZXdNb2RlbC5zZWxlY3RlZEluZGV4ID0gdG9nZ2xlQnV0dG9uSW5kZXg7XG5cbiAgICAgICAgdGhpcy5vbkJ1dHRvbkNoYW5nZWQuZW1pdCh7XG4gICAgICAgICAgICB0b2dnbGVCdXR0b25TZWxlY3RlZDogdGhpcy52aWV3TW9kZWwuaXRlbXNbdG9nZ2xlQnV0dG9uSW5kZXhdLFxuICAgICAgICAgICAgdG9nZ2xlQnV0dG9uSW5kZXhTZWxlY3RlZDogdG9nZ2xlQnV0dG9uSW5kZXhcbiAgICAgICAgfSk7XG4gICAgfVxufVxuIl19