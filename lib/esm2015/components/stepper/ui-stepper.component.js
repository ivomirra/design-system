import * as tslib_1 from "tslib";
import { Component, ViewContainerRef, ComponentFactoryResolver, ViewChild, Input } from '@angular/core';
let UiStepperComponent = class UiStepperComponent {
    constructor(componentFactoryResolver) {
        this.componentFactoryResolver = componentFactoryResolver;
        this.viewModel = {
            items: [],
            selectedIndex: 0
        };
    }
    updateCurrentStep() {
        const selectedItem = this.viewModel.items[this.viewModel.selectedIndex];
        this.viewContainerRef.clear();
        this.currentComponent = this.viewContainerRef.createComponent(this.componentFactoryResolver.resolveComponentFactory(selectedItem.component));
        this.currentComponent.instance['stepContext'] = {
            goToNextStep: this.goToNextStep.bind(this),
            goToPreviousStep: this.goToPreviousStep.bind(this),
            componentContext: selectedItem.context
        };
        this.currentComponent.changeDetectorRef.detectChanges();
    }
    ngOnInit() {
        this.updateCurrentStep();
    }
    ngOnChanges() {
        this.updateCurrentStep();
    }
    goToNextStep() {
        if (this.viewModel.selectedIndex === this.viewModel.items.length - 1) {
            throw new TypeError('You are trying to go to the next step on the last available step.');
        }
        this.viewModel.selectedIndex++;
        this.updateCurrentStep();
    }
    goToPreviousStep() {
        if (this.viewModel.selectedIndex === 0) {
            throw new TypeError('You are trying to go to the previous step on the first available step.');
        }
        this.viewModel.selectedIndex--;
        this.updateCurrentStep();
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiStepperComponent.prototype, "viewModel", void 0);
tslib_1.__decorate([
    ViewChild('currentStep', { read: ViewContainerRef, static: true }),
    tslib_1.__metadata("design:type", ViewContainerRef)
], UiStepperComponent.prototype, "viewContainerRef", void 0);
UiStepperComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-stepper',
        template: "<div class=\"ui-stepper\">\n    <div class=\"ui-stepper__content\">\n        <div class=\"ui-stepper__header\">\n            <div class=\"ui-stepper__header__item\" *ngFor=\"let item of viewModel.items; let index = index;\" [ngClass]=\"{'ui-stepper__header__item--active': index === viewModel.selectedIndex}\">\n                <div class=\"ui-stepper__header__item__content\" >\n                    <span class=\"ui-stepper__header__item__number\">{{index + 1}}</span> {{item.title}}</div>\n                <div class=\"ui-stepper__header__item__arrow\">\n                    <div class=\"ui-stepper__header__arrow-icon\"></div>\n                </div>\n            </div>\n        </div>\n        <div class=\"ui-stepper__body\">            \n            <ng-template #currentStep></ng-template>\n        </div>\n    </div>\n</div>"
    }),
    tslib_1.__metadata("design:paramtypes", [ComponentFactoryResolver])
], UiStepperComponent);
export { UiStepperComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktc3RlcHBlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvc3RlcHBlci91aS1zdGVwcGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUNILFNBQVMsRUFHVCxnQkFBZ0IsRUFDaEIsd0JBQXdCLEVBQ3hCLFNBQVMsRUFDVCxLQUFLLEVBRVIsTUFBTSxlQUFlLENBQUM7QUFNdkIsSUFBYSxrQkFBa0IsR0FBL0IsTUFBYSxrQkFBa0I7SUFlM0IsWUFBb0Isd0JBQWtEO1FBQWxELDZCQUF3QixHQUF4Qix3QkFBd0IsQ0FBMEI7UUFiL0QsY0FBUyxHQUdaO1lBQ0EsS0FBSyxFQUFFLEVBQUU7WUFDVCxhQUFhLEVBQUUsQ0FBQztTQUNuQixDQUFDO0lBT3VFLENBQUM7SUFFbEUsaUJBQWlCO1FBQ3JCLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUN6RCxJQUFJLENBQUMsd0JBQXdCLENBQUMsdUJBQXVCLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUNoRixDQUFDO1FBQ0YsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsR0FBRztZQUM1QyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2xELGdCQUFnQixFQUFFLFlBQVksQ0FBQyxPQUFPO1NBQ3pDLENBQUM7UUFDRixJQUFJLENBQUMsZ0JBQWdCLENBQUMsaUJBQWlCLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDNUQsQ0FBQztJQUVNLFFBQVE7UUFDWCxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRU0sV0FBVztRQUNkLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFTyxZQUFZO1FBQ2hCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEtBQUssSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNsRSxNQUFNLElBQUksU0FBUyxDQUFDLG1FQUFtRSxDQUFDLENBQUM7U0FDNUY7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQy9CLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFTyxnQkFBZ0I7UUFDcEIsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsS0FBSyxDQUFDLEVBQUU7WUFDcEMsTUFBTSxJQUFJLFNBQVMsQ0FBQyx3RUFBd0UsQ0FBQyxDQUFDO1NBQ2pHO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUMvQixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUM3QixDQUFDO0NBQ0osQ0FBQTtBQXBERztJQURDLEtBQUssRUFBRTs7cURBT047QUFLRjtJQURDLFNBQVMsQ0FBQyxhQUFhLEVBQUUsRUFBRSxJQUFJLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDO3NDQUN6QyxnQkFBZ0I7NERBQUM7QUFibEMsa0JBQWtCO0lBSjlCLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxZQUFZO1FBQ3RCLDYwQkFBMEM7S0FDN0MsQ0FBQzs2Q0FnQmdELHdCQUF3QjtHQWY3RCxrQkFBa0IsQ0FzRDlCO1NBdERZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gICAgQ29tcG9uZW50LFxuICAgIEluamVjdCxcbiAgICBPbkluaXQsXG4gICAgVmlld0NvbnRhaW5lclJlZixcbiAgICBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsXG4gICAgVmlld0NoaWxkLFxuICAgIElucHV0LFxuICAgIE9uQ2hhbmdlc1xufSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICd1aS1zdGVwcGVyJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vdWktc3RlcHBlci5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgVWlTdGVwcGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xuICAgIEBJbnB1dCgpXG4gICAgcHVibGljIHZpZXdNb2RlbDoge1xuICAgICAgICBpdGVtczogQXJyYXk8eyB0aXRsZTogc3RyaW5nOyBjb21wb25lbnQ6IGFueTsgY29udGV4dD86IGFueSB9PjtcbiAgICAgICAgc2VsZWN0ZWRJbmRleDogMDtcbiAgICB9ID0ge1xuICAgICAgICBpdGVtczogW10sXG4gICAgICAgIHNlbGVjdGVkSW5kZXg6IDBcbiAgICB9O1xuXG4gICAgcHJpdmF0ZSBjdXJyZW50Q29tcG9uZW50OiBhbnk7XG5cbiAgICBAVmlld0NoaWxkKCdjdXJyZW50U3RlcCcsIHsgcmVhZDogVmlld0NvbnRhaW5lclJlZiwgc3RhdGljOiB0cnVlIH0pXG4gICAgcHJpdmF0ZSB2aWV3Q29udGFpbmVyUmVmOiBWaWV3Q29udGFpbmVyUmVmO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBjb21wb25lbnRGYWN0b3J5UmVzb2x2ZXI6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlcikge31cblxuICAgIHByaXZhdGUgdXBkYXRlQ3VycmVudFN0ZXAoKSB7XG4gICAgICAgIGNvbnN0IHNlbGVjdGVkSXRlbSA9IHRoaXMudmlld01vZGVsLml0ZW1zW3RoaXMudmlld01vZGVsLnNlbGVjdGVkSW5kZXhdO1xuICAgICAgICB0aGlzLnZpZXdDb250YWluZXJSZWYuY2xlYXIoKTtcbiAgICAgICAgdGhpcy5jdXJyZW50Q29tcG9uZW50ID0gdGhpcy52aWV3Q29udGFpbmVyUmVmLmNyZWF0ZUNvbXBvbmVudChcbiAgICAgICAgICAgIHRoaXMuY29tcG9uZW50RmFjdG9yeVJlc29sdmVyLnJlc29sdmVDb21wb25lbnRGYWN0b3J5KHNlbGVjdGVkSXRlbS5jb21wb25lbnQpXG4gICAgICAgICk7XG4gICAgICAgIHRoaXMuY3VycmVudENvbXBvbmVudC5pbnN0YW5jZVsnc3RlcENvbnRleHQnXSA9IHtcbiAgICAgICAgICAgIGdvVG9OZXh0U3RlcDogdGhpcy5nb1RvTmV4dFN0ZXAuYmluZCh0aGlzKSxcbiAgICAgICAgICAgIGdvVG9QcmV2aW91c1N0ZXA6IHRoaXMuZ29Ub1ByZXZpb3VzU3RlcC5iaW5kKHRoaXMpLFxuICAgICAgICAgICAgY29tcG9uZW50Q29udGV4dDogc2VsZWN0ZWRJdGVtLmNvbnRleHRcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5jdXJyZW50Q29tcG9uZW50LmNoYW5nZURldGVjdG9yUmVmLmRldGVjdENoYW5nZXMoKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMudXBkYXRlQ3VycmVudFN0ZXAoKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgbmdPbkNoYW5nZXMoKSB7XG4gICAgICAgIHRoaXMudXBkYXRlQ3VycmVudFN0ZXAoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdvVG9OZXh0U3RlcCgpIHtcbiAgICAgICAgaWYgKHRoaXMudmlld01vZGVsLnNlbGVjdGVkSW5kZXggPT09IHRoaXMudmlld01vZGVsLml0ZW1zLmxlbmd0aCAtIDEpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ1lvdSBhcmUgdHJ5aW5nIHRvIGdvIHRvIHRoZSBuZXh0IHN0ZXAgb24gdGhlIGxhc3QgYXZhaWxhYmxlIHN0ZXAuJyk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy52aWV3TW9kZWwuc2VsZWN0ZWRJbmRleCsrO1xuICAgICAgICB0aGlzLnVwZGF0ZUN1cnJlbnRTdGVwKCk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnb1RvUHJldmlvdXNTdGVwKCkge1xuICAgICAgICBpZiAodGhpcy52aWV3TW9kZWwuc2VsZWN0ZWRJbmRleCA9PT0gMCkge1xuICAgICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignWW91IGFyZSB0cnlpbmcgdG8gZ28gdG8gdGhlIHByZXZpb3VzIHN0ZXAgb24gdGhlIGZpcnN0IGF2YWlsYWJsZSBzdGVwLicpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMudmlld01vZGVsLnNlbGVjdGVkSW5kZXgtLTtcbiAgICAgICAgdGhpcy51cGRhdGVDdXJyZW50U3RlcCgpO1xuICAgIH1cbn1cbiJdfQ==