import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
let UiProgressBarComponent = class UiProgressBarComponent {
    constructor() {
        this.MAX_PERCENTAGE = 100;
        this.isCompleted = false;
    }
    ngOnChanges(changes) {
        this.percentage =
            changes.percentage.currentValue > this.MAX_PERCENTAGE
                ? this.MAX_PERCENTAGE
                : changes.percentage.currentValue;
        this.isCompleted = this.percentage === this.MAX_PERCENTAGE;
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Number)
], UiProgressBarComponent.prototype, "percentage", void 0);
UiProgressBarComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-progress-bar',
        template: "<div class=\"ui-progress-bar\"\n     [ngClass]=\"{\n        'ui-progress-bar--complete': isCompleted\n     }\">\n    <div class=\"ui-progress-bar__foreground\"\n         [ngStyle]=\"{\n             'width': percentage + '%'\n         }\"></div>\n</div>\n"
    }),
    tslib_1.__metadata("design:paramtypes", [])
], UiProgressBarComponent);
export { UiProgressBarComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktcHJvZ3Jlc3MtYmFyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2h1dWItbWF0ZXJpYWwvbGliLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9wcm9ncmVzcy1iYXIvdWktcHJvZ3Jlc3MtYmFyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQTRCLE1BQU0sZUFBZSxDQUFDO0FBTTNFLElBQWEsc0JBQXNCLEdBQW5DLE1BQWEsc0JBQXNCO0lBTS9CO1FBSFEsbUJBQWMsR0FBRyxHQUFHLENBQUM7UUFDdEIsZ0JBQVcsR0FBRyxLQUFLLENBQUM7SUFFWixDQUFDO0lBRVQsV0FBVyxDQUFDLE9BQXNCO1FBQ3JDLElBQUksQ0FBQyxVQUFVO1lBQ1gsT0FBTyxDQUFDLFVBQVUsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGNBQWM7Z0JBQ2pELENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYztnQkFDckIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDO1FBRTFDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLENBQUMsY0FBYyxDQUFDO0lBQy9ELENBQUM7Q0FDSixDQUFBO0FBZlk7SUFBUixLQUFLLEVBQUU7OzBEQUEyQjtBQUQxQixzQkFBc0I7SUFKbEMsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLGlCQUFpQjtRQUMzQiwwUUFBK0M7S0FDbEQsQ0FBQzs7R0FDVyxzQkFBc0IsQ0FnQmxDO1NBaEJZLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uQ2hhbmdlcywgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ3VpLXByb2dyZXNzLWJhcicsXG4gICAgdGVtcGxhdGVVcmw6ICcuL3VpLXByb2dyZXNzLWJhci5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgVWlQcm9ncmVzc0JhckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XG4gICAgQElucHV0KCkgcHVibGljIHBlcmNlbnRhZ2U6IG51bWJlcjtcblxuICAgIHByaXZhdGUgTUFYX1BFUkNFTlRBR0UgPSAxMDA7XG4gICAgcHVibGljIGlzQ29tcGxldGVkID0gZmFsc2U7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgICBwdWJsaWMgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xuICAgICAgICB0aGlzLnBlcmNlbnRhZ2UgPVxuICAgICAgICAgICAgY2hhbmdlcy5wZXJjZW50YWdlLmN1cnJlbnRWYWx1ZSA+IHRoaXMuTUFYX1BFUkNFTlRBR0VcbiAgICAgICAgICAgICAgICA/IHRoaXMuTUFYX1BFUkNFTlRBR0VcbiAgICAgICAgICAgICAgICA6IGNoYW5nZXMucGVyY2VudGFnZS5jdXJyZW50VmFsdWU7XG5cbiAgICAgICAgdGhpcy5pc0NvbXBsZXRlZCA9IHRoaXMucGVyY2VudGFnZSA9PT0gdGhpcy5NQVhfUEVSQ0VOVEFHRTtcbiAgICB9XG59XG4iXX0=