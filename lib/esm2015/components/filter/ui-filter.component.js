import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter, ElementRef, HostListener } from '@angular/core';
import { MaterialHelper } from '../helpers/material.helpers';
let UiFilterComponent = class UiFilterComponent {
    constructor(element, helper) {
        this.element = element;
        this.helper = helper;
        this.ELEMENT_SIZE = 350;
        this.alignRight = false;
        this.alignLeft = false;
        this.selectedOptions = [];
        this.optionContainerIsVisible = false;
        /** Property that should be used for binding a callback to receive the save action **/
        this.onSaveAction = new EventEmitter();
    }
    trackByFn(index, item) {
        return index;
    }
    onDomClicked(event) {
        if (!this.element.nativeElement.contains(event.target) && this.optionContainerIsVisible) {
            this.toggleOptionContainerVisibility();
        }
    }
    toggleOptionContainerVisibility() {
        const absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, this.ELEMENT_SIZE);
        this.optionContainerIsVisible = !this.optionContainerIsVisible;
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
        this.selectedOptions.length = 0;
        this.selectedOptionParent = {};
    }
    onSaveButtonClick() {
        this.onSaveAction.emit({
            selectedOptionParent: this.selectedOptionParent,
            selectedOption: this.selectedOptions.find((item) => item.selected === true) || this.selectedOptions[0]
        });
        this.toggleOptionContainerVisibility();
    }
    setSelectedOption(options, optionIndex) {
        options.forEach((item) => {
            item.selected = false;
        });
        options[optionIndex].selected = true;
    }
    onSelectChange(options, indexOption) {
        this.setSelectedOption(options, indexOption);
        this.selectedOptions = [];
        this.selectedOptionParent = this.helper.merge(this.viewModel.selectBox[indexOption]);
        if (this.viewModel.selectOptions[this.viewModel.selectBox[indexOption].type]) {
            this.selectedOptions = new Array().concat(this.viewModel.selectOptions[this.viewModel.selectBox[indexOption].type].map((value) => {
                return this.helper.merge(value);
            }));
        }
    }
    shouldShowSaveButton() {
        return !!(this.selectedOptionParent &&
            (this.selectedOptionParent.hasOwnProperty('value') || this.selectedOptions.length > 0));
    }
    onSecondarySelectChange(options, indexOption) {
        this.setSelectedOption(options, indexOption);
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiFilterComponent.prototype, "viewModel", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], UiFilterComponent.prototype, "onSaveAction", void 0);
tslib_1.__decorate([
    HostListener('document:click', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], UiFilterComponent.prototype, "onDomClicked", null);
UiFilterComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-filter',
        template: "<div class=\"ui-filter__container\">\n    <i class=\"huub-material-icon ui-filter__icon\" size=\"extra-extra-small\" icon=\"filter\"\n       (click)=\"toggleOptionContainerVisibility()\"></i>\n    <span class=\"ui-filter__text\"\n          (click)=\"toggleOptionContainerVisibility()\">{{viewModel.filterText}}</span>\n    <i class=\"huub-material-icon ui-filter__icon\"\n        icon=\"chevron-down\"\n        size=\"extra-extra-small\"\n        [ngClass]=\"{\n            'ui-filter__icon__tune': optionContainerIsVisible\n        }\"\n       (click)=\"toggleOptionContainerVisibility()\"></i>\n\n    <div class=\"ui-filter__options-container\"\n        [ngClass]=\"{\n            'ui-filter__options-container--visible': optionContainerIsVisible,\n            'ui-filter__options-container--right': alignRight,\n            'ui-filter__options-container--left': alignLeft\n        }\">\n        <ng-container *ngIf=\"optionContainerIsVisible\">\n            <select class=\"ui-filter__select\"\n                    (change)=\"onSelectChange(viewModel.selectBox, $event.target.value)\">\n                <option *ngFor=\"let option of viewModel.selectBox; let index = index; trackBy: trackByFn\"\n                        value=\"{{index}}\">{{option.text}}</option>\n            </select>\n\n            <select *ngIf=\"selectedOptions.length > 0\"\n                    class=\"ui-filter__select\"\n                    (change)=\"onSecondarySelectChange(selectedOptions, $event.target.value)\">\n                <option *ngFor=\"let option of selectedOptions; let index = index; trackBy: trackByFn\"\n                        value=\"{{index}}\">{{option.text}}</option>\n            </select>\n        </ng-container>\n\n        <button *ngIf=\"shouldShowSaveButton()\"\n                type=\"secondary\" class=\"ui-filter__button\"\n                (click)=\"onSaveButtonClick()\">{{viewModel.buttonText}}</button>\n    </div>\n</div>\n"
    }),
    tslib_1.__metadata("design:paramtypes", [ElementRef, MaterialHelper])
], UiFilterComponent);
export { UiFilterComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktZmlsdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2h1dWItbWF0ZXJpYWwvbGliLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9maWx0ZXIvdWktZmlsdGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2pHLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQWE3RCxJQUFhLGlCQUFpQixHQUE5QixNQUFhLGlCQUFpQjtJQXVCMUIsWUFBb0IsT0FBbUIsRUFBVSxNQUFzQjtRQUFuRCxZQUFPLEdBQVAsT0FBTyxDQUFZO1FBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUF0QnRELGlCQUFZLEdBQUcsR0FBRyxDQUFDO1FBRTdCLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNsQixvQkFBZSxHQUFHLEVBQUUsQ0FBQztRQUNyQiw2QkFBd0IsR0FBRyxLQUFLLENBQUM7UUFVeEMsc0ZBQXNGO1FBRS9FLGlCQUFZLEdBR2QsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUVrRCxDQUFDO0lBRXBFLFNBQVMsQ0FBQyxLQUFLLEVBQUUsSUFBSTtRQUN4QixPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBR00sWUFBWSxDQUFDLEtBQUs7UUFDckIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDLHdCQUF3QixFQUFFO1lBQ3JGLElBQUksQ0FBQywrQkFBK0IsRUFBRSxDQUFDO1NBQzFDO0lBQ0wsQ0FBQztJQUVNLCtCQUErQjtRQUNsQyxNQUFNLG9CQUFvQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsK0JBQStCLENBQ3BFLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLHFCQUFxQixFQUFFLENBQUMsSUFBSSxFQUN2RCxJQUFJLENBQUMsWUFBWSxDQUNwQixDQUFDO1FBRUYsSUFBSSxDQUFDLHdCQUF3QixHQUFHLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDO1FBRS9ELElBQUksQ0FBQyxTQUFTLEdBQUcsb0JBQW9CLENBQUMsZUFBZSxDQUFDO1FBQ3RELElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQW9CLENBQUMsZ0JBQWdCLENBQUM7UUFDeEQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxFQUFFLENBQUM7SUFDbkMsQ0FBQztJQUVNLGlCQUFpQjtRQUNwQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQztZQUNuQixvQkFBb0IsRUFBRSxJQUFJLENBQUMsb0JBQW9CO1lBQy9DLGNBQWMsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztTQUN6RyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsK0JBQStCLEVBQUUsQ0FBQztJQUMzQyxDQUFDO0lBRU8saUJBQWlCLENBQUMsT0FBTyxFQUFFLFdBQVc7UUFDMUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQzFCLENBQUMsQ0FBQyxDQUFDO1FBRUgsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7SUFDekMsQ0FBQztJQUVNLGNBQWMsQ0FBQyxPQUFZLEVBQUUsV0FBbUI7UUFDbkQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsQ0FBQztRQUU3QyxJQUFJLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUVyRixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzFFLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQyxNQUFNLENBQ3JDLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFO2dCQUNuRixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3BDLENBQUMsQ0FBQyxDQUNMLENBQUM7U0FDTDtJQUNMLENBQUM7SUFFTSxvQkFBb0I7UUFDdkIsT0FBTyxDQUFDLENBQUMsQ0FDTCxJQUFJLENBQUMsb0JBQW9CO1lBQ3pCLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FDekYsQ0FBQztJQUNOLENBQUM7SUFFTSx1QkFBdUIsQ0FBQyxPQUFZLEVBQUUsV0FBbUI7UUFDNUQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsQ0FBQztJQUNqRCxDQUFDO0NBQ0osQ0FBQTtBQW5GRztJQURDLEtBQUssRUFBRTs7b0RBTU47QUFJRjtJQURDLE1BQU0sRUFBRTtzQ0FDWSxZQUFZO3VEQUdUO0FBU3hCO0lBREMsWUFBWSxDQUFDLGdCQUFnQixFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7Ozs7cURBSzFDO0FBbENRLGlCQUFpQjtJQUo3QixTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsV0FBVztRQUNyQixnNkRBQXlDO0tBQzVDLENBQUM7NkNBd0IrQixVQUFVLEVBQWtCLGNBQWM7R0F2QjlELGlCQUFpQixDQTRGN0I7U0E1RlksaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIEVsZW1lbnRSZWYsIEhvc3RMaXN0ZW5lciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTWF0ZXJpYWxIZWxwZXIgfSBmcm9tICcuLi9oZWxwZXJzL21hdGVyaWFsLmhlbHBlcnMnO1xuXG5leHBvcnQgaW50ZXJmYWNlIE9wdGlvbnNJbnRlcmZhY2Uge1xuICAgIHZhbHVlOiBzdHJpbmc7XG4gICAgdGV4dDogc3RyaW5nO1xuICAgIHNlbGVjdGVkPzogYm9vbGVhbjtcbiAgICB0eXBlPzogc3RyaW5nO1xufVxuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ3VpLWZpbHRlcicsXG4gICAgdGVtcGxhdGVVcmw6ICcuL3VpLWZpbHRlci5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgVWlGaWx0ZXJDb21wb25lbnQge1xuICAgIHByaXZhdGUgcmVhZG9ubHkgRUxFTUVOVF9TSVpFID0gMzUwO1xuICAgIHByaXZhdGUgc2VsZWN0ZWRPcHRpb25QYXJlbnQ7XG4gICAgcHVibGljIGFsaWduUmlnaHQgPSBmYWxzZTtcbiAgICBwdWJsaWMgYWxpZ25MZWZ0ID0gZmFsc2U7XG4gICAgcHVibGljIHNlbGVjdGVkT3B0aW9ucyA9IFtdO1xuICAgIHB1YmxpYyBvcHRpb25Db250YWluZXJJc1Zpc2libGUgPSBmYWxzZTtcblxuICAgIEBJbnB1dCgpXG4gICAgcHVibGljIHZpZXdNb2RlbDoge1xuICAgICAgICBmaWx0ZXJUZXh0OiBzdHJpbmc7XG4gICAgICAgIGJ1dHRvblRleHQ6IHN0cmluZztcbiAgICAgICAgc2VsZWN0Qm94OiBbT3B0aW9uc0ludGVyZmFjZV07XG4gICAgICAgIHNlbGVjdE9wdGlvbnM6IGFueTtcbiAgICB9O1xuXG4gICAgLyoqIFByb3BlcnR5IHRoYXQgc2hvdWxkIGJlIHVzZWQgZm9yIGJpbmRpbmcgYSBjYWxsYmFjayB0byByZWNlaXZlIHRoZSBzYXZlIGFjdGlvbiAqKi9cbiAgICBAT3V0cHV0KClcbiAgICBwdWJsaWMgb25TYXZlQWN0aW9uOiBFdmVudEVtaXR0ZXI8e1xuICAgICAgICBzZWxlY3RlZE9wdGlvbjogYW55O1xuICAgICAgICBzZWxlY3RlZE9wdGlvblBhcmVudDogYW55O1xuICAgIH0+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBlbGVtZW50OiBFbGVtZW50UmVmLCBwcml2YXRlIGhlbHBlcjogTWF0ZXJpYWxIZWxwZXIpIHt9XG5cbiAgICBwdWJsaWMgdHJhY2tCeUZuKGluZGV4LCBpdGVtKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIGluZGV4O1xuICAgIH1cblxuICAgIEBIb3N0TGlzdGVuZXIoJ2RvY3VtZW50OmNsaWNrJywgWyckZXZlbnQnXSlcbiAgICBwdWJsaWMgb25Eb21DbGlja2VkKGV2ZW50KSB7XG4gICAgICAgIGlmICghdGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQuY29udGFpbnMoZXZlbnQudGFyZ2V0KSAmJiB0aGlzLm9wdGlvbkNvbnRhaW5lcklzVmlzaWJsZSkge1xuICAgICAgICAgICAgdGhpcy50b2dnbGVPcHRpb25Db250YWluZXJWaXNpYmlsaXR5KCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgdG9nZ2xlT3B0aW9uQ29udGFpbmVyVmlzaWJpbGl0eSgpOiB2b2lkIHtcbiAgICAgICAgY29uc3QgYWJzb2x1dGVSZW5kZXJDb25maWcgPSB0aGlzLmhlbHBlci5nZXRBYnNvbHV0ZVBvc2l0aW9uUmVuZGVyQ29uZmlnKFxuICAgICAgICAgICAgdGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkubGVmdCxcbiAgICAgICAgICAgIHRoaXMuRUxFTUVOVF9TSVpFXG4gICAgICAgICk7XG5cbiAgICAgICAgdGhpcy5vcHRpb25Db250YWluZXJJc1Zpc2libGUgPSAhdGhpcy5vcHRpb25Db250YWluZXJJc1Zpc2libGU7XG5cbiAgICAgICAgdGhpcy5hbGlnbkxlZnQgPSBhYnNvbHV0ZVJlbmRlckNvbmZpZy5jYW5SZW5kZXJUb0xlZnQ7XG4gICAgICAgIHRoaXMuYWxpZ25SaWdodCA9IGFic29sdXRlUmVuZGVyQ29uZmlnLmNhblJlbmRlclRvUmlnaHQ7XG4gICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb25zLmxlbmd0aCA9IDA7XG4gICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb25QYXJlbnQgPSB7fTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25TYXZlQnV0dG9uQ2xpY2soKTogdm9pZCB7XG4gICAgICAgIHRoaXMub25TYXZlQWN0aW9uLmVtaXQoe1xuICAgICAgICAgICAgc2VsZWN0ZWRPcHRpb25QYXJlbnQ6IHRoaXMuc2VsZWN0ZWRPcHRpb25QYXJlbnQsXG4gICAgICAgICAgICBzZWxlY3RlZE9wdGlvbjogdGhpcy5zZWxlY3RlZE9wdGlvbnMuZmluZCgoaXRlbSkgPT4gaXRlbS5zZWxlY3RlZCA9PT0gdHJ1ZSkgfHwgdGhpcy5zZWxlY3RlZE9wdGlvbnNbMF1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgdGhpcy50b2dnbGVPcHRpb25Db250YWluZXJWaXNpYmlsaXR5KCk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBzZXRTZWxlY3RlZE9wdGlvbihvcHRpb25zLCBvcHRpb25JbmRleCkge1xuICAgICAgICBvcHRpb25zLmZvckVhY2goKGl0ZW0pID0+IHtcbiAgICAgICAgICAgIGl0ZW0uc2VsZWN0ZWQgPSBmYWxzZTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgb3B0aW9uc1tvcHRpb25JbmRleF0uc2VsZWN0ZWQgPSB0cnVlO1xuICAgIH1cblxuICAgIHB1YmxpYyBvblNlbGVjdENoYW5nZShvcHRpb25zOiBhbnksIGluZGV4T3B0aW9uOiBudW1iZXIpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zZXRTZWxlY3RlZE9wdGlvbihvcHRpb25zLCBpbmRleE9wdGlvbik7XG5cbiAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbnMgPSBbXTtcbiAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvblBhcmVudCA9IHRoaXMuaGVscGVyLm1lcmdlKHRoaXMudmlld01vZGVsLnNlbGVjdEJveFtpbmRleE9wdGlvbl0pO1xuXG4gICAgICAgIGlmICh0aGlzLnZpZXdNb2RlbC5zZWxlY3RPcHRpb25zW3RoaXMudmlld01vZGVsLnNlbGVjdEJveFtpbmRleE9wdGlvbl0udHlwZV0pIHtcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb25zID0gbmV3IEFycmF5KCkuY29uY2F0KFxuICAgICAgICAgICAgICAgIHRoaXMudmlld01vZGVsLnNlbGVjdE9wdGlvbnNbdGhpcy52aWV3TW9kZWwuc2VsZWN0Qm94W2luZGV4T3B0aW9uXS50eXBlXS5tYXAoKHZhbHVlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmhlbHBlci5tZXJnZSh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgc2hvdWxkU2hvd1NhdmVCdXR0b24oKSB7XG4gICAgICAgIHJldHVybiAhIShcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb25QYXJlbnQgJiZcbiAgICAgICAgICAgICh0aGlzLnNlbGVjdGVkT3B0aW9uUGFyZW50Lmhhc093blByb3BlcnR5KCd2YWx1ZScpIHx8IHRoaXMuc2VsZWN0ZWRPcHRpb25zLmxlbmd0aCA+IDApXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgcHVibGljIG9uU2Vjb25kYXJ5U2VsZWN0Q2hhbmdlKG9wdGlvbnM6IGFueSwgaW5kZXhPcHRpb246IG51bWJlcik6IHZvaWQge1xuICAgICAgICB0aGlzLnNldFNlbGVjdGVkT3B0aW9uKG9wdGlvbnMsIGluZGV4T3B0aW9uKTtcbiAgICB9XG59XG4iXX0=