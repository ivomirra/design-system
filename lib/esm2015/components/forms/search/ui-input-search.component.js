import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
let UiInputSearchComponent = class UiInputSearchComponent {
    constructor() {
        this.viewModel = {
            placeholder: '',
            value: ''
        };
        /** Property that sould be used for binding a callback to receive the
         input submission **/
        this.onSubmit = new EventEmitter();
        /** Property that sould be used for binding a callback to receive the
         input change state **/
        this.onChange = new EventEmitter();
    }
    /**
     * Method binded to the form submit event.
     */
    onSubmitForm() {
        this.onSubmit.emit(this.viewModel.value);
    }
    /**
     * Method binded to the input change event.
     */
    onInputChange() {
        this.onChange.emit(this.viewModel.value);
    }
    onClearIconClicked() {
        this.viewModel.value = '';
        this.onSubmitForm();
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiInputSearchComponent.prototype, "viewModel", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], UiInputSearchComponent.prototype, "onSubmit", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], UiInputSearchComponent.prototype, "onChange", void 0);
UiInputSearchComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-input-search',
        template: "<form (submit)=\"onSubmitForm()\" class=\"ui-input-search__form\">\n    <input type=\"text\"\n       class=\"ui-input-search\"\n       [attr.placeholder]=\"viewModel.placeholder\"\n       (input)=\"onInputChange()\"\n       [(ngModel)]=\"viewModel.value\"\n       [ngModelOptions]=\"{standalone: true}\">\n    <i class=\"huub-material-icon ui-input-search__icon\" icon=\"search\"></i>\n    <i *ngIf=\"viewModel.value && viewModel.value !== ''\"\n        (click)=\"onClearIconClicked()\"\n        class=\"huub-material-icon ui-input-clear__icon\"\n        icon=\"close\"></i>\n</form>\n"
    }),
    tslib_1.__metadata("design:paramtypes", [])
], UiInputSearchComponent);
export { UiInputSearchComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktaW5wdXQtc2VhcmNoLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2h1dWItbWF0ZXJpYWwvbGliLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9mb3Jtcy9zZWFyY2gvdWktaW5wdXQtc2VhcmNoLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU12RSxJQUFhLHNCQUFzQixHQUFuQyxNQUFhLHNCQUFzQjtJQW9CL0I7UUFsQk8sY0FBUyxHQUtaO1lBQ0EsV0FBVyxFQUFFLEVBQUU7WUFDZixLQUFLLEVBQUUsRUFBRTtTQUNaLENBQUM7UUFFRjs2QkFDcUI7UUFDSixhQUFRLEdBQXlCLElBQUksWUFBWSxFQUFFLENBQUM7UUFFckU7K0JBQ3VCO1FBQ04sYUFBUSxHQUF5QixJQUFJLFlBQVksRUFBRSxDQUFDO0lBRXRELENBQUM7SUFFaEI7O09BRUc7SUFDSSxZQUFZO1FBQ2YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQ7O09BRUc7SUFDSSxhQUFhO1FBQ2hCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQUVNLGtCQUFrQjtRQUNyQixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFFMUIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3hCLENBQUM7Q0FDSixDQUFBO0FBdkNHO0lBREMsS0FBSyxFQUFFOzt5REFTTjtBQUlRO0lBQVQsTUFBTSxFQUFFO3NDQUFrQixZQUFZO3dEQUE4QjtBQUkzRDtJQUFULE1BQU0sRUFBRTtzQ0FBa0IsWUFBWTt3REFBOEI7QUFsQjVELHNCQUFzQjtJQUpsQyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsaUJBQWlCO1FBQzNCLHFsQkFBK0M7S0FDbEQsQ0FBQzs7R0FDVyxzQkFBc0IsQ0F5Q2xDO1NBekNZLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAndWktaW5wdXQtc2VhcmNoJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vdWktaW5wdXQtc2VhcmNoLmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBVaUlucHV0U2VhcmNoQ29tcG9uZW50IHtcbiAgICBASW5wdXQoKVxuICAgIHB1YmxpYyB2aWV3TW9kZWw6IHtcbiAgICAgICAgLyoqIFByb3BlcnR5IHRoYXQgZGVmaW5lcyB0aGUgaW5wdXQgcGxhY2Vob2xkZXIgKiovXG4gICAgICAgIHBsYWNlaG9sZGVyOiBzdHJpbmc7XG4gICAgICAgIC8qKiBQcm9wZXJ0eSB0aGF0IGRlZmluZXMgdGhlIGlucHV0IHZhbHVlICoqL1xuICAgICAgICB2YWx1ZTogc3RyaW5nO1xuICAgIH0gPSB7XG4gICAgICAgIHBsYWNlaG9sZGVyOiAnJyxcbiAgICAgICAgdmFsdWU6ICcnXG4gICAgfTtcblxuICAgIC8qKiBQcm9wZXJ0eSB0aGF0IHNvdWxkIGJlIHVzZWQgZm9yIGJpbmRpbmcgYSBjYWxsYmFjayB0byByZWNlaXZlIHRoZVxuICAgICBpbnB1dCBzdWJtaXNzaW9uICoqL1xuICAgIEBPdXRwdXQoKSBwdWJsaWMgb25TdWJtaXQ6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgLyoqIFByb3BlcnR5IHRoYXQgc291bGQgYmUgdXNlZCBmb3IgYmluZGluZyBhIGNhbGxiYWNrIHRvIHJlY2VpdmUgdGhlXG4gICAgIGlucHV0IGNoYW5nZSBzdGF0ZSAqKi9cbiAgICBAT3V0cHV0KCkgcHVibGljIG9uQ2hhbmdlOiBFdmVudEVtaXR0ZXI8c3RyaW5nPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAgIGNvbnN0cnVjdG9yKCkge31cblxuICAgIC8qKlxuICAgICAqIE1ldGhvZCBiaW5kZWQgdG8gdGhlIGZvcm0gc3VibWl0IGV2ZW50LlxuICAgICAqL1xuICAgIHB1YmxpYyBvblN1Ym1pdEZvcm0oKTogdm9pZCB7XG4gICAgICAgIHRoaXMub25TdWJtaXQuZW1pdCh0aGlzLnZpZXdNb2RlbC52YWx1ZSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogTWV0aG9kIGJpbmRlZCB0byB0aGUgaW5wdXQgY2hhbmdlIGV2ZW50LlxuICAgICAqL1xuICAgIHB1YmxpYyBvbklucHV0Q2hhbmdlKCk6IHZvaWQge1xuICAgICAgICB0aGlzLm9uQ2hhbmdlLmVtaXQodGhpcy52aWV3TW9kZWwudmFsdWUpO1xuICAgIH1cblxuICAgIHB1YmxpYyBvbkNsZWFySWNvbkNsaWNrZWQoKTogdm9pZCB7XG4gICAgICAgIHRoaXMudmlld01vZGVsLnZhbHVlID0gJyc7XG5cbiAgICAgICAgdGhpcy5vblN1Ym1pdEZvcm0oKTtcbiAgICB9XG59XG4iXX0=