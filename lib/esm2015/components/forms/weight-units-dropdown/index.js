import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UiWeightUnitsDropdownComponent } from './ui-weight-units-dropdown.component';
import { WeightUnitsListHelperModule } from 'huub-utils/lib';
import { UiInputSearchModule } from '../../forms/search/index';
import { SearchPipeModule } from '../../pipes/index';
let UiWeightUnitsDropdownModule = class UiWeightUnitsDropdownModule {
};
UiWeightUnitsDropdownModule = tslib_1.__decorate([
    NgModule({
        imports: [CommonModule, FormsModule, WeightUnitsListHelperModule, UiInputSearchModule, SearchPipeModule],
        declarations: [UiWeightUnitsDropdownComponent],
        exports: [UiWeightUnitsDropdownComponent]
    })
], UiWeightUnitsDropdownModule);
export { UiWeightUnitsDropdownModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZm9ybXMvd2VpZ2h0LXVuaXRzLWRyb3Bkb3duL2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLDhCQUE4QixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDdEYsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0QsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDL0QsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFPckQsSUFBYSwyQkFBMkIsR0FBeEMsTUFBYSwyQkFBMkI7Q0FBRyxDQUFBO0FBQTlCLDJCQUEyQjtJQUx2QyxRQUFRLENBQUM7UUFDTixPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsV0FBVyxFQUFFLDJCQUEyQixFQUFFLG1CQUFtQixFQUFFLGdCQUFnQixDQUFDO1FBQ3hHLFlBQVksRUFBRSxDQUFDLDhCQUE4QixDQUFDO1FBQzlDLE9BQU8sRUFBRSxDQUFDLDhCQUE4QixDQUFDO0tBQzVDLENBQUM7R0FDVywyQkFBMkIsQ0FBRztTQUE5QiwyQkFBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IEZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgVWlXZWlnaHRVbml0c0Ryb3Bkb3duQ29tcG9uZW50IH0gZnJvbSAnLi91aS13ZWlnaHQtdW5pdHMtZHJvcGRvd24uY29tcG9uZW50JztcbmltcG9ydCB7IFdlaWdodFVuaXRzTGlzdEhlbHBlck1vZHVsZSB9IGZyb20gJ2h1dWItdXRpbHMvbGliJztcbmltcG9ydCB7IFVpSW5wdXRTZWFyY2hNb2R1bGUgfSBmcm9tICcuLi8uLi9mb3Jtcy9zZWFyY2gvaW5kZXgnO1xuaW1wb3J0IHsgU2VhcmNoUGlwZU1vZHVsZSB9IGZyb20gJy4uLy4uL3BpcGVzL2luZGV4JztcblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlLCBGb3Jtc01vZHVsZSwgV2VpZ2h0VW5pdHNMaXN0SGVscGVyTW9kdWxlLCBVaUlucHV0U2VhcmNoTW9kdWxlLCBTZWFyY2hQaXBlTW9kdWxlXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtVaVdlaWdodFVuaXRzRHJvcGRvd25Db21wb25lbnRdLFxuICAgIGV4cG9ydHM6IFtVaVdlaWdodFVuaXRzRHJvcGRvd25Db21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIFVpV2VpZ2h0VW5pdHNEcm9wZG93bk1vZHVsZSB7fVxuIl19