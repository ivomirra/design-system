import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CurrenciesListHelper } from 'huub-utils/lib';
let UiCurrencyInputComponent = class UiCurrencyInputComponent {
    constructor(currenciesListHelper) {
        this.currenciesListHelper = currenciesListHelper;
        this.onChange = new EventEmitter();
        this.isDropdownOpened = false;
        this.currencySymbol = '';
        this.currenciesList = this.currenciesListHelper.getCurrenciesList();
    }
    ngOnInit() {
        this.currencySymbol = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).symbol;
        this.disabled = this.disabled !== undefined ? this.disabled : false;
    }
    onInputChange() {
        this.onChange.emit({ currency: this.currency, value: this.value });
    }
    openCloseDropdown() {
        if (!this.disabled) {
            this.isDropdownOpened = !this.isDropdownOpened;
        }
    }
    changeCurrency(isoCode) {
        this.currency = isoCode;
        this.onChange.emit({ currency: this.currency, value: this.value });
        this.currencySymbol = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).symbol;
        this.openCloseDropdown();
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], UiCurrencyInputComponent.prototype, "currency", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], UiCurrencyInputComponent.prototype, "language", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], UiCurrencyInputComponent.prototype, "value", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], UiCurrencyInputComponent.prototype, "placeholder", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Boolean)
], UiCurrencyInputComponent.prototype, "disabled", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], UiCurrencyInputComponent.prototype, "onChange", void 0);
UiCurrencyInputComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-currency-input',
        providers: [CurrenciesListHelper],
        template: "<div class=\"ui-currency-input\">\n    <span class=\"currency-char\" (click)=\"openCloseDropdown()\">{{currencySymbol}}</span>\n    <input type=\"number\" class=\"currency-char__input\" [attr.placeholder]=\"placeholder\" (input)=\"onInputChange()\"\n        [(ngModel)]=\"value\" [ngModelOptions]=\"{standalone: true}\" [disabled]=\"disabled\">\n    <div *ngIf=\"isDropdownOpened\" class=\"currency-input__dropdown\" (mouseleave)=\"openCloseDropdown()\">\n        <div *ngFor=\"let currency of currenciesList\" class=\"currency-input__dropdown-item\" (click)=\"changeCurrency(currency.iso3code)\">\n            <span class=\"currency-input__dropdown-char\">{{currency.symbol}}</span>\n            <span class=\"currency-input__dropdown-iso-code\">{{currency.iso3code}}</span>\n            <span class=\"currency-input__dropdown-name\">{{currency.name}}</span>\n        </div>\n    </div>\n</div>"
    }),
    tslib_1.__metadata("design:paramtypes", [CurrenciesListHelper])
], UiCurrencyInputComponent);
export { UiCurrencyInputComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktY3VycmVuY3ktaW5wdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaHV1Yi1tYXRlcmlhbC9saWIvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2Zvcm1zL2N1cnJlbmN5LWlucHV0L3VpLWN1cnJlbmN5LWlucHV0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQU90RCxJQUFhLHdCQUF3QixHQUFyQyxNQUFhLHdCQUF3QjtJQWVqQyxZQUFvQixvQkFBMEM7UUFBMUMseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFzQjtRQUw3QyxhQUFRLEdBQXlCLElBQUksWUFBWSxFQUFFLENBQUM7UUFFOUQscUJBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLG1CQUFjLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGlCQUFpQixFQUFFLENBQUM7SUFDTCxDQUFDO0lBRTNELFFBQVE7UUFDWCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQzVGLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUN4RSxDQUFDO0lBRU0sYUFBYTtRQUNoQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBRU0saUJBQWlCO1FBQ3BCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2hCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztTQUNsRDtJQUNMLENBQUM7SUFFTSxjQUFjLENBQUMsT0FBTztRQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztRQUN4QixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQzVGLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzdCLENBQUM7Q0FDSixDQUFBO0FBckNZO0lBQVIsS0FBSyxFQUFFOzswREFBMEI7QUFDekI7SUFBUixLQUFLLEVBQUU7OzBEQUEwQjtBQUV6QjtJQUFSLEtBQUssRUFBRTs7dURBQXVCO0FBRXRCO0lBQVIsS0FBSyxFQUFFOzs2REFBNkI7QUFFNUI7SUFBUixLQUFLLEVBQUU7OzBEQUEyQjtBQUV6QjtJQUFULE1BQU0sRUFBRTtzQ0FBa0IsWUFBWTswREFBOEI7QUFWNUQsd0JBQXdCO0lBTHBDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxtQkFBbUI7UUFDN0IsU0FBUyxFQUFFLENBQUMsb0JBQW9CLENBQUM7UUFDakMsMjRCQUFpRDtLQUNwRCxDQUFDOzZDQWdCNEMsb0JBQW9CO0dBZnJELHdCQUF3QixDQXNDcEM7U0F0Q1ksd0JBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ3VycmVuY2llc0xpc3RIZWxwZXIgfSBmcm9tICdodXViLXV0aWxzL2xpYic7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAndWktY3VycmVuY3ktaW5wdXQnLFxuICAgIHByb3ZpZGVyczogW0N1cnJlbmNpZXNMaXN0SGVscGVyXSxcbiAgICB0ZW1wbGF0ZVVybDogJy4vdWktY3VycmVuY3ktaW5wdXQuY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFVpQ3VycmVuY3lJbnB1dENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgQElucHV0KCkgcHVibGljIGN1cnJlbmN5Pzogc3RyaW5nO1xuICAgIEBJbnB1dCgpIHB1YmxpYyBsYW5ndWFnZT86IHN0cmluZztcblxuICAgIEBJbnB1dCgpIHB1YmxpYyB2YWx1ZT86IHN0cmluZztcblxuICAgIEBJbnB1dCgpIHB1YmxpYyBwbGFjZWhvbGRlcj86IHN0cmluZztcblxuICAgIEBJbnB1dCgpIHB1YmxpYyBkaXNhYmxlZD86IGJvb2xlYW47XG5cbiAgICBAT3V0cHV0KCkgcHVibGljIG9uQ2hhbmdlOiBFdmVudEVtaXR0ZXI8b2JqZWN0PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAgIHB1YmxpYyBpc0Ryb3Bkb3duT3BlbmVkID0gZmFsc2U7XG4gICAgcHVibGljIGN1cnJlbmN5U3ltYm9sID0gJyc7XG4gICAgcHVibGljIGN1cnJlbmNpZXNMaXN0ID0gdGhpcy5jdXJyZW5jaWVzTGlzdEhlbHBlci5nZXRDdXJyZW5jaWVzTGlzdCgpO1xuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgY3VycmVuY2llc0xpc3RIZWxwZXI6IEN1cnJlbmNpZXNMaXN0SGVscGVyKSB7fVxuXG4gICAgcHVibGljIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLmN1cnJlbmN5U3ltYm9sID0gdGhpcy5jdXJyZW5jaWVzTGlzdEhlbHBlci5nZXRDdXJyZW5jeUJ5SXNvM0NvZGUodGhpcy5jdXJyZW5jeSkuc3ltYm9sO1xuICAgICAgICB0aGlzLmRpc2FibGVkID0gdGhpcy5kaXNhYmxlZCAhPT0gdW5kZWZpbmVkID8gdGhpcy5kaXNhYmxlZCA6IGZhbHNlO1xuICAgIH1cblxuICAgIHB1YmxpYyBvbklucHV0Q2hhbmdlKCk6IHZvaWQge1xuICAgICAgICB0aGlzLm9uQ2hhbmdlLmVtaXQoeyBjdXJyZW5jeTogdGhpcy5jdXJyZW5jeSwgdmFsdWU6IHRoaXMudmFsdWUgfSk7XG4gICAgfVxuXG4gICAgcHVibGljIG9wZW5DbG9zZURyb3Bkb3duKCkge1xuICAgICAgICBpZiAoIXRoaXMuZGlzYWJsZWQpIHtcbiAgICAgICAgICAgIHRoaXMuaXNEcm9wZG93bk9wZW5lZCA9ICF0aGlzLmlzRHJvcGRvd25PcGVuZWQ7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgY2hhbmdlQ3VycmVuY3koaXNvQ29kZSkge1xuICAgICAgICB0aGlzLmN1cnJlbmN5ID0gaXNvQ29kZTtcbiAgICAgICAgdGhpcy5vbkNoYW5nZS5lbWl0KHsgY3VycmVuY3k6IHRoaXMuY3VycmVuY3ksIHZhbHVlOiB0aGlzLnZhbHVlIH0pO1xuICAgICAgICB0aGlzLmN1cnJlbmN5U3ltYm9sID0gdGhpcy5jdXJyZW5jaWVzTGlzdEhlbHBlci5nZXRDdXJyZW5jeUJ5SXNvM0NvZGUodGhpcy5jdXJyZW5jeSkuc3ltYm9sO1xuICAgICAgICB0aGlzLm9wZW5DbG9zZURyb3Bkb3duKCk7XG4gICAgfVxufVxuIl19