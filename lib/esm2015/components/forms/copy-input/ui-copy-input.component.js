import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { UI_COPY_INPUT_CONSTANT } from './ui-copy-input.constant';
import { CopyToClipboardHelper } from 'huub-utils/lib';
let UiCopyInputComponent = class UiCopyInputComponent {
    constructor(copyToClipboardHelper) {
        this.copyToClipboardHelper = copyToClipboardHelper;
        this.onChange = new EventEmitter();
        this.onSubmit = new EventEmitter();
        this.isTooltipActive = false;
        this.isCopied = false;
        this.isEditing = false;
        this.isTypeCurrency = false;
        this.field = {
            left: '0',
            top: '0'
        };
        this.tooltipViewModel = {
            text: 'Tooltip test example.',
            icon: 'info',
            color: 'blue-grey-dark'
        };
        this.backupValue = '';
    }
    ngOnInit() {
        this.viewModel = this.viewModel ? this.viewModel : {};
        this.type = this.type ? this.type : 'text';
        this.isTypeCurrency = this.type === 'currency';
        this.editable = this.editable !== undefined ? this.editable : true;
        this.disabled = this.disabled !== undefined ? this.disabled : false;
        this.viewModel.editText = this.viewModel.editText ? this.viewModel.editText : UI_COPY_INPUT_CONSTANT.EDIT_TEXT;
        this.label = this.label && typeof this.label === 'string' ? { value: this.label } : this.label;
        this.viewModel.tooltipTextBeforeCopy = this.viewModel.tooltipTextBeforeCopy
            ? this.viewModel.tooltipTextBeforeCopy
            : UI_COPY_INPUT_CONSTANT.TOOLTIP_TEXT_BEFORE_COPY;
        this.viewModel.tooltipTextAfterCopy = this.viewModel.tooltipTextAfterCopy
            ? this.viewModel.tooltipTextAfterCopy
            : UI_COPY_INPUT_CONSTANT.TOOLTIP_TEXT_AFTER_COPY;
        if (this.type === 'currency') {
            this.currencyValue = {
                currency: this.currency,
                value: this.value
            };
        }
    }
    onInputChange(currencyValue) {
        this.currencyValue = currencyValue;
        const value = this.type === 'currency' ? this.currencyValue : this.value;
        this.onChange.emit(value);
    }
    copy(event) {
        if (event && !this.isEditing) {
            event.target.blur();
            const value = this.type === 'currency' ? `${this.currencyValue.value} ${this.currencyValue.currency}` : this.value;
            this.copyToClipboardHelper.copyToClipboard(value);
            this.isCopied = true;
        }
    }
    edit() {
        this.isEditing = true;
        this.backupValue = this.value;
    }
    discard() {
        this.isEditing = false;
        this.value = this.backupValue;
        this.backupValue = '';
    }
    save() {
        const value = this.type === 'currency' ? this.currencyValue : this.value;
        this.onSubmit.emit(value);
        this.isEditing = false;
        this.backupValue = '';
    }
    showTooltip(event) {
        if (event) {
            this.field.left = `${event.pageX}px`;
            this.field.top = `${event.pageY}px`;
            this.isTooltipActive = true;
        }
        else {
            this.isTooltipActive = false;
            this.isCopied = false;
        }
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], UiCopyInputComponent.prototype, "value", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], UiCopyInputComponent.prototype, "placeholder", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiCopyInputComponent.prototype, "label", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], UiCopyInputComponent.prototype, "type", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], UiCopyInputComponent.prototype, "currency", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiCopyInputComponent.prototype, "viewModel", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Boolean)
], UiCopyInputComponent.prototype, "disabled", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Boolean)
], UiCopyInputComponent.prototype, "editable", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], UiCopyInputComponent.prototype, "onChange", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], UiCopyInputComponent.prototype, "onSubmit", void 0);
UiCopyInputComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-copy-input',
        providers: [CopyToClipboardHelper],
        template: "<div class=\"ui-copy-input\">\n    <span class=\"ui-copy-input__label\" *ngIf=\"label\">\n        {{label.value}} <ui-tooltips *ngIf=\"label.tooltipViewModel\" [viewModel]=\"label.tooltipViewModel\"></ui-tooltips>\n    </span>\n    <span class=\"ui-copy-input__edit\" *ngIf=\"editable\" (click)=\"edit()\">{{viewModel.editText}}</span>\n    <div class=\"ui-copy-input__wrapper\" (mouseenter)=\"showTooltip($event)\" (mousemove)=\"showTooltip($event)\"\n        (mouseleave)=\"showTooltip()\" (click)=\"copy($event)\">\n        <div *ngIf=\"!isEditing\" class=\"ui-copy-input__blocker\"></div>\n        <input *ngIf=\"!isTypeCurrency\" type=\"text\" class=\"ui-copy-input-area\"\n            [ngClass]=\"{'ui-copy-input-area__editing': isEditing}\" [attr.placeholder]=\"placeholder\"\n            (input)=\"onInputChange()\" [(ngModel)]=\"value\" [ngModelOptions]=\"{standalone: true}\" [disabled]=\"disabled\">\n        <ui-currency-input *ngIf=\"isTypeCurrency\" [currency]=\"currency\" [placeholder]=\"placeholder\"\n            (onChange)=\"onInputChange($event)\" [value]=\"value\" [disabled]=\"disabled\">\n        </ui-currency-input>\n    </div>\n    <div *ngIf=\"isEditing\" class=\"ui-copy-input__edit-buttons\">\n        <div class=\"ui-copy-input__edit-button\" (click)=\"save()\"><i class=\"huub-material-icon\" icon=\"check\"\n                size=\"extra-small\" color=\"green\"></i></div>\n        <div class=\"ui-copy-input__edit-button\" (click)=\"discard()\"><i class=\"huub-material-icon\" icon=\"close\"\n                size=\"extra-small\" color=\"red\"></i></div>\n    </div>\n    <div *ngIf=\"!isEditing && isTooltipActive\" class=\"ui-copy-input-tooltip\"\n        [ngStyle]=\"{left:field.left, top:field.top}\">\n        <div *ngIf=\"!isCopied\">{{viewModel.tooltipTextBeforeCopy}}</div>\n        <div *ngIf=\"isCopied\"><i class=\"huub-material-icon\" icon=\"check\" size=\"extra-extra-small\"></i>\n            {{viewModel.tooltipTextAfterCopy}}\n        </div>\n    </div>\n</div>"
    }),
    tslib_1.__metadata("design:paramtypes", [CopyToClipboardHelper])
], UiCopyInputComponent);
export { UiCopyInputComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktY29weS1pbnB1dC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZm9ybXMvY29weS1pbnB1dC91aS1jb3B5LWlucHV0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNsRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQU92RCxJQUFhLG9CQUFvQixHQUFqQyxNQUFhLG9CQUFvQjtJQXVDN0IsWUFBb0IscUJBQTRDO1FBQTVDLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBdUI7UUF0Qi9DLGFBQVEsR0FBc0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUVqRCxhQUFRLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7UUFFM0Qsb0JBQWUsR0FBRyxLQUFLLENBQUM7UUFDeEIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLFVBQUssR0FBRztZQUNYLElBQUksRUFBRSxHQUFHO1lBQ1QsR0FBRyxFQUFFLEdBQUc7U0FDWCxDQUFDO1FBRUsscUJBQWdCLEdBQUc7WUFDdEIsSUFBSSxFQUFFLHVCQUF1QjtZQUM3QixJQUFJLEVBQUUsTUFBTTtZQUNaLEtBQUssRUFBRSxnQkFBZ0I7U0FDMUIsQ0FBQztRQUVNLGdCQUFXLEdBQUcsRUFBRSxDQUFDO0lBRzBDLENBQUM7SUFFN0QsUUFBUTtRQUNYLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ3RELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQzNDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLElBQUksS0FBSyxVQUFVLENBQUM7UUFDL0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ25FLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUNwRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLFNBQVMsQ0FBQztRQUMvRyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLElBQUksT0FBTyxJQUFJLENBQUMsS0FBSyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQy9GLElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUI7WUFDdkUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCO1lBQ3RDLENBQUMsQ0FBQyxzQkFBc0IsQ0FBQyx3QkFBd0IsQ0FBQztRQUN0RCxJQUFJLENBQUMsU0FBUyxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsb0JBQW9CO1lBQ3JFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLG9CQUFvQjtZQUNyQyxDQUFDLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLENBQUM7UUFDckQsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLFVBQVUsRUFBRTtZQUMxQixJQUFJLENBQUMsYUFBYSxHQUFHO2dCQUNqQixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7Z0JBQ3ZCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSzthQUNwQixDQUFDO1NBQ0w7SUFDTCxDQUFDO0lBRU0sYUFBYSxDQUFDLGFBQWM7UUFDL0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7UUFDbkMsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDekUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUVNLElBQUksQ0FBQyxLQUFLO1FBQ2IsSUFBSSxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQzFCLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDcEIsTUFBTSxLQUFLLEdBQ1AsSUFBSSxDQUFDLElBQUksS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztZQUN6RyxJQUFJLENBQUMscUJBQXFCLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2xELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1NBQ3hCO0lBQ0wsQ0FBQztJQUVNLElBQUk7UUFDUCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDbEMsQ0FBQztJQUVNLE9BQU87UUFDVixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN2QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDOUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUVNLElBQUk7UUFDUCxNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN6RSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN2QixJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBRU0sV0FBVyxDQUFDLEtBQU07UUFDckIsSUFBSSxLQUFLLEVBQUU7WUFDUCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxHQUFHLEtBQUssQ0FBQyxLQUFLLElBQUksQ0FBQztZQUNyQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxHQUFHLEtBQUssQ0FBQyxLQUFLLElBQUksQ0FBQztZQUNwQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztTQUMvQjthQUFNO1lBQ0gsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7WUFDN0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7U0FDekI7SUFDTCxDQUFDO0NBQ0osQ0FBQTtBQTFHWTtJQUFSLEtBQUssRUFBRTs7bURBQXVCO0FBRXRCO0lBQVIsS0FBSyxFQUFFOzt5REFBNkI7QUFFNUI7SUFBUixLQUFLLEVBQUU7O21EQUFvQjtBQUVuQjtJQUFSLEtBQUssRUFBRTs7a0RBQXNCO0FBRXJCO0lBQVIsS0FBSyxFQUFFOztzREFBMEI7QUFFekI7SUFBUixLQUFLLEVBQUU7O3VEQUF3QjtBQUV2QjtJQUFSLEtBQUssRUFBRTs7c0RBQTJCO0FBRTFCO0lBQVIsS0FBSyxFQUFFOztzREFBMkI7QUFFekI7SUFBVCxNQUFNLEVBQUU7c0NBQWtCLFlBQVk7c0RBQTJCO0FBRXhEO0lBQVQsTUFBTSxFQUFFO3NDQUFrQixZQUFZO3NEQUEyQjtBQW5CekQsb0JBQW9CO0lBTGhDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxlQUFlO1FBQ3pCLFNBQVMsRUFBRSxDQUFDLHFCQUFxQixDQUFDO1FBQ2xDLHErREFBNkM7S0FDaEQsQ0FBQzs2Q0F3QzZDLHFCQUFxQjtHQXZDdkQsb0JBQW9CLENBMkdoQztTQTNHWSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBVSV9DT1BZX0lOUFVUX0NPTlNUQU5UIH0gZnJvbSAnLi91aS1jb3B5LWlucHV0LmNvbnN0YW50JztcbmltcG9ydCB7IENvcHlUb0NsaXBib2FyZEhlbHBlciB9IGZyb20gJ2h1dWItdXRpbHMvbGliJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICd1aS1jb3B5LWlucHV0JyxcbiAgICBwcm92aWRlcnM6IFtDb3B5VG9DbGlwYm9hcmRIZWxwZXJdLFxuICAgIHRlbXBsYXRlVXJsOiAnLi91aS1jb3B5LWlucHV0LmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBVaUNvcHlJbnB1dENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgQElucHV0KCkgcHVibGljIHZhbHVlPzogc3RyaW5nO1xuXG4gICAgQElucHV0KCkgcHVibGljIHBsYWNlaG9sZGVyPzogc3RyaW5nO1xuXG4gICAgQElucHV0KCkgcHVibGljIGxhYmVsPzogYW55O1xuXG4gICAgQElucHV0KCkgcHVibGljIHR5cGU/OiBzdHJpbmc7XG5cbiAgICBASW5wdXQoKSBwdWJsaWMgY3VycmVuY3k/OiBzdHJpbmc7XG5cbiAgICBASW5wdXQoKSBwdWJsaWMgdmlld01vZGVsPzogYW55O1xuXG4gICAgQElucHV0KCkgcHVibGljIGRpc2FibGVkPzogYm9vbGVhbjtcblxuICAgIEBJbnB1dCgpIHB1YmxpYyBlZGl0YWJsZT86IGJvb2xlYW47XG5cbiAgICBAT3V0cHV0KCkgcHVibGljIG9uQ2hhbmdlOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAgIEBPdXRwdXQoKSBwdWJsaWMgb25TdWJtaXQ6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgcHVibGljIGlzVG9vbHRpcEFjdGl2ZSA9IGZhbHNlO1xuICAgIHB1YmxpYyBpc0NvcGllZCA9IGZhbHNlO1xuICAgIHB1YmxpYyBpc0VkaXRpbmcgPSBmYWxzZTtcbiAgICBwdWJsaWMgaXNUeXBlQ3VycmVuY3kgPSBmYWxzZTtcbiAgICBwdWJsaWMgZmllbGQgPSB7XG4gICAgICAgIGxlZnQ6ICcwJyxcbiAgICAgICAgdG9wOiAnMCdcbiAgICB9O1xuXG4gICAgcHVibGljIHRvb2x0aXBWaWV3TW9kZWwgPSB7XG4gICAgICAgIHRleHQ6ICdUb29sdGlwIHRlc3QgZXhhbXBsZS4nLFxuICAgICAgICBpY29uOiAnaW5mbycsXG4gICAgICAgIGNvbG9yOiAnYmx1ZS1ncmV5LWRhcmsnXG4gICAgfTtcblxuICAgIHByaXZhdGUgYmFja3VwVmFsdWUgPSAnJztcbiAgICBwcml2YXRlIGN1cnJlbmN5VmFsdWU6IGFueTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgY29weVRvQ2xpcGJvYXJkSGVscGVyOiBDb3B5VG9DbGlwYm9hcmRIZWxwZXIpIHt9XG5cbiAgICBwdWJsaWMgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMudmlld01vZGVsID0gdGhpcy52aWV3TW9kZWwgPyB0aGlzLnZpZXdNb2RlbCA6IHt9O1xuICAgICAgICB0aGlzLnR5cGUgPSB0aGlzLnR5cGUgPyB0aGlzLnR5cGUgOiAndGV4dCc7XG4gICAgICAgIHRoaXMuaXNUeXBlQ3VycmVuY3kgPSB0aGlzLnR5cGUgPT09ICdjdXJyZW5jeSc7XG4gICAgICAgIHRoaXMuZWRpdGFibGUgPSB0aGlzLmVkaXRhYmxlICE9PSB1bmRlZmluZWQgPyB0aGlzLmVkaXRhYmxlIDogdHJ1ZTtcbiAgICAgICAgdGhpcy5kaXNhYmxlZCA9IHRoaXMuZGlzYWJsZWQgIT09IHVuZGVmaW5lZCA/IHRoaXMuZGlzYWJsZWQgOiBmYWxzZTtcbiAgICAgICAgdGhpcy52aWV3TW9kZWwuZWRpdFRleHQgPSB0aGlzLnZpZXdNb2RlbC5lZGl0VGV4dCA/IHRoaXMudmlld01vZGVsLmVkaXRUZXh0IDogVUlfQ09QWV9JTlBVVF9DT05TVEFOVC5FRElUX1RFWFQ7XG4gICAgICAgIHRoaXMubGFiZWwgPSB0aGlzLmxhYmVsICYmIHR5cGVvZiB0aGlzLmxhYmVsID09PSAnc3RyaW5nJyA/IHsgdmFsdWU6IHRoaXMubGFiZWwgfSA6IHRoaXMubGFiZWw7XG4gICAgICAgIHRoaXMudmlld01vZGVsLnRvb2x0aXBUZXh0QmVmb3JlQ29weSA9IHRoaXMudmlld01vZGVsLnRvb2x0aXBUZXh0QmVmb3JlQ29weVxuICAgICAgICAgICAgPyB0aGlzLnZpZXdNb2RlbC50b29sdGlwVGV4dEJlZm9yZUNvcHlcbiAgICAgICAgICAgIDogVUlfQ09QWV9JTlBVVF9DT05TVEFOVC5UT09MVElQX1RFWFRfQkVGT1JFX0NPUFk7XG4gICAgICAgIHRoaXMudmlld01vZGVsLnRvb2x0aXBUZXh0QWZ0ZXJDb3B5ID0gdGhpcy52aWV3TW9kZWwudG9vbHRpcFRleHRBZnRlckNvcHlcbiAgICAgICAgICAgID8gdGhpcy52aWV3TW9kZWwudG9vbHRpcFRleHRBZnRlckNvcHlcbiAgICAgICAgICAgIDogVUlfQ09QWV9JTlBVVF9DT05TVEFOVC5UT09MVElQX1RFWFRfQUZURVJfQ09QWTtcbiAgICAgICAgaWYgKHRoaXMudHlwZSA9PT0gJ2N1cnJlbmN5Jykge1xuICAgICAgICAgICAgdGhpcy5jdXJyZW5jeVZhbHVlID0ge1xuICAgICAgICAgICAgICAgIGN1cnJlbmN5OiB0aGlzLmN1cnJlbmN5LFxuICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLnZhbHVlXG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIG9uSW5wdXRDaGFuZ2UoY3VycmVuY3lWYWx1ZT8pOiB2b2lkIHtcbiAgICAgICAgdGhpcy5jdXJyZW5jeVZhbHVlID0gY3VycmVuY3lWYWx1ZTtcbiAgICAgICAgY29uc3QgdmFsdWUgPSB0aGlzLnR5cGUgPT09ICdjdXJyZW5jeScgPyB0aGlzLmN1cnJlbmN5VmFsdWUgOiB0aGlzLnZhbHVlO1xuICAgICAgICB0aGlzLm9uQ2hhbmdlLmVtaXQodmFsdWUpO1xuICAgIH1cblxuICAgIHB1YmxpYyBjb3B5KGV2ZW50KSB7XG4gICAgICAgIGlmIChldmVudCAmJiAhdGhpcy5pc0VkaXRpbmcpIHtcbiAgICAgICAgICAgIGV2ZW50LnRhcmdldC5ibHVyKCk7XG4gICAgICAgICAgICBjb25zdCB2YWx1ZSA9XG4gICAgICAgICAgICAgICAgdGhpcy50eXBlID09PSAnY3VycmVuY3knID8gYCR7dGhpcy5jdXJyZW5jeVZhbHVlLnZhbHVlfSAke3RoaXMuY3VycmVuY3lWYWx1ZS5jdXJyZW5jeX1gIDogdGhpcy52YWx1ZTtcbiAgICAgICAgICAgIHRoaXMuY29weVRvQ2xpcGJvYXJkSGVscGVyLmNvcHlUb0NsaXBib2FyZCh2YWx1ZSk7XG4gICAgICAgICAgICB0aGlzLmlzQ29waWVkID0gdHJ1ZTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBlZGl0KCkge1xuICAgICAgICB0aGlzLmlzRWRpdGluZyA9IHRydWU7XG4gICAgICAgIHRoaXMuYmFja3VwVmFsdWUgPSB0aGlzLnZhbHVlO1xuICAgIH1cblxuICAgIHB1YmxpYyBkaXNjYXJkKCkge1xuICAgICAgICB0aGlzLmlzRWRpdGluZyA9IGZhbHNlO1xuICAgICAgICB0aGlzLnZhbHVlID0gdGhpcy5iYWNrdXBWYWx1ZTtcbiAgICAgICAgdGhpcy5iYWNrdXBWYWx1ZSA9ICcnO1xuICAgIH1cblxuICAgIHB1YmxpYyBzYXZlKCkge1xuICAgICAgICBjb25zdCB2YWx1ZSA9IHRoaXMudHlwZSA9PT0gJ2N1cnJlbmN5JyA/IHRoaXMuY3VycmVuY3lWYWx1ZSA6IHRoaXMudmFsdWU7XG4gICAgICAgIHRoaXMub25TdWJtaXQuZW1pdCh2YWx1ZSk7XG4gICAgICAgIHRoaXMuaXNFZGl0aW5nID0gZmFsc2U7XG4gICAgICAgIHRoaXMuYmFja3VwVmFsdWUgPSAnJztcbiAgICB9XG5cbiAgICBwdWJsaWMgc2hvd1Rvb2x0aXAoZXZlbnQ/KSB7XG4gICAgICAgIGlmIChldmVudCkge1xuICAgICAgICAgICAgdGhpcy5maWVsZC5sZWZ0ID0gYCR7ZXZlbnQucGFnZVh9cHhgO1xuICAgICAgICAgICAgdGhpcy5maWVsZC50b3AgPSBgJHtldmVudC5wYWdlWX1weGA7XG4gICAgICAgICAgICB0aGlzLmlzVG9vbHRpcEFjdGl2ZSA9IHRydWU7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmlzVG9vbHRpcEFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy5pc0NvcGllZCA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgfVxufVxuIl19