import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter, ElementRef, HostListener } from '@angular/core';
import { MaterialHelper } from '../../helpers/material.helpers';
import * as moment_ from 'moment-mini-ts';
const moment = moment_;
const DEFAULT_CALENDAR_WIDTH = 327;
const COMPONENT_CONFIG = {
    WEEKEND_ISO_CODES: [7, 6]
};
let UiDateInputComponent = class UiDateInputComponent {
    constructor(helper, element) {
        this.helper = helper;
        this.element = element;
        this.placeholder = '';
        this.disablePastDates = false;
        this.disableWeekendDays = false;
        this.dateFormat = '';
        this.datesWithBullets = [];
        this.datesDisabled = [];
        this.isInvalidDate = false;
        this.onDateSelected = new EventEmitter();
        this.daysOfMonth = [];
        this.selectedDate = '';
        this.calendarIsVisible = false;
        this.alignRight = false;
        this.alignLeft = false;
    }
    hideCalendar() {
        this.calendarIsVisible = false;
    }
    onDomClicked(event) {
        if (!this.element.nativeElement.contains(event.target) && this.calendarIsVisible) {
            this.hideCalendar();
        }
    }
    formatDate(clickedDay) {
        const selectedDate = new Date(this.year, this.monthIndexPosition, clickedDay);
        return moment(selectedDate).format(this.dateFormat);
    }
    emitChanges() {
        this.onDateSelected.emit(this.selectedDate);
    }
    getDateWithBulletsIndexOfByDayNumber(dayNumber) {
        const dateToBeCompared = this.formatDate(dayNumber);
        return this.datesWithBullets.findIndex((dateConfig) => {
            return dateToBeCompared === moment(dateConfig.date).format(this.dateFormat);
        });
    }
    buildCalendarDays() {
        // because date = 0, you should pass the month instead month index
        const numberOfDays = new Date(this.year, this.month, 0).getDate();
        const weekDayStartingMonth = new Date(this.year + '-' + this.month + '-01').getDay();
        let daysToShow = [];
        this.daysOfMonth = [];
        for (let w = 1; w <= weekDayStartingMonth; w++) {
            daysToShow.push('');
        }
        for (let i = 1; i <= numberOfDays; i++) {
            if (daysToShow.length < 7) {
                daysToShow.push(i);
            }
            else {
                this.daysOfMonth.push(daysToShow);
                daysToShow = [i];
            }
            if (i === numberOfDays) {
                this.daysOfMonth.push(daysToShow);
            }
        }
    }
    checkIfDateIsValid() {
        this.isInvalidDate = false;
        if (!moment(this.selectedDate, this.dateFormat, true).isValid()) {
            this.isInvalidDate = true;
            return false;
        }
        return true;
    }
    calculateCurrentDate() {
        let currentDate = new Date();
        if (this.selectedDate !== '') {
            currentDate = moment(this.selectedDate, this.dateFormat, true);
        }
        this.year = currentDate.getFullYear ? currentDate.getFullYear() : currentDate.year();
        this.month = currentDate.getMonth ? currentDate.getMonth() + 1 : currentDate.month() + 1;
        this.monthIndexPosition = currentDate.getMonth ? currentDate.getMonth() : currentDate.month();
    }
    ngOnInit() {
        this.dateFormat = this.dateFormat.toUpperCase();
        this.selectedDate = this.value ? moment(this.value).format(this.dateFormat) : '';
        this.calculateCurrentDate();
        this.buildCalendarDays();
    }
    subtractMonth() {
        this.month = this.month - 1;
        this.monthIndexPosition = this.monthIndexPosition - 1;
        if (this.month <= 0) {
            this.year = this.year - 1;
            this.month = 12;
            this.monthIndexPosition = 11;
        }
        this.buildCalendarDays();
    }
    addMonth() {
        this.month = this.month + 1;
        this.monthIndexPosition = this.monthIndexPosition + 1;
        if (this.month >= 13) {
            this.year = this.year + 1;
            this.month = 1;
            this.monthIndexPosition = 0;
        }
        this.buildCalendarDays();
    }
    onInputChanged() {
        if (!this.checkIfDateIsValid()) {
            return;
        }
        this.calculateCurrentDate();
        this.buildCalendarDays();
        this.emitChanges();
    }
    onSelectedDate(clickedDay) {
        this.selectedDate = this.formatDate(clickedDay);
        this.checkIfDateIsValid();
        this.calendarToggleVisibility();
        this.emitChanges();
    }
    isSelectedDate(dayNumber) {
        if (this.selectedDate === '') {
            return false;
        }
        return this.selectedDate === this.formatDate(dayNumber);
    }
    dateHasBullet(dayNumber) {
        return this.getDateWithBulletsIndexOfByDayNumber(dayNumber) > -1;
    }
    getBulletTooltip(dayNumber) {
        const indexFound = this.getDateWithBulletsIndexOfByDayNumber(dayNumber);
        return indexFound > -1 ? this.datesWithBullets[indexFound].tooltip : '';
    }
    isDateDisabled(dayNumber) {
        const dateToBeCompared = this.formatDate(dayNumber);
        const momentDateToBeCompared = new Date(this.year, this.monthIndexPosition, dayNumber);
        if (this.disableWeekendDays &&
            COMPONENT_CONFIG.WEEKEND_ISO_CODES.indexOf(moment(momentDateToBeCompared).isoWeekday()) > -1) {
            return true;
        }
        if (this.disablePastDates && moment(momentDateToBeCompared).isBefore(new Date())) {
            return true;
        }
        return (this.datesDisabled.findIndex((date) => {
            return dateToBeCompared === moment(date).format(this.dateFormat);
        }) > -1);
    }
    calendarToggleVisibility() {
        const calendarWith = this.element.nativeElement.firstElementChild.clientWidth < DEFAULT_CALENDAR_WIDTH
            ? DEFAULT_CALENDAR_WIDTH
            : this.element.nativeElement.firstElementChild.clientWidth;
        const absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, calendarWith);
        this.calculateCurrentDate();
        this.buildCalendarDays();
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
        this.calendarIsVisible = !this.calendarIsVisible;
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Boolean)
], UiDateInputComponent.prototype, "disable", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiDateInputComponent.prototype, "placeholder", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiDateInputComponent.prototype, "monthNames", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiDateInputComponent.prototype, "weekDays", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], UiDateInputComponent.prototype, "bottomLabel", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiDateInputComponent.prototype, "disablePastDates", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiDateInputComponent.prototype, "disableWeekendDays", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiDateInputComponent.prototype, "dateFormat", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], UiDateInputComponent.prototype, "value", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiDateInputComponent.prototype, "datesWithBullets", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiDateInputComponent.prototype, "datesDisabled", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiDateInputComponent.prototype, "isInvalidDate", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], UiDateInputComponent.prototype, "onDateSelected", void 0);
tslib_1.__decorate([
    HostListener('document:click', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], UiDateInputComponent.prototype, "onDomClicked", null);
UiDateInputComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-date-input',
        template: "<div class=\"ui-date-picker\"\n     [ngClass]=\"{\n         'ui-date-picker__calendar-visible': calendarIsVisible\n     }\">\n    <input class=\"ui-date-picker__input\"\n           type=\"text\"\n           placeholder=\"{{placeholder}}\"\n           [disabled]=\"disable\"\n           [(ngModel)]=\"selectedDate\"\n           (keyup)=\"onInputChanged()\"\n           [ngClass]=\"{\n                'form-field__input--error': isInvalidDate\n            }\">\n    <button class=\"ui-date-picker__button\"\n            (click)=\"calendarToggleVisibility()\"\n            [disabled]=\"disable\">\n        <i class=\"huub-material-icon\" icon=\"calendar\"></i>\n    </button>\n\n    <div class=\"ui-date-picker__hideborders\"\n         *ngIf=\"calendarIsVisible\"\n         (click)=\"calendarToggleVisibility()\">\n    </div>\n\n    <div class=\"ui-date-picker__calendar\"\n        *ngIf=\"calendarIsVisible\"\n        [ngClass]=\"{\n            'ui-date-picker__calendar--right': alignRight,\n            'ui-date-picker__calendar--left': alignLeft\n        }\">\n\n        <div class=\"ui-date-picker__month-year\">\n            <button (click)=\"subtractMonth()\">\n                <i class=\"huub-material-icon ui-date-picker__backward\" icon=\"chevron-left\" size=\"extra-small\"></i>\n            </button>\n            <div class=\"ui-date-picker__actual-month-year\">\n                {{monthNames[month - 1]}} {{year}}\n            </div>\n            <button (click)=\"addMonth()\">\n                <i class=\"huub-material-icon ui-date-picker__forward\" icon=\"chevron-right\" size=\"extra-small\"></i>\n            </button>\n        </div>\n\n        <div class=\"ui-date-picker__days-list-container\">\n            <table class=\"ui-date-picker__days-list\">\n                <thead class=\"ui-date-picker__week-days\">\n                    <tr>\n                        <td *ngFor=\"let weekday of weekDays\">\n                            {{weekday}}\n                        </td>\n                    </tr>\n                </thead>\n                <tbody class=\"ui-date-picker__days\">\n                    <tr class=\"ui-date-picker__days-line\"\n                        *ngFor=\"let dayOfMonthSet of daysOfMonth; let i = index\">\n                       <td *ngFor=\"let dayNumber of daysOfMonth[i]\">\n                            <button class=\"ui-date-picker__day\"\n                                 *ngIf=\"dayNumber !== ''\"\n                                 [disabled]=\"isDateDisabled(dayNumber)\"\n                                 [ngClass]=\"{\n                                    'ui-date-picker__day--selected': isSelectedDate(dayNumber)\n                                  }\"\n                                 (click)=\"onSelectedDate(dayNumber)\">\n                             {{dayNumber}}\n                                <ng-container *ngIf=\"dateHasBullet(dayNumber)\">\n                                    <i class=\"ui-date-picker__day-bullet\"></i>\n\n                                    <span class=\"ui-date-picker__day-bullet__tooltip\"\n                                        *ngIf=\"getBulletTooltip(dayNumber) != ''\">\n                                        {{getBulletTooltip(dayNumber)}}</span>\n                                </ng-container>\n                            </button>\n                       </td>\n                    </tr>\n                    <tr *ngIf=\"bottomLabel !== ''\">\n                        <td [attr.colspan]=\"weekDays.length\" class=\"ui-date-picker__bottom-message\">\n                            {{bottomLabel}}\n                        </td>\n                    </tr>\n                </tbody>\n            </table>\n        </div>\n    </div>\n</div>\n"
    }),
    tslib_1.__metadata("design:paramtypes", [MaterialHelper, ElementRef])
], UiDateInputComponent);
export { UiDateInputComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktZGF0ZS1pbnB1dC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZm9ybXMvZGF0ZS1pbnB1dC91aS1kYXRlLWlucHV0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQVUsTUFBTSxFQUFFLFlBQVksRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pHLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUVoRSxPQUFPLEtBQUssT0FBTyxNQUFNLGdCQUFnQixDQUFDO0FBRTFDLE1BQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQztBQUV2QixNQUFNLHNCQUFzQixHQUFHLEdBQUcsQ0FBQztBQUNuQyxNQUFNLGdCQUFnQixHQUFHO0lBQ3JCLGlCQUFpQixFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztDQUM1QixDQUFDO0FBTUYsSUFBYSxvQkFBb0IsR0FBakMsTUFBYSxvQkFBb0I7SUEwQjdCLFlBQW9CLE1BQXNCLEVBQVUsT0FBbUI7UUFBbkQsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFBVSxZQUFPLEdBQVAsT0FBTyxDQUFZO1FBeEJ2RCxnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUlqQixxQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDekIsdUJBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQzFCLGVBQVUsR0FBRyxFQUFFLENBQUM7UUFFaEIscUJBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLGtCQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLGtCQUFhLEdBQUcsS0FBSyxDQUFDO1FBRXJCLG1CQUFjLEdBQXlCLElBQUksWUFBWSxFQUFFLENBQUM7UUFLcEUsZ0JBQVcsR0FBUSxFQUFFLENBQUM7UUFDdEIsaUJBQVksR0FBRyxFQUFFLENBQUM7UUFDbEIsc0JBQWlCLEdBQUcsS0FBSyxDQUFDO1FBRTFCLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsY0FBUyxHQUFHLEtBQUssQ0FBQztJQUVpRCxDQUFDO0lBRW5FLFlBQVk7UUFDaEIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztJQUNuQyxDQUFDO0lBR00sWUFBWSxDQUFDLEtBQUs7UUFDckIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQzlFLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztTQUN2QjtJQUNMLENBQUM7SUFFTyxVQUFVLENBQUMsVUFBVTtRQUN6QixNQUFNLFlBQVksR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUM5RSxPQUFPLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFTyxXQUFXO1FBQ2YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFTyxvQ0FBb0MsQ0FBQyxTQUFTO1FBQ2xELE1BQU0sZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUVwRCxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxVQUFVLEVBQUUsRUFBRTtZQUNsRCxPQUFPLGdCQUFnQixLQUFLLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNoRixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTyxpQkFBaUI7UUFDckIsa0VBQWtFO1FBQ2xFLE1BQU0sWUFBWSxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUVsRSxNQUFNLG9CQUFvQixHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFFckYsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBRXBCLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBRXRCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxvQkFBb0IsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ3ZCO1FBRUQsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLFlBQVksRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNwQyxJQUFJLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUN2QixVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3RCO2lCQUFNO2dCQUNILElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNsQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNwQjtZQUVELElBQUksQ0FBQyxLQUFLLFlBQVksRUFBRTtnQkFDcEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDckM7U0FDSjtJQUNMLENBQUM7SUFFTyxrQkFBa0I7UUFDdEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFFM0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7WUFDN0QsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7WUFDMUIsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRU8sb0JBQW9CO1FBQ3hCLElBQUksV0FBVyxHQUFlLElBQUksSUFBSSxFQUFFLENBQUM7UUFFekMsSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLEVBQUUsRUFBRTtZQUMxQixXQUFXLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUNsRTtRQUVELElBQUksQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDckYsSUFBSSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3pGLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNsRyxDQUFDO0lBRU0sUUFBUTtRQUNYLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNoRCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBRWpGLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFTSxhQUFhO1FBQ2hCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDNUIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLENBQUM7UUFFdEQsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsRUFBRTtZQUNqQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO1lBQzFCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1lBQ2hCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxFQUFFLENBQUM7U0FDaEM7UUFFRCxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRU0sUUFBUTtRQUNYLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDNUIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLENBQUM7UUFFdEQsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsRUFBRTtZQUNsQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO1lBQzFCLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1lBQ2YsSUFBSSxDQUFDLGtCQUFrQixHQUFHLENBQUMsQ0FBQztTQUMvQjtRQUVELElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFTSxjQUFjO1FBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsRUFBRTtZQUM1QixPQUFPO1NBQ1Y7UUFFRCxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUM1QixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVNLGNBQWMsQ0FBQyxVQUFVO1FBQzVCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUVoRCxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztRQUNoQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVNLGNBQWMsQ0FBQyxTQUFTO1FBQzNCLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxFQUFFLEVBQUU7WUFDMUIsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxPQUFPLElBQUksQ0FBQyxZQUFZLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUM1RCxDQUFDO0lBRU0sYUFBYSxDQUFDLFNBQVM7UUFDMUIsT0FBTyxJQUFJLENBQUMsb0NBQW9DLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDckUsQ0FBQztJQUVNLGdCQUFnQixDQUFDLFNBQVM7UUFDN0IsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLG9DQUFvQyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRXhFLE9BQU8sVUFBVSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDNUUsQ0FBQztJQUVNLGNBQWMsQ0FBQyxTQUFTO1FBQzNCLE1BQU0sZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNwRCxNQUFNLHNCQUFzQixHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBRXZGLElBQ0ksSUFBSSxDQUFDLGtCQUFrQjtZQUN2QixnQkFBZ0IsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLHNCQUFzQixDQUFDLENBQUMsVUFBVSxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsRUFDOUY7WUFDRSxPQUFPLElBQUksQ0FBQztTQUNmO1FBRUQsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLElBQUksTUFBTSxDQUFDLHNCQUFzQixDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsRUFBRTtZQUM5RSxPQUFPLElBQUksQ0FBQztTQUNmO1FBRUQsT0FBTyxDQUNILElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7WUFDbEMsT0FBTyxnQkFBZ0IsS0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNyRSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FDVixDQUFDO0lBQ04sQ0FBQztJQUVNLHdCQUF3QjtRQUMzQixNQUFNLFlBQVksR0FDZCxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEdBQUcsc0JBQXNCO1lBQzdFLENBQUMsQ0FBQyxzQkFBc0I7WUFDeEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQztRQUVuRSxNQUFNLG9CQUFvQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsK0JBQStCLENBQ3BFLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLHFCQUFxQixFQUFFLENBQUMsSUFBSSxFQUN2RCxZQUFZLENBQ2YsQ0FBQztRQUVGLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBRXpCLElBQUksQ0FBQyxTQUFTLEdBQUcsb0JBQW9CLENBQUMsZUFBZSxDQUFDO1FBQ3RELElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQW9CLENBQUMsZ0JBQWdCLENBQUM7UUFFeEQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDO0lBQ3JELENBQUM7Q0FDSixDQUFBO0FBek5ZO0lBQVIsS0FBSyxFQUFFOztxREFBeUI7QUFDeEI7SUFBUixLQUFLLEVBQUU7O3lEQUF5QjtBQUN4QjtJQUFSLEtBQUssRUFBRTs7d0RBQXdCO0FBQ3ZCO0lBQVIsS0FBSyxFQUFFOztzREFBc0I7QUFDckI7SUFBUixLQUFLLEVBQUU7O3lEQUE2QjtBQUM1QjtJQUFSLEtBQUssRUFBRTs7OERBQWlDO0FBQ2hDO0lBQVIsS0FBSyxFQUFFOztnRUFBbUM7QUFDbEM7SUFBUixLQUFLLEVBQUU7O3dEQUF5QjtBQUN4QjtJQUFSLEtBQUssRUFBRTs7bURBQXVCO0FBQ3RCO0lBQVIsS0FBSyxFQUFFOzs4REFBK0I7QUFDOUI7SUFBUixLQUFLLEVBQUU7OzJEQUE0QjtBQUMzQjtJQUFSLEtBQUssRUFBRTs7MkRBQThCO0FBRTVCO0lBQVQsTUFBTSxFQUFFO3NDQUF3QixZQUFZOzREQUE4QjtBQW1CM0U7SUFEQyxZQUFZLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQzs7Ozt3REFLMUM7QUFyQ1Esb0JBQW9CO0lBSmhDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxlQUFlO1FBQ3pCLHlxSEFBNkM7S0FDaEQsQ0FBQzs2Q0EyQjhCLGNBQWMsRUFBbUIsVUFBVTtHQTFCOUQsb0JBQW9CLENBME5oQztTQTFOWSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBFbGVtZW50UmVmLCBIb3N0TGlzdGVuZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1hdGVyaWFsSGVscGVyIH0gZnJvbSAnLi4vLi4vaGVscGVycy9tYXRlcmlhbC5oZWxwZXJzJztcblxuaW1wb3J0ICogYXMgbW9tZW50XyBmcm9tICdtb21lbnQtbWluaS10cyc7XG5cbmNvbnN0IG1vbWVudCA9IG1vbWVudF87XG5cbmNvbnN0IERFRkFVTFRfQ0FMRU5EQVJfV0lEVEggPSAzMjc7XG5jb25zdCBDT01QT05FTlRfQ09ORklHID0ge1xuICAgIFdFRUtFTkRfSVNPX0NPREVTOiBbNywgNl1cbn07XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAndWktZGF0ZS1pbnB1dCcsXG4gICAgdGVtcGxhdGVVcmw6ICcuL3VpLWRhdGUtaW5wdXQuY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFVpRGF0ZUlucHV0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICBASW5wdXQoKSBwdWJsaWMgZGlzYWJsZTogYm9vbGVhbjtcbiAgICBASW5wdXQoKSBwdWJsaWMgcGxhY2Vob2xkZXIgPSAnJztcbiAgICBASW5wdXQoKSBwdWJsaWMgbW9udGhOYW1lczogYW55O1xuICAgIEBJbnB1dCgpIHB1YmxpYyB3ZWVrRGF5czogYW55O1xuICAgIEBJbnB1dCgpIHB1YmxpYyBib3R0b21MYWJlbD86IHN0cmluZztcbiAgICBASW5wdXQoKSBwdWJsaWMgZGlzYWJsZVBhc3REYXRlcyA9IGZhbHNlO1xuICAgIEBJbnB1dCgpIHB1YmxpYyBkaXNhYmxlV2Vla2VuZERheXMgPSBmYWxzZTtcbiAgICBASW5wdXQoKSBwcml2YXRlIGRhdGVGb3JtYXQgPSAnJztcbiAgICBASW5wdXQoKSBwcml2YXRlIHZhbHVlOiBzdHJpbmc7XG4gICAgQElucHV0KCkgcHJpdmF0ZSBkYXRlc1dpdGhCdWxsZXRzID0gW107XG4gICAgQElucHV0KCkgcHJpdmF0ZSBkYXRlc0Rpc2FibGVkID0gW107XG4gICAgQElucHV0KCkgcHVibGljIGlzSW52YWxpZERhdGUgPSBmYWxzZTtcblxuICAgIEBPdXRwdXQoKSBwdWJsaWMgb25EYXRlU2VsZWN0ZWQ6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgcHJpdmF0ZSBtb250aEluZGV4UG9zaXRpb246IG51bWJlcjtcbiAgICBwdWJsaWMgeWVhcjogbnVtYmVyO1xuICAgIHB1YmxpYyBtb250aDogbnVtYmVyO1xuICAgIHB1YmxpYyBkYXlzT2ZNb250aDogYW55ID0gW107XG4gICAgcHVibGljIHNlbGVjdGVkRGF0ZSA9ICcnO1xuICAgIHB1YmxpYyBjYWxlbmRhcklzVmlzaWJsZSA9IGZhbHNlO1xuXG4gICAgcHVibGljIGFsaWduUmlnaHQgPSBmYWxzZTtcbiAgICBwdWJsaWMgYWxpZ25MZWZ0ID0gZmFsc2U7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGhlbHBlcjogTWF0ZXJpYWxIZWxwZXIsIHByaXZhdGUgZWxlbWVudDogRWxlbWVudFJlZikge31cblxuICAgIHByaXZhdGUgaGlkZUNhbGVuZGFyKCkge1xuICAgICAgICB0aGlzLmNhbGVuZGFySXNWaXNpYmxlID0gZmFsc2U7XG4gICAgfVxuXG4gICAgQEhvc3RMaXN0ZW5lcignZG9jdW1lbnQ6Y2xpY2snLCBbJyRldmVudCddKVxuICAgIHB1YmxpYyBvbkRvbUNsaWNrZWQoZXZlbnQpIHtcbiAgICAgICAgaWYgKCF0aGlzLmVsZW1lbnQubmF0aXZlRWxlbWVudC5jb250YWlucyhldmVudC50YXJnZXQpICYmIHRoaXMuY2FsZW5kYXJJc1Zpc2libGUpIHtcbiAgICAgICAgICAgIHRoaXMuaGlkZUNhbGVuZGFyKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIGZvcm1hdERhdGUoY2xpY2tlZERheSkge1xuICAgICAgICBjb25zdCBzZWxlY3RlZERhdGUgPSBuZXcgRGF0ZSh0aGlzLnllYXIsIHRoaXMubW9udGhJbmRleFBvc2l0aW9uLCBjbGlja2VkRGF5KTtcbiAgICAgICAgcmV0dXJuIG1vbWVudChzZWxlY3RlZERhdGUpLmZvcm1hdCh0aGlzLmRhdGVGb3JtYXQpO1xuICAgIH1cblxuICAgIHByaXZhdGUgZW1pdENoYW5nZXMoKSB7XG4gICAgICAgIHRoaXMub25EYXRlU2VsZWN0ZWQuZW1pdCh0aGlzLnNlbGVjdGVkRGF0ZSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXREYXRlV2l0aEJ1bGxldHNJbmRleE9mQnlEYXlOdW1iZXIoZGF5TnVtYmVyKSB7XG4gICAgICAgIGNvbnN0IGRhdGVUb0JlQ29tcGFyZWQgPSB0aGlzLmZvcm1hdERhdGUoZGF5TnVtYmVyKTtcblxuICAgICAgICByZXR1cm4gdGhpcy5kYXRlc1dpdGhCdWxsZXRzLmZpbmRJbmRleCgoZGF0ZUNvbmZpZykgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIGRhdGVUb0JlQ29tcGFyZWQgPT09IG1vbWVudChkYXRlQ29uZmlnLmRhdGUpLmZvcm1hdCh0aGlzLmRhdGVGb3JtYXQpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGJ1aWxkQ2FsZW5kYXJEYXlzKCkge1xuICAgICAgICAvLyBiZWNhdXNlIGRhdGUgPSAwLCB5b3Ugc2hvdWxkIHBhc3MgdGhlIG1vbnRoIGluc3RlYWQgbW9udGggaW5kZXhcbiAgICAgICAgY29uc3QgbnVtYmVyT2ZEYXlzID0gbmV3IERhdGUodGhpcy55ZWFyLCB0aGlzLm1vbnRoLCAwKS5nZXREYXRlKCk7XG5cbiAgICAgICAgY29uc3Qgd2Vla0RheVN0YXJ0aW5nTW9udGggPSBuZXcgRGF0ZSh0aGlzLnllYXIgKyAnLScgKyB0aGlzLm1vbnRoICsgJy0wMScpLmdldERheSgpO1xuXG4gICAgICAgIGxldCBkYXlzVG9TaG93ID0gW107XG5cbiAgICAgICAgdGhpcy5kYXlzT2ZNb250aCA9IFtdO1xuXG4gICAgICAgIGZvciAobGV0IHcgPSAxOyB3IDw9IHdlZWtEYXlTdGFydGluZ01vbnRoOyB3KyspIHtcbiAgICAgICAgICAgIGRheXNUb1Nob3cucHVzaCgnJyk7XG4gICAgICAgIH1cblxuICAgICAgICBmb3IgKGxldCBpID0gMTsgaSA8PSBudW1iZXJPZkRheXM7IGkrKykge1xuICAgICAgICAgICAgaWYgKGRheXNUb1Nob3cubGVuZ3RoIDwgNykge1xuICAgICAgICAgICAgICAgIGRheXNUb1Nob3cucHVzaChpKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5kYXlzT2ZNb250aC5wdXNoKGRheXNUb1Nob3cpO1xuICAgICAgICAgICAgICAgIGRheXNUb1Nob3cgPSBbaV07XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChpID09PSBudW1iZXJPZkRheXMpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmRheXNPZk1vbnRoLnB1c2goZGF5c1RvU2hvdyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIGNoZWNrSWZEYXRlSXNWYWxpZCgpIHtcbiAgICAgICAgdGhpcy5pc0ludmFsaWREYXRlID0gZmFsc2U7XG5cbiAgICAgICAgaWYgKCFtb21lbnQodGhpcy5zZWxlY3RlZERhdGUsIHRoaXMuZGF0ZUZvcm1hdCwgdHJ1ZSkuaXNWYWxpZCgpKSB7XG4gICAgICAgICAgICB0aGlzLmlzSW52YWxpZERhdGUgPSB0cnVlO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBjYWxjdWxhdGVDdXJyZW50RGF0ZSgpIHtcbiAgICAgICAgbGV0IGN1cnJlbnREYXRlOiBEYXRlIHwgYW55ID0gbmV3IERhdGUoKTtcblxuICAgICAgICBpZiAodGhpcy5zZWxlY3RlZERhdGUgIT09ICcnKSB7XG4gICAgICAgICAgICBjdXJyZW50RGF0ZSA9IG1vbWVudCh0aGlzLnNlbGVjdGVkRGF0ZSwgdGhpcy5kYXRlRm9ybWF0LCB0cnVlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMueWVhciA9IGN1cnJlbnREYXRlLmdldEZ1bGxZZWFyID8gY3VycmVudERhdGUuZ2V0RnVsbFllYXIoKSA6IGN1cnJlbnREYXRlLnllYXIoKTtcbiAgICAgICAgdGhpcy5tb250aCA9IGN1cnJlbnREYXRlLmdldE1vbnRoID8gY3VycmVudERhdGUuZ2V0TW9udGgoKSArIDEgOiBjdXJyZW50RGF0ZS5tb250aCgpICsgMTtcbiAgICAgICAgdGhpcy5tb250aEluZGV4UG9zaXRpb24gPSBjdXJyZW50RGF0ZS5nZXRNb250aCA/IGN1cnJlbnREYXRlLmdldE1vbnRoKCkgOiBjdXJyZW50RGF0ZS5tb250aCgpO1xuICAgIH1cblxuICAgIHB1YmxpYyBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy5kYXRlRm9ybWF0ID0gdGhpcy5kYXRlRm9ybWF0LnRvVXBwZXJDYXNlKCk7XG4gICAgICAgIHRoaXMuc2VsZWN0ZWREYXRlID0gdGhpcy52YWx1ZSA/IG1vbWVudCh0aGlzLnZhbHVlKS5mb3JtYXQodGhpcy5kYXRlRm9ybWF0KSA6ICcnO1xuXG4gICAgICAgIHRoaXMuY2FsY3VsYXRlQ3VycmVudERhdGUoKTtcbiAgICAgICAgdGhpcy5idWlsZENhbGVuZGFyRGF5cygpO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdWJ0cmFjdE1vbnRoKCkge1xuICAgICAgICB0aGlzLm1vbnRoID0gdGhpcy5tb250aCAtIDE7XG4gICAgICAgIHRoaXMubW9udGhJbmRleFBvc2l0aW9uID0gdGhpcy5tb250aEluZGV4UG9zaXRpb24gLSAxO1xuXG4gICAgICAgIGlmICh0aGlzLm1vbnRoIDw9IDApIHtcbiAgICAgICAgICAgIHRoaXMueWVhciA9IHRoaXMueWVhciAtIDE7XG4gICAgICAgICAgICB0aGlzLm1vbnRoID0gMTI7XG4gICAgICAgICAgICB0aGlzLm1vbnRoSW5kZXhQb3NpdGlvbiA9IDExO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5idWlsZENhbGVuZGFyRGF5cygpO1xuICAgIH1cblxuICAgIHB1YmxpYyBhZGRNb250aCgpIHtcbiAgICAgICAgdGhpcy5tb250aCA9IHRoaXMubW9udGggKyAxO1xuICAgICAgICB0aGlzLm1vbnRoSW5kZXhQb3NpdGlvbiA9IHRoaXMubW9udGhJbmRleFBvc2l0aW9uICsgMTtcblxuICAgICAgICBpZiAodGhpcy5tb250aCA+PSAxMykge1xuICAgICAgICAgICAgdGhpcy55ZWFyID0gdGhpcy55ZWFyICsgMTtcbiAgICAgICAgICAgIHRoaXMubW9udGggPSAxO1xuICAgICAgICAgICAgdGhpcy5tb250aEluZGV4UG9zaXRpb24gPSAwO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5idWlsZENhbGVuZGFyRGF5cygpO1xuICAgIH1cblxuICAgIHB1YmxpYyBvbklucHV0Q2hhbmdlZCgpIHtcbiAgICAgICAgaWYgKCF0aGlzLmNoZWNrSWZEYXRlSXNWYWxpZCgpKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmNhbGN1bGF0ZUN1cnJlbnREYXRlKCk7XG4gICAgICAgIHRoaXMuYnVpbGRDYWxlbmRhckRheXMoKTtcbiAgICAgICAgdGhpcy5lbWl0Q2hhbmdlcygpO1xuICAgIH1cblxuICAgIHB1YmxpYyBvblNlbGVjdGVkRGF0ZShjbGlja2VkRGF5KSB7XG4gICAgICAgIHRoaXMuc2VsZWN0ZWREYXRlID0gdGhpcy5mb3JtYXREYXRlKGNsaWNrZWREYXkpO1xuXG4gICAgICAgIHRoaXMuY2hlY2tJZkRhdGVJc1ZhbGlkKCk7XG4gICAgICAgIHRoaXMuY2FsZW5kYXJUb2dnbGVWaXNpYmlsaXR5KCk7XG4gICAgICAgIHRoaXMuZW1pdENoYW5nZXMoKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgaXNTZWxlY3RlZERhdGUoZGF5TnVtYmVyKSB7XG4gICAgICAgIGlmICh0aGlzLnNlbGVjdGVkRGF0ZSA9PT0gJycpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0aGlzLnNlbGVjdGVkRGF0ZSA9PT0gdGhpcy5mb3JtYXREYXRlKGRheU51bWJlcik7XG4gICAgfVxuXG4gICAgcHVibGljIGRhdGVIYXNCdWxsZXQoZGF5TnVtYmVyKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLmdldERhdGVXaXRoQnVsbGV0c0luZGV4T2ZCeURheU51bWJlcihkYXlOdW1iZXIpID4gLTE7XG4gICAgfVxuXG4gICAgcHVibGljIGdldEJ1bGxldFRvb2x0aXAoZGF5TnVtYmVyKTogc3RyaW5nIHtcbiAgICAgICAgY29uc3QgaW5kZXhGb3VuZCA9IHRoaXMuZ2V0RGF0ZVdpdGhCdWxsZXRzSW5kZXhPZkJ5RGF5TnVtYmVyKGRheU51bWJlcik7XG5cbiAgICAgICAgcmV0dXJuIGluZGV4Rm91bmQgPiAtMSA/IHRoaXMuZGF0ZXNXaXRoQnVsbGV0c1tpbmRleEZvdW5kXS50b29sdGlwIDogJyc7XG4gICAgfVxuXG4gICAgcHVibGljIGlzRGF0ZURpc2FibGVkKGRheU51bWJlcik6IGJvb2xlYW4ge1xuICAgICAgICBjb25zdCBkYXRlVG9CZUNvbXBhcmVkID0gdGhpcy5mb3JtYXREYXRlKGRheU51bWJlcik7XG4gICAgICAgIGNvbnN0IG1vbWVudERhdGVUb0JlQ29tcGFyZWQgPSBuZXcgRGF0ZSh0aGlzLnllYXIsIHRoaXMubW9udGhJbmRleFBvc2l0aW9uLCBkYXlOdW1iZXIpO1xuXG4gICAgICAgIGlmIChcbiAgICAgICAgICAgIHRoaXMuZGlzYWJsZVdlZWtlbmREYXlzICYmXG4gICAgICAgICAgICBDT01QT05FTlRfQ09ORklHLldFRUtFTkRfSVNPX0NPREVTLmluZGV4T2YobW9tZW50KG1vbWVudERhdGVUb0JlQ29tcGFyZWQpLmlzb1dlZWtkYXkoKSkgPiAtMVxuICAgICAgICApIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuZGlzYWJsZVBhc3REYXRlcyAmJiBtb21lbnQobW9tZW50RGF0ZVRvQmVDb21wYXJlZCkuaXNCZWZvcmUobmV3IERhdGUoKSkpIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIHRoaXMuZGF0ZXNEaXNhYmxlZC5maW5kSW5kZXgoKGRhdGUpID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZGF0ZVRvQmVDb21wYXJlZCA9PT0gbW9tZW50KGRhdGUpLmZvcm1hdCh0aGlzLmRhdGVGb3JtYXQpO1xuICAgICAgICAgICAgfSkgPiAtMVxuICAgICAgICApO1xuICAgIH1cblxuICAgIHB1YmxpYyBjYWxlbmRhclRvZ2dsZVZpc2liaWxpdHkoKSB7XG4gICAgICAgIGNvbnN0IGNhbGVuZGFyV2l0aCA9XG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQubmF0aXZlRWxlbWVudC5maXJzdEVsZW1lbnRDaGlsZC5jbGllbnRXaWR0aCA8IERFRkFVTFRfQ0FMRU5EQVJfV0lEVEhcbiAgICAgICAgICAgICAgICA/IERFRkFVTFRfQ0FMRU5EQVJfV0lEVEhcbiAgICAgICAgICAgICAgICA6IHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50LmZpcnN0RWxlbWVudENoaWxkLmNsaWVudFdpZHRoO1xuXG4gICAgICAgIGNvbnN0IGFic29sdXRlUmVuZGVyQ29uZmlnID0gdGhpcy5oZWxwZXIuZ2V0QWJzb2x1dGVQb3NpdGlvblJlbmRlckNvbmZpZyhcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmxlZnQsXG4gICAgICAgICAgICBjYWxlbmRhcldpdGhcbiAgICAgICAgKTtcblxuICAgICAgICB0aGlzLmNhbGN1bGF0ZUN1cnJlbnREYXRlKCk7XG4gICAgICAgIHRoaXMuYnVpbGRDYWxlbmRhckRheXMoKTtcblxuICAgICAgICB0aGlzLmFsaWduTGVmdCA9IGFic29sdXRlUmVuZGVyQ29uZmlnLmNhblJlbmRlclRvTGVmdDtcbiAgICAgICAgdGhpcy5hbGlnblJpZ2h0ID0gYWJzb2x1dGVSZW5kZXJDb25maWcuY2FuUmVuZGVyVG9SaWdodDtcblxuICAgICAgICB0aGlzLmNhbGVuZGFySXNWaXNpYmxlID0gIXRoaXMuY2FsZW5kYXJJc1Zpc2libGU7XG4gICAgfVxufVxuIl19