import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CurrenciesListHelper } from 'huub-utils/lib';
import { UI_CURRENCIES_DROPDOWN_CONSTANTS } from './ui-currencies-dropdown.constant';
let UiCurrenciesDropdownComponent = class UiCurrenciesDropdownComponent {
    constructor(currenciesListHelper) {
        this.currenciesListHelper = currenciesListHelper;
        this.onChange = new EventEmitter();
        this.inputSearchViewModel = {
            placeholder: UI_CURRENCIES_DROPDOWN_CONSTANTS.SEARCH_CURRENCY,
            value: ''
        };
        this.isDropdownOpened = false;
        this.currencySymbol = '';
        this.popularCurrencyTitle = UI_CURRENCIES_DROPDOWN_CONSTANTS.POPULAR_CURRENCIES;
        this.allCurrencyTitle = UI_CURRENCIES_DROPDOWN_CONSTANTS.ALL_CURRENCIES;
        this.currenciesList = this.currenciesListHelper.getCurrenciesList();
        this.popularCurrenciesList = this.currenciesListHelper.getPopularCurrenciesList();
    }
    ngOnInit() {
        this.currencySymbol = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).symbol;
        this.disabled = this.disabled !== undefined ? this.disabled : false;
    }
    onInputChange() {
        this.onChange.emit({ currency: this.currency, value: this.value });
    }
    openCloseDropdown() {
        if (!this.disabled) {
            this.isDropdownOpened = !this.isDropdownOpened;
        }
    }
    changeCurrency(isoCode) {
        this.currency = isoCode;
        this.currencySymbol = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).symbol;
        this.value = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).name;
        this.onChange.emit({ currency: this.currency, value: this.value });
        this.openCloseDropdown();
        this.onClearSearchInput();
    }
    onClearSearchInput() {
        this.inputSearchViewModel.value = '';
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], UiCurrenciesDropdownComponent.prototype, "currency", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], UiCurrenciesDropdownComponent.prototype, "language", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], UiCurrenciesDropdownComponent.prototype, "value", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], UiCurrenciesDropdownComponent.prototype, "placeholder", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Boolean)
], UiCurrenciesDropdownComponent.prototype, "disabled", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], UiCurrenciesDropdownComponent.prototype, "onChange", void 0);
UiCurrenciesDropdownComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-currencies-dropdown',
        template: "<div class=\"ui-currency-input\">\n    <span class=\"currency-char\" (click)=\"openCloseDropdown()\">{{currencySymbol}}</span>\n    <input (click)=\"openCloseDropdown()\" type=\"text\" class=\"currency-char__input\" (input)=\"onInputChange()\" [(ngModel)]=\"value\"\n        [ngModelOptions]=\"{standalone: true}\" readonly>\n    <div *ngIf=\"isDropdownOpened\" class=\"currency-input__dropdown\" (mouseleave)=\"openCloseDropdown()\">\n        <div class=\"ui-currency-input__search--style\">\n            <ui-input-search (onClearIconClicked)=\"onClearSearchInput()\" [viewModel]=\"inputSearchViewModel\" #searchInput ngDefaultControl\n                [(ngModel)]=\"inputSearchViewModel.value\"></ui-input-search>\n        </div>\n        <br />\n        <div *ngIf=\"!inputSearchViewModel.value\">\n            <p class=\"ui-currency-input__dropdown--title\">{{popularCurrencyTitle}}</p>\n            <div *ngFor=\"let currency of popularCurrenciesList\" class=\"currency-input__dropdown-item\" (click)=\"changeCurrency(currency.iso3code)\">\n                <span class=\"currency-input__dropdown-char\">{{currency.symbol}}</span>\n                <span class=\"currency-input__dropdown-iso-code\">{{currency.iso3code}}</span>\n                <span class=\"currency-input__dropdown-name\">{{currency.name}}</span>\n            </div>\n            <br />\n            <hr>\n            <br />\n            <p class=\"ui-currency-input__dropdown--title\">{{allCurrencyTitle}}</p>\n        </div>\n        <div *ngFor=\"let currency of currenciesList | searchPipe:'name,symbol,iso3code':inputSearchViewModel.value\" class=\"currency-input__dropdown-item\" (click)=\"changeCurrency(currency.iso3code)\">\n            <span class=\"currency-input__dropdown-char\">{{currency.symbol}}</span>\n            <span class=\"currency-input__dropdown-iso-code\">{{currency.iso3code}}</span>\n            <span class=\"currency-input__dropdown-name\">{{currency.name}}</span>\n        </div>\n    </div>\n</div>\n"
    }),
    tslib_1.__metadata("design:paramtypes", [CurrenciesListHelper])
], UiCurrenciesDropdownComponent);
export { UiCurrenciesDropdownComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktY3VycmVuY2llcy1kcm9wZG93bi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZm9ybXMvY3VycmVuY3ktZHJvcGRvd24vdWktY3VycmVuY2llcy1kcm9wZG93bi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0UsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGdDQUFnQyxFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFNckYsSUFBYSw2QkFBNkIsR0FBMUMsTUFBYSw2QkFBNkI7SUEyQnRDLFlBQW9CLG9CQUEwQztRQUExQyx5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO1FBaEI3QyxhQUFRLEdBQXlCLElBQUksWUFBWSxFQUFFLENBQUM7UUFFOUQseUJBQW9CLEdBQUc7WUFDMUIsV0FBVyxFQUFFLGdDQUFnQyxDQUFDLGVBQWU7WUFDN0QsS0FBSyxFQUFFLEVBQUU7U0FDWixDQUFDO1FBRUsscUJBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBRXBCLHlCQUFvQixHQUFHLGdDQUFnQyxDQUFDLGtCQUFrQixDQUFDO1FBQzNFLHFCQUFnQixHQUFHLGdDQUFnQyxDQUFDLGNBQWMsQ0FBQztRQUVuRSxtQkFBYyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQy9ELDBCQUFxQixHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO0lBRW5CLENBQUM7SUFFM0QsUUFBUTtRQUNYLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLENBQUM7UUFDNUYsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ3hFLENBQUM7SUFFTSxhQUFhO1FBQ2hCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFTSxpQkFBaUI7UUFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDaEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1NBQ2xEO0lBQ0wsQ0FBQztJQUVNLGNBQWMsQ0FBQyxPQUFlO1FBQ2pDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLENBQUM7UUFDNUYsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUNqRixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUM5QixDQUFDO0lBRU0sa0JBQWtCO1FBQ3JCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO0lBQ3pDLENBQUM7Q0FDSixDQUFBO0FBdkRZO0lBQVIsS0FBSyxFQUFFOzsrREFBMEI7QUFFekI7SUFBUixLQUFLLEVBQUU7OytEQUEwQjtBQUV6QjtJQUFSLEtBQUssRUFBRTs7NERBQXVCO0FBRXRCO0lBQVIsS0FBSyxFQUFFOztrRUFBNkI7QUFFNUI7SUFBUixLQUFLLEVBQUU7OytEQUEyQjtBQUV6QjtJQUFULE1BQU0sRUFBRTtzQ0FBa0IsWUFBWTsrREFBOEI7QUFYNUQsNkJBQTZCO0lBSnpDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSx3QkFBd0I7UUFDbEMsKzlEQUFzRDtLQUN6RCxDQUFDOzZDQTRCNEMsb0JBQW9CO0dBM0JyRCw2QkFBNkIsQ0F3RHpDO1NBeERZLDZCQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEN1cnJlbmNpZXNMaXN0SGVscGVyIH0gZnJvbSAnaHV1Yi11dGlscy9saWInO1xuaW1wb3J0IHsgVUlfQ1VSUkVOQ0lFU19EUk9QRE9XTl9DT05TVEFOVFMgfSBmcm9tICcuL3VpLWN1cnJlbmNpZXMtZHJvcGRvd24uY29uc3RhbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ3VpLWN1cnJlbmNpZXMtZHJvcGRvd24nLFxuICAgIHRlbXBsYXRlVXJsOiAnLi91aS1jdXJyZW5jaWVzLWRyb3Bkb3duLmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBVaUN1cnJlbmNpZXNEcm9wZG93bkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgQElucHV0KCkgcHVibGljIGN1cnJlbmN5Pzogc3RyaW5nO1xuXG4gICAgQElucHV0KCkgcHVibGljIGxhbmd1YWdlPzogc3RyaW5nO1xuXG4gICAgQElucHV0KCkgcHVibGljIHZhbHVlPzogc3RyaW5nO1xuXG4gICAgQElucHV0KCkgcHVibGljIHBsYWNlaG9sZGVyPzogc3RyaW5nO1xuXG4gICAgQElucHV0KCkgcHVibGljIGRpc2FibGVkPzogYm9vbGVhbjtcblxuICAgIEBPdXRwdXQoKSBwdWJsaWMgb25DaGFuZ2U6IEV2ZW50RW1pdHRlcjxvYmplY3Q+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgcHVibGljIGlucHV0U2VhcmNoVmlld01vZGVsID0ge1xuICAgICAgICBwbGFjZWhvbGRlcjogVUlfQ1VSUkVOQ0lFU19EUk9QRE9XTl9DT05TVEFOVFMuU0VBUkNIX0NVUlJFTkNZLFxuICAgICAgICB2YWx1ZTogJydcbiAgICB9O1xuXG4gICAgcHVibGljIGlzRHJvcGRvd25PcGVuZWQgPSBmYWxzZTtcbiAgICBwdWJsaWMgY3VycmVuY3lTeW1ib2wgPSAnJztcblxuICAgIHB1YmxpYyBwb3B1bGFyQ3VycmVuY3lUaXRsZSA9IFVJX0NVUlJFTkNJRVNfRFJPUERPV05fQ09OU1RBTlRTLlBPUFVMQVJfQ1VSUkVOQ0lFUztcbiAgICBwdWJsaWMgYWxsQ3VycmVuY3lUaXRsZSA9IFVJX0NVUlJFTkNJRVNfRFJPUERPV05fQ09OU1RBTlRTLkFMTF9DVVJSRU5DSUVTO1xuXG4gICAgcHVibGljIGN1cnJlbmNpZXNMaXN0ID0gdGhpcy5jdXJyZW5jaWVzTGlzdEhlbHBlci5nZXRDdXJyZW5jaWVzTGlzdCgpO1xuICAgIHB1YmxpYyBwb3B1bGFyQ3VycmVuY2llc0xpc3QgPSB0aGlzLmN1cnJlbmNpZXNMaXN0SGVscGVyLmdldFBvcHVsYXJDdXJyZW5jaWVzTGlzdCgpO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBjdXJyZW5jaWVzTGlzdEhlbHBlcjogQ3VycmVuY2llc0xpc3RIZWxwZXIpIHt9XG5cbiAgICBwdWJsaWMgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMuY3VycmVuY3lTeW1ib2wgPSB0aGlzLmN1cnJlbmNpZXNMaXN0SGVscGVyLmdldEN1cnJlbmN5QnlJc28zQ29kZSh0aGlzLmN1cnJlbmN5KS5zeW1ib2w7XG4gICAgICAgIHRoaXMuZGlzYWJsZWQgPSB0aGlzLmRpc2FibGVkICE9PSB1bmRlZmluZWQgPyB0aGlzLmRpc2FibGVkIDogZmFsc2U7XG4gICAgfVxuXG4gICAgcHVibGljIG9uSW5wdXRDaGFuZ2UoKTogdm9pZCB7XG4gICAgICAgIHRoaXMub25DaGFuZ2UuZW1pdCh7IGN1cnJlbmN5OiB0aGlzLmN1cnJlbmN5LCB2YWx1ZTogdGhpcy52YWx1ZSB9KTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb3BlbkNsb3NlRHJvcGRvd24oKSB7XG4gICAgICAgIGlmICghdGhpcy5kaXNhYmxlZCkge1xuICAgICAgICAgICAgdGhpcy5pc0Ryb3Bkb3duT3BlbmVkID0gIXRoaXMuaXNEcm9wZG93bk9wZW5lZDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBjaGFuZ2VDdXJyZW5jeShpc29Db2RlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5jdXJyZW5jeSA9IGlzb0NvZGU7XG4gICAgICAgIHRoaXMuY3VycmVuY3lTeW1ib2wgPSB0aGlzLmN1cnJlbmNpZXNMaXN0SGVscGVyLmdldEN1cnJlbmN5QnlJc28zQ29kZSh0aGlzLmN1cnJlbmN5KS5zeW1ib2w7XG4gICAgICAgIHRoaXMudmFsdWUgPSB0aGlzLmN1cnJlbmNpZXNMaXN0SGVscGVyLmdldEN1cnJlbmN5QnlJc28zQ29kZSh0aGlzLmN1cnJlbmN5KS5uYW1lO1xuICAgICAgICB0aGlzLm9uQ2hhbmdlLmVtaXQoeyBjdXJyZW5jeTogdGhpcy5jdXJyZW5jeSwgdmFsdWU6IHRoaXMudmFsdWUgfSk7XG4gICAgICAgIHRoaXMub3BlbkNsb3NlRHJvcGRvd24oKTtcbiAgICAgICAgdGhpcy5vbkNsZWFyU2VhcmNoSW5wdXQoKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25DbGVhclNlYXJjaElucHV0KCkge1xuICAgICAgICB0aGlzLmlucHV0U2VhcmNoVmlld01vZGVsLnZhbHVlID0gJyc7XG4gICAgfVxufVxuIl19