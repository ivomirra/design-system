import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpEventType } from '@angular/common/http';
let UiUploadFileEventsHelper = class UiUploadFileEventsHelper {
    constructor() { }
    getAbstractedEventsDataModel(event) {
        switch (event.type) {
            case HttpEventType.Sent:
                return {
                    isUploaded: false,
                    isUploading: false,
                    fileUploadPercentage: 0
                };
            case HttpEventType.UploadProgress:
                return {
                    isUploaded: false,
                    isUploading: true,
                    fileUploadPercentage: Math.round(100 * event.loaded / event.total)
                };
            case HttpEventType.Response:
                return Object.assign({}, event, { isUploaded: true, isUploading: false, fileUploadPercentage: 100 });
        }
    }
};
UiUploadFileEventsHelper = tslib_1.__decorate([
    Injectable(),
    tslib_1.__metadata("design:paramtypes", [])
], UiUploadFileEventsHelper);
export { UiUploadFileEventsHelper };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktdXBsb2FkLWZpbGUuZXZlbnRzLmhlbHBlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2h1dWItbWF0ZXJpYWwvbGliLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9mb3Jtcy91cGxvYWQtZmlsZXMvdWktdXBsb2FkLWZpbGUuZXZlbnRzLmhlbHBlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFHckQsSUFBYSx3QkFBd0IsR0FBckMsTUFBYSx3QkFBd0I7SUFDakMsZ0JBQWUsQ0FBQztJQUVULDRCQUE0QixDQUFDLEtBQVU7UUFDMUMsUUFBUSxLQUFLLENBQUMsSUFBSSxFQUFFO1lBQ2hCLEtBQUssYUFBYSxDQUFDLElBQUk7Z0JBQ25CLE9BQU87b0JBQ0gsVUFBVSxFQUFFLEtBQUs7b0JBQ2pCLFdBQVcsRUFBRSxLQUFLO29CQUNsQixvQkFBb0IsRUFBRSxDQUFDO2lCQUMxQixDQUFDO1lBRU4sS0FBSyxhQUFhLENBQUMsY0FBYztnQkFDN0IsT0FBTztvQkFDSCxVQUFVLEVBQUUsS0FBSztvQkFDakIsV0FBVyxFQUFFLElBQUk7b0JBQ2pCLG9CQUFvQixFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztpQkFDckUsQ0FBQztZQUVOLEtBQUssYUFBYSxDQUFDLFFBQVE7Z0JBQ3ZCLHlCQUNPLEtBQUssSUFDUixVQUFVLEVBQUUsSUFBSSxFQUNoQixXQUFXLEVBQUUsS0FBSyxFQUNsQixvQkFBb0IsRUFBRSxHQUFHLElBQzNCO1NBQ1Q7SUFDTCxDQUFDO0NBQ0osQ0FBQTtBQTVCWSx3QkFBd0I7SUFEcEMsVUFBVSxFQUFFOztHQUNBLHdCQUF3QixDQTRCcEM7U0E1Qlksd0JBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cEV2ZW50VHlwZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFVpVXBsb2FkRmlsZUV2ZW50c0hlbHBlciB7XG4gICAgY29uc3RydWN0b3IoKSB7fVxuXG4gICAgcHVibGljIGdldEFic3RyYWN0ZWRFdmVudHNEYXRhTW9kZWwoZXZlbnQ6IGFueSk6IGFueSB7XG4gICAgICAgIHN3aXRjaCAoZXZlbnQudHlwZSkge1xuICAgICAgICAgICAgY2FzZSBIdHRwRXZlbnRUeXBlLlNlbnQ6XG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgICAgaXNVcGxvYWRlZDogZmFsc2UsXG4gICAgICAgICAgICAgICAgICAgIGlzVXBsb2FkaW5nOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgZmlsZVVwbG9hZFBlcmNlbnRhZ2U6IDBcbiAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBjYXNlIEh0dHBFdmVudFR5cGUuVXBsb2FkUHJvZ3Jlc3M6XG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgICAgaXNVcGxvYWRlZDogZmFsc2UsXG4gICAgICAgICAgICAgICAgICAgIGlzVXBsb2FkaW5nOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICBmaWxlVXBsb2FkUGVyY2VudGFnZTogTWF0aC5yb3VuZCgxMDAgKiBldmVudC5sb2FkZWQgLyBldmVudC50b3RhbClcbiAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBjYXNlIEh0dHBFdmVudFR5cGUuUmVzcG9uc2U6XG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgICAgLi4uZXZlbnQsXG4gICAgICAgICAgICAgICAgICAgIGlzVXBsb2FkZWQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgIGlzVXBsb2FkaW5nOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgZmlsZVVwbG9hZFBlcmNlbnRhZ2U6IDEwMFxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=