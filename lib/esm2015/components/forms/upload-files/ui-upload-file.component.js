import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
let UiUploadFileComponent = class UiUploadFileComponent {
    constructor() {
        this.viewModel = {};
        this.onFileSelected = new EventEmitter();
        this.onFileRemoved = new EventEmitter();
        this.onFileUploadCanceled = new EventEmitter();
        this.onFileViewClicked = new EventEmitter();
        this.componentActionsModel = {
            isBeingDragged: false
        };
    }
    handleSelectedFile(selectedFile) {
        this.viewModel.fileName = selectedFile.name;
        this.viewModel.fileUploadPercentage = 0;
        this.viewModel.isUploading = true;
    }
    onDragOver($event) {
        $event.preventDefault();
        this.componentActionsModel.isBeingDragged = true;
    }
    onDragLeave($event) {
        $event.preventDefault();
        this.componentActionsModel.isBeingDragged = false;
    }
    onDragDrop($event) {
        $event.preventDefault();
        this.componentActionsModel.isBeingDragged = false;
        const selectedFile = $event.dataTransfer.files[0];
        this.handleSelectedFile(selectedFile);
        this.onFileSelected.emit({
            selectedFile,
            viewModel: this.viewModel
        });
    }
    onChange($event) {
        $event.preventDefault();
        const selectedFile = $event.target.files[0];
        this.handleSelectedFile(selectedFile);
        this.onFileSelected.emit({
            selectedFile,
            viewModel: this.viewModel
        });
    }
    onRemoveFileClicked($event) {
        $event.preventDefault();
        this.onFileRemoved.emit({
            viewModel: this.viewModel
        });
    }
    onUploadCancelClicked() {
        this.viewModel.isUploading = false;
        this.viewModel.fileUploadPercentage = 0;
        this.onFileUploadCanceled.emit({
            viewModel: this.viewModel
        });
    }
    onReplaceDocumentClicked($event) {
        $event.preventDefault();
        this.uploadInput.nativeElement.click();
    }
    onViewFileClicked($event) {
        $event.preventDefault();
        this.onFileViewClicked.emit({
            viewModel: this.viewModel
        });
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiUploadFileComponent.prototype, "viewModel", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", Object)
], UiUploadFileComponent.prototype, "onFileSelected", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", Object)
], UiUploadFileComponent.prototype, "onFileRemoved", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", Object)
], UiUploadFileComponent.prototype, "onFileUploadCanceled", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", Object)
], UiUploadFileComponent.prototype, "onFileViewClicked", void 0);
tslib_1.__decorate([
    ViewChild('uploadInput', { read: ElementRef, static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], UiUploadFileComponent.prototype, "uploadInput", void 0);
UiUploadFileComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-upload-file',
        template: "<div class=\"ui-upload-file__container--upload\"\n    [ngClass]=\"{\n        'ui-upload-file__container--dragover': componentActionsModel.isBeingDragged\n    }\"\n    *ngIf=\"!viewModel.isUploaded && !viewModel.isUploading\">\n        Drag and drop a file here to upload, or&nbsp;<span class=\"ui-upload-file__message-link\">Select a file</span>\n        <input type=\"file\"\n                class=\"ui-upload-file__upload-input\"\n                (dragover)=\"onDragOver($event)\"\n\n                (dragleave)=\"onDragLeave($event)\"\n                (dragend)=\"onDragLeave($event)\"\n\n                (drop)=\"onDragDrop($event)\"\n                (change)=\"onChange($event)\">\n</div>\n\n<div class=\"ui-upload-file__container--uploading\" *ngIf=\"viewModel.isUploading\">\n    <div class=\"ui-upload-file__p-bar__container\">\n        <div class=\"ui-upload-file__p-bar__name\">\n            <div>{{viewModel.fileName}}</div>\n            <div>{{viewModel.fileUploadPercentage}}%</div>\n        </div>\n        <ui-progress-bar class=\"ui-upload-file__p-bar\"\n                         size=\"small\"\n                         [percentage]=\"viewModel.fileUploadPercentage\"></ui-progress-bar>\n        <i class=\"huub-material-icon ui-upload-file__p-bar__cancel\"\n            (click)=\"onUploadCancelClicked()\"\n            icon=\"close\"></i>\n    </div>\n</div>\n\n<span class=\"ui-upload-file__container--uploaded\"\n    *ngIf=\"viewModel.isUploaded && !viewModel.isUploading\">\n    <input type=\"file\"\n            #uploadInput\n            class=\"ui-upload-file__dummy-input\"\n            (change)=\"onChange($event)\">\n    <div class=\"ui-upload-file__doc-name\">\n        <i class=\"huub-material-icon\" icon=\"document\"></i>\n        <span class=\"ui-upload-file__doc-name-text\">{{viewModel.fileName}}</span>\n    </div>\n    <div class=\"ui-upload-file__doc-action-container\">\n        <a href=\"\"\n            (click)=\"onViewFileClicked($event)\"\n            class=\"ui-upload-file__doc-action\">View</a>\n        <a href=\"\"\n            (click)=\"onReplaceDocumentClicked($event)\"\n            class=\"ui-upload-file__doc-action\">Replace</a>\n        <a href=\"\"\n            (click)=\"onRemoveFileClicked($event)\"\n            class=\"ui-upload-file__doc-action\">Remove</a>\n    </div>\n</span>\n"
    }),
    tslib_1.__metadata("design:paramtypes", [])
], UiUploadFileComponent);
export { UiUploadFileComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktdXBsb2FkLWZpbGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaHV1Yi1tYXRlcmlhbC9saWIvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2Zvcm1zL3VwbG9hZC1maWxlcy91aS11cGxvYWQtZmlsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU05RixJQUFhLHFCQUFxQixHQUFsQyxNQUFhLHFCQUFxQjtJQWlCOUI7UUFmTyxjQUFTLEdBS1osRUFBRSxDQUFDO1FBRVcsbUJBQWMsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3BDLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNuQyx5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzFDLHNCQUFpQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFPbEQsMEJBQXFCLEdBQUc7WUFDM0IsY0FBYyxFQUFFLEtBQUs7U0FDeEIsQ0FBQztJQUphLENBQUM7SUFNUixrQkFBa0IsQ0FBQyxZQUFZO1FBQ25DLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUM7UUFDNUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsR0FBRyxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO0lBQ3RDLENBQUM7SUFFTSxVQUFVLENBQUMsTUFBTTtRQUNwQixNQUFNLENBQUMsY0FBYyxFQUFFLENBQUM7UUFFeEIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7SUFDckQsQ0FBQztJQUVNLFdBQVcsQ0FBQyxNQUFNO1FBQ3JCLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUV4QixJQUFJLENBQUMscUJBQXFCLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztJQUN0RCxDQUFDO0lBRU0sVUFBVSxDQUFDLE1BQU07UUFDcEIsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRXhCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBRWxELE1BQU0sWUFBWSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRWxELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUV0QyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQztZQUNyQixZQUFZO1lBQ1osU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTO1NBQzVCLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTSxRQUFRLENBQUMsTUFBTTtRQUNsQixNQUFNLENBQUMsY0FBYyxFQUFFLENBQUM7UUFFeEIsTUFBTSxZQUFZLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFNUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRXRDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO1lBQ3JCLFlBQVk7WUFDWixTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7U0FDNUIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLG1CQUFtQixDQUFDLE1BQU07UUFDN0IsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRXhCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO1lBQ3BCLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUztTQUM1QixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0scUJBQXFCO1FBQ3hCLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLG9CQUFvQixHQUFHLENBQUMsQ0FBQztRQUV4QyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDO1lBQzNCLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUztTQUM1QixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sd0JBQXdCLENBQUMsTUFBTTtRQUNsQyxNQUFNLENBQUMsY0FBYyxFQUFFLENBQUM7UUFFeEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDM0MsQ0FBQztJQUVNLGlCQUFpQixDQUFDLE1BQU07UUFDM0IsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRXhCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUM7WUFDeEIsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTO1NBQzVCLENBQUMsQ0FBQztJQUNQLENBQUM7Q0FDSixDQUFBO0FBakdHO0lBREMsS0FBSyxFQUFFOzt3REFNRDtBQUVHO0lBQVQsTUFBTSxFQUFFOzs2REFBNkM7QUFDNUM7SUFBVCxNQUFNLEVBQUU7OzREQUE0QztBQUMzQztJQUFULE1BQU0sRUFBRTs7bUVBQW1EO0FBQ2xEO0lBQVQsTUFBTSxFQUFFOztnRUFBZ0Q7QUFHekQ7SUFEQyxTQUFTLENBQUMsYUFBYSxFQUFFLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUM7c0NBQ3pDLFVBQVU7MERBQWM7QUFmcEMscUJBQXFCO0lBSmpDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxnQkFBZ0I7UUFDMUIsK3lFQUE4QztLQUNqRCxDQUFDOztHQUNXLHFCQUFxQixDQW1HakM7U0FuR1kscUJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIFZpZXdDaGlsZCwgRWxlbWVudFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ3VpLXVwbG9hZC1maWxlJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vdWktdXBsb2FkLWZpbGUuY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFVpVXBsb2FkRmlsZUNvbXBvbmVudCB7XG4gICAgQElucHV0KClcbiAgICBwdWJsaWMgdmlld01vZGVsOiB7XG4gICAgICAgIGlzVXBsb2FkaW5nPzogYm9vbGVhbjtcbiAgICAgICAgaXNVcGxvYWRlZD86IGJvb2xlYW47XG4gICAgICAgIGZpbGVOYW1lPzogc3RyaW5nO1xuICAgICAgICBmaWxlVXBsb2FkUGVyY2VudGFnZT86IG51bWJlciB8IHN0cmluZztcbiAgICB9ID0ge307XG5cbiAgICBAT3V0cHV0KCkgcHJpdmF0ZSBvbkZpbGVTZWxlY3RlZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgcHJpdmF0ZSBvbkZpbGVSZW1vdmVkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBwcml2YXRlIG9uRmlsZVVwbG9hZENhbmNlbGVkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBwcml2YXRlIG9uRmlsZVZpZXdDbGlja2VkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgQFZpZXdDaGlsZCgndXBsb2FkSW5wdXQnLCB7IHJlYWQ6IEVsZW1lbnRSZWYsIHN0YXRpYzogZmFsc2UgfSlcbiAgICBwcml2YXRlIHVwbG9hZElucHV0OiBFbGVtZW50UmVmPEhUTUxFbGVtZW50PjtcblxuICAgIGNvbnN0cnVjdG9yKCkge31cblxuICAgIHB1YmxpYyBjb21wb25lbnRBY3Rpb25zTW9kZWwgPSB7XG4gICAgICAgIGlzQmVpbmdEcmFnZ2VkOiBmYWxzZVxuICAgIH07XG5cbiAgICBwcml2YXRlIGhhbmRsZVNlbGVjdGVkRmlsZShzZWxlY3RlZEZpbGUpIHtcbiAgICAgICAgdGhpcy52aWV3TW9kZWwuZmlsZU5hbWUgPSBzZWxlY3RlZEZpbGUubmFtZTtcbiAgICAgICAgdGhpcy52aWV3TW9kZWwuZmlsZVVwbG9hZFBlcmNlbnRhZ2UgPSAwO1xuICAgICAgICB0aGlzLnZpZXdNb2RlbC5pc1VwbG9hZGluZyA9IHRydWU7XG4gICAgfVxuXG4gICAgcHVibGljIG9uRHJhZ092ZXIoJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgIHRoaXMuY29tcG9uZW50QWN0aW9uc01vZGVsLmlzQmVpbmdEcmFnZ2VkID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25EcmFnTGVhdmUoJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgIHRoaXMuY29tcG9uZW50QWN0aW9uc01vZGVsLmlzQmVpbmdEcmFnZ2VkID0gZmFsc2U7XG4gICAgfVxuXG4gICAgcHVibGljIG9uRHJhZ0Ryb3AoJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgIHRoaXMuY29tcG9uZW50QWN0aW9uc01vZGVsLmlzQmVpbmdEcmFnZ2VkID0gZmFsc2U7XG5cbiAgICAgICAgY29uc3Qgc2VsZWN0ZWRGaWxlID0gJGV2ZW50LmRhdGFUcmFuc2Zlci5maWxlc1swXTtcblxuICAgICAgICB0aGlzLmhhbmRsZVNlbGVjdGVkRmlsZShzZWxlY3RlZEZpbGUpO1xuXG4gICAgICAgIHRoaXMub25GaWxlU2VsZWN0ZWQuZW1pdCh7XG4gICAgICAgICAgICBzZWxlY3RlZEZpbGUsXG4gICAgICAgICAgICB2aWV3TW9kZWw6IHRoaXMudmlld01vZGVsXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHB1YmxpYyBvbkNoYW5nZSgkZXZlbnQpIHtcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgY29uc3Qgc2VsZWN0ZWRGaWxlID0gJGV2ZW50LnRhcmdldC5maWxlc1swXTtcblxuICAgICAgICB0aGlzLmhhbmRsZVNlbGVjdGVkRmlsZShzZWxlY3RlZEZpbGUpO1xuXG4gICAgICAgIHRoaXMub25GaWxlU2VsZWN0ZWQuZW1pdCh7XG4gICAgICAgICAgICBzZWxlY3RlZEZpbGUsXG4gICAgICAgICAgICB2aWV3TW9kZWw6IHRoaXMudmlld01vZGVsXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHB1YmxpYyBvblJlbW92ZUZpbGVDbGlja2VkKCRldmVudCkge1xuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICB0aGlzLm9uRmlsZVJlbW92ZWQuZW1pdCh7XG4gICAgICAgICAgICB2aWV3TW9kZWw6IHRoaXMudmlld01vZGVsXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHB1YmxpYyBvblVwbG9hZENhbmNlbENsaWNrZWQoKSB7XG4gICAgICAgIHRoaXMudmlld01vZGVsLmlzVXBsb2FkaW5nID0gZmFsc2U7XG4gICAgICAgIHRoaXMudmlld01vZGVsLmZpbGVVcGxvYWRQZXJjZW50YWdlID0gMDtcblxuICAgICAgICB0aGlzLm9uRmlsZVVwbG9hZENhbmNlbGVkLmVtaXQoe1xuICAgICAgICAgICAgdmlld01vZGVsOiB0aGlzLnZpZXdNb2RlbFxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25SZXBsYWNlRG9jdW1lbnRDbGlja2VkKCRldmVudCkge1xuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICB0aGlzLnVwbG9hZElucHV0Lm5hdGl2ZUVsZW1lbnQuY2xpY2soKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25WaWV3RmlsZUNsaWNrZWQoJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgIHRoaXMub25GaWxlVmlld0NsaWNrZWQuZW1pdCh7XG4gICAgICAgICAgICB2aWV3TW9kZWw6IHRoaXMudmlld01vZGVsXG4gICAgICAgIH0pO1xuICAgIH1cbn1cbiJdfQ==