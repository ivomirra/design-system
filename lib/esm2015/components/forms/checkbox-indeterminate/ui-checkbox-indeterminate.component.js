import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
let UiCheckboxIndeterminateComponent = class UiCheckboxIndeterminateComponent {
    constructor() {
        this.onChange = new EventEmitter();
        this.statusValues = {
            unchecked: 'unchecked',
            indeterminate: 'indeterminate',
            checked: 'checked'
        };
    }
    getNextStatus(currentStatus) {
        switch (currentStatus) {
            case this.statusValues.unchecked:
                return this.statusValues.indeterminate;
            case this.statusValues.indeterminate:
                return this.statusValues.checked;
            case this.statusValues.checked:
            default:
                return this.statusValues.unchecked;
        }
    }
    ngOnInit() {
        this.status = this.statusValues[this.status] || this.statusValues.unchecked;
    }
    onCheckboxClick() {
        this.status = this.getNextStatus(this.status);
        this.onChange.emit(this.status);
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], UiCheckboxIndeterminateComponent.prototype, "status", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Boolean)
], UiCheckboxIndeterminateComponent.prototype, "disabled", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], UiCheckboxIndeterminateComponent.prototype, "onChange", void 0);
UiCheckboxIndeterminateComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-checkbox-indeterminate',
        template: "<input type=\"checkbox\"\n       class=\"huub-material-icon\"\n       [ngClass]=\"{\n           'indeterminate': status === statusValues.indeterminate\n       }\"\n       [checked]=\"status === statusValues.checked || status === statusValues.indeterminate\"\n       (change)=\"onCheckboxClick()\"\n       [disabled]=\"disabled\">\n"
    }),
    tslib_1.__metadata("design:paramtypes", [])
], UiCheckboxIndeterminateComponent);
export { UiCheckboxIndeterminateComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktY2hlY2tib3gtaW5kZXRlcm1pbmF0ZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZm9ybXMvY2hlY2tib3gtaW5kZXRlcm1pbmF0ZS91aS1jaGVja2JveC1pbmRldGVybWluYXRlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBVSxNQUFNLGVBQWUsQ0FBQztBQU0vRSxJQUFhLGdDQUFnQyxHQUE3QyxNQUFhLGdDQUFnQztJQWF6QztRQVJpQixhQUFRLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7UUFFM0QsaUJBQVksR0FBRztZQUNsQixTQUFTLEVBQUUsV0FBVztZQUN0QixhQUFhLEVBQUUsZUFBZTtZQUM5QixPQUFPLEVBQUUsU0FBUztTQUNyQixDQUFDO0lBRWEsQ0FBQztJQUVSLGFBQWEsQ0FBQyxhQUFhO1FBQy9CLFFBQVEsYUFBYSxFQUFFO1lBQ25CLEtBQUssSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTO2dCQUM1QixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDO1lBQzNDLEtBQUssSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhO2dCQUNoQyxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDO1lBQ3JDLEtBQUssSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUM7WUFDL0I7Z0JBQ0ksT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQztTQUMxQztJQUNMLENBQUM7SUFFTSxRQUFRO1FBQ1gsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQztJQUNoRixDQUFDO0lBRU0sZUFBZTtRQUNsQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRTlDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNwQyxDQUFDO0NBQ0osQ0FBQTtBQW5DWTtJQUFSLEtBQUssRUFBRTs7Z0VBQXdCO0FBRXZCO0lBQVIsS0FBSyxFQUFFOztrRUFBMkI7QUFFekI7SUFBVCxNQUFNLEVBQUU7c0NBQWtCLFlBQVk7a0VBQTJCO0FBTHpELGdDQUFnQztJQUo1QyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsMkJBQTJCO1FBQ3JDLHVWQUF5RDtLQUM1RCxDQUFDOztHQUNXLGdDQUFnQyxDQW9DNUM7U0FwQ1ksZ0NBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ3VpLWNoZWNrYm94LWluZGV0ZXJtaW5hdGUnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi91aS1jaGVja2JveC1pbmRldGVybWluYXRlLmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBVaUNoZWNrYm94SW5kZXRlcm1pbmF0ZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgQElucHV0KCkgcHVibGljIHN0YXR1cz86IHN0cmluZztcblxuICAgIEBJbnB1dCgpIHB1YmxpYyBkaXNhYmxlZD86IGJvb2xlYW47XG5cbiAgICBAT3V0cHV0KCkgcHVibGljIG9uQ2hhbmdlOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAgIHB1YmxpYyBzdGF0dXNWYWx1ZXMgPSB7XG4gICAgICAgIHVuY2hlY2tlZDogJ3VuY2hlY2tlZCcsXG4gICAgICAgIGluZGV0ZXJtaW5hdGU6ICdpbmRldGVybWluYXRlJyxcbiAgICAgICAgY2hlY2tlZDogJ2NoZWNrZWQnXG4gICAgfTtcblxuICAgIGNvbnN0cnVjdG9yKCkge31cblxuICAgIHByaXZhdGUgZ2V0TmV4dFN0YXR1cyhjdXJyZW50U3RhdHVzKSB7XG4gICAgICAgIHN3aXRjaCAoY3VycmVudFN0YXR1cykge1xuICAgICAgICAgICAgY2FzZSB0aGlzLnN0YXR1c1ZhbHVlcy51bmNoZWNrZWQ6XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuc3RhdHVzVmFsdWVzLmluZGV0ZXJtaW5hdGU7XG4gICAgICAgICAgICBjYXNlIHRoaXMuc3RhdHVzVmFsdWVzLmluZGV0ZXJtaW5hdGU6XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuc3RhdHVzVmFsdWVzLmNoZWNrZWQ7XG4gICAgICAgICAgICBjYXNlIHRoaXMuc3RhdHVzVmFsdWVzLmNoZWNrZWQ6XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnN0YXR1c1ZhbHVlcy51bmNoZWNrZWQ7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMuc3RhdHVzID0gdGhpcy5zdGF0dXNWYWx1ZXNbdGhpcy5zdGF0dXNdIHx8IHRoaXMuc3RhdHVzVmFsdWVzLnVuY2hlY2tlZDtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25DaGVja2JveENsaWNrKCk6IHZvaWQge1xuICAgICAgICB0aGlzLnN0YXR1cyA9IHRoaXMuZ2V0TmV4dFN0YXR1cyh0aGlzLnN0YXR1cyk7XG5cbiAgICAgICAgdGhpcy5vbkNoYW5nZS5lbWl0KHRoaXMuc3RhdHVzKTtcbiAgICB9XG59XG4iXX0=