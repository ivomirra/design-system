import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
let UiSpinnerComponent = class UiSpinnerComponent {
    constructor() {
        /** Property that defines the input value **/
        this.allowNegatives = true;
        /** Property that sould be used for binding a callback to receive the current input value
         everytime it changes **/
        this.onValueChanged = new EventEmitter();
    }
    ngOnInit() {
        if (isNaN(this.value)) {
            throw new TypeError('The value defined should be a number');
        }
    }
    onKeyUp() {
        this.onValueChanged.emit(this.value);
    }
    onPlusButtonPressed() {
        this.value += 1;
        this.onValueChanged.emit(this.value);
    }
    onLessButtonPressed() {
        this.value -= 1;
        if (!this.allowNegatives && this.value < 0) {
            this.value = 0;
            return;
        }
        this.onValueChanged.emit(this.value);
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiSpinnerComponent.prototype, "allowNegatives", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Number)
], UiSpinnerComponent.prototype, "value", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Boolean)
], UiSpinnerComponent.prototype, "hasError", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Boolean)
], UiSpinnerComponent.prototype, "disabled", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], UiSpinnerComponent.prototype, "onValueChanged", void 0);
UiSpinnerComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-spinner',
        template: "<input class=\"ui-spinner__input\"\n       #ctrl=\"ngModel\"\n       [(ngModel)]=\"value\"\n       [ngClass]=\"{'ui-spinner__input--error': hasError}\"\n       (keyup)=\"onKeyUp()\"\n       type=\"number\"\n       [attr.disabled]=\"disabled\">\n<button class=\"huub-material-icon huub-material-icon--small ui-spinner__button ui-spinner__button-less\"\n        icon=\"subtract\"\n        (click)=\"onLessButtonPressed()\"\n        [attr.disabled]=\"disabled\"></button>\n<button class=\"huub-material-icon huub-material-icon--small ui-spinner__button ui-spinner__button-plus\"\n        icon=\"add\"\n        (click)=\"onPlusButtonPressed()\"\n        [attr.disabled]=\"disabled\"></button>\n"
    }),
    tslib_1.__metadata("design:paramtypes", [])
], UiSpinnerComponent);
export { UiSpinnerComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktc3Bpbm5lci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZm9ybXMvc3Bpbm5lci91aS1zcGlubmVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBVSxNQUFNLGVBQWUsQ0FBQztBQU0vRSxJQUFhLGtCQUFrQixHQUEvQixNQUFhLGtCQUFrQjtJQWtCM0I7UUFqQkEsNkNBQTZDO1FBQzdCLG1CQUFjLEdBQUcsSUFBSSxDQUFDO1FBWXRDO2lDQUN5QjtRQUNSLG1CQUFjLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7SUFFekQsQ0FBQztJQUVULFFBQVE7UUFDWCxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDbkIsTUFBTSxJQUFJLFNBQVMsQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDO1NBQy9EO0lBQ0wsQ0FBQztJQUVNLE9BQU87UUFDVixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVNLG1CQUFtQjtRQUN0QixJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQztRQUVoQixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVNLG1CQUFtQjtRQUN0QixJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQztRQUVoQixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsRUFBRTtZQUN4QyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztZQUNmLE9BQU87U0FDVjtRQUVELElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN6QyxDQUFDO0NBQ0osQ0FBQTtBQTVDWTtJQUFSLEtBQUssRUFBRTs7MERBQThCO0FBRzdCO0lBQVIsS0FBSyxFQUFFOztpREFBc0I7QUFJckI7SUFBUixLQUFLLEVBQUU7O29EQUEwQjtBQUd6QjtJQUFSLEtBQUssRUFBRTs7b0RBQTBCO0FBSXhCO0lBQVQsTUFBTSxFQUFFO3NDQUF3QixZQUFZOzBEQUEyQjtBQWhCL0Qsa0JBQWtCO0lBSjlCLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxZQUFZO1FBQ3RCLDhyQkFBMEM7S0FDN0MsQ0FBQzs7R0FDVyxrQkFBa0IsQ0E4QzlCO1NBOUNZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICd1aS1zcGlubmVyJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vdWktc3Bpbm5lci5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgVWlTcGlubmVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICAvKiogUHJvcGVydHkgdGhhdCBkZWZpbmVzIHRoZSBpbnB1dCB2YWx1ZSAqKi9cbiAgICBASW5wdXQoKSBwdWJsaWMgYWxsb3dOZWdhdGl2ZXMgPSB0cnVlO1xuXG4gICAgLyoqIFByb3BlcnR5IHRoYXQgZGVmaW5lcyB0aGUgaW5wdXQgdmFsdWUgKiovXG4gICAgQElucHV0KCkgcHVibGljIHZhbHVlOiBudW1iZXI7XG5cbiAgICAvKiogUHJvcGVydHkgdGhhdCBkZWZpbmVzIGlmIHRoZSBpbnB1dCBoYXMgYW55IGVycm9yLCB3aGVuIGEgdHJ1ZSBpcyBwcm92aWRlZCB0aGVcbiAgICAgaW5wdXQgaGFzIGEgZGlmZXJlbnQgc3R5bGluZyBiZWFodmlvciAqKi9cbiAgICBASW5wdXQoKSBwdWJsaWMgaGFzRXJyb3I6IGJvb2xlYW47XG5cbiAgICAvKiogUHJvcGVydHkgdGhhdCBzZXRzIGFsbCB0aGUgY29tcG9uZW50IGFzIGRpc2FibGVkICoqL1xuICAgIEBJbnB1dCgpIHB1YmxpYyBkaXNhYmxlZDogYm9vbGVhbjtcblxuICAgIC8qKiBQcm9wZXJ0eSB0aGF0IHNvdWxkIGJlIHVzZWQgZm9yIGJpbmRpbmcgYSBjYWxsYmFjayB0byByZWNlaXZlIHRoZSBjdXJyZW50IGlucHV0IHZhbHVlXG4gICAgIGV2ZXJ5dGltZSBpdCBjaGFuZ2VzICoqL1xuICAgIEBPdXRwdXQoKSBwdWJsaWMgb25WYWx1ZUNoYW5nZWQ6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgY29uc3RydWN0b3IoKSB7fVxuXG4gICAgcHVibGljIG5nT25Jbml0KCkge1xuICAgICAgICBpZiAoaXNOYU4odGhpcy52YWx1ZSkpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ1RoZSB2YWx1ZSBkZWZpbmVkIHNob3VsZCBiZSBhIG51bWJlcicpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIG9uS2V5VXAoKSB7XG4gICAgICAgIHRoaXMub25WYWx1ZUNoYW5nZWQuZW1pdCh0aGlzLnZhbHVlKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25QbHVzQnV0dG9uUHJlc3NlZCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy52YWx1ZSArPSAxO1xuXG4gICAgICAgIHRoaXMub25WYWx1ZUNoYW5nZWQuZW1pdCh0aGlzLnZhbHVlKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25MZXNzQnV0dG9uUHJlc3NlZCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy52YWx1ZSAtPSAxO1xuXG4gICAgICAgIGlmICghdGhpcy5hbGxvd05lZ2F0aXZlcyAmJiB0aGlzLnZhbHVlIDwgMCkge1xuICAgICAgICAgICAgdGhpcy52YWx1ZSA9IDA7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLm9uVmFsdWVDaGFuZ2VkLmVtaXQodGhpcy52YWx1ZSk7XG4gICAgfVxufVxuIl19