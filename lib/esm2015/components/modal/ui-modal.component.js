import * as tslib_1 from "tslib";
import { Component, Inject, ViewContainerRef, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { UiModalService } from './ui-modal.service';
let UiModalComponent = class UiModalComponent {
    constructor(modal, componentFactoryResolver, componentData) {
        this.modal = modal;
        this.componentFactoryResolver = componentFactoryResolver;
        this.componentData = componentData;
        this.viewModel = {
            title: this.componentData.title
        };
    }
    ngOnInit() {
        this.componentData.childComponents.forEach((component) => {
            const componentCreated = this.viewContainerRef.createComponent(this.componentFactoryResolver.resolveComponentFactory(component));
            componentCreated.changeDetectorRef.detectChanges();
        });
    }
    closeModal() {
        this.modal.close();
    }
};
tslib_1.__decorate([
    ViewChild('childrenContainer', { read: ViewContainerRef, static: true }),
    tslib_1.__metadata("design:type", ViewContainerRef)
], UiModalComponent.prototype, "viewContainerRef", void 0);
UiModalComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-modal',
        template: "<div class=\"ui-modal\">\n    <div class=\"ui-modal__content\">\n        <div class=\"ui-modal__header\">\n            <h2 class=\"ui-modal__header-title\">{{viewModel.title}}</h2>\n            <button class=\"ui-modal__header-button\" (click)=\"closeModal()\">\n                <i class=\"huub-material-icon\" icon=\"close\"></i>\n           </button>\n        </div>\n        <div class=\"ui-modal__body\">\n            <ng-template #childrenContainer></ng-template>\n        </div>\n    </div>\n</div>\n"
    }),
    tslib_1.__param(2, Inject('MODAL_DATA')),
    tslib_1.__metadata("design:paramtypes", [UiModalService,
        ComponentFactoryResolver, Object])
], UiModalComponent);
export { UiModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktbW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaHV1Yi1tYXRlcmlhbC9saWIvIiwic291cmNlcyI6WyJjb21wb25lbnRzL21vZGFsL3VpLW1vZGFsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQVUsZ0JBQWdCLEVBQUUsd0JBQXdCLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2pILE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQU1wRCxJQUFhLGdCQUFnQixHQUE3QixNQUFhLGdCQUFnQjtJQVV6QixZQUNZLEtBQXFCLEVBQ3JCLHdCQUFrRCxFQUM1QixhQUFhO1FBRm5DLFVBQUssR0FBTCxLQUFLLENBQWdCO1FBQ3JCLDZCQUF3QixHQUF4Qix3QkFBd0IsQ0FBMEI7UUFDNUIsa0JBQWEsR0FBYixhQUFhLENBQUE7UUFaeEMsY0FBUyxHQUVaO1lBQ0EsS0FBSyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSztTQUNsQyxDQUFDO0lBU0MsQ0FBQztJQUVHLFFBQVE7UUFDWCxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBRTtZQUNyRCxNQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQzFELElBQUksQ0FBQyx3QkFBd0IsQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLENBQUMsQ0FDbkUsQ0FBQztZQUNGLGdCQUFnQixDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3ZELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLFVBQVU7UUFDYixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3ZCLENBQUM7Q0FDSixDQUFBO0FBcEJHO0lBREMsU0FBUyxDQUFDLG1CQUFtQixFQUFFLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQztzQ0FDL0MsZ0JBQWdCOzBEQUFDO0FBUmxDLGdCQUFnQjtJQUo1QixTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsVUFBVTtRQUNwQixzZ0JBQXdDO0tBQzNDLENBQUM7SUFjTyxtQkFBQSxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUE7NkNBRk4sY0FBYztRQUNLLHdCQUF3QjtHQVpyRCxnQkFBZ0IsQ0E0QjVCO1NBNUJZLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5qZWN0LCBPbkluaXQsIFZpZXdDb250YWluZXJSZWYsIENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBVaU1vZGFsU2VydmljZSB9IGZyb20gJy4vdWktbW9kYWwuc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAndWktbW9kYWwnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi91aS1tb2RhbC5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgVWlNb2RhbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgcHVibGljIHZpZXdNb2RlbDoge1xuICAgICAgICB0aXRsZTogc3RyaW5nO1xuICAgIH0gPSB7XG4gICAgICAgIHRpdGxlOiB0aGlzLmNvbXBvbmVudERhdGEudGl0bGVcbiAgICB9O1xuXG4gICAgQFZpZXdDaGlsZCgnY2hpbGRyZW5Db250YWluZXInLCB7IHJlYWQ6IFZpZXdDb250YWluZXJSZWYsIHN0YXRpYzogdHJ1ZSB9KVxuICAgIHByaXZhdGUgdmlld0NvbnRhaW5lclJlZjogVmlld0NvbnRhaW5lclJlZjtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIG1vZGFsOiBVaU1vZGFsU2VydmljZSxcbiAgICAgICAgcHJpdmF0ZSBjb21wb25lbnRGYWN0b3J5UmVzb2x2ZXI6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlcixcbiAgICAgICAgQEluamVjdCgnTU9EQUxfREFUQScpIHByaXZhdGUgY29tcG9uZW50RGF0YVxuICAgICkge31cblxuICAgIHB1YmxpYyBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy5jb21wb25lbnREYXRhLmNoaWxkQ29tcG9uZW50cy5mb3JFYWNoKChjb21wb25lbnQpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IGNvbXBvbmVudENyZWF0ZWQgPSB0aGlzLnZpZXdDb250YWluZXJSZWYuY3JlYXRlQ29tcG9uZW50KFxuICAgICAgICAgICAgICAgIHRoaXMuY29tcG9uZW50RmFjdG9yeVJlc29sdmVyLnJlc29sdmVDb21wb25lbnRGYWN0b3J5KGNvbXBvbmVudClcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgICBjb21wb25lbnRDcmVhdGVkLmNoYW5nZURldGVjdG9yUmVmLmRldGVjdENoYW5nZXMoKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHVibGljIGNsb3NlTW9kYWwoKSB7XG4gICAgICAgIHRoaXMubW9kYWwuY2xvc2UoKTtcbiAgICB9XG59XG4iXX0=