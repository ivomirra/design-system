import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
let UiFilterTagsComponent = class UiFilterTagsComponent {
    constructor() {
        /** Property that should be used for binding a callback to receive the
         input submission **/
        this.onLabelRemoved = new EventEmitter();
    }
    /**
     * Method bound to the form submit event.
     */
    onLabelRemovedClick(labelIndex) {
        if (!this.viewModel.items[labelIndex]) {
            return;
        }
        const removedLabel = this.viewModel.items[labelIndex];
        this.viewModel.items.splice(labelIndex, 1);
        this.onLabelRemoved.emit({
            removedLabel,
            removedIndex: labelIndex,
            items: this.viewModel.items
        });
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiFilterTagsComponent.prototype, "viewModel", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], UiFilterTagsComponent.prototype, "onLabelRemoved", void 0);
UiFilterTagsComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-filter-tags',
        template: "<label *ngFor=\"let label of viewModel.items; let labelIndex = index;\"\n        class=\"ui-filter-tags__label\">\n        <span class=\"ui-filter-tags__span\">{{label.text}}</span>\n        <i class=\"huub-material-icon ui-filter-tags__icon\"\n            icon=\"close\"\n            size=\"extra-extra-small\"\n            (click)=\"onLabelRemovedClick(labelIndex)\"></i>\n</label>\n"
    }),
    tslib_1.__metadata("design:paramtypes", [])
], UiFilterTagsComponent);
export { UiFilterTagsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktZmlsdGVyLXRhZ3MuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaHV1Yi1tYXRlcmlhbC9saWIvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2ZpbHRlci10YWdzL3VpLWZpbHRlci10YWdzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU12RSxJQUFhLHFCQUFxQixHQUFsQyxNQUFhLHFCQUFxQjtJQWdCOUI7UUFUQTs2QkFDcUI7UUFFZCxtQkFBYyxHQUloQixJQUFJLFlBQVksRUFBRSxDQUFDO0lBRVQsQ0FBQztJQUVoQjs7T0FFRztJQUNJLG1CQUFtQixDQUFDLFVBQWtCO1FBQ3pDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsRUFBRTtZQUNuQyxPQUFPO1NBQ1Y7UUFFRCxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUV0RCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBRTNDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO1lBQ3JCLFlBQVk7WUFDWixZQUFZLEVBQUUsVUFBVTtZQUN4QixLQUFLLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLO1NBQzlCLENBQUMsQ0FBQztJQUNQLENBQUM7Q0FDSixDQUFBO0FBakNHO0lBREMsS0FBSyxFQUFFOzt3REFHTjtBQUtGO0lBREMsTUFBTSxFQUFFO3NDQUNjLFlBQVk7NkRBSVg7QUFkZixxQkFBcUI7SUFKakMsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLGdCQUFnQjtRQUMxQiw2WUFBOEM7S0FDakQsQ0FBQzs7R0FDVyxxQkFBcUIsQ0FvQ2pDO1NBcENZLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAndWktZmlsdGVyLXRhZ3MnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi91aS1maWx0ZXItdGFncy5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgVWlGaWx0ZXJUYWdzQ29tcG9uZW50IHtcbiAgICAvKiogUHJvcGVydHkgdGhhdCBkZWZpbmVzIHRoZSB2aWV3TW9kZWwgdG8gYmUgcmVuZGVyZWQgYnkgdWktZmlsdGVyLXRhZ3MgKiovXG4gICAgQElucHV0KClcbiAgICBwdWJsaWMgdmlld01vZGVsOiB7XG4gICAgICAgIGl0ZW1zOiBvYmplY3RbXTtcbiAgICB9O1xuXG4gICAgLyoqIFByb3BlcnR5IHRoYXQgc2hvdWxkIGJlIHVzZWQgZm9yIGJpbmRpbmcgYSBjYWxsYmFjayB0byByZWNlaXZlIHRoZVxuICAgICBpbnB1dCBzdWJtaXNzaW9uICoqL1xuICAgIEBPdXRwdXQoKVxuICAgIHB1YmxpYyBvbkxhYmVsUmVtb3ZlZDogRXZlbnRFbWl0dGVyPHtcbiAgICAgICAgcmVtb3ZlZExhYmVsOiBvYmplY3Q7XG4gICAgICAgIHJlbW92ZWRJbmRleDogbnVtYmVyO1xuICAgICAgICBpdGVtczogb2JqZWN0W107XG4gICAgfT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgICAvKipcbiAgICAgKiBNZXRob2QgYm91bmQgdG8gdGhlIGZvcm0gc3VibWl0IGV2ZW50LlxuICAgICAqL1xuICAgIHB1YmxpYyBvbkxhYmVsUmVtb3ZlZENsaWNrKGxhYmVsSW5kZXg6IG51bWJlcik6IHZvaWQge1xuICAgICAgICBpZiAoIXRoaXMudmlld01vZGVsLml0ZW1zW2xhYmVsSW5kZXhdKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCByZW1vdmVkTGFiZWwgPSB0aGlzLnZpZXdNb2RlbC5pdGVtc1tsYWJlbEluZGV4XTtcblxuICAgICAgICB0aGlzLnZpZXdNb2RlbC5pdGVtcy5zcGxpY2UobGFiZWxJbmRleCwgMSk7XG5cbiAgICAgICAgdGhpcy5vbkxhYmVsUmVtb3ZlZC5lbWl0KHtcbiAgICAgICAgICAgIHJlbW92ZWRMYWJlbCxcbiAgICAgICAgICAgIHJlbW92ZWRJbmRleDogbGFiZWxJbmRleCxcbiAgICAgICAgICAgIGl0ZW1zOiB0aGlzLnZpZXdNb2RlbC5pdGVtc1xuICAgICAgICB9KTtcbiAgICB9XG59XG4iXX0=