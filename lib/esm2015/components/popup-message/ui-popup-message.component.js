import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
let UiPopupMessageComponent = class UiPopupMessageComponent {
    constructor() { }
    hidePopup() {
        this.viewModel.isVisible = false;
    }
    onButtonClicked(option) {
        if (option && option.callback) {
            option.callback(option);
        }
        this.hidePopup();
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiPopupMessageComponent.prototype, "viewModel", void 0);
UiPopupMessageComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-popup-message',
        template: "<div class=\"ui-popup\" *ngIf=\"viewModel.isVisible\">\n    <div class=\"ui-popup__overlay ui-popup__overlay--visible\"\n         (click)=\"hidePopup()\">\n    </div>\n    <div class=\"ui-popup__content\">\n        <div class=\"ui-popup__header\">\n            <h2 class=\"ui-popup__header-title\">{{viewModel.title}}</h2>\n        </div>\n        <div class=\"ui-popup__body\" [innerHTML]=\"viewModel.message\"></div>\n        <div class=\"ui-popup__footer\">\n            <button *ngIf=\"viewModel.cancel.button_text!== ''\"\n                    type=\"tertiary\"\n                    class=\"ui-popup__cancel\"\n                    (click)=\"onButtonClicked(viewModel.cancel)\">\n                {{viewModel.cancel.button_text}}\n            </button>\n            <button  *ngIf=\"viewModel.submit.button_text!== ''\"\n                     type=\"secondary\"\n                     (click)=\"onButtonClicked(viewModel.submit)\">\n                {{viewModel.submit.button_text}}\n            </button>\n        </div>\n    </div>\n</div>\n"
    }),
    tslib_1.__metadata("design:paramtypes", [])
], UiPopupMessageComponent);
export { UiPopupMessageComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktcG9wdXAtbWVzc2FnZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvcG9wdXAtbWVzc2FnZS91aS1wb3B1cC1tZXNzYWdlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFNakQsSUFBYSx1QkFBdUIsR0FBcEMsTUFBYSx1QkFBdUI7SUFnQmhDLGdCQUFlLENBQUM7SUFFVCxTQUFTO1FBQ1osSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO0lBQ3JDLENBQUM7SUFFTSxlQUFlLENBQUMsTUFBK0I7UUFDbEQsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLFFBQVEsRUFBRTtZQUMzQixNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQzNCO1FBRUQsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ3JCLENBQUM7Q0FDSixDQUFBO0FBM0JHO0lBREMsS0FBSyxFQUFFOzswREFhTjtBQWRPLHVCQUF1QjtJQUpuQyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsa0JBQWtCO1FBQzVCLDhoQ0FBZ0Q7S0FDbkQsQ0FBQzs7R0FDVyx1QkFBdUIsQ0E2Qm5DO1NBN0JZLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICd1aS1wb3B1cC1tZXNzYWdlJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vdWktcG9wdXAtbWVzc2FnZS5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgVWlQb3B1cE1lc3NhZ2VDb21wb25lbnQge1xuICAgIEBJbnB1dCgpXG4gICAgcHVibGljIHZpZXdNb2RlbDoge1xuICAgICAgICBpc1Zpc2libGU6IGJvb2xlYW47XG4gICAgICAgIHRpdGxlOiBzdHJpbmc7XG4gICAgICAgIG1lc3NhZ2U6IHN0cmluZztcbiAgICAgICAgc3VibWl0OiB7XG4gICAgICAgICAgICBidXR0b25fdGV4dDogc3RyaW5nO1xuICAgICAgICAgICAgY2FsbGJhY2s6IEZ1bmN0aW9uO1xuICAgICAgICB9O1xuICAgICAgICBjYW5jZWw6IHtcbiAgICAgICAgICAgIGJ1dHRvbl90ZXh0OiBzdHJpbmc7XG4gICAgICAgICAgICBjYWxsYmFjazogRnVuY3Rpb247XG4gICAgICAgIH07XG4gICAgfTtcblxuICAgIGNvbnN0cnVjdG9yKCkge31cblxuICAgIHB1YmxpYyBoaWRlUG9wdXAoKTogdm9pZCB7XG4gICAgICAgIHRoaXMudmlld01vZGVsLmlzVmlzaWJsZSA9IGZhbHNlO1xuICAgIH1cblxuICAgIHB1YmxpYyBvbkJ1dHRvbkNsaWNrZWQob3B0aW9uPzogeyBjYWxsYmFjazogRnVuY3Rpb24gfSk6IHZvaWQge1xuICAgICAgICBpZiAob3B0aW9uICYmIG9wdGlvbi5jYWxsYmFjaykge1xuICAgICAgICAgICAgb3B0aW9uLmNhbGxiYWNrKG9wdGlvbik7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmhpZGVQb3B1cCgpO1xuICAgIH1cbn1cbiJdfQ==