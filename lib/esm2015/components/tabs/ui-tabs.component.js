import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
let UiTabsComponent = class UiTabsComponent {
    constructor() {
        /** Property that sould be used for binding a callback to receive the tab clicked **/
        this.onTabChanged = new EventEmitter();
    }
    ngOnInit() {
        this.viewModel.selectedIndex = this.viewModel.selectedIndex || 0;
    }
    /**
     * Method used to update the selected tab. This method also triggers
     * the eventEmitter for onTabChanged callback that the parent may have
     * configured.
     * selectedTab Current tab clicked
     * selectedTabIndex Current tab index clicked
     */
    onTabClicked(selectedTab, selectedTabIndex) {
        if (selectedTab.disabled) {
            return;
        }
        this.onTabChanged.emit({
            selectedTab,
            selectedTabIndex
        });
        this.viewModel.selectedIndex = selectedTabIndex;
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiTabsComponent.prototype, "viewModel", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], UiTabsComponent.prototype, "onTabChanged", void 0);
UiTabsComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-tabs',
        template: "<nav class=\"ui-tabs__nav\">\n    <ul class=\"ui-tabs__nav-items\">        \n        <li *ngFor=\"let item of viewModel.items; let itemIndex = index;\"\n            [ngClass]=\"{\n                'ui-tabs__nav-item': !(itemIndex === viewModel.selectedIndex) && !item.disabled,\n                'ui-tabs__nav-item--active': itemIndex === viewModel.selectedIndex,\n                'ui-tabs__nav-item--disabled': item.disabled\n            }\">\n            <a (click)=\"onTabClicked(item, itemIndex)\" class=\"ui-tabs__nav-link\" [innerHTML]=\"item.text\"></a>\n        </li>\n    </ul>\n</nav>\n"
    }),
    tslib_1.__metadata("design:paramtypes", [])
], UiTabsComponent);
export { UiTabsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktdGFicy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvdGFicy91aS10YWJzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBVSxNQUFNLGVBQWUsQ0FBQztBQU0vRSxJQUFhLGVBQWUsR0FBNUIsTUFBYSxlQUFlO0lBZXhCO1FBUEEscUZBQXFGO1FBRTlFLGlCQUFZLEdBR2QsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUVULENBQUM7SUFFVCxRQUFRO1FBQ1gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLElBQUksQ0FBQyxDQUFDO0lBQ3JFLENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDSSxZQUFZLENBQUMsV0FBZ0IsRUFBRSxnQkFBd0I7UUFDMUQsSUFBSSxXQUFXLENBQUMsUUFBUSxFQUFFO1lBQ3RCLE9BQU87U0FDVjtRQUVELElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO1lBQ25CLFdBQVc7WUFDWCxnQkFBZ0I7U0FDbkIsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEdBQUcsZ0JBQWdCLENBQUM7SUFDcEQsQ0FBQztDQUNKLENBQUE7QUFyQ0c7SUFEQyxLQUFLLEVBQUU7O2tEQUlOO0FBSUY7SUFEQyxNQUFNLEVBQUU7c0NBQ1ksWUFBWTtxREFHVDtBQWJmLGVBQWU7SUFKM0IsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLFNBQVM7UUFDbkIsOGxCQUF1QztLQUMxQyxDQUFDOztHQUNXLGVBQWUsQ0F3QzNCO1NBeENZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAndWktdGFicycsXG4gICAgdGVtcGxhdGVVcmw6ICcuL3VpLXRhYnMuY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFVpVGFic0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgLyoqIFByb3BlcnR5IHRoYXQgZGVmaW5lcyB0aGUgdmlld01vZGVsIHRvIGJlIHJlbmRlcmVkIGJ5IHVpLXRhYnMgKiovXG4gICAgQElucHV0KClcbiAgICBwdWJsaWMgdmlld01vZGVsOiB7XG4gICAgICAgIGl0ZW1zOiBvYmplY3RbXTtcbiAgICAgICAgc2VsZWN0ZWRJbmRleDogbnVtYmVyO1xuICAgIH07XG5cbiAgICAvKiogUHJvcGVydHkgdGhhdCBzb3VsZCBiZSB1c2VkIGZvciBiaW5kaW5nIGEgY2FsbGJhY2sgdG8gcmVjZWl2ZSB0aGUgdGFiIGNsaWNrZWQgKiovXG4gICAgQE91dHB1dCgpXG4gICAgcHVibGljIG9uVGFiQ2hhbmdlZDogRXZlbnRFbWl0dGVyPHtcbiAgICAgICAgc2VsZWN0ZWRUYWI6IG9iamVjdDtcbiAgICAgICAgc2VsZWN0ZWRUYWJJbmRleDogbnVtYmVyO1xuICAgIH0+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgY29uc3RydWN0b3IoKSB7fVxuXG4gICAgcHVibGljIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLnZpZXdNb2RlbC5zZWxlY3RlZEluZGV4ID0gdGhpcy52aWV3TW9kZWwuc2VsZWN0ZWRJbmRleCB8fCAwO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIE1ldGhvZCB1c2VkIHRvIHVwZGF0ZSB0aGUgc2VsZWN0ZWQgdGFiLiBUaGlzIG1ldGhvZCBhbHNvIHRyaWdnZXJzXG4gICAgICogdGhlIGV2ZW50RW1pdHRlciBmb3Igb25UYWJDaGFuZ2VkIGNhbGxiYWNrIHRoYXQgdGhlIHBhcmVudCBtYXkgaGF2ZVxuICAgICAqIGNvbmZpZ3VyZWQuXG4gICAgICogc2VsZWN0ZWRUYWIgQ3VycmVudCB0YWIgY2xpY2tlZFxuICAgICAqIHNlbGVjdGVkVGFiSW5kZXggQ3VycmVudCB0YWIgaW5kZXggY2xpY2tlZFxuICAgICAqL1xuICAgIHB1YmxpYyBvblRhYkNsaWNrZWQoc2VsZWN0ZWRUYWI6IGFueSwgc2VsZWN0ZWRUYWJJbmRleDogbnVtYmVyKTogdm9pZCB7XG4gICAgICAgIGlmIChzZWxlY3RlZFRhYi5kaXNhYmxlZCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5vblRhYkNoYW5nZWQuZW1pdCh7XG4gICAgICAgICAgICBzZWxlY3RlZFRhYixcbiAgICAgICAgICAgIHNlbGVjdGVkVGFiSW5kZXhcbiAgICAgICAgfSk7XG5cbiAgICAgICAgdGhpcy52aWV3TW9kZWwuc2VsZWN0ZWRJbmRleCA9IHNlbGVjdGVkVGFiSW5kZXg7XG4gICAgfVxufVxuIl19