import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
let UiAutoCompleteComponent = class UiAutoCompleteComponent {
    constructor() {
        this.searchString = '';
        this.selectedOptions = [];
        this.processedOptionsList = [];
        this.showOptionsList = false;
        this.filterTagsViewModel = {
            items: []
        };
        this.optionsList = [];
        this.multiple = true;
        this.defaultValues = [];
        this.hasError = false;
        this.disabled = false;
        this.onItemsChanged = new EventEmitter();
    }
    emitUpdates() {
        this.onItemsChanged.emit({
            selectedItems: this.selectedOptions
        });
    }
    ngOnInit() {
        this.processedOptionsList = this.optionsList;
        this.selectedOptions = this.defaultValues;
        if (this.multiple) {
            this.filterTagsViewModel.items = new Array().concat(this.selectedOptions);
        }
        if (!this.multiple) {
            this.searchString = this.defaultValues.length === 1 ? this.defaultValues[0].text : '';
        }
    }
    showOptionsMenu() {
        this.showOptionsList = true;
    }
    hideOptionsMenu() {
        this.showOptionsList = false;
    }
    searchValue(searchString) {
        this.processedOptionsList = this.optionsList.filter((item) => {
            return item.text.toLowerCase().includes(searchString.toLowerCase());
        });
    }
    clearSearch() {
        this.searchString = '';
        if (!this.multiple) {
            this.selectedOptions.length = 0;
            this.emitUpdates();
        }
    }
    onFilterTagRemoved(filterData) {
        this.selectedOptions.splice(filterData.removedIndex, 1);
        this.emitUpdates();
    }
    onOptionSelected(optionSelected) {
        this.hideOptionsMenu();
        if (this.multiple) {
            this.clearSearch();
            this.ngOnInit();
            this.selectedOptions.push(optionSelected);
            this.filterTagsViewModel.items.push(optionSelected);
        }
        else {
            this.searchString = optionSelected.text;
            this.selectedOptions[0] = optionSelected;
        }
        this.emitUpdates();
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Array)
], UiAutoCompleteComponent.prototype, "optionsList", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiAutoCompleteComponent.prototype, "multiple", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Array)
], UiAutoCompleteComponent.prototype, "defaultValues", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], UiAutoCompleteComponent.prototype, "placeholder", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiAutoCompleteComponent.prototype, "hasError", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], UiAutoCompleteComponent.prototype, "disabled", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], UiAutoCompleteComponent.prototype, "onItemsChanged", void 0);
UiAutoCompleteComponent = tslib_1.__decorate([
    Component({
        selector: 'ui-auto-complete',
        template: "<div class=\"ui-autocomplete\"\n    (mouseleave)=\"hideOptionsMenu()\">\n    <input type=\"text\"\n           class=\"ui-autocomplete__textbox\"\n           [attr.placeholder]=\"placeholder\"\n           (keyup)=\"searchValue(searchString)\"\n           (focus)=\"showOptionsMenu()\"\n           [(ngModel)]=\"searchString\"\n           [ngClass]=\"{'form-field__input--error': hasError}\"\n           [disabled]=\"disabled\">\n    <button *ngIf=\"searchString !== ''\"\n            class=\"ui-autocomplete__clear\"\n            (click)=\"clearSearch()\">\n        <i class=\"huub-material-icon\"\n            size=\"small\"\n            icon=\"close\"></i>\n    </button>\n    <ui-filter-tags *ngIf=\"selectedOptions.length > 0\"\n                    class=\"ui-autocomplete__filter-tags\"\n                    (onLabelRemoved)=\"onFilterTagRemoved($event)\"\n                    [viewModel]=\"filterTagsViewModel\"></ui-filter-tags>\n    <div class=\"ui-autocomplete__selectbox\"\n         [ngClass]=\"{'ui-autocomplete__selectbox--visible': showOptionsList}\">\n        <a class=\"ui-autocomplete__option\"\n             *ngFor=\"let option of processedOptionsList\"\n             (click)=\"onOptionSelected(option)\">{{option.text}}</a>\n    </div>\n</div>\n"
    }),
    tslib_1.__metadata("design:paramtypes", [])
], UiAutoCompleteComponent);
export { UiAutoCompleteComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktYXV0by1jb21wbGV0ZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvYXV0by1jb21wbGV0ZS91aS1hdXRvLWNvbXBsZXRlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU0vRSxJQUFhLHVCQUF1QixHQUFwQyxNQUFhLHVCQUF1QjtJQXVCaEM7UUF0Qk8saUJBQVksR0FBRyxFQUFFLENBQUM7UUFDbEIsb0JBQWUsR0FBUSxFQUFFLENBQUM7UUFDMUIseUJBQW9CLEdBQVEsRUFBRSxDQUFDO1FBQy9CLG9CQUFlLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLHdCQUFtQixHQUV0QjtZQUNBLEtBQUssRUFBRSxFQUFFO1NBQ1osQ0FBQztRQUVlLGdCQUFXLEdBQVUsRUFBRSxDQUFDO1FBQ3hCLGFBQVEsR0FBRyxJQUFJLENBQUM7UUFDaEIsa0JBQWEsR0FBVSxFQUFFLENBQUM7UUFFM0IsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBRzFCLG1CQUFjLEdBRWhCLElBQUksWUFBWSxFQUFFLENBQUM7SUFFVCxDQUFDO0lBRVIsV0FBVztRQUNmLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO1lBQ3JCLGFBQWEsRUFBRSxJQUFJLENBQUMsZUFBZTtTQUN0QyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sUUFBUTtRQUNYLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQzdDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUUxQyxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDZixJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxHQUFHLElBQUksS0FBSyxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUM3RTtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2hCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1NBQ3pGO0lBQ0wsQ0FBQztJQUVNLGVBQWU7UUFDbEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7SUFDaEMsQ0FBQztJQUVNLGVBQWU7UUFDbEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7SUFDakMsQ0FBQztJQUVNLFdBQVcsQ0FBQyxZQUFZO1FBQzNCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO1lBQ3pELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7UUFDeEUsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sV0FBVztRQUNkLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1FBRXZCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2hCLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUNoQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDdEI7SUFDTCxDQUFDO0lBRU0sa0JBQWtCLENBQUMsVUFBVTtRQUNoQyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBRXhELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRU0sZ0JBQWdCLENBQUMsY0FBYztRQUNsQyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFFdkIsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2YsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ25CLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNoQixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztTQUN2RDthQUFNO1lBQ0gsSUFBSSxDQUFDLFlBQVksR0FBRyxjQUFjLENBQUMsSUFBSSxDQUFDO1lBQ3hDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLEdBQUcsY0FBYyxDQUFDO1NBQzVDO1FBRUQsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7Q0FDSixDQUFBO0FBN0VZO0lBQVIsS0FBSyxFQUFFOzs0REFBaUM7QUFDaEM7SUFBUixLQUFLLEVBQUU7O3lEQUF5QjtBQUN4QjtJQUFSLEtBQUssRUFBRTs7OERBQW1DO0FBQ2xDO0lBQVIsS0FBSyxFQUFFOzs0REFBNEI7QUFDM0I7SUFBUixLQUFLLEVBQUU7O3lEQUF5QjtBQUN4QjtJQUFSLEtBQUssRUFBRTs7eURBQXlCO0FBR2pDO0lBREMsTUFBTSxFQUFFO3NDQUNjLFlBQVk7K0RBRVg7QUFyQmYsdUJBQXVCO0lBSm5DLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxrQkFBa0I7UUFDNUIsMHZDQUFnRDtLQUNuRCxDQUFDOztHQUNXLHVCQUF1QixDQXdGbkM7U0F4RlksdUJBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ3VpLWF1dG8tY29tcGxldGUnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi91aS1hdXRvLWNvbXBsZXRlLmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBVaUF1dG9Db21wbGV0ZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgcHVibGljIHNlYXJjaFN0cmluZyA9ICcnO1xuICAgIHB1YmxpYyBzZWxlY3RlZE9wdGlvbnM6IGFueSA9IFtdO1xuICAgIHB1YmxpYyBwcm9jZXNzZWRPcHRpb25zTGlzdDogYW55ID0gW107XG4gICAgcHVibGljIHNob3dPcHRpb25zTGlzdCA9IGZhbHNlO1xuICAgIHB1YmxpYyBmaWx0ZXJUYWdzVmlld01vZGVsOiB7XG4gICAgICAgIGl0ZW1zOiBhbnlbXTtcbiAgICB9ID0ge1xuICAgICAgICBpdGVtczogW11cbiAgICB9O1xuXG4gICAgQElucHV0KCkgcHJpdmF0ZSBvcHRpb25zTGlzdDogYW55W10gPSBbXTtcbiAgICBASW5wdXQoKSBwcml2YXRlIG11bHRpcGxlID0gdHJ1ZTtcbiAgICBASW5wdXQoKSBwcml2YXRlIGRlZmF1bHRWYWx1ZXM6IGFueVtdID0gW107XG4gICAgQElucHV0KCkgcHVibGljIHBsYWNlaG9sZGVyOiBzdHJpbmc7XG4gICAgQElucHV0KCkgcHVibGljIGhhc0Vycm9yID0gZmFsc2U7XG4gICAgQElucHV0KCkgcHVibGljIGRpc2FibGVkID0gZmFsc2U7XG5cbiAgICBAT3V0cHV0KClcbiAgICBwdWJsaWMgb25JdGVtc0NoYW5nZWQ6IEV2ZW50RW1pdHRlcjx7XG4gICAgICAgIHNlbGVjdGVkSXRlbXM6IGFueVtdO1xuICAgIH0+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgY29uc3RydWN0b3IoKSB7fVxuXG4gICAgcHJpdmF0ZSBlbWl0VXBkYXRlcygpIHtcbiAgICAgICAgdGhpcy5vbkl0ZW1zQ2hhbmdlZC5lbWl0KHtcbiAgICAgICAgICAgIHNlbGVjdGVkSXRlbXM6IHRoaXMuc2VsZWN0ZWRPcHRpb25zXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHB1YmxpYyBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy5wcm9jZXNzZWRPcHRpb25zTGlzdCA9IHRoaXMub3B0aW9uc0xpc3Q7XG4gICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb25zID0gdGhpcy5kZWZhdWx0VmFsdWVzO1xuXG4gICAgICAgIGlmICh0aGlzLm11bHRpcGxlKSB7XG4gICAgICAgICAgICB0aGlzLmZpbHRlclRhZ3NWaWV3TW9kZWwuaXRlbXMgPSBuZXcgQXJyYXkoKS5jb25jYXQodGhpcy5zZWxlY3RlZE9wdGlvbnMpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCF0aGlzLm11bHRpcGxlKSB7XG4gICAgICAgICAgICB0aGlzLnNlYXJjaFN0cmluZyA9IHRoaXMuZGVmYXVsdFZhbHVlcy5sZW5ndGggPT09IDEgPyB0aGlzLmRlZmF1bHRWYWx1ZXNbMF0udGV4dCA6ICcnO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHNob3dPcHRpb25zTWVudSgpIHtcbiAgICAgICAgdGhpcy5zaG93T3B0aW9uc0xpc3QgPSB0cnVlO1xuICAgIH1cblxuICAgIHB1YmxpYyBoaWRlT3B0aW9uc01lbnUoKSB7XG4gICAgICAgIHRoaXMuc2hvd09wdGlvbnNMaXN0ID0gZmFsc2U7XG4gICAgfVxuXG4gICAgcHVibGljIHNlYXJjaFZhbHVlKHNlYXJjaFN0cmluZykge1xuICAgICAgICB0aGlzLnByb2Nlc3NlZE9wdGlvbnNMaXN0ID0gdGhpcy5vcHRpb25zTGlzdC5maWx0ZXIoKGl0ZW0pID0+IHtcbiAgICAgICAgICAgIHJldHVybiBpdGVtLnRleHQudG9Mb3dlckNhc2UoKS5pbmNsdWRlcyhzZWFyY2hTdHJpbmcudG9Mb3dlckNhc2UoKSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHB1YmxpYyBjbGVhclNlYXJjaCgpIHtcbiAgICAgICAgdGhpcy5zZWFyY2hTdHJpbmcgPSAnJztcblxuICAgICAgICBpZiAoIXRoaXMubXVsdGlwbGUpIHtcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb25zLmxlbmd0aCA9IDA7XG4gICAgICAgICAgICB0aGlzLmVtaXRVcGRhdGVzKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgb25GaWx0ZXJUYWdSZW1vdmVkKGZpbHRlckRhdGEpIHtcbiAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbnMuc3BsaWNlKGZpbHRlckRhdGEucmVtb3ZlZEluZGV4LCAxKTtcblxuICAgICAgICB0aGlzLmVtaXRVcGRhdGVzKCk7XG4gICAgfVxuXG4gICAgcHVibGljIG9uT3B0aW9uU2VsZWN0ZWQob3B0aW9uU2VsZWN0ZWQpIHtcbiAgICAgICAgdGhpcy5oaWRlT3B0aW9uc01lbnUoKTtcblxuICAgICAgICBpZiAodGhpcy5tdWx0aXBsZSkge1xuICAgICAgICAgICAgdGhpcy5jbGVhclNlYXJjaCgpO1xuICAgICAgICAgICAgdGhpcy5uZ09uSW5pdCgpO1xuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbnMucHVzaChvcHRpb25TZWxlY3RlZCk7XG4gICAgICAgICAgICB0aGlzLmZpbHRlclRhZ3NWaWV3TW9kZWwuaXRlbXMucHVzaChvcHRpb25TZWxlY3RlZCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLnNlYXJjaFN0cmluZyA9IG9wdGlvblNlbGVjdGVkLnRleHQ7XG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9uc1swXSA9IG9wdGlvblNlbGVjdGVkO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5lbWl0VXBkYXRlcygpO1xuICAgIH1cbn1cbiJdfQ==