import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiProgressBarComponent } from './ui-progress-bar.component';
var UiProgressBarModule = /** @class */ (function () {
    function UiProgressBarModule() {
    }
    UiProgressBarModule = tslib_1.__decorate([
        NgModule({
            imports: [CommonModule],
            declarations: [UiProgressBarComponent],
            exports: [UiProgressBarComponent]
        })
    ], UiProgressBarModule);
    return UiProgressBarModule;
}());
export { UiProgressBarModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvcHJvZ3Jlc3MtYmFyL2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQU9yRTtJQUFBO0lBQWtDLENBQUM7SUFBdEIsbUJBQW1CO1FBTC9CLFFBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztZQUN2QixZQUFZLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQztZQUN0QyxPQUFPLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQztTQUNwQyxDQUFDO09BQ1csbUJBQW1CLENBQUc7SUFBRCwwQkFBQztDQUFBLEFBQW5DLElBQW1DO1NBQXRCLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5pbXBvcnQgeyBVaVByb2dyZXNzQmFyQ29tcG9uZW50IH0gZnJvbSAnLi91aS1wcm9ncmVzcy1iYXIuY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtVaVByb2dyZXNzQmFyQ29tcG9uZW50XSxcbiAgICBleHBvcnRzOiBbVWlQcm9ncmVzc0JhckNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgVWlQcm9ncmVzc0Jhck1vZHVsZSB7fVxuIl19