import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
var UiProgressBarComponent = /** @class */ (function () {
    function UiProgressBarComponent() {
        this.MAX_PERCENTAGE = 100;
        this.isCompleted = false;
    }
    UiProgressBarComponent.prototype.ngOnChanges = function (changes) {
        this.percentage =
            changes.percentage.currentValue > this.MAX_PERCENTAGE
                ? this.MAX_PERCENTAGE
                : changes.percentage.currentValue;
        this.isCompleted = this.percentage === this.MAX_PERCENTAGE;
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Number)
    ], UiProgressBarComponent.prototype, "percentage", void 0);
    UiProgressBarComponent = tslib_1.__decorate([
        Component({
            selector: 'ui-progress-bar',
            template: "<div class=\"ui-progress-bar\"\n     [ngClass]=\"{\n        'ui-progress-bar--complete': isCompleted\n     }\">\n    <div class=\"ui-progress-bar__foreground\"\n         [ngStyle]=\"{\n             'width': percentage + '%'\n         }\"></div>\n</div>\n"
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], UiProgressBarComponent);
    return UiProgressBarComponent;
}());
export { UiProgressBarComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktcHJvZ3Jlc3MtYmFyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2h1dWItbWF0ZXJpYWwvbGliLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9wcm9ncmVzcy1iYXIvdWktcHJvZ3Jlc3MtYmFyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQTRCLE1BQU0sZUFBZSxDQUFDO0FBTTNFO0lBTUk7UUFIUSxtQkFBYyxHQUFHLEdBQUcsQ0FBQztRQUN0QixnQkFBVyxHQUFHLEtBQUssQ0FBQztJQUVaLENBQUM7SUFFVCw0Q0FBVyxHQUFsQixVQUFtQixPQUFzQjtRQUNyQyxJQUFJLENBQUMsVUFBVTtZQUNYLE9BQU8sQ0FBQyxVQUFVLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjO2dCQUNqRCxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWM7Z0JBQ3JCLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQztRQUUxQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxVQUFVLEtBQUssSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUMvRCxDQUFDO0lBZFE7UUFBUixLQUFLLEVBQUU7OzhEQUEyQjtJQUQxQixzQkFBc0I7UUFKbEMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQiwwUUFBK0M7U0FDbEQsQ0FBQzs7T0FDVyxzQkFBc0IsQ0FnQmxDO0lBQUQsNkJBQUM7Q0FBQSxBQWhCRCxJQWdCQztTQWhCWSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkNoYW5nZXMsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICd1aS1wcm9ncmVzcy1iYXInLFxuICAgIHRlbXBsYXRlVXJsOiAnLi91aS1wcm9ncmVzcy1iYXIuY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFVpUHJvZ3Jlc3NCYXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkNoYW5nZXMge1xuICAgIEBJbnB1dCgpIHB1YmxpYyBwZXJjZW50YWdlOiBudW1iZXI7XG5cbiAgICBwcml2YXRlIE1BWF9QRVJDRU5UQUdFID0gMTAwO1xuICAgIHB1YmxpYyBpc0NvbXBsZXRlZCA9IGZhbHNlO1xuXG4gICAgY29uc3RydWN0b3IoKSB7fVxuXG4gICAgcHVibGljIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5wZXJjZW50YWdlID1cbiAgICAgICAgICAgIGNoYW5nZXMucGVyY2VudGFnZS5jdXJyZW50VmFsdWUgPiB0aGlzLk1BWF9QRVJDRU5UQUdFXG4gICAgICAgICAgICAgICAgPyB0aGlzLk1BWF9QRVJDRU5UQUdFXG4gICAgICAgICAgICAgICAgOiBjaGFuZ2VzLnBlcmNlbnRhZ2UuY3VycmVudFZhbHVlO1xuXG4gICAgICAgIHRoaXMuaXNDb21wbGV0ZWQgPSB0aGlzLnBlcmNlbnRhZ2UgPT09IHRoaXMuTUFYX1BFUkNFTlRBR0U7XG4gICAgfVxufVxuIl19