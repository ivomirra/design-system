import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiPaginationComponent } from './ui-pagination.component';
import { UiPaginationViewModelHelper } from './ui-pagination.view-model.helper';
var UiPaginationModule = /** @class */ (function () {
    function UiPaginationModule() {
    }
    UiPaginationModule = tslib_1.__decorate([
        NgModule({
            imports: [CommonModule],
            declarations: [UiPaginationComponent],
            providers: [UiPaginationViewModelHelper],
            exports: [UiPaginationComponent]
        })
    ], UiPaginationModule);
    return UiPaginationModule;
}());
export { UiPaginationModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvcGFnaW5hdGlvbi9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFL0MsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDbEUsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFRaEY7SUFBQTtJQUFpQyxDQUFDO0lBQXJCLGtCQUFrQjtRQU45QixRQUFRLENBQUM7WUFDTixPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7WUFDdkIsWUFBWSxFQUFFLENBQUMscUJBQXFCLENBQUM7WUFDckMsU0FBUyxFQUFFLENBQUMsMkJBQTJCLENBQUM7WUFDeEMsT0FBTyxFQUFFLENBQUMscUJBQXFCLENBQUM7U0FDbkMsQ0FBQztPQUNXLGtCQUFrQixDQUFHO0lBQUQseUJBQUM7Q0FBQSxBQUFsQyxJQUFrQztTQUFyQixrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcblxuaW1wb3J0IHsgVWlQYWdpbmF0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi91aS1wYWdpbmF0aW9uLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBVaVBhZ2luYXRpb25WaWV3TW9kZWxIZWxwZXIgfSBmcm9tICcuL3VpLXBhZ2luYXRpb24udmlldy1tb2RlbC5oZWxwZXInO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtDb21tb25Nb2R1bGVdLFxuICAgIGRlY2xhcmF0aW9uczogW1VpUGFnaW5hdGlvbkNvbXBvbmVudF0sXG4gICAgcHJvdmlkZXJzOiBbVWlQYWdpbmF0aW9uVmlld01vZGVsSGVscGVyXSxcbiAgICBleHBvcnRzOiBbVWlQYWdpbmF0aW9uQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBVaVBhZ2luYXRpb25Nb2R1bGUge31cbiJdfQ==