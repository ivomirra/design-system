import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { UI_PAGINATION_CONSTANT } from './ui-pagination.constant';
var UiPaginationComponent = /** @class */ (function () {
    function UiPaginationComponent() {
        this.paginationDirections = UI_PAGINATION_CONSTANT.DIRECTIONS;
        this.viewModel = {
            currentPage: 0,
            totalPages: 0,
            isPreviousDisabled: true,
            isNextDisabled: true,
            nextText: UI_PAGINATION_CONSTANT.DEFAULT_BUTTON_TEXT.NEXT,
            previousText: UI_PAGINATION_CONSTANT.DEFAULT_BUTTON_TEXT.PREVIOUS,
            canShowPagination: false,
            showingItemsOfTotalString: ''
        };
        this.onPageLinkClicked = new EventEmitter();
    }
    UiPaginationComponent.prototype.onPageLinkClick = function (pageDirection) {
        this.onPageLinkClicked.emit(pageDirection);
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], UiPaginationComponent.prototype, "viewModel", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", Object)
    ], UiPaginationComponent.prototype, "onPageLinkClicked", void 0);
    UiPaginationComponent = tslib_1.__decorate([
        Component({
            selector: 'ui-pagination',
            template: "<div class=\"ui-pagination-container\" *ngIf=\"viewModel.canShowPagination\">\n    <div class=\"ui-pagination-totals\">{{viewModel.showingItemsOfTotalString}}</div>\n    <div class=\"ui-pagination-links-container\">\n        <button class=\"ui-pagination-links\"\n            [disabled]=\"viewModel.isPreviousDisabled\"\n            (click)=\"onPageLinkClick(paginationDirections.PREVIOUS)\"\n            type=\"tertiary\"><i class=\"huub-material-icon ui-pagination-icon-left\" icon=\"chevron-left\"></i>{{viewModel.previousText}}</button>\n        <button class=\"ui-pagination-links\"\n            [disabled]=\"viewModel.isNextDisabled\"\n            (click)=\"onPageLinkClick(paginationDirections.NEXT)\"\n            type=\"tertiary\">{{viewModel.nextText}}<i class=\"huub-material-icon ui-pagination-icon-right\" icon=\"chevron-right\"></i></button>\n    </div>\n</div>\n",
            providers: []
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], UiPaginationComponent);
    return UiPaginationComponent;
}());
export { UiPaginationComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktcGFnaW5hdGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvcGFnaW5hdGlvbi91aS1wYWdpbmF0aW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV2RSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQU9sRTtJQUNJO1FBRU8seUJBQW9CLEdBQUcsc0JBQXNCLENBQUMsVUFBVSxDQUFDO1FBR3pELGNBQVMsR0FBaUM7WUFDN0MsV0FBVyxFQUFFLENBQUM7WUFDZCxVQUFVLEVBQUUsQ0FBQztZQUNiLGtCQUFrQixFQUFFLElBQUk7WUFDeEIsY0FBYyxFQUFFLElBQUk7WUFDcEIsUUFBUSxFQUFFLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLElBQUk7WUFDekQsWUFBWSxFQUFFLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLFFBQVE7WUFDakUsaUJBQWlCLEVBQUUsS0FBSztZQUN4Qix5QkFBeUIsRUFBRSxFQUFFO1NBQ2hDLENBQUM7UUFFZSxzQkFBaUIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO0lBaEJ6QyxDQUFDO0lBa0JULCtDQUFlLEdBQXRCLFVBQXVCLGFBQWE7UUFDaEMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBZkQ7UUFEQyxLQUFLLEVBQUU7OzREQVVOO0lBRVE7UUFBVCxNQUFNLEVBQUU7O29FQUErQztJQWpCL0MscUJBQXFCO1FBTGpDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxlQUFlO1lBQ3pCLHkzQkFBNkM7WUFDN0MsU0FBUyxFQUFFLEVBQUU7U0FDaEIsQ0FBQzs7T0FDVyxxQkFBcUIsQ0FzQmpDO0lBQUQsNEJBQUM7Q0FBQSxBQXRCRCxJQXNCQztTQXRCWSxxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUGFnaW5hdGlvblZpZXdNb2RlbEludGVyZmFjZSB9IGZyb20gJy4vdWktcGFnaW5hdGlvbi5pbnRlcmZhY2UnO1xuaW1wb3J0IHsgVUlfUEFHSU5BVElPTl9DT05TVEFOVCB9IGZyb20gJy4vdWktcGFnaW5hdGlvbi5jb25zdGFudCc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAndWktcGFnaW5hdGlvbicsXG4gICAgdGVtcGxhdGVVcmw6ICcuL3VpLXBhZ2luYXRpb24uY29tcG9uZW50Lmh0bWwnLFxuICAgIHByb3ZpZGVyczogW11cbn0pXG5leHBvcnQgY2xhc3MgVWlQYWdpbmF0aW9uQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgICBwdWJsaWMgcGFnaW5hdGlvbkRpcmVjdGlvbnMgPSBVSV9QQUdJTkFUSU9OX0NPTlNUQU5ULkRJUkVDVElPTlM7XG5cbiAgICBASW5wdXQoKVxuICAgIHB1YmxpYyB2aWV3TW9kZWw6IFBhZ2luYXRpb25WaWV3TW9kZWxJbnRlcmZhY2UgPSB7XG4gICAgICAgIGN1cnJlbnRQYWdlOiAwLFxuICAgICAgICB0b3RhbFBhZ2VzOiAwLFxuICAgICAgICBpc1ByZXZpb3VzRGlzYWJsZWQ6IHRydWUsXG4gICAgICAgIGlzTmV4dERpc2FibGVkOiB0cnVlLFxuICAgICAgICBuZXh0VGV4dDogVUlfUEFHSU5BVElPTl9DT05TVEFOVC5ERUZBVUxUX0JVVFRPTl9URVhULk5FWFQsXG4gICAgICAgIHByZXZpb3VzVGV4dDogVUlfUEFHSU5BVElPTl9DT05TVEFOVC5ERUZBVUxUX0JVVFRPTl9URVhULlBSRVZJT1VTLFxuICAgICAgICBjYW5TaG93UGFnaW5hdGlvbjogZmFsc2UsXG4gICAgICAgIHNob3dpbmdJdGVtc09mVG90YWxTdHJpbmc6ICcnXG4gICAgfTtcblxuICAgIEBPdXRwdXQoKSBwdWJsaWMgb25QYWdlTGlua0NsaWNrZWQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgICBwdWJsaWMgb25QYWdlTGlua0NsaWNrKHBhZ2VEaXJlY3Rpb24pIHtcbiAgICAgICAgdGhpcy5vblBhZ2VMaW5rQ2xpY2tlZC5lbWl0KHBhZ2VEaXJlY3Rpb24pO1xuICAgIH1cbn1cbiJdfQ==