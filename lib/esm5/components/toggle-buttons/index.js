import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiToggleButtonsComponent } from './ui-toggle-buttons.component';
var UiToggleButtonsModule = /** @class */ (function () {
    function UiToggleButtonsModule() {
    }
    UiToggleButtonsModule = tslib_1.__decorate([
        NgModule({
            imports: [CommonModule],
            declarations: [UiToggleButtonsComponent],
            exports: [UiToggleButtonsComponent]
        })
    ], UiToggleButtonsModule);
    return UiToggleButtonsModule;
}());
export { UiToggleButtonsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvdG9nZ2xlLWJ1dHRvbnMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBT3pFO0lBQUE7SUFBb0MsQ0FBQztJQUF4QixxQkFBcUI7UUFMakMsUUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO1lBQ3ZCLFlBQVksRUFBRSxDQUFDLHdCQUF3QixDQUFDO1lBQ3hDLE9BQU8sRUFBRSxDQUFDLHdCQUF3QixDQUFDO1NBQ3RDLENBQUM7T0FDVyxxQkFBcUIsQ0FBRztJQUFELDRCQUFDO0NBQUEsQUFBckMsSUFBcUM7U0FBeEIscUJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cbmltcG9ydCB7IFVpVG9nZ2xlQnV0dG9uc0NvbXBvbmVudCB9IGZyb20gJy4vdWktdG9nZ2xlLWJ1dHRvbnMuY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtVaVRvZ2dsZUJ1dHRvbnNDb21wb25lbnRdLFxuICAgIGV4cG9ydHM6IFtVaVRvZ2dsZUJ1dHRvbnNDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIFVpVG9nZ2xlQnV0dG9uc01vZHVsZSB7fVxuIl19