import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialHelpersModule } from '../../helpers/index';
import { UiTooltipsComponent } from './ui-tooltips.component';
var UiTooltipsModule = /** @class */ (function () {
    function UiTooltipsModule() {
    }
    UiTooltipsModule = tslib_1.__decorate([
        NgModule({
            imports: [CommonModule, MaterialHelpersModule],
            declarations: [UiTooltipsComponent],
            exports: [UiTooltipsComponent]
        })
    ], UiTooltipsModule);
    return UiTooltipsModule;
}());
export { UiTooltipsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvbWVzc2FnaW5nL3Rvb2x0aXBzL2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUU1RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQU85RDtJQUFBO0lBQStCLENBQUM7SUFBbkIsZ0JBQWdCO1FBTDVCLFFBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRSxDQUFDLFlBQVksRUFBRSxxQkFBcUIsQ0FBQztZQUM5QyxZQUFZLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQztZQUNuQyxPQUFPLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQztTQUNqQyxDQUFDO09BQ1csZ0JBQWdCLENBQUc7SUFBRCx1QkFBQztDQUFBLEFBQWhDLElBQWdDO1NBQW5CLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgTWF0ZXJpYWxIZWxwZXJzTW9kdWxlIH0gZnJvbSAnLi4vLi4vaGVscGVycy9pbmRleCc7XG5cbmltcG9ydCB7IFVpVG9vbHRpcHNDb21wb25lbnQgfSBmcm9tICcuL3VpLXRvb2x0aXBzLmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZSwgTWF0ZXJpYWxIZWxwZXJzTW9kdWxlXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtVaVRvb2x0aXBzQ29tcG9uZW50XSxcbiAgICBleHBvcnRzOiBbVWlUb29sdGlwc0NvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgVWlUb29sdGlwc01vZHVsZSB7fVxuIl19