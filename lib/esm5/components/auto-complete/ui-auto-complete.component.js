import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
var UiAutoCompleteComponent = /** @class */ (function () {
    function UiAutoCompleteComponent() {
        this.searchString = '';
        this.selectedOptions = [];
        this.processedOptionsList = [];
        this.showOptionsList = false;
        this.filterTagsViewModel = {
            items: []
        };
        this.optionsList = [];
        this.multiple = true;
        this.defaultValues = [];
        this.hasError = false;
        this.disabled = false;
        this.onItemsChanged = new EventEmitter();
    }
    UiAutoCompleteComponent.prototype.emitUpdates = function () {
        this.onItemsChanged.emit({
            selectedItems: this.selectedOptions
        });
    };
    UiAutoCompleteComponent.prototype.ngOnInit = function () {
        this.processedOptionsList = this.optionsList;
        this.selectedOptions = this.defaultValues;
        if (this.multiple) {
            this.filterTagsViewModel.items = new Array().concat(this.selectedOptions);
        }
        if (!this.multiple) {
            this.searchString = this.defaultValues.length === 1 ? this.defaultValues[0].text : '';
        }
    };
    UiAutoCompleteComponent.prototype.showOptionsMenu = function () {
        this.showOptionsList = true;
    };
    UiAutoCompleteComponent.prototype.hideOptionsMenu = function () {
        this.showOptionsList = false;
    };
    UiAutoCompleteComponent.prototype.searchValue = function (searchString) {
        this.processedOptionsList = this.optionsList.filter(function (item) {
            return item.text.toLowerCase().includes(searchString.toLowerCase());
        });
    };
    UiAutoCompleteComponent.prototype.clearSearch = function () {
        this.searchString = '';
        if (!this.multiple) {
            this.selectedOptions.length = 0;
            this.emitUpdates();
        }
    };
    UiAutoCompleteComponent.prototype.onFilterTagRemoved = function (filterData) {
        this.selectedOptions.splice(filterData.removedIndex, 1);
        this.emitUpdates();
    };
    UiAutoCompleteComponent.prototype.onOptionSelected = function (optionSelected) {
        this.hideOptionsMenu();
        if (this.multiple) {
            this.clearSearch();
            this.ngOnInit();
            this.selectedOptions.push(optionSelected);
            this.filterTagsViewModel.items.push(optionSelected);
        }
        else {
            this.searchString = optionSelected.text;
            this.selectedOptions[0] = optionSelected;
        }
        this.emitUpdates();
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Array)
    ], UiAutoCompleteComponent.prototype, "optionsList", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], UiAutoCompleteComponent.prototype, "multiple", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Array)
    ], UiAutoCompleteComponent.prototype, "defaultValues", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], UiAutoCompleteComponent.prototype, "placeholder", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], UiAutoCompleteComponent.prototype, "hasError", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], UiAutoCompleteComponent.prototype, "disabled", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], UiAutoCompleteComponent.prototype, "onItemsChanged", void 0);
    UiAutoCompleteComponent = tslib_1.__decorate([
        Component({
            selector: 'ui-auto-complete',
            template: "<div class=\"ui-autocomplete\"\n    (mouseleave)=\"hideOptionsMenu()\">\n    <input type=\"text\"\n           class=\"ui-autocomplete__textbox\"\n           [attr.placeholder]=\"placeholder\"\n           (keyup)=\"searchValue(searchString)\"\n           (focus)=\"showOptionsMenu()\"\n           [(ngModel)]=\"searchString\"\n           [ngClass]=\"{'form-field__input--error': hasError}\"\n           [disabled]=\"disabled\">\n    <button *ngIf=\"searchString !== ''\"\n            class=\"ui-autocomplete__clear\"\n            (click)=\"clearSearch()\">\n        <i class=\"huub-material-icon\"\n            size=\"small\"\n            icon=\"close\"></i>\n    </button>\n    <ui-filter-tags *ngIf=\"selectedOptions.length > 0\"\n                    class=\"ui-autocomplete__filter-tags\"\n                    (onLabelRemoved)=\"onFilterTagRemoved($event)\"\n                    [viewModel]=\"filterTagsViewModel\"></ui-filter-tags>\n    <div class=\"ui-autocomplete__selectbox\"\n         [ngClass]=\"{'ui-autocomplete__selectbox--visible': showOptionsList}\">\n        <a class=\"ui-autocomplete__option\"\n             *ngFor=\"let option of processedOptionsList\"\n             (click)=\"onOptionSelected(option)\">{{option.text}}</a>\n    </div>\n</div>\n"
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], UiAutoCompleteComponent);
    return UiAutoCompleteComponent;
}());
export { UiAutoCompleteComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktYXV0by1jb21wbGV0ZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvYXV0by1jb21wbGV0ZS91aS1hdXRvLWNvbXBsZXRlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU0vRTtJQXVCSTtRQXRCTyxpQkFBWSxHQUFHLEVBQUUsQ0FBQztRQUNsQixvQkFBZSxHQUFRLEVBQUUsQ0FBQztRQUMxQix5QkFBb0IsR0FBUSxFQUFFLENBQUM7UUFDL0Isb0JBQWUsR0FBRyxLQUFLLENBQUM7UUFDeEIsd0JBQW1CLEdBRXRCO1lBQ0EsS0FBSyxFQUFFLEVBQUU7U0FDWixDQUFDO1FBRWUsZ0JBQVcsR0FBVSxFQUFFLENBQUM7UUFDeEIsYUFBUSxHQUFHLElBQUksQ0FBQztRQUNoQixrQkFBYSxHQUFVLEVBQUUsQ0FBQztRQUUzQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFHMUIsbUJBQWMsR0FFaEIsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUVULENBQUM7SUFFUiw2Q0FBVyxHQUFuQjtRQUNJLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO1lBQ3JCLGFBQWEsRUFBRSxJQUFJLENBQUMsZUFBZTtTQUN0QyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sMENBQVEsR0FBZjtRQUNJLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQzdDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUUxQyxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDZixJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxHQUFHLElBQUksS0FBSyxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUM3RTtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2hCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1NBQ3pGO0lBQ0wsQ0FBQztJQUVNLGlEQUFlLEdBQXRCO1FBQ0ksSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7SUFDaEMsQ0FBQztJQUVNLGlEQUFlLEdBQXRCO1FBQ0ksSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7SUFDakMsQ0FBQztJQUVNLDZDQUFXLEdBQWxCLFVBQW1CLFlBQVk7UUFDM0IsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFVBQUMsSUFBSTtZQUNyRCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1FBQ3hFLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLDZDQUFXLEdBQWxCO1FBQ0ksSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7UUFFdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDaEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1lBQ2hDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUN0QjtJQUNMLENBQUM7SUFFTSxvREFBa0IsR0FBekIsVUFBMEIsVUFBVTtRQUNoQyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBRXhELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRU0sa0RBQWdCLEdBQXZCLFVBQXdCLGNBQWM7UUFDbEMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBRXZCLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNmLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNuQixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDaEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDMUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDdkQ7YUFBTTtZQUNILElBQUksQ0FBQyxZQUFZLEdBQUcsY0FBYyxDQUFDLElBQUksQ0FBQztZQUN4QyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxHQUFHLGNBQWMsQ0FBQztTQUM1QztRQUVELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBNUVRO1FBQVIsS0FBSyxFQUFFOztnRUFBaUM7SUFDaEM7UUFBUixLQUFLLEVBQUU7OzZEQUF5QjtJQUN4QjtRQUFSLEtBQUssRUFBRTs7a0VBQW1DO0lBQ2xDO1FBQVIsS0FBSyxFQUFFOztnRUFBNEI7SUFDM0I7UUFBUixLQUFLLEVBQUU7OzZEQUF5QjtJQUN4QjtRQUFSLEtBQUssRUFBRTs7NkRBQXlCO0lBR2pDO1FBREMsTUFBTSxFQUFFOzBDQUNjLFlBQVk7bUVBRVg7SUFyQmYsdUJBQXVCO1FBSm5DLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsMHZDQUFnRDtTQUNuRCxDQUFDOztPQUNXLHVCQUF1QixDQXdGbkM7SUFBRCw4QkFBQztDQUFBLEFBeEZELElBd0ZDO1NBeEZZLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICd1aS1hdXRvLWNvbXBsZXRlJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vdWktYXV0by1jb21wbGV0ZS5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgVWlBdXRvQ29tcGxldGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIHB1YmxpYyBzZWFyY2hTdHJpbmcgPSAnJztcbiAgICBwdWJsaWMgc2VsZWN0ZWRPcHRpb25zOiBhbnkgPSBbXTtcbiAgICBwdWJsaWMgcHJvY2Vzc2VkT3B0aW9uc0xpc3Q6IGFueSA9IFtdO1xuICAgIHB1YmxpYyBzaG93T3B0aW9uc0xpc3QgPSBmYWxzZTtcbiAgICBwdWJsaWMgZmlsdGVyVGFnc1ZpZXdNb2RlbDoge1xuICAgICAgICBpdGVtczogYW55W107XG4gICAgfSA9IHtcbiAgICAgICAgaXRlbXM6IFtdXG4gICAgfTtcblxuICAgIEBJbnB1dCgpIHByaXZhdGUgb3B0aW9uc0xpc3Q6IGFueVtdID0gW107XG4gICAgQElucHV0KCkgcHJpdmF0ZSBtdWx0aXBsZSA9IHRydWU7XG4gICAgQElucHV0KCkgcHJpdmF0ZSBkZWZhdWx0VmFsdWVzOiBhbnlbXSA9IFtdO1xuICAgIEBJbnB1dCgpIHB1YmxpYyBwbGFjZWhvbGRlcjogc3RyaW5nO1xuICAgIEBJbnB1dCgpIHB1YmxpYyBoYXNFcnJvciA9IGZhbHNlO1xuICAgIEBJbnB1dCgpIHB1YmxpYyBkaXNhYmxlZCA9IGZhbHNlO1xuXG4gICAgQE91dHB1dCgpXG4gICAgcHVibGljIG9uSXRlbXNDaGFuZ2VkOiBFdmVudEVtaXR0ZXI8e1xuICAgICAgICBzZWxlY3RlZEl0ZW1zOiBhbnlbXTtcbiAgICB9PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAgIGNvbnN0cnVjdG9yKCkge31cblxuICAgIHByaXZhdGUgZW1pdFVwZGF0ZXMoKSB7XG4gICAgICAgIHRoaXMub25JdGVtc0NoYW5nZWQuZW1pdCh7XG4gICAgICAgICAgICBzZWxlY3RlZEl0ZW1zOiB0aGlzLnNlbGVjdGVkT3B0aW9uc1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwdWJsaWMgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMucHJvY2Vzc2VkT3B0aW9uc0xpc3QgPSB0aGlzLm9wdGlvbnNMaXN0O1xuICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9ucyA9IHRoaXMuZGVmYXVsdFZhbHVlcztcblxuICAgICAgICBpZiAodGhpcy5tdWx0aXBsZSkge1xuICAgICAgICAgICAgdGhpcy5maWx0ZXJUYWdzVmlld01vZGVsLml0ZW1zID0gbmV3IEFycmF5KCkuY29uY2F0KHRoaXMuc2VsZWN0ZWRPcHRpb25zKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICghdGhpcy5tdWx0aXBsZSkge1xuICAgICAgICAgICAgdGhpcy5zZWFyY2hTdHJpbmcgPSB0aGlzLmRlZmF1bHRWYWx1ZXMubGVuZ3RoID09PSAxID8gdGhpcy5kZWZhdWx0VmFsdWVzWzBdLnRleHQgOiAnJztcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBzaG93T3B0aW9uc01lbnUoKSB7XG4gICAgICAgIHRoaXMuc2hvd09wdGlvbnNMaXN0ID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgaGlkZU9wdGlvbnNNZW51KCkge1xuICAgICAgICB0aGlzLnNob3dPcHRpb25zTGlzdCA9IGZhbHNlO1xuICAgIH1cblxuICAgIHB1YmxpYyBzZWFyY2hWYWx1ZShzZWFyY2hTdHJpbmcpIHtcbiAgICAgICAgdGhpcy5wcm9jZXNzZWRPcHRpb25zTGlzdCA9IHRoaXMub3B0aW9uc0xpc3QuZmlsdGVyKChpdGVtKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gaXRlbS50ZXh0LnRvTG93ZXJDYXNlKCkuaW5jbHVkZXMoc2VhcmNoU3RyaW5nLnRvTG93ZXJDYXNlKCkpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwdWJsaWMgY2xlYXJTZWFyY2goKSB7XG4gICAgICAgIHRoaXMuc2VhcmNoU3RyaW5nID0gJyc7XG5cbiAgICAgICAgaWYgKCF0aGlzLm11bHRpcGxlKSB7XG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9ucy5sZW5ndGggPSAwO1xuICAgICAgICAgICAgdGhpcy5lbWl0VXBkYXRlcygpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIG9uRmlsdGVyVGFnUmVtb3ZlZChmaWx0ZXJEYXRhKSB7XG4gICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb25zLnNwbGljZShmaWx0ZXJEYXRhLnJlbW92ZWRJbmRleCwgMSk7XG5cbiAgICAgICAgdGhpcy5lbWl0VXBkYXRlcygpO1xuICAgIH1cblxuICAgIHB1YmxpYyBvbk9wdGlvblNlbGVjdGVkKG9wdGlvblNlbGVjdGVkKSB7XG4gICAgICAgIHRoaXMuaGlkZU9wdGlvbnNNZW51KCk7XG5cbiAgICAgICAgaWYgKHRoaXMubXVsdGlwbGUpIHtcbiAgICAgICAgICAgIHRoaXMuY2xlYXJTZWFyY2goKTtcbiAgICAgICAgICAgIHRoaXMubmdPbkluaXQoKTtcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb25zLnB1c2gob3B0aW9uU2VsZWN0ZWQpO1xuICAgICAgICAgICAgdGhpcy5maWx0ZXJUYWdzVmlld01vZGVsLml0ZW1zLnB1c2gob3B0aW9uU2VsZWN0ZWQpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5zZWFyY2hTdHJpbmcgPSBvcHRpb25TZWxlY3RlZC50ZXh0O1xuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbnNbMF0gPSBvcHRpb25TZWxlY3RlZDtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuZW1pdFVwZGF0ZXMoKTtcbiAgICB9XG59XG4iXX0=