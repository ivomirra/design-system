import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UiFilterTagsModule } from '../filter-tags/index';
import { UiAutoCompleteComponent } from './ui-auto-complete.component';
var UiAutoCompleteModule = /** @class */ (function () {
    function UiAutoCompleteModule() {
    }
    UiAutoCompleteModule = tslib_1.__decorate([
        NgModule({
            imports: [CommonModule, FormsModule, UiFilterTagsModule],
            declarations: [UiAutoCompleteComponent],
            exports: [UiAutoCompleteComponent]
        })
    ], UiAutoCompleteModule);
    return UiAutoCompleteModule;
}());
export { UiAutoCompleteModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvYXV0by1jb21wbGV0ZS9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRTFELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBT3ZFO0lBQUE7SUFBbUMsQ0FBQztJQUF2QixvQkFBb0I7UUFMaEMsUUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsWUFBWSxFQUFFLFdBQVcsRUFBRSxrQkFBa0IsQ0FBQztZQUN4RCxZQUFZLEVBQUUsQ0FBQyx1QkFBdUIsQ0FBQztZQUN2QyxPQUFPLEVBQUUsQ0FBQyx1QkFBdUIsQ0FBQztTQUNyQyxDQUFDO09BQ1csb0JBQW9CLENBQUc7SUFBRCwyQkFBQztDQUFBLEFBQXBDLElBQW9DO1NBQXZCLG9CQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBVaUZpbHRlclRhZ3NNb2R1bGUgfSBmcm9tICcuLi9maWx0ZXItdGFncy9pbmRleCc7XG5cbmltcG9ydCB7IFVpQXV0b0NvbXBsZXRlQ29tcG9uZW50IH0gZnJvbSAnLi91aS1hdXRvLWNvbXBsZXRlLmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZSwgRm9ybXNNb2R1bGUsIFVpRmlsdGVyVGFnc01vZHVsZV0sXG4gICAgZGVjbGFyYXRpb25zOiBbVWlBdXRvQ29tcGxldGVDb21wb25lbnRdLFxuICAgIGV4cG9ydHM6IFtVaUF1dG9Db21wbGV0ZUNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgVWlBdXRvQ29tcGxldGVNb2R1bGUge31cbiJdfQ==