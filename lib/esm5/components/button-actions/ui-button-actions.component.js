import * as tslib_1 from "tslib";
import { Component, Input, ElementRef } from '@angular/core';
import { MaterialHelper } from '../helpers/material.helpers';
var UiButtonActionsComponent = /** @class */ (function () {
    function UiButtonActionsComponent(element, helper) {
        this.element = element;
        this.helper = helper;
        this.DEFAULT_TYPE = 'tertiary';
        this.ELEMENT_SIZE = 200;
        this.alignRight = false;
        this.alignLeft = false;
        this.type = this.DEFAULT_TYPE;
        this.optionContainerIsVisible = false;
    }
    UiButtonActionsComponent.prototype.showOptionContainerVisibility = function () {
        var absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, this.ELEMENT_SIZE);
        this.optionContainerIsVisible = true;
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
    };
    UiButtonActionsComponent.prototype.hideOptionContainerVisibility = function () {
        this.optionContainerIsVisible = false;
    };
    UiButtonActionsComponent.prototype.onLinkClicked = function (link) {
        if (!link.callback || typeof link.callback !== 'function') {
            return;
        }
        link.callback(link);
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], UiButtonActionsComponent.prototype, "viewModel", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], UiButtonActionsComponent.prototype, "type", void 0);
    UiButtonActionsComponent = tslib_1.__decorate([
        Component({
            selector: 'ui-button-actions',
            template: "<div class=\"ui-button-actions\"\n    (mouseleave)=\"hideOptionContainerVisibility()\">\n    <button [attr.type]=\"type\"\n            role=\"icon\"\n            (click)=\"showOptionContainerVisibility()\"></button>\n    <div class=\"ui-button-actions__actions-list\"\n        [ngClass]=\"{\n            'ui-button-actions__actions-list--visible': optionContainerIsVisible,\n            'ui-button-actions__actions-list--right': alignRight,\n            'ui-button-actions__actions-list--left': alignLeft\n        }\"\n        (mouseleave)=\"hideOptionContainerVisibility()\">\n            <a *ngFor=\"let link of viewModel.items\"\n                class=\"ui-button-actions__actions-list-link\"\n                (click)=\"onLinkClicked(link)\">\n                {{link.text}}\n            </a>\n    </div>\n</div>\n"
        }),
        tslib_1.__metadata("design:paramtypes", [ElementRef, MaterialHelper])
    ], UiButtonActionsComponent);
    return UiButtonActionsComponent;
}());
export { UiButtonActionsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktYnV0dG9uLWFjdGlvbnMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaHV1Yi1tYXRlcmlhbC9saWIvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2J1dHRvbi1hY3Rpb25zL3VpLWJ1dHRvbi1hY3Rpb25zLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzdELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQU03RDtJQW9CSSxrQ0FBb0IsT0FBbUIsRUFBVSxNQUFzQjtRQUFuRCxZQUFPLEdBQVAsT0FBTyxDQUFZO1FBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFuQi9ELGlCQUFZLEdBQUcsVUFBVSxDQUFDO1FBQzFCLGlCQUFZLEdBQUcsR0FBRyxDQUFDO1FBQ3BCLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsY0FBUyxHQUFHLEtBQUssQ0FBQztRQVlULFNBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBRWxDLDZCQUF3QixHQUFHLEtBQUssQ0FBQztJQUVrQyxDQUFDO0lBRXBFLGdFQUE2QixHQUFwQztRQUNJLElBQU0sb0JBQW9CLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQywrQkFBK0IsQ0FDcEUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxJQUFJLEVBQ3ZELElBQUksQ0FBQyxZQUFZLENBQ3BCLENBQUM7UUFFRixJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxTQUFTLEdBQUcsb0JBQW9CLENBQUMsZUFBZSxDQUFDO1FBQ3RELElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQW9CLENBQUMsZ0JBQWdCLENBQUM7SUFDNUQsQ0FBQztJQUVNLGdFQUE2QixHQUFwQztRQUNJLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxLQUFLLENBQUM7SUFDMUMsQ0FBQztJQUVNLGdEQUFhLEdBQXBCLFVBQXFCLElBQUk7UUFDckIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksT0FBTyxJQUFJLENBQUMsUUFBUSxLQUFLLFVBQVUsRUFBRTtZQUN2RCxPQUFPO1NBQ1Y7UUFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3hCLENBQUM7SUFwQ0Q7UUFEQyxLQUFLLEVBQUU7OytEQVFOO0lBRU87UUFBUixLQUFLLEVBQUU7OzBEQUFpQztJQWhCaEMsd0JBQXdCO1FBSnBDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxtQkFBbUI7WUFDN0IsNHpCQUFpRDtTQUNwRCxDQUFDO2lEQXFCK0IsVUFBVSxFQUFrQixjQUFjO09BcEI5RCx3QkFBd0IsQ0E0Q3BDO0lBQUQsK0JBQUM7Q0FBQSxBQTVDRCxJQTRDQztTQTVDWSx3QkFBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBFbGVtZW50UmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNYXRlcmlhbEhlbHBlciB9IGZyb20gJy4uL2hlbHBlcnMvbWF0ZXJpYWwuaGVscGVycyc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAndWktYnV0dG9uLWFjdGlvbnMnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi91aS1idXR0b24tYWN0aW9ucy5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgVWlCdXR0b25BY3Rpb25zQ29tcG9uZW50IHtcbiAgICBwcml2YXRlIERFRkFVTFRfVFlQRSA9ICd0ZXJ0aWFyeSc7XG4gICAgcHJpdmF0ZSBFTEVNRU5UX1NJWkUgPSAyMDA7XG4gICAgcHVibGljIGFsaWduUmlnaHQgPSBmYWxzZTtcbiAgICBwdWJsaWMgYWxpZ25MZWZ0ID0gZmFsc2U7XG5cbiAgICBASW5wdXQoKVxuICAgIHB1YmxpYyB2aWV3TW9kZWw6IHtcbiAgICAgICAgaXRlbXM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICB0ZXh0OiBzdHJpbmc7XG4gICAgICAgICAgICAgICAgY2FsbGJhY2s/OiBGdW5jdGlvbjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgXTtcbiAgICB9O1xuXG4gICAgQElucHV0KCkgcHVibGljIHR5cGUgPSB0aGlzLkRFRkFVTFRfVFlQRTtcblxuICAgIHB1YmxpYyBvcHRpb25Db250YWluZXJJc1Zpc2libGUgPSBmYWxzZTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgZWxlbWVudDogRWxlbWVudFJlZiwgcHJpdmF0ZSBoZWxwZXI6IE1hdGVyaWFsSGVscGVyKSB7fVxuXG4gICAgcHVibGljIHNob3dPcHRpb25Db250YWluZXJWaXNpYmlsaXR5KCk6IHZvaWQge1xuICAgICAgICBjb25zdCBhYnNvbHV0ZVJlbmRlckNvbmZpZyA9IHRoaXMuaGVscGVyLmdldEFic29sdXRlUG9zaXRpb25SZW5kZXJDb25maWcoXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQubmF0aXZlRWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5sZWZ0LFxuICAgICAgICAgICAgdGhpcy5FTEVNRU5UX1NJWkVcbiAgICAgICAgKTtcblxuICAgICAgICB0aGlzLm9wdGlvbkNvbnRhaW5lcklzVmlzaWJsZSA9IHRydWU7XG4gICAgICAgIHRoaXMuYWxpZ25MZWZ0ID0gYWJzb2x1dGVSZW5kZXJDb25maWcuY2FuUmVuZGVyVG9MZWZ0O1xuICAgICAgICB0aGlzLmFsaWduUmlnaHQgPSBhYnNvbHV0ZVJlbmRlckNvbmZpZy5jYW5SZW5kZXJUb1JpZ2h0O1xuICAgIH1cblxuICAgIHB1YmxpYyBoaWRlT3B0aW9uQ29udGFpbmVyVmlzaWJpbGl0eSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5vcHRpb25Db250YWluZXJJc1Zpc2libGUgPSBmYWxzZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25MaW5rQ2xpY2tlZChsaW5rKSB7XG4gICAgICAgIGlmICghbGluay5jYWxsYmFjayB8fCB0eXBlb2YgbGluay5jYWxsYmFjayAhPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgbGluay5jYWxsYmFjayhsaW5rKTtcbiAgICB9XG59XG4iXX0=