import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialHelpersModule } from '../helpers/index';
import { UiButtonActionsComponent } from './ui-button-actions.component';
var UiButtonActionsModule = /** @class */ (function () {
    function UiButtonActionsModule() {
    }
    UiButtonActionsModule = tslib_1.__decorate([
        NgModule({
            imports: [CommonModule, MaterialHelpersModule],
            declarations: [UiButtonActionsComponent],
            exports: [UiButtonActionsComponent]
        })
    ], UiButtonActionsModule);
    return UiButtonActionsModule;
}());
export { UiButtonActionsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvYnV0dG9uLWFjdGlvbnMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRXpELE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBT3pFO0lBQUE7SUFBb0MsQ0FBQztJQUF4QixxQkFBcUI7UUFMakMsUUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsWUFBWSxFQUFFLHFCQUFxQixDQUFDO1lBQzlDLFlBQVksRUFBRSxDQUFDLHdCQUF3QixDQUFDO1lBQ3hDLE9BQU8sRUFBRSxDQUFDLHdCQUF3QixDQUFDO1NBQ3RDLENBQUM7T0FDVyxxQkFBcUIsQ0FBRztJQUFELDRCQUFDO0NBQUEsQUFBckMsSUFBcUM7U0FBeEIscUJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBNYXRlcmlhbEhlbHBlcnNNb2R1bGUgfSBmcm9tICcuLi9oZWxwZXJzL2luZGV4JztcblxuaW1wb3J0IHsgVWlCdXR0b25BY3Rpb25zQ29tcG9uZW50IH0gZnJvbSAnLi91aS1idXR0b24tYWN0aW9ucy5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtDb21tb25Nb2R1bGUsIE1hdGVyaWFsSGVscGVyc01vZHVsZV0sXG4gICAgZGVjbGFyYXRpb25zOiBbVWlCdXR0b25BY3Rpb25zQ29tcG9uZW50XSxcbiAgICBleHBvcnRzOiBbVWlCdXR0b25BY3Rpb25zQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBVaUJ1dHRvbkFjdGlvbnNNb2R1bGUge31cbiJdfQ==