import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiPopupMessageComponent } from './ui-popup-message.component';
var UiPopupMessageModule = /** @class */ (function () {
    function UiPopupMessageModule() {
    }
    UiPopupMessageModule = tslib_1.__decorate([
        NgModule({
            imports: [CommonModule],
            declarations: [UiPopupMessageComponent],
            exports: [UiPopupMessageComponent]
        })
    ], UiPopupMessageModule);
    return UiPopupMessageModule;
}());
export { UiPopupMessageModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvcG9wdXAtbWVzc2FnZS9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFL0MsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFPdkU7SUFBQTtJQUFtQyxDQUFDO0lBQXZCLG9CQUFvQjtRQUxoQyxRQUFRLENBQUM7WUFDTixPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7WUFDdkIsWUFBWSxFQUFFLENBQUMsdUJBQXVCLENBQUM7WUFDdkMsT0FBTyxFQUFFLENBQUMsdUJBQXVCLENBQUM7U0FDckMsQ0FBQztPQUNXLG9CQUFvQixDQUFHO0lBQUQsMkJBQUM7Q0FBQSxBQUFwQyxJQUFvQztTQUF2QixvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcblxuaW1wb3J0IHsgVWlQb3B1cE1lc3NhZ2VDb21wb25lbnQgfSBmcm9tICcuL3VpLXBvcHVwLW1lc3NhZ2UuY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtVaVBvcHVwTWVzc2FnZUNvbXBvbmVudF0sXG4gICAgZXhwb3J0czogW1VpUG9wdXBNZXNzYWdlQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBVaVBvcHVwTWVzc2FnZU1vZHVsZSB7fVxuIl19