import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
var UiPopupMessageComponent = /** @class */ (function () {
    function UiPopupMessageComponent() {
    }
    UiPopupMessageComponent.prototype.hidePopup = function () {
        this.viewModel.isVisible = false;
    };
    UiPopupMessageComponent.prototype.onButtonClicked = function (option) {
        if (option && option.callback) {
            option.callback(option);
        }
        this.hidePopup();
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], UiPopupMessageComponent.prototype, "viewModel", void 0);
    UiPopupMessageComponent = tslib_1.__decorate([
        Component({
            selector: 'ui-popup-message',
            template: "<div class=\"ui-popup\" *ngIf=\"viewModel.isVisible\">\n    <div class=\"ui-popup__overlay ui-popup__overlay--visible\"\n         (click)=\"hidePopup()\">\n    </div>\n    <div class=\"ui-popup__content\">\n        <div class=\"ui-popup__header\">\n            <h2 class=\"ui-popup__header-title\">{{viewModel.title}}</h2>\n        </div>\n        <div class=\"ui-popup__body\" [innerHTML]=\"viewModel.message\"></div>\n        <div class=\"ui-popup__footer\">\n            <button *ngIf=\"viewModel.cancel.button_text!== ''\"\n                    type=\"tertiary\"\n                    class=\"ui-popup__cancel\"\n                    (click)=\"onButtonClicked(viewModel.cancel)\">\n                {{viewModel.cancel.button_text}}\n            </button>\n            <button  *ngIf=\"viewModel.submit.button_text!== ''\"\n                     type=\"secondary\"\n                     (click)=\"onButtonClicked(viewModel.submit)\">\n                {{viewModel.submit.button_text}}\n            </button>\n        </div>\n    </div>\n</div>\n"
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], UiPopupMessageComponent);
    return UiPopupMessageComponent;
}());
export { UiPopupMessageComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktcG9wdXAtbWVzc2FnZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvcG9wdXAtbWVzc2FnZS91aS1wb3B1cC1tZXNzYWdlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFNakQ7SUFnQkk7SUFBZSxDQUFDO0lBRVQsMkNBQVMsR0FBaEI7UUFDSSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7SUFDckMsQ0FBQztJQUVNLGlEQUFlLEdBQXRCLFVBQXVCLE1BQStCO1FBQ2xELElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUU7WUFDM0IsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUMzQjtRQUVELElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBMUJEO1FBREMsS0FBSyxFQUFFOzs4REFhTjtJQWRPLHVCQUF1QjtRQUpuQyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsa0JBQWtCO1lBQzVCLDhoQ0FBZ0Q7U0FDbkQsQ0FBQzs7T0FDVyx1QkFBdUIsQ0E2Qm5DO0lBQUQsOEJBQUM7Q0FBQSxBQTdCRCxJQTZCQztTQTdCWSx1QkFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAndWktcG9wdXAtbWVzc2FnZScsXG4gICAgdGVtcGxhdGVVcmw6ICcuL3VpLXBvcHVwLW1lc3NhZ2UuY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFVpUG9wdXBNZXNzYWdlQ29tcG9uZW50IHtcbiAgICBASW5wdXQoKVxuICAgIHB1YmxpYyB2aWV3TW9kZWw6IHtcbiAgICAgICAgaXNWaXNpYmxlOiBib29sZWFuO1xuICAgICAgICB0aXRsZTogc3RyaW5nO1xuICAgICAgICBtZXNzYWdlOiBzdHJpbmc7XG4gICAgICAgIHN1Ym1pdDoge1xuICAgICAgICAgICAgYnV0dG9uX3RleHQ6IHN0cmluZztcbiAgICAgICAgICAgIGNhbGxiYWNrOiBGdW5jdGlvbjtcbiAgICAgICAgfTtcbiAgICAgICAgY2FuY2VsOiB7XG4gICAgICAgICAgICBidXR0b25fdGV4dDogc3RyaW5nO1xuICAgICAgICAgICAgY2FsbGJhY2s6IEZ1bmN0aW9uO1xuICAgICAgICB9O1xuICAgIH07XG5cbiAgICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgICBwdWJsaWMgaGlkZVBvcHVwKCk6IHZvaWQge1xuICAgICAgICB0aGlzLnZpZXdNb2RlbC5pc1Zpc2libGUgPSBmYWxzZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25CdXR0b25DbGlja2VkKG9wdGlvbj86IHsgY2FsbGJhY2s6IEZ1bmN0aW9uIH0pOiB2b2lkIHtcbiAgICAgICAgaWYgKG9wdGlvbiAmJiBvcHRpb24uY2FsbGJhY2spIHtcbiAgICAgICAgICAgIG9wdGlvbi5jYWxsYmFjayhvcHRpb24pO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5oaWRlUG9wdXAoKTtcbiAgICB9XG59XG4iXX0=