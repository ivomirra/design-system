import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiFilterTagsComponent } from './ui-filter-tags.component';
var UiFilterTagsModule = /** @class */ (function () {
    function UiFilterTagsModule() {
    }
    UiFilterTagsModule = tslib_1.__decorate([
        NgModule({
            imports: [CommonModule],
            declarations: [UiFilterTagsComponent],
            exports: [UiFilterTagsComponent]
        })
    ], UiFilterTagsModule);
    return UiFilterTagsModule;
}());
export { UiFilterTagsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZmlsdGVyLXRhZ3MvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBT25FO0lBQUE7SUFBaUMsQ0FBQztJQUFyQixrQkFBa0I7UUFMOUIsUUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO1lBQ3ZCLFlBQVksRUFBRSxDQUFDLHFCQUFxQixDQUFDO1lBQ3JDLE9BQU8sRUFBRSxDQUFDLHFCQUFxQixDQUFDO1NBQ25DLENBQUM7T0FDVyxrQkFBa0IsQ0FBRztJQUFELHlCQUFDO0NBQUEsQUFBbEMsSUFBa0M7U0FBckIsa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cbmltcG9ydCB7IFVpRmlsdGVyVGFnc0NvbXBvbmVudCB9IGZyb20gJy4vdWktZmlsdGVyLXRhZ3MuY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtVaUZpbHRlclRhZ3NDb21wb25lbnRdLFxuICAgIGV4cG9ydHM6IFtVaUZpbHRlclRhZ3NDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIFVpRmlsdGVyVGFnc01vZHVsZSB7fVxuIl19