import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
var UiFilterTagsComponent = /** @class */ (function () {
    function UiFilterTagsComponent() {
        /** Property that should be used for binding a callback to receive the
         input submission **/
        this.onLabelRemoved = new EventEmitter();
    }
    /**
     * Method bound to the form submit event.
     */
    UiFilterTagsComponent.prototype.onLabelRemovedClick = function (labelIndex) {
        if (!this.viewModel.items[labelIndex]) {
            return;
        }
        var removedLabel = this.viewModel.items[labelIndex];
        this.viewModel.items.splice(labelIndex, 1);
        this.onLabelRemoved.emit({
            removedLabel: removedLabel,
            removedIndex: labelIndex,
            items: this.viewModel.items
        });
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], UiFilterTagsComponent.prototype, "viewModel", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], UiFilterTagsComponent.prototype, "onLabelRemoved", void 0);
    UiFilterTagsComponent = tslib_1.__decorate([
        Component({
            selector: 'ui-filter-tags',
            template: "<label *ngFor=\"let label of viewModel.items; let labelIndex = index;\"\n        class=\"ui-filter-tags__label\">\n        <span class=\"ui-filter-tags__span\">{{label.text}}</span>\n        <i class=\"huub-material-icon ui-filter-tags__icon\"\n            icon=\"close\"\n            size=\"extra-extra-small\"\n            (click)=\"onLabelRemovedClick(labelIndex)\"></i>\n</label>\n"
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], UiFilterTagsComponent);
    return UiFilterTagsComponent;
}());
export { UiFilterTagsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktZmlsdGVyLXRhZ3MuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaHV1Yi1tYXRlcmlhbC9saWIvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2ZpbHRlci10YWdzL3VpLWZpbHRlci10YWdzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU12RTtJQWdCSTtRQVRBOzZCQUNxQjtRQUVkLG1CQUFjLEdBSWhCLElBQUksWUFBWSxFQUFFLENBQUM7SUFFVCxDQUFDO0lBRWhCOztPQUVHO0lBQ0ksbURBQW1CLEdBQTFCLFVBQTJCLFVBQWtCO1FBQ3pDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsRUFBRTtZQUNuQyxPQUFPO1NBQ1Y7UUFFRCxJQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUV0RCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBRTNDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO1lBQ3JCLFlBQVksY0FBQTtZQUNaLFlBQVksRUFBRSxVQUFVO1lBQ3hCLEtBQUssRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUs7U0FDOUIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQWhDRDtRQURDLEtBQUssRUFBRTs7NERBR047SUFLRjtRQURDLE1BQU0sRUFBRTswQ0FDYyxZQUFZO2lFQUlYO0lBZGYscUJBQXFCO1FBSmpDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxnQkFBZ0I7WUFDMUIsNllBQThDO1NBQ2pELENBQUM7O09BQ1cscUJBQXFCLENBb0NqQztJQUFELDRCQUFDO0NBQUEsQUFwQ0QsSUFvQ0M7U0FwQ1kscUJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICd1aS1maWx0ZXItdGFncycsXG4gICAgdGVtcGxhdGVVcmw6ICcuL3VpLWZpbHRlci10YWdzLmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBVaUZpbHRlclRhZ3NDb21wb25lbnQge1xuICAgIC8qKiBQcm9wZXJ0eSB0aGF0IGRlZmluZXMgdGhlIHZpZXdNb2RlbCB0byBiZSByZW5kZXJlZCBieSB1aS1maWx0ZXItdGFncyAqKi9cbiAgICBASW5wdXQoKVxuICAgIHB1YmxpYyB2aWV3TW9kZWw6IHtcbiAgICAgICAgaXRlbXM6IG9iamVjdFtdO1xuICAgIH07XG5cbiAgICAvKiogUHJvcGVydHkgdGhhdCBzaG91bGQgYmUgdXNlZCBmb3IgYmluZGluZyBhIGNhbGxiYWNrIHRvIHJlY2VpdmUgdGhlXG4gICAgIGlucHV0IHN1Ym1pc3Npb24gKiovXG4gICAgQE91dHB1dCgpXG4gICAgcHVibGljIG9uTGFiZWxSZW1vdmVkOiBFdmVudEVtaXR0ZXI8e1xuICAgICAgICByZW1vdmVkTGFiZWw6IG9iamVjdDtcbiAgICAgICAgcmVtb3ZlZEluZGV4OiBudW1iZXI7XG4gICAgICAgIGl0ZW1zOiBvYmplY3RbXTtcbiAgICB9PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAgIGNvbnN0cnVjdG9yKCkge31cblxuICAgIC8qKlxuICAgICAqIE1ldGhvZCBib3VuZCB0byB0aGUgZm9ybSBzdWJtaXQgZXZlbnQuXG4gICAgICovXG4gICAgcHVibGljIG9uTGFiZWxSZW1vdmVkQ2xpY2sobGFiZWxJbmRleDogbnVtYmVyKTogdm9pZCB7XG4gICAgICAgIGlmICghdGhpcy52aWV3TW9kZWwuaXRlbXNbbGFiZWxJbmRleF0pIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IHJlbW92ZWRMYWJlbCA9IHRoaXMudmlld01vZGVsLml0ZW1zW2xhYmVsSW5kZXhdO1xuXG4gICAgICAgIHRoaXMudmlld01vZGVsLml0ZW1zLnNwbGljZShsYWJlbEluZGV4LCAxKTtcblxuICAgICAgICB0aGlzLm9uTGFiZWxSZW1vdmVkLmVtaXQoe1xuICAgICAgICAgICAgcmVtb3ZlZExhYmVsLFxuICAgICAgICAgICAgcmVtb3ZlZEluZGV4OiBsYWJlbEluZGV4LFxuICAgICAgICAgICAgaXRlbXM6IHRoaXMudmlld01vZGVsLml0ZW1zXG4gICAgICAgIH0pO1xuICAgIH1cbn1cbiJdfQ==