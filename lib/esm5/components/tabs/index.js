import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiTabsComponent } from './ui-tabs.component';
var UiTabsModule = /** @class */ (function () {
    function UiTabsModule() {
    }
    UiTabsModule = tslib_1.__decorate([
        NgModule({
            imports: [CommonModule],
            declarations: [UiTabsComponent],
            exports: [UiTabsComponent]
        })
    ], UiTabsModule);
    return UiTabsModule;
}());
export { UiTabsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvdGFicy9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFL0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBT3REO0lBQUE7SUFBMkIsQ0FBQztJQUFmLFlBQVk7UUFMeEIsUUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO1lBQ3ZCLFlBQVksRUFBRSxDQUFDLGVBQWUsQ0FBQztZQUMvQixPQUFPLEVBQUUsQ0FBQyxlQUFlLENBQUM7U0FDN0IsQ0FBQztPQUNXLFlBQVksQ0FBRztJQUFELG1CQUFDO0NBQUEsQUFBNUIsSUFBNEI7U0FBZixZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cbmltcG9ydCB7IFVpVGFic0NvbXBvbmVudCB9IGZyb20gJy4vdWktdGFicy5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtDb21tb25Nb2R1bGVdLFxuICAgIGRlY2xhcmF0aW9uczogW1VpVGFic0NvbXBvbmVudF0sXG4gICAgZXhwb3J0czogW1VpVGFic0NvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgVWlUYWJzTW9kdWxlIHt9XG4iXX0=