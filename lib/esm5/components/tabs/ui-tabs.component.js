import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
var UiTabsComponent = /** @class */ (function () {
    function UiTabsComponent() {
        /** Property that sould be used for binding a callback to receive the tab clicked **/
        this.onTabChanged = new EventEmitter();
    }
    UiTabsComponent.prototype.ngOnInit = function () {
        this.viewModel.selectedIndex = this.viewModel.selectedIndex || 0;
    };
    /**
     * Method used to update the selected tab. This method also triggers
     * the eventEmitter for onTabChanged callback that the parent may have
     * configured.
     * selectedTab Current tab clicked
     * selectedTabIndex Current tab index clicked
     */
    UiTabsComponent.prototype.onTabClicked = function (selectedTab, selectedTabIndex) {
        if (selectedTab.disabled) {
            return;
        }
        this.onTabChanged.emit({
            selectedTab: selectedTab,
            selectedTabIndex: selectedTabIndex
        });
        this.viewModel.selectedIndex = selectedTabIndex;
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], UiTabsComponent.prototype, "viewModel", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], UiTabsComponent.prototype, "onTabChanged", void 0);
    UiTabsComponent = tslib_1.__decorate([
        Component({
            selector: 'ui-tabs',
            template: "<nav class=\"ui-tabs__nav\">\n    <ul class=\"ui-tabs__nav-items\">        \n        <li *ngFor=\"let item of viewModel.items; let itemIndex = index;\"\n            [ngClass]=\"{\n                'ui-tabs__nav-item': !(itemIndex === viewModel.selectedIndex) && !item.disabled,\n                'ui-tabs__nav-item--active': itemIndex === viewModel.selectedIndex,\n                'ui-tabs__nav-item--disabled': item.disabled\n            }\">\n            <a (click)=\"onTabClicked(item, itemIndex)\" class=\"ui-tabs__nav-link\" [innerHTML]=\"item.text\"></a>\n        </li>\n    </ul>\n</nav>\n"
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], UiTabsComponent);
    return UiTabsComponent;
}());
export { UiTabsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktdGFicy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvdGFicy91aS10YWJzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBVSxNQUFNLGVBQWUsQ0FBQztBQU0vRTtJQWVJO1FBUEEscUZBQXFGO1FBRTlFLGlCQUFZLEdBR2QsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUVULENBQUM7SUFFVCxrQ0FBUSxHQUFmO1FBQ0ksSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLElBQUksQ0FBQyxDQUFDO0lBQ3JFLENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDSSxzQ0FBWSxHQUFuQixVQUFvQixXQUFnQixFQUFFLGdCQUF3QjtRQUMxRCxJQUFJLFdBQVcsQ0FBQyxRQUFRLEVBQUU7WUFDdEIsT0FBTztTQUNWO1FBRUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7WUFDbkIsV0FBVyxhQUFBO1lBQ1gsZ0JBQWdCLGtCQUFBO1NBQ25CLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxHQUFHLGdCQUFnQixDQUFDO0lBQ3BELENBQUM7SUFwQ0Q7UUFEQyxLQUFLLEVBQUU7O3NEQUlOO0lBSUY7UUFEQyxNQUFNLEVBQUU7MENBQ1ksWUFBWTt5REFHVDtJQWJmLGVBQWU7UUFKM0IsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLFNBQVM7WUFDbkIsOGxCQUF1QztTQUMxQyxDQUFDOztPQUNXLGVBQWUsQ0F3QzNCO0lBQUQsc0JBQUM7Q0FBQSxBQXhDRCxJQXdDQztTQXhDWSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ3VpLXRhYnMnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi91aS10YWJzLmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBVaVRhYnNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIC8qKiBQcm9wZXJ0eSB0aGF0IGRlZmluZXMgdGhlIHZpZXdNb2RlbCB0byBiZSByZW5kZXJlZCBieSB1aS10YWJzICoqL1xuICAgIEBJbnB1dCgpXG4gICAgcHVibGljIHZpZXdNb2RlbDoge1xuICAgICAgICBpdGVtczogb2JqZWN0W107XG4gICAgICAgIHNlbGVjdGVkSW5kZXg6IG51bWJlcjtcbiAgICB9O1xuXG4gICAgLyoqIFByb3BlcnR5IHRoYXQgc291bGQgYmUgdXNlZCBmb3IgYmluZGluZyBhIGNhbGxiYWNrIHRvIHJlY2VpdmUgdGhlIHRhYiBjbGlja2VkICoqL1xuICAgIEBPdXRwdXQoKVxuICAgIHB1YmxpYyBvblRhYkNoYW5nZWQ6IEV2ZW50RW1pdHRlcjx7XG4gICAgICAgIHNlbGVjdGVkVGFiOiBvYmplY3Q7XG4gICAgICAgIHNlbGVjdGVkVGFiSW5kZXg6IG51bWJlcjtcbiAgICB9PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAgIGNvbnN0cnVjdG9yKCkge31cblxuICAgIHB1YmxpYyBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy52aWV3TW9kZWwuc2VsZWN0ZWRJbmRleCA9IHRoaXMudmlld01vZGVsLnNlbGVjdGVkSW5kZXggfHwgMDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBNZXRob2QgdXNlZCB0byB1cGRhdGUgdGhlIHNlbGVjdGVkIHRhYi4gVGhpcyBtZXRob2QgYWxzbyB0cmlnZ2Vyc1xuICAgICAqIHRoZSBldmVudEVtaXR0ZXIgZm9yIG9uVGFiQ2hhbmdlZCBjYWxsYmFjayB0aGF0IHRoZSBwYXJlbnQgbWF5IGhhdmVcbiAgICAgKiBjb25maWd1cmVkLlxuICAgICAqIHNlbGVjdGVkVGFiIEN1cnJlbnQgdGFiIGNsaWNrZWRcbiAgICAgKiBzZWxlY3RlZFRhYkluZGV4IEN1cnJlbnQgdGFiIGluZGV4IGNsaWNrZWRcbiAgICAgKi9cbiAgICBwdWJsaWMgb25UYWJDbGlja2VkKHNlbGVjdGVkVGFiOiBhbnksIHNlbGVjdGVkVGFiSW5kZXg6IG51bWJlcik6IHZvaWQge1xuICAgICAgICBpZiAoc2VsZWN0ZWRUYWIuZGlzYWJsZWQpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMub25UYWJDaGFuZ2VkLmVtaXQoe1xuICAgICAgICAgICAgc2VsZWN0ZWRUYWIsXG4gICAgICAgICAgICBzZWxlY3RlZFRhYkluZGV4XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMudmlld01vZGVsLnNlbGVjdGVkSW5kZXggPSBzZWxlY3RlZFRhYkluZGV4O1xuICAgIH1cbn1cbiJdfQ==