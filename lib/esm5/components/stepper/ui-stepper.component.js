import * as tslib_1 from "tslib";
import { Component, ViewContainerRef, ComponentFactoryResolver, ViewChild, Input } from '@angular/core';
var UiStepperComponent = /** @class */ (function () {
    function UiStepperComponent(componentFactoryResolver) {
        this.componentFactoryResolver = componentFactoryResolver;
        this.viewModel = {
            items: [],
            selectedIndex: 0
        };
    }
    UiStepperComponent.prototype.updateCurrentStep = function () {
        var selectedItem = this.viewModel.items[this.viewModel.selectedIndex];
        this.viewContainerRef.clear();
        this.currentComponent = this.viewContainerRef.createComponent(this.componentFactoryResolver.resolveComponentFactory(selectedItem.component));
        this.currentComponent.instance['stepContext'] = {
            goToNextStep: this.goToNextStep.bind(this),
            goToPreviousStep: this.goToPreviousStep.bind(this),
            componentContext: selectedItem.context
        };
        this.currentComponent.changeDetectorRef.detectChanges();
    };
    UiStepperComponent.prototype.ngOnInit = function () {
        this.updateCurrentStep();
    };
    UiStepperComponent.prototype.ngOnChanges = function () {
        this.updateCurrentStep();
    };
    UiStepperComponent.prototype.goToNextStep = function () {
        if (this.viewModel.selectedIndex === this.viewModel.items.length - 1) {
            throw new TypeError('You are trying to go to the next step on the last available step.');
        }
        this.viewModel.selectedIndex++;
        this.updateCurrentStep();
    };
    UiStepperComponent.prototype.goToPreviousStep = function () {
        if (this.viewModel.selectedIndex === 0) {
            throw new TypeError('You are trying to go to the previous step on the first available step.');
        }
        this.viewModel.selectedIndex--;
        this.updateCurrentStep();
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], UiStepperComponent.prototype, "viewModel", void 0);
    tslib_1.__decorate([
        ViewChild('currentStep', { read: ViewContainerRef, static: true }),
        tslib_1.__metadata("design:type", ViewContainerRef)
    ], UiStepperComponent.prototype, "viewContainerRef", void 0);
    UiStepperComponent = tslib_1.__decorate([
        Component({
            selector: 'ui-stepper',
            template: "<div class=\"ui-stepper\">\n    <div class=\"ui-stepper__content\">\n        <div class=\"ui-stepper__header\">\n            <div class=\"ui-stepper__header__item\" *ngFor=\"let item of viewModel.items; let index = index;\" [ngClass]=\"{'ui-stepper__header__item--active': index === viewModel.selectedIndex}\">\n                <div class=\"ui-stepper__header__item__content\" >\n                    <span class=\"ui-stepper__header__item__number\">{{index + 1}}</span> {{item.title}}</div>\n                <div class=\"ui-stepper__header__item__arrow\">\n                    <div class=\"ui-stepper__header__arrow-icon\"></div>\n                </div>\n            </div>\n        </div>\n        <div class=\"ui-stepper__body\">            \n            <ng-template #currentStep></ng-template>\n        </div>\n    </div>\n</div>"
        }),
        tslib_1.__metadata("design:paramtypes", [ComponentFactoryResolver])
    ], UiStepperComponent);
    return UiStepperComponent;
}());
export { UiStepperComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktc3RlcHBlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvc3RlcHBlci91aS1zdGVwcGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUNILFNBQVMsRUFHVCxnQkFBZ0IsRUFDaEIsd0JBQXdCLEVBQ3hCLFNBQVMsRUFDVCxLQUFLLEVBRVIsTUFBTSxlQUFlLENBQUM7QUFNdkI7SUFlSSw0QkFBb0Isd0JBQWtEO1FBQWxELDZCQUF3QixHQUF4Qix3QkFBd0IsQ0FBMEI7UUFiL0QsY0FBUyxHQUdaO1lBQ0EsS0FBSyxFQUFFLEVBQUU7WUFDVCxhQUFhLEVBQUUsQ0FBQztTQUNuQixDQUFDO0lBT3VFLENBQUM7SUFFbEUsOENBQWlCLEdBQXpCO1FBQ0ksSUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUN4RSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDOUIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQ3pELElBQUksQ0FBQyx3QkFBd0IsQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQ2hGLENBQUM7UUFDRixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxHQUFHO1lBQzVDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDbEQsZ0JBQWdCLEVBQUUsWUFBWSxDQUFDLE9BQU87U0FDekMsQ0FBQztRQUNGLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUM1RCxDQUFDO0lBRU0scUNBQVEsR0FBZjtRQUNJLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFTSx3Q0FBVyxHQUFsQjtRQUNJLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFTyx5Q0FBWSxHQUFwQjtRQUNJLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEtBQUssSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNsRSxNQUFNLElBQUksU0FBUyxDQUFDLG1FQUFtRSxDQUFDLENBQUM7U0FDNUY7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQy9CLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFTyw2Q0FBZ0IsR0FBeEI7UUFDSSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxLQUFLLENBQUMsRUFBRTtZQUNwQyxNQUFNLElBQUksU0FBUyxDQUFDLHdFQUF3RSxDQUFDLENBQUM7U0FDakc7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQy9CLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFuREQ7UUFEQyxLQUFLLEVBQUU7O3lEQU9OO0lBS0Y7UUFEQyxTQUFTLENBQUMsYUFBYSxFQUFFLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQzswQ0FDekMsZ0JBQWdCO2dFQUFDO0lBYmxDLGtCQUFrQjtRQUo5QixTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsWUFBWTtZQUN0Qiw2MEJBQTBDO1NBQzdDLENBQUM7aURBZ0JnRCx3QkFBd0I7T0FmN0Qsa0JBQWtCLENBc0Q5QjtJQUFELHlCQUFDO0NBQUEsQUF0REQsSUFzREM7U0F0RFksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgICBDb21wb25lbnQsXG4gICAgSW5qZWN0LFxuICAgIE9uSW5pdCxcbiAgICBWaWV3Q29udGFpbmVyUmVmLFxuICAgIENvbXBvbmVudEZhY3RvcnlSZXNvbHZlcixcbiAgICBWaWV3Q2hpbGQsXG4gICAgSW5wdXQsXG4gICAgT25DaGFuZ2VzXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ3VpLXN0ZXBwZXInLFxuICAgIHRlbXBsYXRlVXJsOiAnLi91aS1zdGVwcGVyLmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBVaVN0ZXBwZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XG4gICAgQElucHV0KClcbiAgICBwdWJsaWMgdmlld01vZGVsOiB7XG4gICAgICAgIGl0ZW1zOiBBcnJheTx7IHRpdGxlOiBzdHJpbmc7IGNvbXBvbmVudDogYW55OyBjb250ZXh0PzogYW55IH0+O1xuICAgICAgICBzZWxlY3RlZEluZGV4OiAwO1xuICAgIH0gPSB7XG4gICAgICAgIGl0ZW1zOiBbXSxcbiAgICAgICAgc2VsZWN0ZWRJbmRleDogMFxuICAgIH07XG5cbiAgICBwcml2YXRlIGN1cnJlbnRDb21wb25lbnQ6IGFueTtcblxuICAgIEBWaWV3Q2hpbGQoJ2N1cnJlbnRTdGVwJywgeyByZWFkOiBWaWV3Q29udGFpbmVyUmVmLCBzdGF0aWM6IHRydWUgfSlcbiAgICBwcml2YXRlIHZpZXdDb250YWluZXJSZWY6IFZpZXdDb250YWluZXJSZWY7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNvbXBvbmVudEZhY3RvcnlSZXNvbHZlcjogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyKSB7fVxuXG4gICAgcHJpdmF0ZSB1cGRhdGVDdXJyZW50U3RlcCgpIHtcbiAgICAgICAgY29uc3Qgc2VsZWN0ZWRJdGVtID0gdGhpcy52aWV3TW9kZWwuaXRlbXNbdGhpcy52aWV3TW9kZWwuc2VsZWN0ZWRJbmRleF07XG4gICAgICAgIHRoaXMudmlld0NvbnRhaW5lclJlZi5jbGVhcigpO1xuICAgICAgICB0aGlzLmN1cnJlbnRDb21wb25lbnQgPSB0aGlzLnZpZXdDb250YWluZXJSZWYuY3JlYXRlQ29tcG9uZW50KFxuICAgICAgICAgICAgdGhpcy5jb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIucmVzb2x2ZUNvbXBvbmVudEZhY3Rvcnkoc2VsZWN0ZWRJdGVtLmNvbXBvbmVudClcbiAgICAgICAgKTtcbiAgICAgICAgdGhpcy5jdXJyZW50Q29tcG9uZW50Lmluc3RhbmNlWydzdGVwQ29udGV4dCddID0ge1xuICAgICAgICAgICAgZ29Ub05leHRTdGVwOiB0aGlzLmdvVG9OZXh0U3RlcC5iaW5kKHRoaXMpLFxuICAgICAgICAgICAgZ29Ub1ByZXZpb3VzU3RlcDogdGhpcy5nb1RvUHJldmlvdXNTdGVwLmJpbmQodGhpcyksXG4gICAgICAgICAgICBjb21wb25lbnRDb250ZXh0OiBzZWxlY3RlZEl0ZW0uY29udGV4dFxuICAgICAgICB9O1xuICAgICAgICB0aGlzLmN1cnJlbnRDb21wb25lbnQuY2hhbmdlRGV0ZWN0b3JSZWYuZGV0ZWN0Q2hhbmdlcygpO1xuICAgIH1cblxuICAgIHB1YmxpYyBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy51cGRhdGVDdXJyZW50U3RlcCgpO1xuICAgIH1cblxuICAgIHB1YmxpYyBuZ09uQ2hhbmdlcygpIHtcbiAgICAgICAgdGhpcy51cGRhdGVDdXJyZW50U3RlcCgpO1xuICAgIH1cblxuICAgIHByaXZhdGUgZ29Ub05leHRTdGVwKCkge1xuICAgICAgICBpZiAodGhpcy52aWV3TW9kZWwuc2VsZWN0ZWRJbmRleCA9PT0gdGhpcy52aWV3TW9kZWwuaXRlbXMubGVuZ3RoIC0gMSkge1xuICAgICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignWW91IGFyZSB0cnlpbmcgdG8gZ28gdG8gdGhlIG5leHQgc3RlcCBvbiB0aGUgbGFzdCBhdmFpbGFibGUgc3RlcC4nKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnZpZXdNb2RlbC5zZWxlY3RlZEluZGV4Kys7XG4gICAgICAgIHRoaXMudXBkYXRlQ3VycmVudFN0ZXAoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdvVG9QcmV2aW91c1N0ZXAoKSB7XG4gICAgICAgIGlmICh0aGlzLnZpZXdNb2RlbC5zZWxlY3RlZEluZGV4ID09PSAwKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdZb3UgYXJlIHRyeWluZyB0byBnbyB0byB0aGUgcHJldmlvdXMgc3RlcCBvbiB0aGUgZmlyc3QgYXZhaWxhYmxlIHN0ZXAuJyk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy52aWV3TW9kZWwuc2VsZWN0ZWRJbmRleC0tO1xuICAgICAgICB0aGlzLnVwZGF0ZUN1cnJlbnRTdGVwKCk7XG4gICAgfVxufVxuIl19