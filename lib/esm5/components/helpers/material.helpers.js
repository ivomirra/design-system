import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
var MaterialHelper = /** @class */ (function () {
    function MaterialHelper() {
    }
    MaterialHelper.prototype.getAbsolutePositionRenderConfig = function (elementOffSet, elementSize) {
        return {
            canRenderToRight: window.innerWidth > elementOffSet + elementSize,
            canRenderToLeft: elementOffSet > elementSize
        };
    };
    MaterialHelper.prototype.merge = function () {
        var _this = this;
        var objects = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            objects[_i] = arguments[_i];
        }
        var isObject = function (obj) { return obj && typeof obj === 'object'; };
        var newObject = {};
        objects.forEach(function (obj) {
            Object.keys(obj).forEach(function (key) {
                var pVal = newObject[key];
                var oVal = obj[key];
                if (Array.isArray(oVal)) {
                    newObject[key] = new Array().concat(oVal);
                }
                else if (isObject(oVal)) {
                    newObject[key] = _this.merge(pVal || {}, oVal);
                }
                else {
                    newObject[key] = oVal;
                }
            });
        });
        return newObject;
    };
    MaterialHelper = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [])
    ], MaterialHelper);
    return MaterialHelper;
}());
export { MaterialHelper };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwuaGVscGVycy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2h1dWItbWF0ZXJpYWwvbGliLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9oZWxwZXJzL21hdGVyaWFsLmhlbHBlcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHM0M7SUFDSTtJQUFlLENBQUM7SUFFVCx3REFBK0IsR0FBdEMsVUFBdUMsYUFBcUIsRUFBRSxXQUFtQjtRQUM3RSxPQUFPO1lBQ0gsZ0JBQWdCLEVBQUUsTUFBTSxDQUFDLFVBQVUsR0FBRyxhQUFhLEdBQUcsV0FBVztZQUNqRSxlQUFlLEVBQUUsYUFBYSxHQUFHLFdBQVc7U0FDL0MsQ0FBQztJQUNOLENBQUM7SUFFTSw4QkFBSyxHQUFaO1FBQUEsaUJBb0JDO1FBcEJZLGlCQUFVO2FBQVYsVUFBVSxFQUFWLHFCQUFVLEVBQVYsSUFBVTtZQUFWLDRCQUFVOztRQUNuQixJQUFNLFFBQVEsR0FBRyxVQUFDLEdBQUcsSUFBSyxPQUFBLEdBQUcsSUFBSSxPQUFPLEdBQUcsS0FBSyxRQUFRLEVBQTlCLENBQThCLENBQUM7UUFDekQsSUFBTSxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBRXJCLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQyxHQUFHO1lBQ2hCLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQUMsR0FBRztnQkFDekIsSUFBTSxJQUFJLEdBQUcsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUM1QixJQUFNLElBQUksR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBRXRCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDckIsU0FBUyxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksS0FBSyxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUM3QztxQkFBTSxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDdkIsU0FBUyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQztpQkFDakQ7cUJBQU07b0JBQ0gsU0FBUyxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQztpQkFDekI7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO1FBRUgsT0FBTyxTQUFTLENBQUM7SUFDckIsQ0FBQztJQTlCUSxjQUFjO1FBRDFCLFVBQVUsRUFBRTs7T0FDQSxjQUFjLENBK0IxQjtJQUFELHFCQUFDO0NBQUEsQUEvQkQsSUErQkM7U0EvQlksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIE1hdGVyaWFsSGVscGVyIHtcbiAgICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgICBwdWJsaWMgZ2V0QWJzb2x1dGVQb3NpdGlvblJlbmRlckNvbmZpZyhlbGVtZW50T2ZmU2V0OiBudW1iZXIsIGVsZW1lbnRTaXplOiBudW1iZXIpOiBhbnkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgY2FuUmVuZGVyVG9SaWdodDogd2luZG93LmlubmVyV2lkdGggPiBlbGVtZW50T2ZmU2V0ICsgZWxlbWVudFNpemUsXG4gICAgICAgICAgICBjYW5SZW5kZXJUb0xlZnQ6IGVsZW1lbnRPZmZTZXQgPiBlbGVtZW50U2l6ZVxuICAgICAgICB9O1xuICAgIH1cblxuICAgIHB1YmxpYyBtZXJnZSguLi5vYmplY3RzKTogYW55IHtcbiAgICAgICAgY29uc3QgaXNPYmplY3QgPSAob2JqKSA9PiBvYmogJiYgdHlwZW9mIG9iaiA9PT0gJ29iamVjdCc7XG4gICAgICAgIGNvbnN0IG5ld09iamVjdCA9IHt9O1xuXG4gICAgICAgIG9iamVjdHMuZm9yRWFjaCgob2JqKSA9PiB7XG4gICAgICAgICAgICBPYmplY3Qua2V5cyhvYmopLmZvckVhY2goKGtleSkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IHBWYWwgPSBuZXdPYmplY3Rba2V5XTtcbiAgICAgICAgICAgICAgICBjb25zdCBvVmFsID0gb2JqW2tleV07XG5cbiAgICAgICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShvVmFsKSkge1xuICAgICAgICAgICAgICAgICAgICBuZXdPYmplY3Rba2V5XSA9IG5ldyBBcnJheSgpLmNvbmNhdChvVmFsKTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGlzT2JqZWN0KG9WYWwpKSB7XG4gICAgICAgICAgICAgICAgICAgIG5ld09iamVjdFtrZXldID0gdGhpcy5tZXJnZShwVmFsIHx8IHt9LCBvVmFsKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBuZXdPYmplY3Rba2V5XSA9IG9WYWw7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiBuZXdPYmplY3Q7XG4gICAgfVxufVxuIl19