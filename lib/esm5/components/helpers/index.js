import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { MaterialHelper } from './material.helpers';
var MaterialHelpersModule = /** @class */ (function () {
    function MaterialHelpersModule() {
    }
    MaterialHelpersModule = tslib_1.__decorate([
        NgModule({
            providers: [MaterialHelper]
        })
    ], MaterialHelpersModule);
    return MaterialHelpersModule;
}());
export { MaterialHelpersModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvaGVscGVycy9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV6QyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFLcEQ7SUFBQTtJQUFvQyxDQUFDO0lBQXhCLHFCQUFxQjtRQUhqQyxRQUFRLENBQUM7WUFDTixTQUFTLEVBQUUsQ0FBQyxjQUFjLENBQUM7U0FDOUIsQ0FBQztPQUNXLHFCQUFxQixDQUFHO0lBQUQsNEJBQUM7Q0FBQSxBQUFyQyxJQUFxQztTQUF4QixxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBNYXRlcmlhbEhlbHBlciB9IGZyb20gJy4vbWF0ZXJpYWwuaGVscGVycyc7XG5cbkBOZ01vZHVsZSh7XG4gICAgcHJvdmlkZXJzOiBbTWF0ZXJpYWxIZWxwZXJdXG59KVxuZXhwb3J0IGNsYXNzIE1hdGVyaWFsSGVscGVyc01vZHVsZSB7fVxuIl19