import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
var SearchPipe = /** @class */ (function () {
    function SearchPipe() {
    }
    SearchPipe.prototype.transform = function (value, keys, term) {
        if (value === void 0) { value = []; }
        if (!term) {
            return value;
        }
        return value.filter(function (item) {
            return keys.split(',').some(function (key) { return item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key]); });
        });
    };
    SearchPipe = tslib_1.__decorate([
        Pipe({
            name: 'searchPipe'
        })
    ], SearchPipe);
    return SearchPipe;
}());
export { SearchPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLXBpcGUucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2h1dWItbWF0ZXJpYWwvbGliLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9waXBlcy9zZWFyY2gtcGlwZS5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUtwRDtJQUFBO0lBVUEsQ0FBQztJQVRVLDhCQUFTLEdBQWhCLFVBQWlCLEtBQVUsRUFBRSxJQUFZLEVBQUUsSUFBWTtRQUF0QyxzQkFBQSxFQUFBLFVBQVU7UUFDdkIsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNQLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBRUQsT0FBTyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQUMsSUFBUztZQUMxQixPQUFBLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxJQUFJLE1BQU0sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFsRSxDQUFrRSxDQUFDO1FBQWpHLENBQWlHLENBQ3BHLENBQUM7SUFDTixDQUFDO0lBVFEsVUFBVTtRQUh0QixJQUFJLENBQUM7WUFDRixJQUFJLEVBQUUsWUFBWTtTQUNyQixDQUFDO09BQ1csVUFBVSxDQVV0QjtJQUFELGlCQUFDO0NBQUEsQUFWRCxJQVVDO1NBVlksVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe1xuICAgIG5hbWU6ICdzZWFyY2hQaXBlJ1xufSlcbmV4cG9ydCBjbGFzcyBTZWFyY2hQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG4gICAgcHVibGljIHRyYW5zZm9ybSh2YWx1ZSA9IFtdLCBrZXlzOiBzdHJpbmcsIHRlcm06IHN0cmluZykge1xuICAgICAgICBpZiAoIXRlcm0pIHtcbiAgICAgICAgICAgIHJldHVybiB2YWx1ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB2YWx1ZS5maWx0ZXIoKGl0ZW06IGFueSkgPT5cbiAgICAgICAgICAgIGtleXMuc3BsaXQoJywnKS5zb21lKChrZXkpID0+IGl0ZW0uaGFzT3duUHJvcGVydHkoa2V5KSAmJiBuZXcgUmVnRXhwKHRlcm0sICdnaScpLnRlc3QoaXRlbVtrZXldKSlcbiAgICAgICAgKTtcbiAgICB9XG59XG4iXX0=