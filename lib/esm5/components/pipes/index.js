import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { SearchPipe } from './search-pipe.pipe';
var SearchPipeModule = /** @class */ (function () {
    function SearchPipeModule() {
    }
    SearchPipeModule = tslib_1.__decorate([
        NgModule({
            declarations: [SearchPipe],
            exports: [SearchPipe]
        })
    ], SearchPipeModule);
    return SearchPipeModule;
}());
export { SearchPipeModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvcGlwZXMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBTWhEO0lBQUE7SUFBK0IsQ0FBQztJQUFuQixnQkFBZ0I7UUFKNUIsUUFBUSxDQUFDO1lBQ04sWUFBWSxFQUFFLENBQUMsVUFBVSxDQUFDO1lBQzFCLE9BQU8sRUFBRSxDQUFDLFVBQVUsQ0FBQztTQUN4QixDQUFDO09BQ1csZ0JBQWdCLENBQUc7SUFBRCx1QkFBQztDQUFBLEFBQWhDLElBQWdDO1NBQW5CLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTZWFyY2hQaXBlIH0gZnJvbSAnLi9zZWFyY2gtcGlwZS5waXBlJztcblxuQE5nTW9kdWxlKHtcbiAgICBkZWNsYXJhdGlvbnM6IFtTZWFyY2hQaXBlXSxcbiAgICBleHBvcnRzOiBbU2VhcmNoUGlwZV1cbn0pXG5leHBvcnQgY2xhc3MgU2VhcmNoUGlwZU1vZHVsZSB7fVxuIl19