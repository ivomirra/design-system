import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
var UiUploadFileComponent = /** @class */ (function () {
    function UiUploadFileComponent() {
        this.viewModel = {};
        this.onFileSelected = new EventEmitter();
        this.onFileRemoved = new EventEmitter();
        this.onFileUploadCanceled = new EventEmitter();
        this.onFileViewClicked = new EventEmitter();
        this.componentActionsModel = {
            isBeingDragged: false
        };
    }
    UiUploadFileComponent.prototype.handleSelectedFile = function (selectedFile) {
        this.viewModel.fileName = selectedFile.name;
        this.viewModel.fileUploadPercentage = 0;
        this.viewModel.isUploading = true;
    };
    UiUploadFileComponent.prototype.onDragOver = function ($event) {
        $event.preventDefault();
        this.componentActionsModel.isBeingDragged = true;
    };
    UiUploadFileComponent.prototype.onDragLeave = function ($event) {
        $event.preventDefault();
        this.componentActionsModel.isBeingDragged = false;
    };
    UiUploadFileComponent.prototype.onDragDrop = function ($event) {
        $event.preventDefault();
        this.componentActionsModel.isBeingDragged = false;
        var selectedFile = $event.dataTransfer.files[0];
        this.handleSelectedFile(selectedFile);
        this.onFileSelected.emit({
            selectedFile: selectedFile,
            viewModel: this.viewModel
        });
    };
    UiUploadFileComponent.prototype.onChange = function ($event) {
        $event.preventDefault();
        var selectedFile = $event.target.files[0];
        this.handleSelectedFile(selectedFile);
        this.onFileSelected.emit({
            selectedFile: selectedFile,
            viewModel: this.viewModel
        });
    };
    UiUploadFileComponent.prototype.onRemoveFileClicked = function ($event) {
        $event.preventDefault();
        this.onFileRemoved.emit({
            viewModel: this.viewModel
        });
    };
    UiUploadFileComponent.prototype.onUploadCancelClicked = function () {
        this.viewModel.isUploading = false;
        this.viewModel.fileUploadPercentage = 0;
        this.onFileUploadCanceled.emit({
            viewModel: this.viewModel
        });
    };
    UiUploadFileComponent.prototype.onReplaceDocumentClicked = function ($event) {
        $event.preventDefault();
        this.uploadInput.nativeElement.click();
    };
    UiUploadFileComponent.prototype.onViewFileClicked = function ($event) {
        $event.preventDefault();
        this.onFileViewClicked.emit({
            viewModel: this.viewModel
        });
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], UiUploadFileComponent.prototype, "viewModel", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", Object)
    ], UiUploadFileComponent.prototype, "onFileSelected", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", Object)
    ], UiUploadFileComponent.prototype, "onFileRemoved", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", Object)
    ], UiUploadFileComponent.prototype, "onFileUploadCanceled", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", Object)
    ], UiUploadFileComponent.prototype, "onFileViewClicked", void 0);
    tslib_1.__decorate([
        ViewChild('uploadInput', { read: ElementRef, static: false }),
        tslib_1.__metadata("design:type", ElementRef)
    ], UiUploadFileComponent.prototype, "uploadInput", void 0);
    UiUploadFileComponent = tslib_1.__decorate([
        Component({
            selector: 'ui-upload-file',
            template: "<div class=\"ui-upload-file__container--upload\"\n    [ngClass]=\"{\n        'ui-upload-file__container--dragover': componentActionsModel.isBeingDragged\n    }\"\n    *ngIf=\"!viewModel.isUploaded && !viewModel.isUploading\">\n        Drag and drop a file here to upload, or&nbsp;<span class=\"ui-upload-file__message-link\">Select a file</span>\n        <input type=\"file\"\n                class=\"ui-upload-file__upload-input\"\n                (dragover)=\"onDragOver($event)\"\n\n                (dragleave)=\"onDragLeave($event)\"\n                (dragend)=\"onDragLeave($event)\"\n\n                (drop)=\"onDragDrop($event)\"\n                (change)=\"onChange($event)\">\n</div>\n\n<div class=\"ui-upload-file__container--uploading\" *ngIf=\"viewModel.isUploading\">\n    <div class=\"ui-upload-file__p-bar__container\">\n        <div class=\"ui-upload-file__p-bar__name\">\n            <div>{{viewModel.fileName}}</div>\n            <div>{{viewModel.fileUploadPercentage}}%</div>\n        </div>\n        <ui-progress-bar class=\"ui-upload-file__p-bar\"\n                         size=\"small\"\n                         [percentage]=\"viewModel.fileUploadPercentage\"></ui-progress-bar>\n        <i class=\"huub-material-icon ui-upload-file__p-bar__cancel\"\n            (click)=\"onUploadCancelClicked()\"\n            icon=\"close\"></i>\n    </div>\n</div>\n\n<span class=\"ui-upload-file__container--uploaded\"\n    *ngIf=\"viewModel.isUploaded && !viewModel.isUploading\">\n    <input type=\"file\"\n            #uploadInput\n            class=\"ui-upload-file__dummy-input\"\n            (change)=\"onChange($event)\">\n    <div class=\"ui-upload-file__doc-name\">\n        <i class=\"huub-material-icon\" icon=\"document\"></i>\n        <span class=\"ui-upload-file__doc-name-text\">{{viewModel.fileName}}</span>\n    </div>\n    <div class=\"ui-upload-file__doc-action-container\">\n        <a href=\"\"\n            (click)=\"onViewFileClicked($event)\"\n            class=\"ui-upload-file__doc-action\">View</a>\n        <a href=\"\"\n            (click)=\"onReplaceDocumentClicked($event)\"\n            class=\"ui-upload-file__doc-action\">Replace</a>\n        <a href=\"\"\n            (click)=\"onRemoveFileClicked($event)\"\n            class=\"ui-upload-file__doc-action\">Remove</a>\n    </div>\n</span>\n"
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], UiUploadFileComponent);
    return UiUploadFileComponent;
}());
export { UiUploadFileComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktdXBsb2FkLWZpbGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaHV1Yi1tYXRlcmlhbC9saWIvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2Zvcm1zL3VwbG9hZC1maWxlcy91aS11cGxvYWQtZmlsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU05RjtJQWlCSTtRQWZPLGNBQVMsR0FLWixFQUFFLENBQUM7UUFFVyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDcEMsa0JBQWEsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ25DLHlCQUFvQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDMUMsc0JBQWlCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQU9sRCwwQkFBcUIsR0FBRztZQUMzQixjQUFjLEVBQUUsS0FBSztTQUN4QixDQUFDO0lBSmEsQ0FBQztJQU1SLGtEQUFrQixHQUExQixVQUEyQixZQUFZO1FBQ25DLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUM7UUFDNUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsR0FBRyxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO0lBQ3RDLENBQUM7SUFFTSwwQ0FBVSxHQUFqQixVQUFrQixNQUFNO1FBQ3BCLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUV4QixJQUFJLENBQUMscUJBQXFCLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztJQUNyRCxDQUFDO0lBRU0sMkNBQVcsR0FBbEIsVUFBbUIsTUFBTTtRQUNyQixNQUFNLENBQUMsY0FBYyxFQUFFLENBQUM7UUFFeEIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7SUFDdEQsQ0FBQztJQUVNLDBDQUFVLEdBQWpCLFVBQWtCLE1BQU07UUFDcEIsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRXhCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBRWxELElBQU0sWUFBWSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRWxELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUV0QyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQztZQUNyQixZQUFZLGNBQUE7WUFDWixTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7U0FDNUIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLHdDQUFRLEdBQWYsVUFBZ0IsTUFBTTtRQUNsQixNQUFNLENBQUMsY0FBYyxFQUFFLENBQUM7UUFFeEIsSUFBTSxZQUFZLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFNUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRXRDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO1lBQ3JCLFlBQVksY0FBQTtZQUNaLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUztTQUM1QixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sbURBQW1CLEdBQTFCLFVBQTJCLE1BQU07UUFDN0IsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRXhCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO1lBQ3BCLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUztTQUM1QixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0scURBQXFCLEdBQTVCO1FBQ0ksSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxTQUFTLENBQUMsb0JBQW9CLEdBQUcsQ0FBQyxDQUFDO1FBRXhDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUM7WUFDM0IsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTO1NBQzVCLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTSx3REFBd0IsR0FBL0IsVUFBZ0MsTUFBTTtRQUNsQyxNQUFNLENBQUMsY0FBYyxFQUFFLENBQUM7UUFFeEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDM0MsQ0FBQztJQUVNLGlEQUFpQixHQUF4QixVQUF5QixNQUFNO1FBQzNCLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUV4QixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDO1lBQ3hCLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUztTQUM1QixDQUFDLENBQUM7SUFDUCxDQUFDO0lBaEdEO1FBREMsS0FBSyxFQUFFOzs0REFNRDtJQUVHO1FBQVQsTUFBTSxFQUFFOztpRUFBNkM7SUFDNUM7UUFBVCxNQUFNLEVBQUU7O2dFQUE0QztJQUMzQztRQUFULE1BQU0sRUFBRTs7dUVBQW1EO0lBQ2xEO1FBQVQsTUFBTSxFQUFFOztvRUFBZ0Q7SUFHekQ7UUFEQyxTQUFTLENBQUMsYUFBYSxFQUFFLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUM7MENBQ3pDLFVBQVU7OERBQWM7SUFmcEMscUJBQXFCO1FBSmpDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxnQkFBZ0I7WUFDMUIsK3lFQUE4QztTQUNqRCxDQUFDOztPQUNXLHFCQUFxQixDQW1HakM7SUFBRCw0QkFBQztDQUFBLEFBbkdELElBbUdDO1NBbkdZLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICd1aS11cGxvYWQtZmlsZScsXG4gICAgdGVtcGxhdGVVcmw6ICcuL3VpLXVwbG9hZC1maWxlLmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBVaVVwbG9hZEZpbGVDb21wb25lbnQge1xuICAgIEBJbnB1dCgpXG4gICAgcHVibGljIHZpZXdNb2RlbDoge1xuICAgICAgICBpc1VwbG9hZGluZz86IGJvb2xlYW47XG4gICAgICAgIGlzVXBsb2FkZWQ/OiBib29sZWFuO1xuICAgICAgICBmaWxlTmFtZT86IHN0cmluZztcbiAgICAgICAgZmlsZVVwbG9hZFBlcmNlbnRhZ2U/OiBudW1iZXIgfCBzdHJpbmc7XG4gICAgfSA9IHt9O1xuXG4gICAgQE91dHB1dCgpIHByaXZhdGUgb25GaWxlU2VsZWN0ZWQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIHByaXZhdGUgb25GaWxlUmVtb3ZlZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgcHJpdmF0ZSBvbkZpbGVVcGxvYWRDYW5jZWxlZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBAT3V0cHV0KCkgcHJpdmF0ZSBvbkZpbGVWaWV3Q2xpY2tlZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAgIEBWaWV3Q2hpbGQoJ3VwbG9hZElucHV0JywgeyByZWFkOiBFbGVtZW50UmVmLCBzdGF0aWM6IGZhbHNlIH0pXG4gICAgcHJpdmF0ZSB1cGxvYWRJbnB1dDogRWxlbWVudFJlZjxIVE1MRWxlbWVudD47XG5cbiAgICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgICBwdWJsaWMgY29tcG9uZW50QWN0aW9uc01vZGVsID0ge1xuICAgICAgICBpc0JlaW5nRHJhZ2dlZDogZmFsc2VcbiAgICB9O1xuXG4gICAgcHJpdmF0ZSBoYW5kbGVTZWxlY3RlZEZpbGUoc2VsZWN0ZWRGaWxlKSB7XG4gICAgICAgIHRoaXMudmlld01vZGVsLmZpbGVOYW1lID0gc2VsZWN0ZWRGaWxlLm5hbWU7XG4gICAgICAgIHRoaXMudmlld01vZGVsLmZpbGVVcGxvYWRQZXJjZW50YWdlID0gMDtcbiAgICAgICAgdGhpcy52aWV3TW9kZWwuaXNVcGxvYWRpbmcgPSB0cnVlO1xuICAgIH1cblxuICAgIHB1YmxpYyBvbkRyYWdPdmVyKCRldmVudCkge1xuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICB0aGlzLmNvbXBvbmVudEFjdGlvbnNNb2RlbC5pc0JlaW5nRHJhZ2dlZCA9IHRydWU7XG4gICAgfVxuXG4gICAgcHVibGljIG9uRHJhZ0xlYXZlKCRldmVudCkge1xuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICB0aGlzLmNvbXBvbmVudEFjdGlvbnNNb2RlbC5pc0JlaW5nRHJhZ2dlZCA9IGZhbHNlO1xuICAgIH1cblxuICAgIHB1YmxpYyBvbkRyYWdEcm9wKCRldmVudCkge1xuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICB0aGlzLmNvbXBvbmVudEFjdGlvbnNNb2RlbC5pc0JlaW5nRHJhZ2dlZCA9IGZhbHNlO1xuXG4gICAgICAgIGNvbnN0IHNlbGVjdGVkRmlsZSA9ICRldmVudC5kYXRhVHJhbnNmZXIuZmlsZXNbMF07XG5cbiAgICAgICAgdGhpcy5oYW5kbGVTZWxlY3RlZEZpbGUoc2VsZWN0ZWRGaWxlKTtcblxuICAgICAgICB0aGlzLm9uRmlsZVNlbGVjdGVkLmVtaXQoe1xuICAgICAgICAgICAgc2VsZWN0ZWRGaWxlLFxuICAgICAgICAgICAgdmlld01vZGVsOiB0aGlzLnZpZXdNb2RlbFxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25DaGFuZ2UoJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgIGNvbnN0IHNlbGVjdGVkRmlsZSA9ICRldmVudC50YXJnZXQuZmlsZXNbMF07XG5cbiAgICAgICAgdGhpcy5oYW5kbGVTZWxlY3RlZEZpbGUoc2VsZWN0ZWRGaWxlKTtcblxuICAgICAgICB0aGlzLm9uRmlsZVNlbGVjdGVkLmVtaXQoe1xuICAgICAgICAgICAgc2VsZWN0ZWRGaWxlLFxuICAgICAgICAgICAgdmlld01vZGVsOiB0aGlzLnZpZXdNb2RlbFxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25SZW1vdmVGaWxlQ2xpY2tlZCgkZXZlbnQpIHtcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgdGhpcy5vbkZpbGVSZW1vdmVkLmVtaXQoe1xuICAgICAgICAgICAgdmlld01vZGVsOiB0aGlzLnZpZXdNb2RlbFxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25VcGxvYWRDYW5jZWxDbGlja2VkKCkge1xuICAgICAgICB0aGlzLnZpZXdNb2RlbC5pc1VwbG9hZGluZyA9IGZhbHNlO1xuICAgICAgICB0aGlzLnZpZXdNb2RlbC5maWxlVXBsb2FkUGVyY2VudGFnZSA9IDA7XG5cbiAgICAgICAgdGhpcy5vbkZpbGVVcGxvYWRDYW5jZWxlZC5lbWl0KHtcbiAgICAgICAgICAgIHZpZXdNb2RlbDogdGhpcy52aWV3TW9kZWxcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHVibGljIG9uUmVwbGFjZURvY3VtZW50Q2xpY2tlZCgkZXZlbnQpIHtcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgdGhpcy51cGxvYWRJbnB1dC5uYXRpdmVFbGVtZW50LmNsaWNrKCk7XG4gICAgfVxuXG4gICAgcHVibGljIG9uVmlld0ZpbGVDbGlja2VkKCRldmVudCkge1xuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICB0aGlzLm9uRmlsZVZpZXdDbGlja2VkLmVtaXQoe1xuICAgICAgICAgICAgdmlld01vZGVsOiB0aGlzLnZpZXdNb2RlbFxuICAgICAgICB9KTtcbiAgICB9XG59XG4iXX0=