import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UiUploadFileComponent } from './ui-upload-file.component';
import { UiUploadFileEventsHelper } from './ui-upload-file.events.helper';
import { UiProgressBarModule } from '../../progress-bar/index';
var UiUploadFileModule = /** @class */ (function () {
    function UiUploadFileModule() {
    }
    UiUploadFileModule = tslib_1.__decorate([
        NgModule({
            imports: [CommonModule, FormsModule, UiProgressBarModule],
            providers: [UiUploadFileEventsHelper],
            declarations: [UiUploadFileComponent],
            exports: [UiUploadFileComponent]
        })
    ], UiUploadFileModule);
    return UiUploadFileModule;
}());
export { UiUploadFileModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZm9ybXMvdXBsb2FkLWZpbGVzL2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFN0MsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDbkUsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDMUUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFRL0Q7SUFBQTtJQUFpQyxDQUFDO0lBQXJCLGtCQUFrQjtRQU45QixRQUFRLENBQUM7WUFDTixPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixDQUFDO1lBQ3pELFNBQVMsRUFBRSxDQUFDLHdCQUF3QixDQUFDO1lBQ3JDLFlBQVksRUFBRSxDQUFDLHFCQUFxQixDQUFDO1lBQ3JDLE9BQU8sRUFBRSxDQUFDLHFCQUFxQixDQUFDO1NBQ25DLENBQUM7T0FDVyxrQkFBa0IsQ0FBRztJQUFELHlCQUFDO0NBQUEsQUFBbEMsSUFBa0M7U0FBckIsa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuaW1wb3J0IHsgVWlVcGxvYWRGaWxlQ29tcG9uZW50IH0gZnJvbSAnLi91aS11cGxvYWQtZmlsZS5jb21wb25lbnQnO1xuaW1wb3J0IHsgVWlVcGxvYWRGaWxlRXZlbnRzSGVscGVyIH0gZnJvbSAnLi91aS11cGxvYWQtZmlsZS5ldmVudHMuaGVscGVyJztcbmltcG9ydCB7IFVpUHJvZ3Jlc3NCYXJNb2R1bGUgfSBmcm9tICcuLi8uLi9wcm9ncmVzcy1iYXIvaW5kZXgnO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtDb21tb25Nb2R1bGUsIEZvcm1zTW9kdWxlLCBVaVByb2dyZXNzQmFyTW9kdWxlXSxcbiAgICBwcm92aWRlcnM6IFtVaVVwbG9hZEZpbGVFdmVudHNIZWxwZXJdLFxuICAgIGRlY2xhcmF0aW9uczogW1VpVXBsb2FkRmlsZUNvbXBvbmVudF0sXG4gICAgZXhwb3J0czogW1VpVXBsb2FkRmlsZUNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgVWlVcGxvYWRGaWxlTW9kdWxlIHt9XG4iXX0=