import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpEventType } from '@angular/common/http';
var UiUploadFileEventsHelper = /** @class */ (function () {
    function UiUploadFileEventsHelper() {
    }
    UiUploadFileEventsHelper.prototype.getAbstractedEventsDataModel = function (event) {
        switch (event.type) {
            case HttpEventType.Sent:
                return {
                    isUploaded: false,
                    isUploading: false,
                    fileUploadPercentage: 0
                };
            case HttpEventType.UploadProgress:
                return {
                    isUploaded: false,
                    isUploading: true,
                    fileUploadPercentage: Math.round(100 * event.loaded / event.total)
                };
            case HttpEventType.Response:
                return tslib_1.__assign({}, event, { isUploaded: true, isUploading: false, fileUploadPercentage: 100 });
        }
    };
    UiUploadFileEventsHelper = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [])
    ], UiUploadFileEventsHelper);
    return UiUploadFileEventsHelper;
}());
export { UiUploadFileEventsHelper };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktdXBsb2FkLWZpbGUuZXZlbnRzLmhlbHBlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2h1dWItbWF0ZXJpYWwvbGliLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9mb3Jtcy91cGxvYWQtZmlsZXMvdWktdXBsb2FkLWZpbGUuZXZlbnRzLmhlbHBlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFHckQ7SUFDSTtJQUFlLENBQUM7SUFFVCwrREFBNEIsR0FBbkMsVUFBb0MsS0FBVTtRQUMxQyxRQUFRLEtBQUssQ0FBQyxJQUFJLEVBQUU7WUFDaEIsS0FBSyxhQUFhLENBQUMsSUFBSTtnQkFDbkIsT0FBTztvQkFDSCxVQUFVLEVBQUUsS0FBSztvQkFDakIsV0FBVyxFQUFFLEtBQUs7b0JBQ2xCLG9CQUFvQixFQUFFLENBQUM7aUJBQzFCLENBQUM7WUFFTixLQUFLLGFBQWEsQ0FBQyxjQUFjO2dCQUM3QixPQUFPO29CQUNILFVBQVUsRUFBRSxLQUFLO29CQUNqQixXQUFXLEVBQUUsSUFBSTtvQkFDakIsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO2lCQUNyRSxDQUFDO1lBRU4sS0FBSyxhQUFhLENBQUMsUUFBUTtnQkFDdkIsNEJBQ08sS0FBSyxJQUNSLFVBQVUsRUFBRSxJQUFJLEVBQ2hCLFdBQVcsRUFBRSxLQUFLLEVBQ2xCLG9CQUFvQixFQUFFLEdBQUcsSUFDM0I7U0FDVDtJQUNMLENBQUM7SUEzQlEsd0JBQXdCO1FBRHBDLFVBQVUsRUFBRTs7T0FDQSx3QkFBd0IsQ0E0QnBDO0lBQUQsK0JBQUM7Q0FBQSxBQTVCRCxJQTRCQztTQTVCWSx3QkFBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwRXZlbnRUeXBlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgVWlVcGxvYWRGaWxlRXZlbnRzSGVscGVyIHtcbiAgICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgICBwdWJsaWMgZ2V0QWJzdHJhY3RlZEV2ZW50c0RhdGFNb2RlbChldmVudDogYW55KTogYW55IHtcbiAgICAgICAgc3dpdGNoIChldmVudC50eXBlKSB7XG4gICAgICAgICAgICBjYXNlIEh0dHBFdmVudFR5cGUuU2VudDpcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICBpc1VwbG9hZGVkOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgaXNVcGxvYWRpbmc6IGZhbHNlLFxuICAgICAgICAgICAgICAgICAgICBmaWxlVXBsb2FkUGVyY2VudGFnZTogMFxuICAgICAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIGNhc2UgSHR0cEV2ZW50VHlwZS5VcGxvYWRQcm9ncmVzczpcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICBpc1VwbG9hZGVkOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgaXNVcGxvYWRpbmc6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgIGZpbGVVcGxvYWRQZXJjZW50YWdlOiBNYXRoLnJvdW5kKDEwMCAqIGV2ZW50LmxvYWRlZCAvIGV2ZW50LnRvdGFsKVxuICAgICAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIGNhc2UgSHR0cEV2ZW50VHlwZS5SZXNwb25zZTpcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICAuLi5ldmVudCxcbiAgICAgICAgICAgICAgICAgICAgaXNVcGxvYWRlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgaXNVcGxvYWRpbmc6IGZhbHNlLFxuICAgICAgICAgICAgICAgICAgICBmaWxlVXBsb2FkUGVyY2VudGFnZTogMTAwXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgfVxuICAgIH1cbn1cbiJdfQ==