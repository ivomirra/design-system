import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CurrenciesListHelperModule } from 'huub-utils/lib';
import { UiCurrencyInputComponent } from './ui-currency-input.component';
var UiCurrencyInputModule = /** @class */ (function () {
    function UiCurrencyInputModule() {
    }
    UiCurrencyInputModule = tslib_1.__decorate([
        NgModule({
            imports: [CommonModule, FormsModule, CurrenciesListHelperModule],
            declarations: [UiCurrencyInputComponent],
            exports: [UiCurrencyInputComponent]
        })
    ], UiCurrencyInputModule);
    return UiCurrencyInputModule;
}());
export { UiCurrencyInputModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZm9ybXMvY3VycmVuY3ktaW5wdXQvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQU96RTtJQUFBO0lBQW9DLENBQUM7SUFBeEIscUJBQXFCO1FBTGpDLFFBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRSxDQUFDLFlBQVksRUFBRSxXQUFXLEVBQUUsMEJBQTBCLENBQUM7WUFDaEUsWUFBWSxFQUFFLENBQUMsd0JBQXdCLENBQUM7WUFDeEMsT0FBTyxFQUFFLENBQUMsd0JBQXdCLENBQUM7U0FDdEMsQ0FBQztPQUNXLHFCQUFxQixDQUFHO0lBQUQsNEJBQUM7Q0FBQSxBQUFyQyxJQUFxQztTQUF4QixxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IEZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgQ3VycmVuY2llc0xpc3RIZWxwZXJNb2R1bGUgfSBmcm9tICdodXViLXV0aWxzL2xpYic7XG5pbXBvcnQgeyBVaUN1cnJlbmN5SW5wdXRDb21wb25lbnQgfSBmcm9tICcuL3VpLWN1cnJlbmN5LWlucHV0LmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZSwgRm9ybXNNb2R1bGUsIEN1cnJlbmNpZXNMaXN0SGVscGVyTW9kdWxlXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtVaUN1cnJlbmN5SW5wdXRDb21wb25lbnRdLFxuICAgIGV4cG9ydHM6IFtVaUN1cnJlbmN5SW5wdXRDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIFVpQ3VycmVuY3lJbnB1dE1vZHVsZSB7fVxuIl19