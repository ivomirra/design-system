import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CurrenciesListHelper } from 'huub-utils/lib';
var UiCurrencyInputComponent = /** @class */ (function () {
    function UiCurrencyInputComponent(currenciesListHelper) {
        this.currenciesListHelper = currenciesListHelper;
        this.onChange = new EventEmitter();
        this.isDropdownOpened = false;
        this.currencySymbol = '';
        this.currenciesList = this.currenciesListHelper.getCurrenciesList();
    }
    UiCurrencyInputComponent.prototype.ngOnInit = function () {
        this.currencySymbol = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).symbol;
        this.disabled = this.disabled !== undefined ? this.disabled : false;
    };
    UiCurrencyInputComponent.prototype.onInputChange = function () {
        this.onChange.emit({ currency: this.currency, value: this.value });
    };
    UiCurrencyInputComponent.prototype.openCloseDropdown = function () {
        if (!this.disabled) {
            this.isDropdownOpened = !this.isDropdownOpened;
        }
    };
    UiCurrencyInputComponent.prototype.changeCurrency = function (isoCode) {
        this.currency = isoCode;
        this.onChange.emit({ currency: this.currency, value: this.value });
        this.currencySymbol = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).symbol;
        this.openCloseDropdown();
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], UiCurrencyInputComponent.prototype, "currency", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], UiCurrencyInputComponent.prototype, "language", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], UiCurrencyInputComponent.prototype, "value", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], UiCurrencyInputComponent.prototype, "placeholder", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], UiCurrencyInputComponent.prototype, "disabled", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], UiCurrencyInputComponent.prototype, "onChange", void 0);
    UiCurrencyInputComponent = tslib_1.__decorate([
        Component({
            selector: 'ui-currency-input',
            providers: [CurrenciesListHelper],
            template: "<div class=\"ui-currency-input\">\n    <span class=\"currency-char\" (click)=\"openCloseDropdown()\">{{currencySymbol}}</span>\n    <input type=\"number\" class=\"currency-char__input\" [attr.placeholder]=\"placeholder\" (input)=\"onInputChange()\"\n        [(ngModel)]=\"value\" [ngModelOptions]=\"{standalone: true}\" [disabled]=\"disabled\">\n    <div *ngIf=\"isDropdownOpened\" class=\"currency-input__dropdown\" (mouseleave)=\"openCloseDropdown()\">\n        <div *ngFor=\"let currency of currenciesList\" class=\"currency-input__dropdown-item\" (click)=\"changeCurrency(currency.iso3code)\">\n            <span class=\"currency-input__dropdown-char\">{{currency.symbol}}</span>\n            <span class=\"currency-input__dropdown-iso-code\">{{currency.iso3code}}</span>\n            <span class=\"currency-input__dropdown-name\">{{currency.name}}</span>\n        </div>\n    </div>\n</div>"
        }),
        tslib_1.__metadata("design:paramtypes", [CurrenciesListHelper])
    ], UiCurrencyInputComponent);
    return UiCurrencyInputComponent;
}());
export { UiCurrencyInputComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktY3VycmVuY3ktaW5wdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaHV1Yi1tYXRlcmlhbC9saWIvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2Zvcm1zL2N1cnJlbmN5LWlucHV0L3VpLWN1cnJlbmN5LWlucHV0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQU90RDtJQWVJLGtDQUFvQixvQkFBMEM7UUFBMUMseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFzQjtRQUw3QyxhQUFRLEdBQXlCLElBQUksWUFBWSxFQUFFLENBQUM7UUFFOUQscUJBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLG1CQUFjLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGlCQUFpQixFQUFFLENBQUM7SUFDTCxDQUFDO0lBRTNELDJDQUFRLEdBQWY7UUFDSSxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQzVGLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUN4RSxDQUFDO0lBRU0sZ0RBQWEsR0FBcEI7UUFDSSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBRU0sb0RBQWlCLEdBQXhCO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDaEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1NBQ2xEO0lBQ0wsQ0FBQztJQUVNLGlEQUFjLEdBQXJCLFVBQXNCLE9BQU87UUFDekIsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7UUFDeEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUM1RixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBcENRO1FBQVIsS0FBSyxFQUFFOzs4REFBMEI7SUFDekI7UUFBUixLQUFLLEVBQUU7OzhEQUEwQjtJQUV6QjtRQUFSLEtBQUssRUFBRTs7MkRBQXVCO0lBRXRCO1FBQVIsS0FBSyxFQUFFOztpRUFBNkI7SUFFNUI7UUFBUixLQUFLLEVBQUU7OzhEQUEyQjtJQUV6QjtRQUFULE1BQU0sRUFBRTswQ0FBa0IsWUFBWTs4REFBOEI7SUFWNUQsd0JBQXdCO1FBTHBDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxtQkFBbUI7WUFDN0IsU0FBUyxFQUFFLENBQUMsb0JBQW9CLENBQUM7WUFDakMsMjRCQUFpRDtTQUNwRCxDQUFDO2lEQWdCNEMsb0JBQW9CO09BZnJELHdCQUF3QixDQXNDcEM7SUFBRCwrQkFBQztDQUFBLEFBdENELElBc0NDO1NBdENZLHdCQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEN1cnJlbmNpZXNMaXN0SGVscGVyIH0gZnJvbSAnaHV1Yi11dGlscy9saWInO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ3VpLWN1cnJlbmN5LWlucHV0JyxcbiAgICBwcm92aWRlcnM6IFtDdXJyZW5jaWVzTGlzdEhlbHBlcl0sXG4gICAgdGVtcGxhdGVVcmw6ICcuL3VpLWN1cnJlbmN5LWlucHV0LmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBVaUN1cnJlbmN5SW5wdXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIEBJbnB1dCgpIHB1YmxpYyBjdXJyZW5jeT86IHN0cmluZztcbiAgICBASW5wdXQoKSBwdWJsaWMgbGFuZ3VhZ2U/OiBzdHJpbmc7XG5cbiAgICBASW5wdXQoKSBwdWJsaWMgdmFsdWU/OiBzdHJpbmc7XG5cbiAgICBASW5wdXQoKSBwdWJsaWMgcGxhY2Vob2xkZXI/OiBzdHJpbmc7XG5cbiAgICBASW5wdXQoKSBwdWJsaWMgZGlzYWJsZWQ/OiBib29sZWFuO1xuXG4gICAgQE91dHB1dCgpIHB1YmxpYyBvbkNoYW5nZTogRXZlbnRFbWl0dGVyPG9iamVjdD4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgICBwdWJsaWMgaXNEcm9wZG93bk9wZW5lZCA9IGZhbHNlO1xuICAgIHB1YmxpYyBjdXJyZW5jeVN5bWJvbCA9ICcnO1xuICAgIHB1YmxpYyBjdXJyZW5jaWVzTGlzdCA9IHRoaXMuY3VycmVuY2llc0xpc3RIZWxwZXIuZ2V0Q3VycmVuY2llc0xpc3QoKTtcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGN1cnJlbmNpZXNMaXN0SGVscGVyOiBDdXJyZW5jaWVzTGlzdEhlbHBlcikge31cblxuICAgIHB1YmxpYyBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy5jdXJyZW5jeVN5bWJvbCA9IHRoaXMuY3VycmVuY2llc0xpc3RIZWxwZXIuZ2V0Q3VycmVuY3lCeUlzbzNDb2RlKHRoaXMuY3VycmVuY3kpLnN5bWJvbDtcbiAgICAgICAgdGhpcy5kaXNhYmxlZCA9IHRoaXMuZGlzYWJsZWQgIT09IHVuZGVmaW5lZCA/IHRoaXMuZGlzYWJsZWQgOiBmYWxzZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25JbnB1dENoYW5nZSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5vbkNoYW5nZS5lbWl0KHsgY3VycmVuY3k6IHRoaXMuY3VycmVuY3ksIHZhbHVlOiB0aGlzLnZhbHVlIH0pO1xuICAgIH1cblxuICAgIHB1YmxpYyBvcGVuQ2xvc2VEcm9wZG93bigpIHtcbiAgICAgICAgaWYgKCF0aGlzLmRpc2FibGVkKSB7XG4gICAgICAgICAgICB0aGlzLmlzRHJvcGRvd25PcGVuZWQgPSAhdGhpcy5pc0Ryb3Bkb3duT3BlbmVkO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIGNoYW5nZUN1cnJlbmN5KGlzb0NvZGUpIHtcbiAgICAgICAgdGhpcy5jdXJyZW5jeSA9IGlzb0NvZGU7XG4gICAgICAgIHRoaXMub25DaGFuZ2UuZW1pdCh7IGN1cnJlbmN5OiB0aGlzLmN1cnJlbmN5LCB2YWx1ZTogdGhpcy52YWx1ZSB9KTtcbiAgICAgICAgdGhpcy5jdXJyZW5jeVN5bWJvbCA9IHRoaXMuY3VycmVuY2llc0xpc3RIZWxwZXIuZ2V0Q3VycmVuY3lCeUlzbzNDb2RlKHRoaXMuY3VycmVuY3kpLnN5bWJvbDtcbiAgICAgICAgdGhpcy5vcGVuQ2xvc2VEcm9wZG93bigpO1xuICAgIH1cbn1cbiJdfQ==