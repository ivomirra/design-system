import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { WeightUnitsListHelper } from 'huub-utils/lib';
import { UI_WEIGHT_UNITS_DROPDOWN_CONSTANTS } from './ui-weight-units-dropdown.constant';
var UiWeightUnitsDropdownComponent = /** @class */ (function () {
    function UiWeightUnitsDropdownComponent(weightUnitsHelper) {
        this.weightUnitsHelper = weightUnitsHelper;
        this.inputSearchViewModel = {
            placeholder: UI_WEIGHT_UNITS_DROPDOWN_CONSTANTS.SEARCH_WEIGHT_UNIT,
            value: ''
        };
        this.onChange = new EventEmitter();
        this.isDropdownOpened = false;
        this.weightUnitsList = this.weightUnitsHelper.getWeightUnitListOrderByName();
        this.popularWeightUnitsList = this.weightUnitsHelper.getPopularWeightUnitsListOrderByName();
        this.popularWeightUnitsTitle = UI_WEIGHT_UNITS_DROPDOWN_CONSTANTS.POPULAR_WEIGHT_UNITS;
        this.allWeightUnitsTitle = UI_WEIGHT_UNITS_DROPDOWN_CONSTANTS.ALL_WEIGHT_UNITS;
    }
    UiWeightUnitsDropdownComponent.prototype.ngOnInit = function () {
        this.weightUnitName = this.weightUnitsHelper.getWeightUnitBySymbol(this.weightUnitSymbol).name;
    };
    UiWeightUnitsDropdownComponent.prototype.onInputChange = function () {
        this.onChange.emit({ weightUnitSymbol: this.weightUnitSymbol });
    };
    UiWeightUnitsDropdownComponent.prototype.openCloseDropdown = function () {
        this.isDropdownOpened = !this.isDropdownOpened;
        this.weightUnitsList = this.weightUnitsHelper.getWeightUnitListOrderByName();
    };
    UiWeightUnitsDropdownComponent.prototype.changeWeight = function (symbol) {
        this.weightUnitSymbol = symbol;
        this.weightUnitName = this.weightUnitsHelper.getWeightUnitBySymbol(this.weightUnitSymbol).name;
        this.onChange.emit({ weightUnitSymbol: this.weightUnitSymbol });
        this.openCloseDropdown();
        this.onClearSearchInput();
    };
    UiWeightUnitsDropdownComponent.prototype.onClearSearchInput = function () {
        this.inputSearchViewModel.value = '';
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], UiWeightUnitsDropdownComponent.prototype, "weightUnitSymbol", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], UiWeightUnitsDropdownComponent.prototype, "onChange", void 0);
    UiWeightUnitsDropdownComponent = tslib_1.__decorate([
        Component({
            selector: 'ui-weight-units-dropdown',
            template: "<div class=\"ui-weight-input\">\n    <span class=\"weight-char\" (click)=\"openCloseDropdown()\">{{weightUnitSymbol}}</span>\n    <input (click)=\"openCloseDropdown()\" type=\"text\" class=\"weight-char__input\" (input)=\"onInputChange()\" [(ngModel)]=\"weightUnitName\"\n        [ngModelOptions]=\"{standalone: true}\" readonly>\n    <div *ngIf=\"isDropdownOpened\" style=\"min-width: 300px;\" class=\"weight-input__dropdown\" (mouseleave)=\"openCloseDropdown()\">\n        <div class=\"ui-weight-input__search--style\">\n            <ui-input-search (onClearIconClicked)=\"onClearSearchInput()\" [viewModel]=\"inputSearchViewModel\" #searchInput ngDefaultControl\n                [(ngModel)]=\"inputSearchViewModel.value\"></ui-input-search>\n        </div>\n        <br />\n        <div *ngIf=\"!inputSearchViewModel.value\">\n            <p class=\"ui-weight-input__dropdown--title\">{{popularWeightUnitsTitle}}</p>\n            <div *ngFor=\"let weight of popularWeightUnitsList\" class=\"weight-input__dropdown-item\" (click)=\"changeWeight(weight.symbol)\">\n                <span class=\"weight-input__dropdown-char\">{{weight.symbol}}</span>\n                <span class=\"weight-input__dropdown-name\">{{weight.name}}</span>\n            </div>\n            <br />\n            <hr>\n            <br />\n            <p class=\"ui-weight-input__dropdown--title\">{{allWeightUnitsTitle}}</p>\n        </div>\n        <div *ngFor=\"let weight of weightUnitsList | searchPipe:'name,symbol':inputSearchViewModel.value\" class=\"weight-input__dropdown-item\"\n            (click)=\"changeWeight(weight.symbol)\">\n            <span class=\"weight-input__dropdown-char\">{{weight.symbol}}</span>\n            <span class=\"weight-input__dropdown-name\">{{weight.name}}</span>\n        </div>\n    </div>\n</div>\n"
        }),
        tslib_1.__metadata("design:paramtypes", [WeightUnitsListHelper])
    ], UiWeightUnitsDropdownComponent);
    return UiWeightUnitsDropdownComponent;
}());
export { UiWeightUnitsDropdownComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktd2VpZ2h0LXVuaXRzLWRyb3Bkb3duLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2h1dWItbWF0ZXJpYWwvbGliLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9mb3Jtcy93ZWlnaHQtdW5pdHMtZHJvcGRvd24vdWktd2VpZ2h0LXVuaXRzLWRyb3Bkb3duLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN2RCxPQUFPLEVBQUUsa0NBQWtDLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQU16RjtJQW1CSSx3Q0FBb0IsaUJBQXdDO1FBQXhDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBdUI7UUFkckQseUJBQW9CLEdBQUc7WUFDMUIsV0FBVyxFQUFFLGtDQUFrQyxDQUFDLGtCQUFrQjtZQUNsRSxLQUFLLEVBQUUsRUFBRTtTQUNaLENBQUM7UUFFZSxhQUFRLEdBQXlCLElBQUksWUFBWSxFQUFFLENBQUM7UUFFOUQscUJBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLG9CQUFlLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLDRCQUE0QixFQUFFLENBQUM7UUFDeEUsMkJBQXNCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLG9DQUFvQyxFQUFFLENBQUM7UUFFdkYsNEJBQXVCLEdBQUcsa0NBQWtDLENBQUMsb0JBQW9CLENBQUM7UUFDbEYsd0JBQW1CLEdBQUcsa0NBQWtDLENBQUMsZ0JBQWdCLENBQUM7SUFFbEIsQ0FBQztJQUV6RCxpREFBUSxHQUFmO1FBQ0ksSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsSUFBSSxDQUFDO0lBQ25HLENBQUM7SUFFTSxzREFBYSxHQUFwQjtRQUNJLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQztJQUNwRSxDQUFDO0lBRU0sMERBQWlCLEdBQXhCO1FBQ0ksSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1FBQy9DLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLDRCQUE0QixFQUFFLENBQUM7SUFDakYsQ0FBQztJQUVNLHFEQUFZLEdBQW5CLFVBQW9CLE1BQU07UUFDdEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLE1BQU0sQ0FBQztRQUMvQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDL0YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDO1FBQ2hFLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO0lBQzlCLENBQUM7SUFFTSwyREFBa0IsR0FBekI7UUFDSSxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztJQUN6QyxDQUFDO0lBM0NRO1FBQVIsS0FBSyxFQUFFOzs0RUFBa0M7SUFTaEM7UUFBVCxNQUFNLEVBQUU7MENBQWtCLFlBQVk7b0VBQThCO0lBVjVELDhCQUE4QjtRQUoxQyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsMEJBQTBCO1lBQ3BDLG95REFBd0Q7U0FDM0QsQ0FBQztpREFvQnlDLHFCQUFxQjtPQW5CbkQsOEJBQThCLENBNkMxQztJQUFELHFDQUFDO0NBQUEsQUE3Q0QsSUE2Q0M7U0E3Q1ksOEJBQThCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgV2VpZ2h0VW5pdHNMaXN0SGVscGVyIH0gZnJvbSAnaHV1Yi11dGlscy9saWInO1xuaW1wb3J0IHsgVUlfV0VJR0hUX1VOSVRTX0RST1BET1dOX0NPTlNUQU5UUyB9IGZyb20gJy4vdWktd2VpZ2h0LXVuaXRzLWRyb3Bkb3duLmNvbnN0YW50JztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICd1aS13ZWlnaHQtdW5pdHMtZHJvcGRvd24nLFxuICAgIHRlbXBsYXRlVXJsOiAnLi91aS13ZWlnaHQtdW5pdHMtZHJvcGRvd24uY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFVpV2VpZ2h0VW5pdHNEcm9wZG93bkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgQElucHV0KCkgcHVibGljIHdlaWdodFVuaXRTeW1ib2w/OiBzdHJpbmc7XG5cbiAgICBwdWJsaWMgd2VpZ2h0VW5pdE5hbWU/OiBzdHJpbmc7XG5cbiAgICBwdWJsaWMgaW5wdXRTZWFyY2hWaWV3TW9kZWwgPSB7XG4gICAgICAgIHBsYWNlaG9sZGVyOiBVSV9XRUlHSFRfVU5JVFNfRFJPUERPV05fQ09OU1RBTlRTLlNFQVJDSF9XRUlHSFRfVU5JVCxcbiAgICAgICAgdmFsdWU6ICcnXG4gICAgfTtcblxuICAgIEBPdXRwdXQoKSBwdWJsaWMgb25DaGFuZ2U6IEV2ZW50RW1pdHRlcjxvYmplY3Q+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgcHVibGljIGlzRHJvcGRvd25PcGVuZWQgPSBmYWxzZTtcbiAgICBwdWJsaWMgd2VpZ2h0VW5pdHNMaXN0ID0gdGhpcy53ZWlnaHRVbml0c0hlbHBlci5nZXRXZWlnaHRVbml0TGlzdE9yZGVyQnlOYW1lKCk7XG4gICAgcHVibGljIHBvcHVsYXJXZWlnaHRVbml0c0xpc3QgPSB0aGlzLndlaWdodFVuaXRzSGVscGVyLmdldFBvcHVsYXJXZWlnaHRVbml0c0xpc3RPcmRlckJ5TmFtZSgpO1xuXG4gICAgcHVibGljIHBvcHVsYXJXZWlnaHRVbml0c1RpdGxlID0gVUlfV0VJR0hUX1VOSVRTX0RST1BET1dOX0NPTlNUQU5UUy5QT1BVTEFSX1dFSUdIVF9VTklUUztcbiAgICBwdWJsaWMgYWxsV2VpZ2h0VW5pdHNUaXRsZSA9IFVJX1dFSUdIVF9VTklUU19EUk9QRE9XTl9DT05TVEFOVFMuQUxMX1dFSUdIVF9VTklUUztcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgd2VpZ2h0VW5pdHNIZWxwZXI6IFdlaWdodFVuaXRzTGlzdEhlbHBlcikge31cblxuICAgIHB1YmxpYyBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy53ZWlnaHRVbml0TmFtZSA9IHRoaXMud2VpZ2h0VW5pdHNIZWxwZXIuZ2V0V2VpZ2h0VW5pdEJ5U3ltYm9sKHRoaXMud2VpZ2h0VW5pdFN5bWJvbCkubmFtZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25JbnB1dENoYW5nZSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5vbkNoYW5nZS5lbWl0KHsgd2VpZ2h0VW5pdFN5bWJvbDogdGhpcy53ZWlnaHRVbml0U3ltYm9sIH0pO1xuICAgIH1cblxuICAgIHB1YmxpYyBvcGVuQ2xvc2VEcm9wZG93bigpIHtcbiAgICAgICAgdGhpcy5pc0Ryb3Bkb3duT3BlbmVkID0gIXRoaXMuaXNEcm9wZG93bk9wZW5lZDtcbiAgICAgICAgdGhpcy53ZWlnaHRVbml0c0xpc3QgPSB0aGlzLndlaWdodFVuaXRzSGVscGVyLmdldFdlaWdodFVuaXRMaXN0T3JkZXJCeU5hbWUoKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgY2hhbmdlV2VpZ2h0KHN5bWJvbCkge1xuICAgICAgICB0aGlzLndlaWdodFVuaXRTeW1ib2wgPSBzeW1ib2w7XG4gICAgICAgIHRoaXMud2VpZ2h0VW5pdE5hbWUgPSB0aGlzLndlaWdodFVuaXRzSGVscGVyLmdldFdlaWdodFVuaXRCeVN5bWJvbCh0aGlzLndlaWdodFVuaXRTeW1ib2wpLm5hbWU7XG4gICAgICAgIHRoaXMub25DaGFuZ2UuZW1pdCh7IHdlaWdodFVuaXRTeW1ib2w6IHRoaXMud2VpZ2h0VW5pdFN5bWJvbCB9KTtcbiAgICAgICAgdGhpcy5vcGVuQ2xvc2VEcm9wZG93bigpO1xuICAgICAgICB0aGlzLm9uQ2xlYXJTZWFyY2hJbnB1dCgpO1xuICAgIH1cblxuICAgIHB1YmxpYyBvbkNsZWFyU2VhcmNoSW5wdXQoKSB7XG4gICAgICAgIHRoaXMuaW5wdXRTZWFyY2hWaWV3TW9kZWwudmFsdWUgPSAnJztcbiAgICB9XG59XG4iXX0=