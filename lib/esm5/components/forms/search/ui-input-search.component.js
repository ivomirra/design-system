import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
var UiInputSearchComponent = /** @class */ (function () {
    function UiInputSearchComponent() {
        this.viewModel = {
            placeholder: '',
            value: ''
        };
        /** Property that sould be used for binding a callback to receive the
         input submission **/
        this.onSubmit = new EventEmitter();
        /** Property that sould be used for binding a callback to receive the
         input change state **/
        this.onChange = new EventEmitter();
    }
    /**
     * Method binded to the form submit event.
     */
    UiInputSearchComponent.prototype.onSubmitForm = function () {
        this.onSubmit.emit(this.viewModel.value);
    };
    /**
     * Method binded to the input change event.
     */
    UiInputSearchComponent.prototype.onInputChange = function () {
        this.onChange.emit(this.viewModel.value);
    };
    UiInputSearchComponent.prototype.onClearIconClicked = function () {
        this.viewModel.value = '';
        this.onSubmitForm();
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], UiInputSearchComponent.prototype, "viewModel", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], UiInputSearchComponent.prototype, "onSubmit", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], UiInputSearchComponent.prototype, "onChange", void 0);
    UiInputSearchComponent = tslib_1.__decorate([
        Component({
            selector: 'ui-input-search',
            template: "<form (submit)=\"onSubmitForm()\" class=\"ui-input-search__form\">\n    <input type=\"text\"\n       class=\"ui-input-search\"\n       [attr.placeholder]=\"viewModel.placeholder\"\n       (input)=\"onInputChange()\"\n       [(ngModel)]=\"viewModel.value\"\n       [ngModelOptions]=\"{standalone: true}\">\n    <i class=\"huub-material-icon ui-input-search__icon\" icon=\"search\"></i>\n    <i *ngIf=\"viewModel.value && viewModel.value !== ''\"\n        (click)=\"onClearIconClicked()\"\n        class=\"huub-material-icon ui-input-clear__icon\"\n        icon=\"close\"></i>\n</form>\n"
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], UiInputSearchComponent);
    return UiInputSearchComponent;
}());
export { UiInputSearchComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktaW5wdXQtc2VhcmNoLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2h1dWItbWF0ZXJpYWwvbGliLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9mb3Jtcy9zZWFyY2gvdWktaW5wdXQtc2VhcmNoLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU12RTtJQW9CSTtRQWxCTyxjQUFTLEdBS1o7WUFDQSxXQUFXLEVBQUUsRUFBRTtZQUNmLEtBQUssRUFBRSxFQUFFO1NBQ1osQ0FBQztRQUVGOzZCQUNxQjtRQUNKLGFBQVEsR0FBeUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUVyRTsrQkFDdUI7UUFDTixhQUFRLEdBQXlCLElBQUksWUFBWSxFQUFFLENBQUM7SUFFdEQsQ0FBQztJQUVoQjs7T0FFRztJQUNJLDZDQUFZLEdBQW5CO1FBQ0ksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQ7O09BRUc7SUFDSSw4Q0FBYSxHQUFwQjtRQUNJLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQUVNLG1EQUFrQixHQUF6QjtRQUNJLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUUxQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDeEIsQ0FBQztJQXRDRDtRQURDLEtBQUssRUFBRTs7NkRBU047SUFJUTtRQUFULE1BQU0sRUFBRTswQ0FBa0IsWUFBWTs0REFBOEI7SUFJM0Q7UUFBVCxNQUFNLEVBQUU7MENBQWtCLFlBQVk7NERBQThCO0lBbEI1RCxzQkFBc0I7UUFKbEMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixxbEJBQStDO1NBQ2xELENBQUM7O09BQ1csc0JBQXNCLENBeUNsQztJQUFELDZCQUFDO0NBQUEsQUF6Q0QsSUF5Q0M7U0F6Q1ksc0JBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICd1aS1pbnB1dC1zZWFyY2gnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi91aS1pbnB1dC1zZWFyY2guY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFVpSW5wdXRTZWFyY2hDb21wb25lbnQge1xuICAgIEBJbnB1dCgpXG4gICAgcHVibGljIHZpZXdNb2RlbDoge1xuICAgICAgICAvKiogUHJvcGVydHkgdGhhdCBkZWZpbmVzIHRoZSBpbnB1dCBwbGFjZWhvbGRlciAqKi9cbiAgICAgICAgcGxhY2Vob2xkZXI6IHN0cmluZztcbiAgICAgICAgLyoqIFByb3BlcnR5IHRoYXQgZGVmaW5lcyB0aGUgaW5wdXQgdmFsdWUgKiovXG4gICAgICAgIHZhbHVlOiBzdHJpbmc7XG4gICAgfSA9IHtcbiAgICAgICAgcGxhY2Vob2xkZXI6ICcnLFxuICAgICAgICB2YWx1ZTogJydcbiAgICB9O1xuXG4gICAgLyoqIFByb3BlcnR5IHRoYXQgc291bGQgYmUgdXNlZCBmb3IgYmluZGluZyBhIGNhbGxiYWNrIHRvIHJlY2VpdmUgdGhlXG4gICAgIGlucHV0IHN1Ym1pc3Npb24gKiovXG4gICAgQE91dHB1dCgpIHB1YmxpYyBvblN1Ym1pdDogRXZlbnRFbWl0dGVyPHN0cmluZz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgICAvKiogUHJvcGVydHkgdGhhdCBzb3VsZCBiZSB1c2VkIGZvciBiaW5kaW5nIGEgY2FsbGJhY2sgdG8gcmVjZWl2ZSB0aGVcbiAgICAgaW5wdXQgY2hhbmdlIHN0YXRlICoqL1xuICAgIEBPdXRwdXQoKSBwdWJsaWMgb25DaGFuZ2U6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgY29uc3RydWN0b3IoKSB7fVxuXG4gICAgLyoqXG4gICAgICogTWV0aG9kIGJpbmRlZCB0byB0aGUgZm9ybSBzdWJtaXQgZXZlbnQuXG4gICAgICovXG4gICAgcHVibGljIG9uU3VibWl0Rm9ybSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5vblN1Ym1pdC5lbWl0KHRoaXMudmlld01vZGVsLnZhbHVlKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBNZXRob2QgYmluZGVkIHRvIHRoZSBpbnB1dCBjaGFuZ2UgZXZlbnQuXG4gICAgICovXG4gICAgcHVibGljIG9uSW5wdXRDaGFuZ2UoKTogdm9pZCB7XG4gICAgICAgIHRoaXMub25DaGFuZ2UuZW1pdCh0aGlzLnZpZXdNb2RlbC52YWx1ZSk7XG4gICAgfVxuXG4gICAgcHVibGljIG9uQ2xlYXJJY29uQ2xpY2tlZCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy52aWV3TW9kZWwudmFsdWUgPSAnJztcblxuICAgICAgICB0aGlzLm9uU3VibWl0Rm9ybSgpO1xuICAgIH1cbn1cbiJdfQ==