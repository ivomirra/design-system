import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CopyToClipboardHelperModule } from 'huub-utils/lib';
import { UiCopyInputComponent } from './ui-copy-input.component';
import { UiCurrencyInputModule } from './../currency-input/index';
import { UiTooltipsModule } from './../../messaging/tooltips/index';
var UiCopyInputModule = /** @class */ (function () {
    function UiCopyInputModule() {
    }
    UiCopyInputModule = tslib_1.__decorate([
        NgModule({
            imports: [CommonModule, FormsModule, CopyToClipboardHelperModule, UiCurrencyInputModule, UiTooltipsModule],
            declarations: [UiCopyInputComponent],
            exports: [UiCopyInputComponent]
        })
    ], UiCopyInputModule);
    return UiCopyInputModule;
}());
export { UiCopyInputModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZm9ybXMvY29weS1pbnB1dC9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBT3BFO0lBQUE7SUFBZ0MsQ0FBQztJQUFwQixpQkFBaUI7UUFMN0IsUUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsWUFBWSxFQUFFLFdBQVcsRUFBRSwyQkFBMkIsRUFBRSxxQkFBcUIsRUFBRSxnQkFBZ0IsQ0FBQztZQUMxRyxZQUFZLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztZQUNwQyxPQUFPLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztTQUNsQyxDQUFDO09BQ1csaUJBQWlCLENBQUc7SUFBRCx3QkFBQztDQUFBLEFBQWpDLElBQWlDO1NBQXBCLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBDb3B5VG9DbGlwYm9hcmRIZWxwZXJNb2R1bGUgfSBmcm9tICdodXViLXV0aWxzL2xpYic7XG5pbXBvcnQgeyBVaUNvcHlJbnB1dENvbXBvbmVudCB9IGZyb20gJy4vdWktY29weS1pbnB1dC5jb21wb25lbnQnO1xuaW1wb3J0IHsgVWlDdXJyZW5jeUlucHV0TW9kdWxlIH0gZnJvbSAnLi8uLi9jdXJyZW5jeS1pbnB1dC9pbmRleCc7XG5pbXBvcnQgeyBVaVRvb2x0aXBzTW9kdWxlIH0gZnJvbSAnLi8uLi8uLi9tZXNzYWdpbmcvdG9vbHRpcHMvaW5kZXgnO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtDb21tb25Nb2R1bGUsIEZvcm1zTW9kdWxlLCBDb3B5VG9DbGlwYm9hcmRIZWxwZXJNb2R1bGUsIFVpQ3VycmVuY3lJbnB1dE1vZHVsZSwgVWlUb29sdGlwc01vZHVsZV0sXG4gICAgZGVjbGFyYXRpb25zOiBbVWlDb3B5SW5wdXRDb21wb25lbnRdLFxuICAgIGV4cG9ydHM6IFtVaUNvcHlJbnB1dENvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgVWlDb3B5SW5wdXRNb2R1bGUge31cbiJdfQ==