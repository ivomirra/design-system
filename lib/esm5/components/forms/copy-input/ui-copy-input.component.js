import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { UI_COPY_INPUT_CONSTANT } from './ui-copy-input.constant';
import { CopyToClipboardHelper } from 'huub-utils/lib';
var UiCopyInputComponent = /** @class */ (function () {
    function UiCopyInputComponent(copyToClipboardHelper) {
        this.copyToClipboardHelper = copyToClipboardHelper;
        this.onChange = new EventEmitter();
        this.onSubmit = new EventEmitter();
        this.isTooltipActive = false;
        this.isCopied = false;
        this.isEditing = false;
        this.isTypeCurrency = false;
        this.field = {
            left: '0',
            top: '0'
        };
        this.tooltipViewModel = {
            text: 'Tooltip test example.',
            icon: 'info',
            color: 'blue-grey-dark'
        };
        this.backupValue = '';
    }
    UiCopyInputComponent.prototype.ngOnInit = function () {
        this.viewModel = this.viewModel ? this.viewModel : {};
        this.type = this.type ? this.type : 'text';
        this.isTypeCurrency = this.type === 'currency';
        this.editable = this.editable !== undefined ? this.editable : true;
        this.disabled = this.disabled !== undefined ? this.disabled : false;
        this.viewModel.editText = this.viewModel.editText ? this.viewModel.editText : UI_COPY_INPUT_CONSTANT.EDIT_TEXT;
        this.label = this.label && typeof this.label === 'string' ? { value: this.label } : this.label;
        this.viewModel.tooltipTextBeforeCopy = this.viewModel.tooltipTextBeforeCopy
            ? this.viewModel.tooltipTextBeforeCopy
            : UI_COPY_INPUT_CONSTANT.TOOLTIP_TEXT_BEFORE_COPY;
        this.viewModel.tooltipTextAfterCopy = this.viewModel.tooltipTextAfterCopy
            ? this.viewModel.tooltipTextAfterCopy
            : UI_COPY_INPUT_CONSTANT.TOOLTIP_TEXT_AFTER_COPY;
        if (this.type === 'currency') {
            this.currencyValue = {
                currency: this.currency,
                value: this.value
            };
        }
    };
    UiCopyInputComponent.prototype.onInputChange = function (currencyValue) {
        this.currencyValue = currencyValue;
        var value = this.type === 'currency' ? this.currencyValue : this.value;
        this.onChange.emit(value);
    };
    UiCopyInputComponent.prototype.copy = function (event) {
        if (event && !this.isEditing) {
            event.target.blur();
            var value = this.type === 'currency' ? this.currencyValue.value + " " + this.currencyValue.currency : this.value;
            this.copyToClipboardHelper.copyToClipboard(value);
            this.isCopied = true;
        }
    };
    UiCopyInputComponent.prototype.edit = function () {
        this.isEditing = true;
        this.backupValue = this.value;
    };
    UiCopyInputComponent.prototype.discard = function () {
        this.isEditing = false;
        this.value = this.backupValue;
        this.backupValue = '';
    };
    UiCopyInputComponent.prototype.save = function () {
        var value = this.type === 'currency' ? this.currencyValue : this.value;
        this.onSubmit.emit(value);
        this.isEditing = false;
        this.backupValue = '';
    };
    UiCopyInputComponent.prototype.showTooltip = function (event) {
        if (event) {
            this.field.left = event.pageX + "px";
            this.field.top = event.pageY + "px";
            this.isTooltipActive = true;
        }
        else {
            this.isTooltipActive = false;
            this.isCopied = false;
        }
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], UiCopyInputComponent.prototype, "value", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], UiCopyInputComponent.prototype, "placeholder", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], UiCopyInputComponent.prototype, "label", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], UiCopyInputComponent.prototype, "type", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], UiCopyInputComponent.prototype, "currency", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], UiCopyInputComponent.prototype, "viewModel", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], UiCopyInputComponent.prototype, "disabled", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], UiCopyInputComponent.prototype, "editable", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], UiCopyInputComponent.prototype, "onChange", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], UiCopyInputComponent.prototype, "onSubmit", void 0);
    UiCopyInputComponent = tslib_1.__decorate([
        Component({
            selector: 'ui-copy-input',
            providers: [CopyToClipboardHelper],
            template: "<div class=\"ui-copy-input\">\n    <span class=\"ui-copy-input__label\" *ngIf=\"label\">\n        {{label.value}} <ui-tooltips *ngIf=\"label.tooltipViewModel\" [viewModel]=\"label.tooltipViewModel\"></ui-tooltips>\n    </span>\n    <span class=\"ui-copy-input__edit\" *ngIf=\"editable\" (click)=\"edit()\">{{viewModel.editText}}</span>\n    <div class=\"ui-copy-input__wrapper\" (mouseenter)=\"showTooltip($event)\" (mousemove)=\"showTooltip($event)\"\n        (mouseleave)=\"showTooltip()\" (click)=\"copy($event)\">\n        <div *ngIf=\"!isEditing\" class=\"ui-copy-input__blocker\"></div>\n        <input *ngIf=\"!isTypeCurrency\" type=\"text\" class=\"ui-copy-input-area\"\n            [ngClass]=\"{'ui-copy-input-area__editing': isEditing}\" [attr.placeholder]=\"placeholder\"\n            (input)=\"onInputChange()\" [(ngModel)]=\"value\" [ngModelOptions]=\"{standalone: true}\" [disabled]=\"disabled\">\n        <ui-currency-input *ngIf=\"isTypeCurrency\" [currency]=\"currency\" [placeholder]=\"placeholder\"\n            (onChange)=\"onInputChange($event)\" [value]=\"value\" [disabled]=\"disabled\">\n        </ui-currency-input>\n    </div>\n    <div *ngIf=\"isEditing\" class=\"ui-copy-input__edit-buttons\">\n        <div class=\"ui-copy-input__edit-button\" (click)=\"save()\"><i class=\"huub-material-icon\" icon=\"check\"\n                size=\"extra-small\" color=\"green\"></i></div>\n        <div class=\"ui-copy-input__edit-button\" (click)=\"discard()\"><i class=\"huub-material-icon\" icon=\"close\"\n                size=\"extra-small\" color=\"red\"></i></div>\n    </div>\n    <div *ngIf=\"!isEditing && isTooltipActive\" class=\"ui-copy-input-tooltip\"\n        [ngStyle]=\"{left:field.left, top:field.top}\">\n        <div *ngIf=\"!isCopied\">{{viewModel.tooltipTextBeforeCopy}}</div>\n        <div *ngIf=\"isCopied\"><i class=\"huub-material-icon\" icon=\"check\" size=\"extra-extra-small\"></i>\n            {{viewModel.tooltipTextAfterCopy}}\n        </div>\n    </div>\n</div>"
        }),
        tslib_1.__metadata("design:paramtypes", [CopyToClipboardHelper])
    ], UiCopyInputComponent);
    return UiCopyInputComponent;
}());
export { UiCopyInputComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktY29weS1pbnB1dC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZm9ybXMvY29weS1pbnB1dC91aS1jb3B5LWlucHV0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNsRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQU92RDtJQXVDSSw4QkFBb0IscUJBQTRDO1FBQTVDLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBdUI7UUF0Qi9DLGFBQVEsR0FBc0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUVqRCxhQUFRLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7UUFFM0Qsb0JBQWUsR0FBRyxLQUFLLENBQUM7UUFDeEIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLFVBQUssR0FBRztZQUNYLElBQUksRUFBRSxHQUFHO1lBQ1QsR0FBRyxFQUFFLEdBQUc7U0FDWCxDQUFDO1FBRUsscUJBQWdCLEdBQUc7WUFDdEIsSUFBSSxFQUFFLHVCQUF1QjtZQUM3QixJQUFJLEVBQUUsTUFBTTtZQUNaLEtBQUssRUFBRSxnQkFBZ0I7U0FDMUIsQ0FBQztRQUVNLGdCQUFXLEdBQUcsRUFBRSxDQUFDO0lBRzBDLENBQUM7SUFFN0QsdUNBQVEsR0FBZjtRQUNJLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ3RELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQzNDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLElBQUksS0FBSyxVQUFVLENBQUM7UUFDL0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ25FLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUNwRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLFNBQVMsQ0FBQztRQUMvRyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLElBQUksT0FBTyxJQUFJLENBQUMsS0FBSyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQy9GLElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUI7WUFDdkUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCO1lBQ3RDLENBQUMsQ0FBQyxzQkFBc0IsQ0FBQyx3QkFBd0IsQ0FBQztRQUN0RCxJQUFJLENBQUMsU0FBUyxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsb0JBQW9CO1lBQ3JFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLG9CQUFvQjtZQUNyQyxDQUFDLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLENBQUM7UUFDckQsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLFVBQVUsRUFBRTtZQUMxQixJQUFJLENBQUMsYUFBYSxHQUFHO2dCQUNqQixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7Z0JBQ3ZCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSzthQUNwQixDQUFDO1NBQ0w7SUFDTCxDQUFDO0lBRU0sNENBQWEsR0FBcEIsVUFBcUIsYUFBYztRQUMvQixJQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQztRQUNuQyxJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN6RSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBRU0sbUNBQUksR0FBWCxVQUFZLEtBQUs7UUFDYixJQUFJLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDMUIsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNwQixJQUFNLEtBQUssR0FDUCxJQUFJLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQyxDQUFDLENBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLFNBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7WUFDekcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNsRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztTQUN4QjtJQUNMLENBQUM7SUFFTSxtQ0FBSSxHQUFYO1FBQ0ksSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDdEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ2xDLENBQUM7SUFFTSxzQ0FBTyxHQUFkO1FBQ0ksSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDdkIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQzlCLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFTSxtQ0FBSSxHQUFYO1FBQ0ksSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDekUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDdkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUVNLDBDQUFXLEdBQWxCLFVBQW1CLEtBQU07UUFDckIsSUFBSSxLQUFLLEVBQUU7WUFDUCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksR0FBTSxLQUFLLENBQUMsS0FBSyxPQUFJLENBQUM7WUFDckMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQU0sS0FBSyxDQUFDLEtBQUssT0FBSSxDQUFDO1lBQ3BDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1NBQy9CO2FBQU07WUFDSCxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztZQUM3QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztTQUN6QjtJQUNMLENBQUM7SUF6R1E7UUFBUixLQUFLLEVBQUU7O3VEQUF1QjtJQUV0QjtRQUFSLEtBQUssRUFBRTs7NkRBQTZCO0lBRTVCO1FBQVIsS0FBSyxFQUFFOzt1REFBb0I7SUFFbkI7UUFBUixLQUFLLEVBQUU7O3NEQUFzQjtJQUVyQjtRQUFSLEtBQUssRUFBRTs7MERBQTBCO0lBRXpCO1FBQVIsS0FBSyxFQUFFOzsyREFBd0I7SUFFdkI7UUFBUixLQUFLLEVBQUU7OzBEQUEyQjtJQUUxQjtRQUFSLEtBQUssRUFBRTs7MERBQTJCO0lBRXpCO1FBQVQsTUFBTSxFQUFFOzBDQUFrQixZQUFZOzBEQUEyQjtJQUV4RDtRQUFULE1BQU0sRUFBRTswQ0FBa0IsWUFBWTswREFBMkI7SUFuQnpELG9CQUFvQjtRQUxoQyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsZUFBZTtZQUN6QixTQUFTLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQztZQUNsQyxxK0RBQTZDO1NBQ2hELENBQUM7aURBd0M2QyxxQkFBcUI7T0F2Q3ZELG9CQUFvQixDQTJHaEM7SUFBRCwyQkFBQztDQUFBLEFBM0dELElBMkdDO1NBM0dZLG9CQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFVJX0NPUFlfSU5QVVRfQ09OU1RBTlQgfSBmcm9tICcuL3VpLWNvcHktaW5wdXQuY29uc3RhbnQnO1xuaW1wb3J0IHsgQ29weVRvQ2xpcGJvYXJkSGVscGVyIH0gZnJvbSAnaHV1Yi11dGlscy9saWInO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ3VpLWNvcHktaW5wdXQnLFxuICAgIHByb3ZpZGVyczogW0NvcHlUb0NsaXBib2FyZEhlbHBlcl0sXG4gICAgdGVtcGxhdGVVcmw6ICcuL3VpLWNvcHktaW5wdXQuY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFVpQ29weUlucHV0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICBASW5wdXQoKSBwdWJsaWMgdmFsdWU/OiBzdHJpbmc7XG5cbiAgICBASW5wdXQoKSBwdWJsaWMgcGxhY2Vob2xkZXI/OiBzdHJpbmc7XG5cbiAgICBASW5wdXQoKSBwdWJsaWMgbGFiZWw/OiBhbnk7XG5cbiAgICBASW5wdXQoKSBwdWJsaWMgdHlwZT86IHN0cmluZztcblxuICAgIEBJbnB1dCgpIHB1YmxpYyBjdXJyZW5jeT86IHN0cmluZztcblxuICAgIEBJbnB1dCgpIHB1YmxpYyB2aWV3TW9kZWw/OiBhbnk7XG5cbiAgICBASW5wdXQoKSBwdWJsaWMgZGlzYWJsZWQ/OiBib29sZWFuO1xuXG4gICAgQElucHV0KCkgcHVibGljIGVkaXRhYmxlPzogYm9vbGVhbjtcblxuICAgIEBPdXRwdXQoKSBwdWJsaWMgb25DaGFuZ2U6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgQE91dHB1dCgpIHB1YmxpYyBvblN1Ym1pdDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgICBwdWJsaWMgaXNUb29sdGlwQWN0aXZlID0gZmFsc2U7XG4gICAgcHVibGljIGlzQ29waWVkID0gZmFsc2U7XG4gICAgcHVibGljIGlzRWRpdGluZyA9IGZhbHNlO1xuICAgIHB1YmxpYyBpc1R5cGVDdXJyZW5jeSA9IGZhbHNlO1xuICAgIHB1YmxpYyBmaWVsZCA9IHtcbiAgICAgICAgbGVmdDogJzAnLFxuICAgICAgICB0b3A6ICcwJ1xuICAgIH07XG5cbiAgICBwdWJsaWMgdG9vbHRpcFZpZXdNb2RlbCA9IHtcbiAgICAgICAgdGV4dDogJ1Rvb2x0aXAgdGVzdCBleGFtcGxlLicsXG4gICAgICAgIGljb246ICdpbmZvJyxcbiAgICAgICAgY29sb3I6ICdibHVlLWdyZXktZGFyaydcbiAgICB9O1xuXG4gICAgcHJpdmF0ZSBiYWNrdXBWYWx1ZSA9ICcnO1xuICAgIHByaXZhdGUgY3VycmVuY3lWYWx1ZTogYW55O1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBjb3B5VG9DbGlwYm9hcmRIZWxwZXI6IENvcHlUb0NsaXBib2FyZEhlbHBlcikge31cblxuICAgIHB1YmxpYyBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy52aWV3TW9kZWwgPSB0aGlzLnZpZXdNb2RlbCA/IHRoaXMudmlld01vZGVsIDoge307XG4gICAgICAgIHRoaXMudHlwZSA9IHRoaXMudHlwZSA/IHRoaXMudHlwZSA6ICd0ZXh0JztcbiAgICAgICAgdGhpcy5pc1R5cGVDdXJyZW5jeSA9IHRoaXMudHlwZSA9PT0gJ2N1cnJlbmN5JztcbiAgICAgICAgdGhpcy5lZGl0YWJsZSA9IHRoaXMuZWRpdGFibGUgIT09IHVuZGVmaW5lZCA/IHRoaXMuZWRpdGFibGUgOiB0cnVlO1xuICAgICAgICB0aGlzLmRpc2FibGVkID0gdGhpcy5kaXNhYmxlZCAhPT0gdW5kZWZpbmVkID8gdGhpcy5kaXNhYmxlZCA6IGZhbHNlO1xuICAgICAgICB0aGlzLnZpZXdNb2RlbC5lZGl0VGV4dCA9IHRoaXMudmlld01vZGVsLmVkaXRUZXh0ID8gdGhpcy52aWV3TW9kZWwuZWRpdFRleHQgOiBVSV9DT1BZX0lOUFVUX0NPTlNUQU5ULkVESVRfVEVYVDtcbiAgICAgICAgdGhpcy5sYWJlbCA9IHRoaXMubGFiZWwgJiYgdHlwZW9mIHRoaXMubGFiZWwgPT09ICdzdHJpbmcnID8geyB2YWx1ZTogdGhpcy5sYWJlbCB9IDogdGhpcy5sYWJlbDtcbiAgICAgICAgdGhpcy52aWV3TW9kZWwudG9vbHRpcFRleHRCZWZvcmVDb3B5ID0gdGhpcy52aWV3TW9kZWwudG9vbHRpcFRleHRCZWZvcmVDb3B5XG4gICAgICAgICAgICA/IHRoaXMudmlld01vZGVsLnRvb2x0aXBUZXh0QmVmb3JlQ29weVxuICAgICAgICAgICAgOiBVSV9DT1BZX0lOUFVUX0NPTlNUQU5ULlRPT0xUSVBfVEVYVF9CRUZPUkVfQ09QWTtcbiAgICAgICAgdGhpcy52aWV3TW9kZWwudG9vbHRpcFRleHRBZnRlckNvcHkgPSB0aGlzLnZpZXdNb2RlbC50b29sdGlwVGV4dEFmdGVyQ29weVxuICAgICAgICAgICAgPyB0aGlzLnZpZXdNb2RlbC50b29sdGlwVGV4dEFmdGVyQ29weVxuICAgICAgICAgICAgOiBVSV9DT1BZX0lOUFVUX0NPTlNUQU5ULlRPT0xUSVBfVEVYVF9BRlRFUl9DT1BZO1xuICAgICAgICBpZiAodGhpcy50eXBlID09PSAnY3VycmVuY3knKSB7XG4gICAgICAgICAgICB0aGlzLmN1cnJlbmN5VmFsdWUgPSB7XG4gICAgICAgICAgICAgICAgY3VycmVuY3k6IHRoaXMuY3VycmVuY3ksXG4gICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMudmFsdWVcbiAgICAgICAgICAgIH07XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgb25JbnB1dENoYW5nZShjdXJyZW5jeVZhbHVlPyk6IHZvaWQge1xuICAgICAgICB0aGlzLmN1cnJlbmN5VmFsdWUgPSBjdXJyZW5jeVZhbHVlO1xuICAgICAgICBjb25zdCB2YWx1ZSA9IHRoaXMudHlwZSA9PT0gJ2N1cnJlbmN5JyA/IHRoaXMuY3VycmVuY3lWYWx1ZSA6IHRoaXMudmFsdWU7XG4gICAgICAgIHRoaXMub25DaGFuZ2UuZW1pdCh2YWx1ZSk7XG4gICAgfVxuXG4gICAgcHVibGljIGNvcHkoZXZlbnQpIHtcbiAgICAgICAgaWYgKGV2ZW50ICYmICF0aGlzLmlzRWRpdGluZykge1xuICAgICAgICAgICAgZXZlbnQudGFyZ2V0LmJsdXIoKTtcbiAgICAgICAgICAgIGNvbnN0IHZhbHVlID1cbiAgICAgICAgICAgICAgICB0aGlzLnR5cGUgPT09ICdjdXJyZW5jeScgPyBgJHt0aGlzLmN1cnJlbmN5VmFsdWUudmFsdWV9ICR7dGhpcy5jdXJyZW5jeVZhbHVlLmN1cnJlbmN5fWAgOiB0aGlzLnZhbHVlO1xuICAgICAgICAgICAgdGhpcy5jb3B5VG9DbGlwYm9hcmRIZWxwZXIuY29weVRvQ2xpcGJvYXJkKHZhbHVlKTtcbiAgICAgICAgICAgIHRoaXMuaXNDb3BpZWQgPSB0cnVlO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIGVkaXQoKSB7XG4gICAgICAgIHRoaXMuaXNFZGl0aW5nID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5iYWNrdXBWYWx1ZSA9IHRoaXMudmFsdWU7XG4gICAgfVxuXG4gICAgcHVibGljIGRpc2NhcmQoKSB7XG4gICAgICAgIHRoaXMuaXNFZGl0aW5nID0gZmFsc2U7XG4gICAgICAgIHRoaXMudmFsdWUgPSB0aGlzLmJhY2t1cFZhbHVlO1xuICAgICAgICB0aGlzLmJhY2t1cFZhbHVlID0gJyc7XG4gICAgfVxuXG4gICAgcHVibGljIHNhdmUoKSB7XG4gICAgICAgIGNvbnN0IHZhbHVlID0gdGhpcy50eXBlID09PSAnY3VycmVuY3knID8gdGhpcy5jdXJyZW5jeVZhbHVlIDogdGhpcy52YWx1ZTtcbiAgICAgICAgdGhpcy5vblN1Ym1pdC5lbWl0KHZhbHVlKTtcbiAgICAgICAgdGhpcy5pc0VkaXRpbmcgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5iYWNrdXBWYWx1ZSA9ICcnO1xuICAgIH1cblxuICAgIHB1YmxpYyBzaG93VG9vbHRpcChldmVudD8pIHtcbiAgICAgICAgaWYgKGV2ZW50KSB7XG4gICAgICAgICAgICB0aGlzLmZpZWxkLmxlZnQgPSBgJHtldmVudC5wYWdlWH1weGA7XG4gICAgICAgICAgICB0aGlzLmZpZWxkLnRvcCA9IGAke2V2ZW50LnBhZ2VZfXB4YDtcbiAgICAgICAgICAgIHRoaXMuaXNUb29sdGlwQWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuaXNUb29sdGlwQWN0aXZlID0gZmFsc2U7XG4gICAgICAgICAgICB0aGlzLmlzQ29waWVkID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=