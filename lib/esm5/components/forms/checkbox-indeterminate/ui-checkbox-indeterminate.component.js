import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
var UiCheckboxIndeterminateComponent = /** @class */ (function () {
    function UiCheckboxIndeterminateComponent() {
        this.onChange = new EventEmitter();
        this.statusValues = {
            unchecked: 'unchecked',
            indeterminate: 'indeterminate',
            checked: 'checked'
        };
    }
    UiCheckboxIndeterminateComponent.prototype.getNextStatus = function (currentStatus) {
        switch (currentStatus) {
            case this.statusValues.unchecked:
                return this.statusValues.indeterminate;
            case this.statusValues.indeterminate:
                return this.statusValues.checked;
            case this.statusValues.checked:
            default:
                return this.statusValues.unchecked;
        }
    };
    UiCheckboxIndeterminateComponent.prototype.ngOnInit = function () {
        this.status = this.statusValues[this.status] || this.statusValues.unchecked;
    };
    UiCheckboxIndeterminateComponent.prototype.onCheckboxClick = function () {
        this.status = this.getNextStatus(this.status);
        this.onChange.emit(this.status);
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], UiCheckboxIndeterminateComponent.prototype, "status", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], UiCheckboxIndeterminateComponent.prototype, "disabled", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], UiCheckboxIndeterminateComponent.prototype, "onChange", void 0);
    UiCheckboxIndeterminateComponent = tslib_1.__decorate([
        Component({
            selector: 'ui-checkbox-indeterminate',
            template: "<input type=\"checkbox\"\n       class=\"huub-material-icon\"\n       [ngClass]=\"{\n           'indeterminate': status === statusValues.indeterminate\n       }\"\n       [checked]=\"status === statusValues.checked || status === statusValues.indeterminate\"\n       (change)=\"onCheckboxClick()\"\n       [disabled]=\"disabled\">\n"
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], UiCheckboxIndeterminateComponent);
    return UiCheckboxIndeterminateComponent;
}());
export { UiCheckboxIndeterminateComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktY2hlY2tib3gtaW5kZXRlcm1pbmF0ZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZm9ybXMvY2hlY2tib3gtaW5kZXRlcm1pbmF0ZS91aS1jaGVja2JveC1pbmRldGVybWluYXRlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBVSxNQUFNLGVBQWUsQ0FBQztBQU0vRTtJQWFJO1FBUmlCLGFBQVEsR0FBc0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUUzRCxpQkFBWSxHQUFHO1lBQ2xCLFNBQVMsRUFBRSxXQUFXO1lBQ3RCLGFBQWEsRUFBRSxlQUFlO1lBQzlCLE9BQU8sRUFBRSxTQUFTO1NBQ3JCLENBQUM7SUFFYSxDQUFDO0lBRVIsd0RBQWEsR0FBckIsVUFBc0IsYUFBYTtRQUMvQixRQUFRLGFBQWEsRUFBRTtZQUNuQixLQUFLLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUztnQkFDNUIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQztZQUMzQyxLQUFLLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYTtnQkFDaEMsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQztZQUNyQyxLQUFLLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDO1lBQy9CO2dCQUNJLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUM7U0FDMUM7SUFDTCxDQUFDO0lBRU0sbURBQVEsR0FBZjtRQUNJLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUM7SUFDaEYsQ0FBQztJQUVNLDBEQUFlLEdBQXRCO1FBQ0ksSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUU5QyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQWxDUTtRQUFSLEtBQUssRUFBRTs7b0VBQXdCO0lBRXZCO1FBQVIsS0FBSyxFQUFFOztzRUFBMkI7SUFFekI7UUFBVCxNQUFNLEVBQUU7MENBQWtCLFlBQVk7c0VBQTJCO0lBTHpELGdDQUFnQztRQUo1QyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsMkJBQTJCO1lBQ3JDLHVWQUF5RDtTQUM1RCxDQUFDOztPQUNXLGdDQUFnQyxDQW9DNUM7SUFBRCx1Q0FBQztDQUFBLEFBcENELElBb0NDO1NBcENZLGdDQUFnQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICd1aS1jaGVja2JveC1pbmRldGVybWluYXRlJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vdWktY2hlY2tib3gtaW5kZXRlcm1pbmF0ZS5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgVWlDaGVja2JveEluZGV0ZXJtaW5hdGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIEBJbnB1dCgpIHB1YmxpYyBzdGF0dXM/OiBzdHJpbmc7XG5cbiAgICBASW5wdXQoKSBwdWJsaWMgZGlzYWJsZWQ/OiBib29sZWFuO1xuXG4gICAgQE91dHB1dCgpIHB1YmxpYyBvbkNoYW5nZTogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgICBwdWJsaWMgc3RhdHVzVmFsdWVzID0ge1xuICAgICAgICB1bmNoZWNrZWQ6ICd1bmNoZWNrZWQnLFxuICAgICAgICBpbmRldGVybWluYXRlOiAnaW5kZXRlcm1pbmF0ZScsXG4gICAgICAgIGNoZWNrZWQ6ICdjaGVja2VkJ1xuICAgIH07XG5cbiAgICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgICBwcml2YXRlIGdldE5leHRTdGF0dXMoY3VycmVudFN0YXR1cykge1xuICAgICAgICBzd2l0Y2ggKGN1cnJlbnRTdGF0dXMpIHtcbiAgICAgICAgICAgIGNhc2UgdGhpcy5zdGF0dXNWYWx1ZXMudW5jaGVja2VkOlxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnN0YXR1c1ZhbHVlcy5pbmRldGVybWluYXRlO1xuICAgICAgICAgICAgY2FzZSB0aGlzLnN0YXR1c1ZhbHVlcy5pbmRldGVybWluYXRlOlxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnN0YXR1c1ZhbHVlcy5jaGVja2VkO1xuICAgICAgICAgICAgY2FzZSB0aGlzLnN0YXR1c1ZhbHVlcy5jaGVja2VkOlxuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5zdGF0dXNWYWx1ZXMudW5jaGVja2VkO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLnN0YXR1cyA9IHRoaXMuc3RhdHVzVmFsdWVzW3RoaXMuc3RhdHVzXSB8fCB0aGlzLnN0YXR1c1ZhbHVlcy51bmNoZWNrZWQ7XG4gICAgfVxuXG4gICAgcHVibGljIG9uQ2hlY2tib3hDbGljaygpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zdGF0dXMgPSB0aGlzLmdldE5leHRTdGF0dXModGhpcy5zdGF0dXMpO1xuXG4gICAgICAgIHRoaXMub25DaGFuZ2UuZW1pdCh0aGlzLnN0YXR1cyk7XG4gICAgfVxufVxuIl19