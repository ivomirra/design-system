import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UiCheckboxIndeterminateComponent } from './ui-checkbox-indeterminate.component';
var UiCheckboxIndeterminateModule = /** @class */ (function () {
    function UiCheckboxIndeterminateModule() {
    }
    UiCheckboxIndeterminateModule = tslib_1.__decorate([
        NgModule({
            imports: [CommonModule, FormsModule],
            declarations: [UiCheckboxIndeterminateComponent],
            exports: [UiCheckboxIndeterminateComponent]
        })
    ], UiCheckboxIndeterminateModule);
    return UiCheckboxIndeterminateModule;
}());
export { UiCheckboxIndeterminateModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZm9ybXMvY2hlY2tib3gtaW5kZXRlcm1pbmF0ZS9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxnQ0FBZ0MsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBT3pGO0lBQUE7SUFBNEMsQ0FBQztJQUFoQyw2QkFBNkI7UUFMekMsUUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsWUFBWSxFQUFFLFdBQVcsQ0FBQztZQUNwQyxZQUFZLEVBQUUsQ0FBQyxnQ0FBZ0MsQ0FBQztZQUNoRCxPQUFPLEVBQUUsQ0FBQyxnQ0FBZ0MsQ0FBQztTQUM5QyxDQUFDO09BQ1csNkJBQTZCLENBQUc7SUFBRCxvQ0FBQztDQUFBLEFBQTdDLElBQTZDO1NBQWhDLDZCQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBVaUNoZWNrYm94SW5kZXRlcm1pbmF0ZUNvbXBvbmVudCB9IGZyb20gJy4vdWktY2hlY2tib3gtaW5kZXRlcm1pbmF0ZS5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtDb21tb25Nb2R1bGUsIEZvcm1zTW9kdWxlXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtVaUNoZWNrYm94SW5kZXRlcm1pbmF0ZUNvbXBvbmVudF0sXG4gICAgZXhwb3J0czogW1VpQ2hlY2tib3hJbmRldGVybWluYXRlQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBVaUNoZWNrYm94SW5kZXRlcm1pbmF0ZU1vZHVsZSB7fVxuIl19