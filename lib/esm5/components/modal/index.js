import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';
import { UiModalService } from './ui-modal.service';
import { UiModalComponent } from './ui-modal.component';
var UiModalModule = /** @class */ (function () {
    function UiModalModule() {
    }
    UiModalModule = tslib_1.__decorate([
        NgModule({
            imports: [OverlayModule, CommonModule],
            entryComponents: [UiModalComponent],
            declarations: [UiModalComponent],
            providers: [UiModalService],
            exports: [UiModalComponent]
        })
    ], UiModalModule);
    return UiModalModule;
}());
export { UiModalModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvbW9kYWwvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUVyRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFTeEQ7SUFBQTtJQUE0QixDQUFDO0lBQWhCLGFBQWE7UUFQekIsUUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsYUFBYSxFQUFFLFlBQVksQ0FBQztZQUN0QyxlQUFlLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQztZQUNuQyxZQUFZLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQztZQUNoQyxTQUFTLEVBQUUsQ0FBQyxjQUFjLENBQUM7WUFDM0IsT0FBTyxFQUFFLENBQUMsZ0JBQWdCLENBQUM7U0FDOUIsQ0FBQztPQUNXLGFBQWEsQ0FBRztJQUFELG9CQUFDO0NBQUEsQUFBN0IsSUFBNkI7U0FBaEIsYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5pbXBvcnQgeyBPdmVybGF5TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL292ZXJsYXknO1xuXG5pbXBvcnQgeyBVaU1vZGFsU2VydmljZSB9IGZyb20gJy4vdWktbW9kYWwuc2VydmljZSc7XG5pbXBvcnQgeyBVaU1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi91aS1tb2RhbC5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtPdmVybGF5TW9kdWxlLCBDb21tb25Nb2R1bGVdLFxuICAgIGVudHJ5Q29tcG9uZW50czogW1VpTW9kYWxDb21wb25lbnRdLFxuICAgIGRlY2xhcmF0aW9uczogW1VpTW9kYWxDb21wb25lbnRdLFxuICAgIHByb3ZpZGVyczogW1VpTW9kYWxTZXJ2aWNlXSxcbiAgICBleHBvcnRzOiBbVWlNb2RhbENvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgVWlNb2RhbE1vZHVsZSB7fVxuIl19