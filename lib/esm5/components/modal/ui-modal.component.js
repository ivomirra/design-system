import * as tslib_1 from "tslib";
import { Component, Inject, ViewContainerRef, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { UiModalService } from './ui-modal.service';
var UiModalComponent = /** @class */ (function () {
    function UiModalComponent(modal, componentFactoryResolver, componentData) {
        this.modal = modal;
        this.componentFactoryResolver = componentFactoryResolver;
        this.componentData = componentData;
        this.viewModel = {
            title: this.componentData.title
        };
    }
    UiModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.componentData.childComponents.forEach(function (component) {
            var componentCreated = _this.viewContainerRef.createComponent(_this.componentFactoryResolver.resolveComponentFactory(component));
            componentCreated.changeDetectorRef.detectChanges();
        });
    };
    UiModalComponent.prototype.closeModal = function () {
        this.modal.close();
    };
    tslib_1.__decorate([
        ViewChild('childrenContainer', { read: ViewContainerRef, static: true }),
        tslib_1.__metadata("design:type", ViewContainerRef)
    ], UiModalComponent.prototype, "viewContainerRef", void 0);
    UiModalComponent = tslib_1.__decorate([
        Component({
            selector: 'ui-modal',
            template: "<div class=\"ui-modal\">\n    <div class=\"ui-modal__content\">\n        <div class=\"ui-modal__header\">\n            <h2 class=\"ui-modal__header-title\">{{viewModel.title}}</h2>\n            <button class=\"ui-modal__header-button\" (click)=\"closeModal()\">\n                <i class=\"huub-material-icon\" icon=\"close\"></i>\n           </button>\n        </div>\n        <div class=\"ui-modal__body\">\n            <ng-template #childrenContainer></ng-template>\n        </div>\n    </div>\n</div>\n"
        }),
        tslib_1.__param(2, Inject('MODAL_DATA')),
        tslib_1.__metadata("design:paramtypes", [UiModalService,
            ComponentFactoryResolver, Object])
    ], UiModalComponent);
    return UiModalComponent;
}());
export { UiModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktbW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaHV1Yi1tYXRlcmlhbC9saWIvIiwic291cmNlcyI6WyJjb21wb25lbnRzL21vZGFsL3VpLW1vZGFsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQVUsZ0JBQWdCLEVBQUUsd0JBQXdCLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2pILE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQU1wRDtJQVVJLDBCQUNZLEtBQXFCLEVBQ3JCLHdCQUFrRCxFQUM1QixhQUFhO1FBRm5DLFVBQUssR0FBTCxLQUFLLENBQWdCO1FBQ3JCLDZCQUF3QixHQUF4Qix3QkFBd0IsQ0FBMEI7UUFDNUIsa0JBQWEsR0FBYixhQUFhLENBQUE7UUFaeEMsY0FBUyxHQUVaO1lBQ0EsS0FBSyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSztTQUNsQyxDQUFDO0lBU0MsQ0FBQztJQUVHLG1DQUFRLEdBQWY7UUFBQSxpQkFPQztRQU5HLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFDLFNBQVM7WUFDakQsSUFBTSxnQkFBZ0IsR0FBRyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUMxRCxLQUFJLENBQUMsd0JBQXdCLENBQUMsdUJBQXVCLENBQUMsU0FBUyxDQUFDLENBQ25FLENBQUM7WUFDRixnQkFBZ0IsQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUN2RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTSxxQ0FBVSxHQUFqQjtRQUNJLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQW5CRDtRQURDLFNBQVMsQ0FBQyxtQkFBbUIsRUFBRSxFQUFFLElBQUksRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUM7MENBQy9DLGdCQUFnQjs4REFBQztJQVJsQyxnQkFBZ0I7UUFKNUIsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLFVBQVU7WUFDcEIsc2dCQUF3QztTQUMzQyxDQUFDO1FBY08sbUJBQUEsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFBO2lEQUZOLGNBQWM7WUFDSyx3QkFBd0I7T0FackQsZ0JBQWdCLENBNEI1QjtJQUFELHVCQUFDO0NBQUEsQUE1QkQsSUE0QkM7U0E1QlksZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbmplY3QsIE9uSW5pdCwgVmlld0NvbnRhaW5lclJlZiwgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFVpTW9kYWxTZXJ2aWNlIH0gZnJvbSAnLi91aS1tb2RhbC5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICd1aS1tb2RhbCcsXG4gICAgdGVtcGxhdGVVcmw6ICcuL3VpLW1vZGFsLmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBVaU1vZGFsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICBwdWJsaWMgdmlld01vZGVsOiB7XG4gICAgICAgIHRpdGxlOiBzdHJpbmc7XG4gICAgfSA9IHtcbiAgICAgICAgdGl0bGU6IHRoaXMuY29tcG9uZW50RGF0YS50aXRsZVxuICAgIH07XG5cbiAgICBAVmlld0NoaWxkKCdjaGlsZHJlbkNvbnRhaW5lcicsIHsgcmVhZDogVmlld0NvbnRhaW5lclJlZiwgc3RhdGljOiB0cnVlIH0pXG4gICAgcHJpdmF0ZSB2aWV3Q29udGFpbmVyUmVmOiBWaWV3Q29udGFpbmVyUmVmO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgbW9kYWw6IFVpTW9kYWxTZXJ2aWNlLFxuICAgICAgICBwcml2YXRlIGNvbXBvbmVudEZhY3RvcnlSZXNvbHZlcjogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLFxuICAgICAgICBASW5qZWN0KCdNT0RBTF9EQVRBJykgcHJpdmF0ZSBjb21wb25lbnREYXRhXG4gICAgKSB7fVxuXG4gICAgcHVibGljIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLmNvbXBvbmVudERhdGEuY2hpbGRDb21wb25lbnRzLmZvckVhY2goKGNvbXBvbmVudCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgY29tcG9uZW50Q3JlYXRlZCA9IHRoaXMudmlld0NvbnRhaW5lclJlZi5jcmVhdGVDb21wb25lbnQoXG4gICAgICAgICAgICAgICAgdGhpcy5jb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIucmVzb2x2ZUNvbXBvbmVudEZhY3RvcnkoY29tcG9uZW50KVxuICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIGNvbXBvbmVudENyZWF0ZWQuY2hhbmdlRGV0ZWN0b3JSZWYuZGV0ZWN0Q2hhbmdlcygpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwdWJsaWMgY2xvc2VNb2RhbCgpIHtcbiAgICAgICAgdGhpcy5tb2RhbC5jbG9zZSgpO1xuICAgIH1cbn1cbiJdfQ==