import * as tslib_1 from "tslib";
import { Subject } from 'rxjs';
import { Injectable, Injector } from '@angular/core';
import { Overlay, OverlayConfig } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
var UiModalService = /** @class */ (function () {
    function UiModalService(overlay) {
        this.overlay = overlay;
    }
    UiModalService.prototype.disposeModal = function (context) {
        this.overlayRef.dispose();
        this.modalSubject.next(context || null);
        this.modalSubject.unsubscribe();
    };
    UiModalService.prototype.openModal = function (componentParent, childComponents, modalData, backDropCallCallback) {
        var _this = this;
        if (modalData === void 0) { modalData = {}; }
        var positionStrategy = this.overlay
            .position()
            .global()
            .centerHorizontally()
            .centerVertically();
        this.modalSubject = new Subject();
        /** Creates overlay */
        this.overlayRef = this.overlay.create(new OverlayConfig({
            hasBackdrop: true,
            scrollStrategy: this.overlay.scrollStrategies.block(),
            positionStrategy: positionStrategy
        }));
        /** Appends component to overlay */
        this.overlayRef.attach(new ComponentPortal(componentParent, null, Injector.create({
            providers: [
                {
                    provide: 'MODAL_DATA',
                    useValue: tslib_1.__assign({}, modalData, { childComponents: childComponents })
                }
            ]
        })));
        /** Appends subscribes overlay close */
        this.overlayRef.backdropClick().subscribe(function () {
            if (backDropCallCallback) {
                backDropCallCallback();
                return;
            }
            _this.disposeModal();
        });
        return {
            afterClose: function () {
                return _this.modalSubject.asObservable();
            }
        };
    };
    UiModalService.prototype.close = function (data) {
        this.disposeModal(data);
    };
    UiModalService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [Overlay])
    ], UiModalService);
    return UiModalService;
}());
export { UiModalService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktbW9kYWwuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2h1dWItbWF0ZXJpYWwvbGliLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9tb2RhbC91aS1tb2RhbC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQy9CLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3JELE9BQU8sRUFBRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDOUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBR3REO0lBSUksd0JBQW9CLE9BQWdCO1FBQWhCLFlBQU8sR0FBUCxPQUFPLENBQVM7SUFBRyxDQUFDO0lBRWhDLHFDQUFZLEdBQXBCLFVBQXFCLE9BQVE7UUFDekIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNwQyxDQUFDO0lBRU0sa0NBQVMsR0FBaEIsVUFBaUIsZUFBZSxFQUFFLGVBQTJCLEVBQUUsU0FBYyxFQUFFLG9CQUFxQjtRQUFwRyxpQkFvREM7UUFwRDhELDBCQUFBLEVBQUEsY0FBYztRQUN6RSxJQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQyxPQUFPO2FBQ2hDLFFBQVEsRUFBRTthQUNWLE1BQU0sRUFBRTthQUNSLGtCQUFrQixFQUFFO2FBQ3BCLGdCQUFnQixFQUFFLENBQUM7UUFFeEIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLE9BQU8sRUFBTyxDQUFDO1FBRXZDLHNCQUFzQjtRQUN0QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUNqQyxJQUFJLGFBQWEsQ0FBQztZQUNkLFdBQVcsRUFBRSxJQUFJO1lBQ2pCLGNBQWMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRTtZQUNyRCxnQkFBZ0Isa0JBQUE7U0FDbkIsQ0FBQyxDQUNMLENBQUM7UUFFRixtQ0FBbUM7UUFDbkMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQ2xCLElBQUksZUFBZSxDQUNmLGVBQWUsRUFDZixJQUFJLEVBQ0osUUFBUSxDQUFDLE1BQU0sQ0FBQztZQUNaLFNBQVMsRUFBRTtnQkFDUDtvQkFDSSxPQUFPLEVBQUUsWUFBWTtvQkFDckIsUUFBUSx1QkFDRCxTQUFTLElBQ1osZUFBZSxpQkFBQSxHQUNsQjtpQkFDSjthQUNKO1NBQ0osQ0FBQyxDQUNMLENBQ0osQ0FBQztRQUVGLHVDQUF1QztRQUN2QyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDLFNBQVMsQ0FBQztZQUN0QyxJQUFJLG9CQUFvQixFQUFFO2dCQUN0QixvQkFBb0IsRUFBRSxDQUFDO2dCQUN2QixPQUFPO2FBQ1Y7WUFFRCxLQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFFSCxPQUFPO1lBQ0gsVUFBVSxFQUFFO2dCQUNSLE9BQU8sS0FBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUM1QyxDQUFDO1NBQ0osQ0FBQztJQUNOLENBQUM7SUFFTSw4QkFBSyxHQUFaLFVBQWEsSUFBSztRQUNkLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQXBFUSxjQUFjO1FBRDFCLFVBQVUsRUFBRTtpREFLb0IsT0FBTztPQUozQixjQUFjLENBcUUxQjtJQUFELHFCQUFDO0NBQUEsQUFyRUQsSUFxRUM7U0FyRVksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IEluamVjdGFibGUsIEluamVjdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPdmVybGF5LCBPdmVybGF5Q29uZmlnIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL292ZXJsYXknO1xuaW1wb3J0IHsgQ29tcG9uZW50UG9ydGFsIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL3BvcnRhbCc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBVaU1vZGFsU2VydmljZSB7XG4gICAgcHJpdmF0ZSBtb2RhbFN1YmplY3Q7XG4gICAgcHJpdmF0ZSBvdmVybGF5UmVmO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBvdmVybGF5OiBPdmVybGF5KSB7fVxuXG4gICAgcHJpdmF0ZSBkaXNwb3NlTW9kYWwoY29udGV4dD8pIHtcbiAgICAgICAgdGhpcy5vdmVybGF5UmVmLmRpc3Bvc2UoKTtcbiAgICAgICAgdGhpcy5tb2RhbFN1YmplY3QubmV4dChjb250ZXh0IHx8IG51bGwpO1xuICAgICAgICB0aGlzLm1vZGFsU3ViamVjdC51bnN1YnNjcmliZSgpO1xuICAgIH1cblxuICAgIHB1YmxpYyBvcGVuTW9kYWwoY29tcG9uZW50UGFyZW50LCBjaGlsZENvbXBvbmVudHM6IEFycmF5PGFueT4sIG1vZGFsRGF0YSA9IHt9LCBiYWNrRHJvcENhbGxDYWxsYmFjaz8pIHtcbiAgICAgICAgY29uc3QgcG9zaXRpb25TdHJhdGVneSA9IHRoaXMub3ZlcmxheVxuICAgICAgICAgICAgLnBvc2l0aW9uKClcbiAgICAgICAgICAgIC5nbG9iYWwoKVxuICAgICAgICAgICAgLmNlbnRlckhvcml6b250YWxseSgpXG4gICAgICAgICAgICAuY2VudGVyVmVydGljYWxseSgpO1xuXG4gICAgICAgIHRoaXMubW9kYWxTdWJqZWN0ID0gbmV3IFN1YmplY3Q8YW55PigpO1xuXG4gICAgICAgIC8qKiBDcmVhdGVzIG92ZXJsYXkgKi9cbiAgICAgICAgdGhpcy5vdmVybGF5UmVmID0gdGhpcy5vdmVybGF5LmNyZWF0ZShcbiAgICAgICAgICAgIG5ldyBPdmVybGF5Q29uZmlnKHtcbiAgICAgICAgICAgICAgICBoYXNCYWNrZHJvcDogdHJ1ZSxcbiAgICAgICAgICAgICAgICBzY3JvbGxTdHJhdGVneTogdGhpcy5vdmVybGF5LnNjcm9sbFN0cmF0ZWdpZXMuYmxvY2soKSxcbiAgICAgICAgICAgICAgICBwb3NpdGlvblN0cmF0ZWd5XG4gICAgICAgICAgICB9KVxuICAgICAgICApO1xuXG4gICAgICAgIC8qKiBBcHBlbmRzIGNvbXBvbmVudCB0byBvdmVybGF5ICovXG4gICAgICAgIHRoaXMub3ZlcmxheVJlZi5hdHRhY2goXG4gICAgICAgICAgICBuZXcgQ29tcG9uZW50UG9ydGFsKFxuICAgICAgICAgICAgICAgIGNvbXBvbmVudFBhcmVudCxcbiAgICAgICAgICAgICAgICBudWxsLFxuICAgICAgICAgICAgICAgIEluamVjdG9yLmNyZWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb3ZpZGU6ICdNT0RBTF9EQVRBJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2VWYWx1ZToge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuLi5tb2RhbERhdGEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoaWxkQ29tcG9uZW50c1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICApXG4gICAgICAgICk7XG5cbiAgICAgICAgLyoqIEFwcGVuZHMgc3Vic2NyaWJlcyBvdmVybGF5IGNsb3NlICovXG4gICAgICAgIHRoaXMub3ZlcmxheVJlZi5iYWNrZHJvcENsaWNrKCkuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgICAgIGlmIChiYWNrRHJvcENhbGxDYWxsYmFjaykge1xuICAgICAgICAgICAgICAgIGJhY2tEcm9wQ2FsbENhbGxiYWNrKCk7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLmRpc3Bvc2VNb2RhbCgpO1xuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgYWZ0ZXJDbG9zZTogKCkgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLm1vZGFsU3ViamVjdC5hc09ic2VydmFibGUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICB9XG5cbiAgICBwdWJsaWMgY2xvc2UoZGF0YT8pIHtcbiAgICAgICAgdGhpcy5kaXNwb3NlTW9kYWwoZGF0YSk7XG4gICAgfVxufVxuIl19