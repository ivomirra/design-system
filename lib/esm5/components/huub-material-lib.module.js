import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';
import { MaterialHelpersModule } from './helpers/index';
import { UiModalModule } from './modal/index';
import { UiToggleButtonsModule } from './toggle-buttons/index';
import { UiTabsModule } from './tabs/index';
import { UiFilterTagsModule } from './filter-tags/index';
import { UiFilterModule } from './filter/index';
import { UiSpinnerModule } from './forms/spinner/index';
import { UiInputSearchModule } from './forms/search/index';
import { UiTooltipsModule } from './messaging/tooltips/index';
import { UiButtonActionsModule } from './button-actions/index';
import { UiPopupMessageModule } from './popup-message/index';
import { UiAutoCompleteModule } from './auto-complete/index';
import { UiDateInputModule } from './forms/date-input/index';
import { UiProgressBarModule } from './progress-bar/index';
import { UiUploadFileModule } from './forms/upload-files/index';
import { UiPaginationModule } from './pagination/index';
import { UiStepperModule } from './stepper/index';
import { UiCurrencyInputModule } from './forms/currency-input/index';
import { UiCopyInputModule } from './forms/copy-input/index';
import { UiCheckboxIndeterminateModule } from './forms/checkbox-indeterminate/index';
import { TooltipModule } from './messaging/tooltip/index';
import { UiWeightUnitsDropdownModule } from './forms/weight-units-dropdown/index';
import { UiCurrenciesDropdownModule } from './forms/currency-dropdown/index';
import { SearchPipeModule } from './pipes/index';
import { ReactiveFormsModule } from '@angular/forms';
var HuubMaterialLibModule = /** @class */ (function () {
    function HuubMaterialLibModule() {
    }
    HuubMaterialLibModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                OverlayModule,
                MaterialHelpersModule,
                UiModalModule,
                UiToggleButtonsModule,
                UiTabsModule,
                UiFilterTagsModule,
                UiFilterModule,
                UiSpinnerModule,
                UiInputSearchModule,
                UiTooltipsModule,
                UiButtonActionsModule,
                UiPopupMessageModule,
                UiAutoCompleteModule,
                UiDateInputModule,
                UiProgressBarModule,
                UiPaginationModule,
                UiUploadFileModule,
                UiStepperModule,
                UiCurrencyInputModule,
                UiCopyInputModule,
                UiCheckboxIndeterminateModule,
                TooltipModule,
                UiWeightUnitsDropdownModule,
                UiCurrenciesDropdownModule,
                SearchPipeModule
            ],
            exports: [
                CommonModule,
                FormsModule,
                OverlayModule,
                MaterialHelpersModule,
                UiModalModule,
                UiToggleButtonsModule,
                UiTabsModule,
                UiFilterTagsModule,
                UiFilterModule,
                UiSpinnerModule,
                UiInputSearchModule,
                UiTooltipsModule,
                UiButtonActionsModule,
                UiPopupMessageModule,
                UiAutoCompleteModule,
                UiDateInputModule,
                UiProgressBarModule,
                UiPaginationModule,
                UiUploadFileModule,
                UiStepperModule,
                UiCurrencyInputModule,
                UiCopyInputModule,
                UiCheckboxIndeterminateModule,
                TooltipModule,
                UiWeightUnitsDropdownModule,
                UiCurrenciesDropdownModule,
                SearchPipeModule,
                ReactiveFormsModule
            ]
        })
    ], HuubMaterialLibModule);
    return HuubMaterialLibModule;
}());
export { HuubMaterialLibModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHV1Yi1tYXRlcmlhbC1saWIubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vaHV1Yi1tYXRlcmlhbC9saWIvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2h1dWItbWF0ZXJpYWwtbGliLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUVyRCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlDLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQy9ELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDNUMsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDekQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ2hELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUMzRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUM5RCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMvRCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUMzRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUNoRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDbEQsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDckUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDN0QsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDckYsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQzFELE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNqRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQWdFckQ7SUFBQTtJQUFvQyxDQUFDO0lBQXhCLHFCQUFxQjtRQTlEakMsUUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFO2dCQUNMLFlBQVk7Z0JBQ1osV0FBVztnQkFDWCxtQkFBbUI7Z0JBQ25CLGFBQWE7Z0JBQ2IscUJBQXFCO2dCQUNyQixhQUFhO2dCQUNiLHFCQUFxQjtnQkFDckIsWUFBWTtnQkFDWixrQkFBa0I7Z0JBQ2xCLGNBQWM7Z0JBQ2QsZUFBZTtnQkFDZixtQkFBbUI7Z0JBQ25CLGdCQUFnQjtnQkFDaEIscUJBQXFCO2dCQUNyQixvQkFBb0I7Z0JBQ3BCLG9CQUFvQjtnQkFDcEIsaUJBQWlCO2dCQUNqQixtQkFBbUI7Z0JBQ25CLGtCQUFrQjtnQkFDbEIsa0JBQWtCO2dCQUNsQixlQUFlO2dCQUNmLHFCQUFxQjtnQkFDckIsaUJBQWlCO2dCQUNqQiw2QkFBNkI7Z0JBQzdCLGFBQWE7Z0JBQ2IsMkJBQTJCO2dCQUMzQiwwQkFBMEI7Z0JBQzFCLGdCQUFnQjthQUNuQjtZQUNELE9BQU8sRUFBRTtnQkFDTCxZQUFZO2dCQUNaLFdBQVc7Z0JBQ1gsYUFBYTtnQkFDYixxQkFBcUI7Z0JBQ3JCLGFBQWE7Z0JBQ2IscUJBQXFCO2dCQUNyQixZQUFZO2dCQUNaLGtCQUFrQjtnQkFDbEIsY0FBYztnQkFDZCxlQUFlO2dCQUNmLG1CQUFtQjtnQkFDbkIsZ0JBQWdCO2dCQUNoQixxQkFBcUI7Z0JBQ3JCLG9CQUFvQjtnQkFDcEIsb0JBQW9CO2dCQUNwQixpQkFBaUI7Z0JBQ2pCLG1CQUFtQjtnQkFDbkIsa0JBQWtCO2dCQUNsQixrQkFBa0I7Z0JBQ2xCLGVBQWU7Z0JBQ2YscUJBQXFCO2dCQUNyQixpQkFBaUI7Z0JBQ2pCLDZCQUE2QjtnQkFDN0IsYUFBYTtnQkFDYiwyQkFBMkI7Z0JBQzNCLDBCQUEwQjtnQkFDMUIsZ0JBQWdCO2dCQUNoQixtQkFBbUI7YUFDdEI7U0FDSixDQUFDO09BQ1cscUJBQXFCLENBQUc7SUFBRCw0QkFBQztDQUFBLEFBQXJDLElBQXFDO1NBQXhCLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBPdmVybGF5TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL292ZXJsYXknO1xuXG5pbXBvcnQgeyBNYXRlcmlhbEhlbHBlcnNNb2R1bGUgfSBmcm9tICcuL2hlbHBlcnMvaW5kZXgnO1xuaW1wb3J0IHsgVWlNb2RhbE1vZHVsZSB9IGZyb20gJy4vbW9kYWwvaW5kZXgnO1xuaW1wb3J0IHsgVWlUb2dnbGVCdXR0b25zTW9kdWxlIH0gZnJvbSAnLi90b2dnbGUtYnV0dG9ucy9pbmRleCc7XG5pbXBvcnQgeyBVaVRhYnNNb2R1bGUgfSBmcm9tICcuL3RhYnMvaW5kZXgnO1xuaW1wb3J0IHsgVWlGaWx0ZXJUYWdzTW9kdWxlIH0gZnJvbSAnLi9maWx0ZXItdGFncy9pbmRleCc7XG5pbXBvcnQgeyBVaUZpbHRlck1vZHVsZSB9IGZyb20gJy4vZmlsdGVyL2luZGV4JztcbmltcG9ydCB7IFVpU3Bpbm5lck1vZHVsZSB9IGZyb20gJy4vZm9ybXMvc3Bpbm5lci9pbmRleCc7XG5pbXBvcnQgeyBVaUlucHV0U2VhcmNoTW9kdWxlIH0gZnJvbSAnLi9mb3Jtcy9zZWFyY2gvaW5kZXgnO1xuaW1wb3J0IHsgVWlUb29sdGlwc01vZHVsZSB9IGZyb20gJy4vbWVzc2FnaW5nL3Rvb2x0aXBzL2luZGV4JztcbmltcG9ydCB7IFVpQnV0dG9uQWN0aW9uc01vZHVsZSB9IGZyb20gJy4vYnV0dG9uLWFjdGlvbnMvaW5kZXgnO1xuaW1wb3J0IHsgVWlQb3B1cE1lc3NhZ2VNb2R1bGUgfSBmcm9tICcuL3BvcHVwLW1lc3NhZ2UvaW5kZXgnO1xuaW1wb3J0IHsgVWlBdXRvQ29tcGxldGVNb2R1bGUgfSBmcm9tICcuL2F1dG8tY29tcGxldGUvaW5kZXgnO1xuaW1wb3J0IHsgVWlEYXRlSW5wdXRNb2R1bGUgfSBmcm9tICcuL2Zvcm1zL2RhdGUtaW5wdXQvaW5kZXgnO1xuaW1wb3J0IHsgVWlQcm9ncmVzc0Jhck1vZHVsZSB9IGZyb20gJy4vcHJvZ3Jlc3MtYmFyL2luZGV4JztcbmltcG9ydCB7IFVpVXBsb2FkRmlsZU1vZHVsZSB9IGZyb20gJy4vZm9ybXMvdXBsb2FkLWZpbGVzL2luZGV4JztcbmltcG9ydCB7IFVpUGFnaW5hdGlvbk1vZHVsZSB9IGZyb20gJy4vcGFnaW5hdGlvbi9pbmRleCc7XG5pbXBvcnQgeyBVaVN0ZXBwZXJNb2R1bGUgfSBmcm9tICcuL3N0ZXBwZXIvaW5kZXgnO1xuaW1wb3J0IHsgVWlDdXJyZW5jeUlucHV0TW9kdWxlIH0gZnJvbSAnLi9mb3Jtcy9jdXJyZW5jeS1pbnB1dC9pbmRleCc7XG5pbXBvcnQgeyBVaUNvcHlJbnB1dE1vZHVsZSB9IGZyb20gJy4vZm9ybXMvY29weS1pbnB1dC9pbmRleCc7XG5pbXBvcnQgeyBVaUNoZWNrYm94SW5kZXRlcm1pbmF0ZU1vZHVsZSB9IGZyb20gJy4vZm9ybXMvY2hlY2tib3gtaW5kZXRlcm1pbmF0ZS9pbmRleCc7XG5pbXBvcnQgeyBUb29sdGlwTW9kdWxlIH0gZnJvbSAnLi9tZXNzYWdpbmcvdG9vbHRpcC9pbmRleCc7XG5pbXBvcnQgeyBVaVdlaWdodFVuaXRzRHJvcGRvd25Nb2R1bGUgfSBmcm9tICcuL2Zvcm1zL3dlaWdodC11bml0cy1kcm9wZG93bi9pbmRleCc7XG5pbXBvcnQgeyBVaUN1cnJlbmNpZXNEcm9wZG93bk1vZHVsZSB9IGZyb20gJy4vZm9ybXMvY3VycmVuY3ktZHJvcGRvd24vaW5kZXgnO1xuaW1wb3J0IHsgU2VhcmNoUGlwZU1vZHVsZSB9IGZyb20gJy4vcGlwZXMvaW5kZXgnO1xuaW1wb3J0IHsgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbXG4gICAgICAgIENvbW1vbk1vZHVsZSxcbiAgICAgICAgRm9ybXNNb2R1bGUsXG4gICAgICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXG4gICAgICAgIE92ZXJsYXlNb2R1bGUsXG4gICAgICAgIE1hdGVyaWFsSGVscGVyc01vZHVsZSxcbiAgICAgICAgVWlNb2RhbE1vZHVsZSxcbiAgICAgICAgVWlUb2dnbGVCdXR0b25zTW9kdWxlLFxuICAgICAgICBVaVRhYnNNb2R1bGUsXG4gICAgICAgIFVpRmlsdGVyVGFnc01vZHVsZSxcbiAgICAgICAgVWlGaWx0ZXJNb2R1bGUsXG4gICAgICAgIFVpU3Bpbm5lck1vZHVsZSxcbiAgICAgICAgVWlJbnB1dFNlYXJjaE1vZHVsZSxcbiAgICAgICAgVWlUb29sdGlwc01vZHVsZSxcbiAgICAgICAgVWlCdXR0b25BY3Rpb25zTW9kdWxlLFxuICAgICAgICBVaVBvcHVwTWVzc2FnZU1vZHVsZSxcbiAgICAgICAgVWlBdXRvQ29tcGxldGVNb2R1bGUsXG4gICAgICAgIFVpRGF0ZUlucHV0TW9kdWxlLFxuICAgICAgICBVaVByb2dyZXNzQmFyTW9kdWxlLFxuICAgICAgICBVaVBhZ2luYXRpb25Nb2R1bGUsXG4gICAgICAgIFVpVXBsb2FkRmlsZU1vZHVsZSxcbiAgICAgICAgVWlTdGVwcGVyTW9kdWxlLFxuICAgICAgICBVaUN1cnJlbmN5SW5wdXRNb2R1bGUsXG4gICAgICAgIFVpQ29weUlucHV0TW9kdWxlLFxuICAgICAgICBVaUNoZWNrYm94SW5kZXRlcm1pbmF0ZU1vZHVsZSxcbiAgICAgICAgVG9vbHRpcE1vZHVsZSxcbiAgICAgICAgVWlXZWlnaHRVbml0c0Ryb3Bkb3duTW9kdWxlLFxuICAgICAgICBVaUN1cnJlbmNpZXNEcm9wZG93bk1vZHVsZSxcbiAgICAgICAgU2VhcmNoUGlwZU1vZHVsZVxuICAgIF0sXG4gICAgZXhwb3J0czogW1xuICAgICAgICBDb21tb25Nb2R1bGUsXG4gICAgICAgIEZvcm1zTW9kdWxlLFxuICAgICAgICBPdmVybGF5TW9kdWxlLFxuICAgICAgICBNYXRlcmlhbEhlbHBlcnNNb2R1bGUsXG4gICAgICAgIFVpTW9kYWxNb2R1bGUsXG4gICAgICAgIFVpVG9nZ2xlQnV0dG9uc01vZHVsZSxcbiAgICAgICAgVWlUYWJzTW9kdWxlLFxuICAgICAgICBVaUZpbHRlclRhZ3NNb2R1bGUsXG4gICAgICAgIFVpRmlsdGVyTW9kdWxlLFxuICAgICAgICBVaVNwaW5uZXJNb2R1bGUsXG4gICAgICAgIFVpSW5wdXRTZWFyY2hNb2R1bGUsXG4gICAgICAgIFVpVG9vbHRpcHNNb2R1bGUsXG4gICAgICAgIFVpQnV0dG9uQWN0aW9uc01vZHVsZSxcbiAgICAgICAgVWlQb3B1cE1lc3NhZ2VNb2R1bGUsXG4gICAgICAgIFVpQXV0b0NvbXBsZXRlTW9kdWxlLFxuICAgICAgICBVaURhdGVJbnB1dE1vZHVsZSxcbiAgICAgICAgVWlQcm9ncmVzc0Jhck1vZHVsZSxcbiAgICAgICAgVWlQYWdpbmF0aW9uTW9kdWxlLFxuICAgICAgICBVaVVwbG9hZEZpbGVNb2R1bGUsXG4gICAgICAgIFVpU3RlcHBlck1vZHVsZSxcbiAgICAgICAgVWlDdXJyZW5jeUlucHV0TW9kdWxlLFxuICAgICAgICBVaUNvcHlJbnB1dE1vZHVsZSxcbiAgICAgICAgVWlDaGVja2JveEluZGV0ZXJtaW5hdGVNb2R1bGUsXG4gICAgICAgIFRvb2x0aXBNb2R1bGUsXG4gICAgICAgIFVpV2VpZ2h0VW5pdHNEcm9wZG93bk1vZHVsZSxcbiAgICAgICAgVWlDdXJyZW5jaWVzRHJvcGRvd25Nb2R1bGUsXG4gICAgICAgIFNlYXJjaFBpcGVNb2R1bGUsXG4gICAgICAgIFJlYWN0aXZlRm9ybXNNb2R1bGVcbiAgICBdXG59KVxuZXhwb3J0IGNsYXNzIEh1dWJNYXRlcmlhbExpYk1vZHVsZSB7fVxuIl19