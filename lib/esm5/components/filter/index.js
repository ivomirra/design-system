import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialHelpersModule } from '../helpers/index';
import { UiFilterComponent } from './ui-filter.component';
var UiFilterModule = /** @class */ (function () {
    function UiFilterModule() {
    }
    UiFilterModule = tslib_1.__decorate([
        NgModule({
            imports: [CommonModule, MaterialHelpersModule],
            declarations: [UiFilterComponent],
            exports: [UiFilterComponent]
        })
    ], UiFilterModule);
    return UiFilterModule;
}());
export { UiFilterModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9odXViLW1hdGVyaWFsL2xpYi8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZmlsdGVyL2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUV6RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQU8xRDtJQUFBO0lBQTZCLENBQUM7SUFBakIsY0FBYztRQUwxQixRQUFRLENBQUM7WUFDTixPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUscUJBQXFCLENBQUM7WUFDOUMsWUFBWSxFQUFFLENBQUMsaUJBQWlCLENBQUM7WUFDakMsT0FBTyxFQUFFLENBQUMsaUJBQWlCLENBQUM7U0FDL0IsQ0FBQztPQUNXLGNBQWMsQ0FBRztJQUFELHFCQUFDO0NBQUEsQUFBOUIsSUFBOEI7U0FBakIsY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgTWF0ZXJpYWxIZWxwZXJzTW9kdWxlIH0gZnJvbSAnLi4vaGVscGVycy9pbmRleCc7XG5cbmltcG9ydCB7IFVpRmlsdGVyQ29tcG9uZW50IH0gZnJvbSAnLi91aS1maWx0ZXIuY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlLCBNYXRlcmlhbEhlbHBlcnNNb2R1bGVdLFxuICAgIGRlY2xhcmF0aW9uczogW1VpRmlsdGVyQ29tcG9uZW50XSxcbiAgICBleHBvcnRzOiBbVWlGaWx0ZXJDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIFVpRmlsdGVyTW9kdWxlIHt9XG4iXX0=