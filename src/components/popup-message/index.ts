import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UiPopupMessageComponent } from './ui-popup-message.component';

@NgModule({
    imports: [CommonModule],
    declarations: [UiPopupMessageComponent],
    exports: [UiPopupMessageComponent]
})
export class UiPopupMessageModule {}
