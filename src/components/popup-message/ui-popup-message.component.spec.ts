import { TestBed } from '@angular/core/testing';
import { UiPopupMessageComponent } from './ui-popup-message.component';

describe('UiPopupMessageComponent', () => {
    let fixture;
    let testedComponent;
    let compiledComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [UiPopupMessageComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiPopupMessageComponent);
        testedComponent = fixture.debugElement.componentInstance;
        compiledComponent = fixture.debugElement.nativeElement;

        testedComponent.viewModel = {};

        fixture.detectChanges();
    });

    describe('#hidePopup', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                testedComponent.viewModel.isVisible = true;
                testedComponent.hidePopup();
            });

            it('should set isVisible from viewModel as false', () => {
                expect(testedComponent.viewModel.isVisible).toEqual(false);
            });
        });
    });

    describe('#onButtonClicked', () => {
        describe('when invoked', () => {
            const callbackSpy = jasmine.createSpy('callback');

            beforeEach(() => {
                testedComponent.viewModel.isVisible = true;
                testedComponent.onButtonClicked();
            });

            afterEach(() => {
                callbackSpy.calls.reset();
            });

            it('should set isVisible from viewModel as false', () => {
                expect(testedComponent.viewModel.isVisible).toEqual(false);
            });

            describe('and an option without callback is provided', () => {
                it("should'nt call the callback spy", () => {
                    testedComponent.onButtonClicked({});

                    expect(callbackSpy).not.toHaveBeenCalled();
                });
            });

            describe('and an option with callback is provided', () => {
                it('should call the callback spy', () => {
                    testedComponent.onButtonClicked({
                        callback: callbackSpy,
                        text: 'Example'
                    });

                    expect(callbackSpy).toHaveBeenCalledWith({
                        callback: jasmine.any(Function),
                        text: 'Example'
                    });
                });
            });
        });
    });
});
