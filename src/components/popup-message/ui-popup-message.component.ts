import { Component, Input } from '@angular/core';

@Component({
    selector: 'ui-popup-message',
    templateUrl: './ui-popup-message.component.html'
})
export class UiPopupMessageComponent {
    @Input()
    public viewModel: {
        isVisible: boolean;
        title: string;
        message: string;
        submit: {
            button_text: string;
            callback: Function;
        };
        cancel: {
            button_text: string;
            callback: Function;
        };
    };

    constructor() {}

    public hidePopup(): void {
        this.viewModel.isVisible = false;
    }

    public onButtonClicked(option?: { callback: Function }): void {
        if (option && option.callback) {
            option.callback(option);
        }

        this.hidePopup();
    }
}
