import { TestBed } from '@angular/core/testing';

import { SearchPipe } from './search-pipe.pipe';

describe('SearchPipe', () => {
    let testedService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [SearchPipe]
        }).compileComponents();
    });

    beforeEach(() => {
        testedService = TestBed.get(SearchPipe);
    });

    it('should create', () => {
        expect(testedService).toBeTruthy();
    });

    describe('#transform', () => {
        describe('when invoked', () => {
            it('should accept a undefined value and fallback to an array', () => {
                const value = undefined;
                const keys = 'somekey, otherkey';
                const term = 'somekey';

                const result = testedService.transform(value, keys, term);
                const expectedValue = [];

                expect(result).toEqual(expectedValue);
            });

            it('should return the same value when no term or keys are provided', () => {
                const value = 1;

                const result = testedService.transform(value);
                const expectedValue = value;

                expect(result).toEqual(expectedValue);
            });

            it('should return a filtered value array by the provided term', () => {
                const value = [{ somekey: 'somekey' }, { otherkey: 'otherkey' }];
                const keys = 'somekey, otherkey';
                const term = 'somekey';

                const result = testedService.transform(value, keys, term);
                const expectedValue = [{ somekey: 'somekey' }];

                expect(result).toEqual(expectedValue);
            });
        });
    });
});
