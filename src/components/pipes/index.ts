import { NgModule } from '@angular/core';
import { SearchPipe } from './search-pipe.pipe';

@NgModule({
    declarations: [SearchPipe],
    exports: [SearchPipe]
})
export class SearchPipeModule {}
