import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
    selector: 'ui-progress-bar',
    templateUrl: './ui-progress-bar.component.html'
})
export class UiProgressBarComponent implements OnChanges {
    @Input() public percentage: number;

    private MAX_PERCENTAGE = 100;
    public isCompleted = false;

    constructor() {}

    public ngOnChanges(changes: SimpleChanges): void {
        this.percentage =
            changes.percentage.currentValue > this.MAX_PERCENTAGE
                ? this.MAX_PERCENTAGE
                : changes.percentage.currentValue;

        this.isCompleted = this.percentage === this.MAX_PERCENTAGE;
    }
}
