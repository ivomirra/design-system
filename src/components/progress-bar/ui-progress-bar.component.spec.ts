import { TestBed } from '@angular/core/testing';
import { UiProgressBarComponent } from './ui-progress-bar.component';

describe('UiProgressBarComponent', () => {
    let fixture;
    let testedComponent;
    let compiledComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [UiProgressBarComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiProgressBarComponent);
        testedComponent = fixture.debugElement.componentInstance;
        compiledComponent = fixture.debugElement.nativeElement;

        testedComponent.viewModel = {};

        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    describe('#ngOnChanges', () => {
        describe('when invoked', () => {
            describe('and percentage given is lower than 100', () => {
                beforeEach(() => {
                    testedComponent.percentage = 80;
                    const obj = {
                        percentage: {
                            currentValue: 75
                        }
                    };
                    testedComponent.ngOnChanges(obj);
                });

                it('should change percentage to the new value', () => {
                    expect(testedComponent.percentage).toEqual(75);
                });

                it('should set isCompleted to false', () => {
                    expect(testedComponent.isCompleted).toEqual(false);
                });
            });
            describe('and percentage given is equal to 100', () => {
                beforeEach(() => {
                    testedComponent.percentage = 80;
                    const obj = {
                        percentage: {
                            currentValue: 100
                        }
                    };
                    testedComponent.ngOnChanges(obj);
                });

                it('should change percentage to the new value', () => {
                    expect(testedComponent.percentage).toEqual(100);
                });

                it('should set isCompleted to true', () => {
                    expect(testedComponent.isCompleted).toEqual(true);
                });
            });

            describe('and percentage given is greater than 100', () => {
                beforeEach(() => {
                    testedComponent.percentage = 80;
                    const obj = {
                        percentage: {
                            currentValue: 105
                        }
                    };
                    testedComponent.ngOnChanges(obj);
                });

                it('should change the value to 100', () => {
                    expect(testedComponent.percentage).toEqual(100);
                });

                it('should set isCompleted to true', () => {
                    expect(testedComponent.isCompleted).toEqual(true);
                });
            });
        });
    });
});
