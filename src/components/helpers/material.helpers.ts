import { Injectable } from '@angular/core';

@Injectable()
export class MaterialHelper {
    constructor() {}

    public getAbsolutePositionRenderConfig(elementOffSet: number, elementSize: number): any {
        return {
            canRenderToRight: window.innerWidth > elementOffSet + elementSize,
            canRenderToLeft: elementOffSet > elementSize
        };
    }

    public merge(...objects): any {
        const isObject = (obj) => obj && typeof obj === 'object';
        const newObject = {};

        objects.forEach((obj) => {
            Object.keys(obj).forEach((key) => {
                const pVal = newObject[key];
                const oVal = obj[key];

                if (Array.isArray(oVal)) {
                    newObject[key] = new Array().concat(oVal);
                } else if (isObject(oVal)) {
                    newObject[key] = this.merge(pVal || {}, oVal);
                } else {
                    newObject[key] = oVal;
                }
            });
        });

        return newObject;
    }
}
