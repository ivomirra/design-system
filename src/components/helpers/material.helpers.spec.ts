import { TestBed } from '@angular/core/testing';

import { MaterialHelper } from './material.helpers';

describe('MaterialHelper', () => {
    let testedService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [MaterialHelper]
        }).compileComponents();
    });

    beforeEach(() => {
        testedService = TestBed.get(MaterialHelper);

        spyOnProperty(window, 'innerWidth', 'get').and.returnValue(500);
    });

    it('should create', () => {
        expect(testedService).toBeTruthy();
    });

    describe('#getAbsolutePositionRenderConfig', () => {
        describe('when invoked', () => {
            describe('and window.innerWidth is greater than elementOffSet + elementSize', () => {
                it('should return canRenderToRight: true', () => {
                    const elementOffset = 200;
                    const elementWidth = 200;

                    const result = testedService.getAbsolutePositionRenderConfig(elementOffset, elementWidth);

                    expect(result.canRenderToRight).toEqual(true);
                });
            });

            describe('and window.innerWidth is less than elementOffSet + elementSize', () => {
                it('should return canRenderToRight: false', () => {
                    const elementOffset = 400;
                    const elementWidth = 200;

                    const result = testedService.getAbsolutePositionRenderConfig(elementOffset, elementWidth);

                    expect(result.canRenderToRight).toEqual(false);
                });
            });

            describe('and elementOffset - elementSize is greater than 0', () => {
                it('should return canRenderToLeft: true', () => {
                    const elementOffset = 300;
                    const elementWidth = 200;

                    const result = testedService.getAbsolutePositionRenderConfig(elementOffset, elementWidth);

                    expect(result.canRenderToLeft).toEqual(true);
                });
            });

            describe('and elementOffset - elementSize is less than 0', () => {
                it('should return canRenderToLeft: false', () => {
                    const elementOffset = 200;
                    const elementWidth = 200;

                    const result = testedService.getAbsolutePositionRenderConfig(elementOffset, elementWidth);

                    expect(result.canRenderToLeft).toEqual(false);
                });
            });
        });
    });

    describe('#merge', () => {
        describe('when invoked', () => {
            it('should create a new object with different references', () => {
                const objectOne = {
                    a: 1,
                    b: {},
                    d: [2]
                };
                const objectTwo = {
                    a: 2,
                    b: {},
                    c: () => {}
                };

                const objectTree = {
                    d: [1, 2, 3]
                };

                const result = testedService.merge(objectOne, objectTwo, objectTree);

                expect(result).toEqual({
                    a: 2,
                    b: {},
                    c: jasmine.any(Function),
                    d: [1, 2, 3]
                });
            });
        });
    });
});
