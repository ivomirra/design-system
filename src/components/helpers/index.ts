import { NgModule } from '@angular/core';

import { MaterialHelper } from './material.helpers';

@NgModule({
    providers: [MaterialHelper]
})
export class MaterialHelpersModule {}
