import { TestBed } from '@angular/core/testing';
import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';

import { UiStepperComponent } from './ui-stepper.component';

describe('UiModalComponent', () => {
    let fixture;
    let testedComponent;

    const detectChangesSpy = jasmine.createSpy('detectChangesSpy');

    const ViewContainerRefSpy = jasmine.createSpyObj('ViewContainerRefSpy', ['createComponent', 'clear']);
    ViewContainerRefSpy.createComponent.and.returnValue({
        changeDetectorRef: {
            detectChanges: detectChangesSpy
        },
        instance: {}
    });

    const ComponentFactoryResolverSpy = jasmine.createSpyObj('ComponentFactoryResolverSpy', [
        'resolveComponentFactory'
    ]);

    const updateCurrentStepSpy = jasmine.createSpy('updateCurrentStep');

    const getTestingModuleConfiguration = () => {
        return {
            declarations: [UiStepperComponent],
            providers: [
                {
                    provide: ComponentFactoryResolver,
                    useValue: ComponentFactoryResolverSpy
                }
            ]
        };
    };

    describe('#ngOnInit', () => {
        describe('when invoked with one component', () => {
            beforeEach(() => {
                TestBed.configureTestingModule(getTestingModuleConfiguration()).compileComponents();
                fixture = TestBed.createComponent(UiStepperComponent);
                testedComponent = fixture.debugElement.componentInstance;
                testedComponent.viewContainerRef = ViewContainerRefSpy;
                testedComponent.viewModel.items = [{ component: {} }];
                fixture.detectChanges();
            });

            it('should call viewContainerRef.clear()', () => {
                expect(ViewContainerRefSpy.clear).toHaveBeenCalled();
            });

            it('should pass the stepContext to the created component', () => {
                expect(testedComponent.currentComponent.instance.stepContext).not.toBeUndefined();
            });

            it('should call componentFactoryResolver.resolveComponentFactory()', () => {
                expect(ComponentFactoryResolverSpy.resolveComponentFactory).toHaveBeenCalled();
            });

            it('should call viewContainerRef.createComponent() once', () => {
                expect(ViewContainerRefSpy.createComponent).toHaveBeenCalled();
            });

            it('should call componentCreated.changeDetectorRef.detectChanges() once', () => {
                expect(detectChangesSpy).toHaveBeenCalled();
            });
        });
    });

    describe('#ngOnInit', () => {
        describe('when invoked with one component', () => {
            beforeEach(() => {
                TestBed.configureTestingModule(getTestingModuleConfiguration()).compileComponents();
                fixture = TestBed.createComponent(UiStepperComponent);
                testedComponent = fixture.debugElement.componentInstance;
                testedComponent.viewContainerRef = ViewContainerRefSpy;
                testedComponent.viewModel.items = [{ component: {} }];
                fixture.detectChanges();
            });

            it('should call viewContainerRef.clear()', () => {
                testedComponent.ngOnChanges();
                expect(ViewContainerRefSpy.clear).toHaveBeenCalled();
            });

            it('should pass the stepContext to the created component', () => {
                testedComponent.ngOnChanges();
                expect(testedComponent.currentComponent.instance.stepContext).not.toBeUndefined();
            });

            it('should call componentFactoryResolver.resolveComponentFactory()', () => {
                testedComponent.ngOnChanges();
                expect(ComponentFactoryResolverSpy.resolveComponentFactory).toHaveBeenCalled();
            });

            it('should call viewContainerRef.createComponent() once', () => {
                testedComponent.ngOnChanges();
                expect(ViewContainerRefSpy.createComponent).toHaveBeenCalled();
            });

            it('should call componentCreated.changeDetectorRef.detectChanges() once', () => {
                testedComponent.ngOnChanges();
                expect(detectChangesSpy).toHaveBeenCalled();
            });
        });
    });

    describe('#goToNextStep', () => {
        describe('when invoked', () => {
            describe('and the selectedIndex is 0', () => {
                beforeEach(() => {
                    TestBed.configureTestingModule(getTestingModuleConfiguration()).compileComponents();
                    fixture = TestBed.createComponent(UiStepperComponent);
                    testedComponent = fixture.debugElement.componentInstance;
                    updateCurrentStepSpy.calls.reset();
                    testedComponent.updateCurrentStep = updateCurrentStepSpy;
                    testedComponent.goToNextStep();
                });

                it('should increment the selectedIndex', () => {
                    expect(testedComponent.viewModel.selectedIndex).toEqual(1);
                });

                it('should call the function updateCurrentStep', () => {
                    expect(updateCurrentStepSpy).toHaveBeenCalled();
                    expect(updateCurrentStepSpy.calls.count()).toBe(1);
                });
            });
            describe('and the selectedIndex is is the last(9)', () => {
                beforeEach(() => {
                    TestBed.configureTestingModule(getTestingModuleConfiguration()).compileComponents();
                    fixture = TestBed.createComponent(UiStepperComponent);
                    testedComponent = fixture.debugElement.componentInstance;
                    updateCurrentStepSpy.calls.reset();
                    testedComponent.updateCurrentStep = updateCurrentStepSpy;
                    testedComponent.viewModel.items = new Array(10);
                    testedComponent.viewModel.items.fill({ component: {}, context: {} });
                    testedComponent.viewModel.selectedIndex = 9;
                });

                it('should throw an error', () => {
                    expect(() => {
                        testedComponent.goToNextStep();
                    }).toThrowError(TypeError, 'You are trying to go to the next step on the last available step.');
                });

                it('should not increment the selectedIndex', () => {
                    expect(testedComponent.viewModel.selectedIndex).toEqual(9);
                });
            });
        });
    });

    describe('#goToPreviousStep', () => {
        describe('when invoked', () => {
            describe('and the selectedIndex is 2', () => {
                beforeEach(() => {
                    TestBed.configureTestingModule(getTestingModuleConfiguration()).compileComponents();
                    fixture = TestBed.createComponent(UiStepperComponent);
                    testedComponent = fixture.debugElement.componentInstance;
                    updateCurrentStepSpy.calls.reset();
                    testedComponent.updateCurrentStep = updateCurrentStepSpy;
                    testedComponent.viewModel.selectedIndex = 2;
                    testedComponent.goToPreviousStep();
                });

                it('should decrement the selectedIndex', () => {
                    expect(testedComponent.viewModel.selectedIndex).toEqual(1);
                });

                it('should call the function updateCurrentStep', () => {
                    expect(updateCurrentStepSpy).toHaveBeenCalled();
                    expect(updateCurrentStepSpy.calls.count()).toBe(1);
                });
            });

            describe('and the selectedIndex is 0', () => {
                beforeEach(() => {
                    TestBed.configureTestingModule(getTestingModuleConfiguration()).compileComponents();
                    fixture = TestBed.createComponent(UiStepperComponent);
                    testedComponent = fixture.debugElement.componentInstance;
                    updateCurrentStepSpy.calls.reset();
                    testedComponent.updateCurrentStep = updateCurrentStepSpy;
                    testedComponent.viewModel.selectedIndex = 0;
                });

                it('should throw an error', () => {
                    expect(() => {
                        testedComponent.goToPreviousStep();
                    }).toThrowError(
                        TypeError,
                        'You are trying to go to the previous step on the first available step.'
                    );
                });

                it('should not decrement the selectedIndex', () => {
                    expect(testedComponent.viewModel.selectedIndex).toEqual(0);
                });
            });
        });
    });
});
