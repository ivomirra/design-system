import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UiStepperComponent } from './ui-stepper.component';

@NgModule({
    imports: [CommonModule],
    declarations: [UiStepperComponent],
    exports: [UiStepperComponent]
})
export class UiStepperModule {}
