import {
    Component,
    Inject,
    OnInit,
    ViewContainerRef,
    ComponentFactoryResolver,
    ViewChild,
    Input,
    OnChanges
} from '@angular/core';

@Component({
    selector: 'ui-stepper',
    templateUrl: './ui-stepper.component.html'
})
export class UiStepperComponent implements OnInit, OnChanges {
    @Input()
    public viewModel: {
        items: Array<{ title: string; component: any; context?: any }>;
        selectedIndex: 0;
    } = {
        items: [],
        selectedIndex: 0
    };

    private currentComponent: any;

    @ViewChild('currentStep', { read: ViewContainerRef, static: true })
    private viewContainerRef: ViewContainerRef;

    constructor(private componentFactoryResolver: ComponentFactoryResolver) {}

    private updateCurrentStep() {
        const selectedItem = this.viewModel.items[this.viewModel.selectedIndex];
        this.viewContainerRef.clear();
        this.currentComponent = this.viewContainerRef.createComponent(
            this.componentFactoryResolver.resolveComponentFactory(selectedItem.component)
        );
        this.currentComponent.instance['stepContext'] = {
            goToNextStep: this.goToNextStep.bind(this),
            goToPreviousStep: this.goToPreviousStep.bind(this),
            componentContext: selectedItem.context
        };
        this.currentComponent.changeDetectorRef.detectChanges();
    }

    public ngOnInit() {
        this.updateCurrentStep();
    }

    public ngOnChanges() {
        this.updateCurrentStep();
    }

    private goToNextStep() {
        if (this.viewModel.selectedIndex === this.viewModel.items.length - 1) {
            throw new TypeError('You are trying to go to the next step on the last available step.');
        }
        this.viewModel.selectedIndex++;
        this.updateCurrentStep();
    }

    private goToPreviousStep() {
        if (this.viewModel.selectedIndex === 0) {
            throw new TypeError('You are trying to go to the previous step on the first available step.');
        }
        this.viewModel.selectedIndex--;
        this.updateCurrentStep();
    }
}
