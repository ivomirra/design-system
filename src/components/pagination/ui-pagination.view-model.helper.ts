import { Injectable } from '@angular/core';
import { UI_PAGINATION_CONSTANT } from './ui-pagination.constant';

@Injectable()
export class UiPaginationViewModelHelper {
    constructor() {}

    public getGenericPaginationViewModel(
        totalItemsPerPage,
        previousApiUrl,
        nextApiUrl,
        currentPageNumber,
        totalPages,
        totalItems
    ) {
        let firstItem: any = totalItemsPerPage * (currentPageNumber - 1) + 1;
        const possibleLastItem = totalItemsPerPage * currentPageNumber;
        const lastItem = possibleLastItem > totalItems ? totalItems : possibleLastItem;

        if (firstItem > lastItem) {
            firstItem = lastItem;
        }

        return {
            currentPage: currentPageNumber,
            isPreviousDisabled: !previousApiUrl,
            isNextDisabled: !nextApiUrl,
            canShowPagination: totalPages > 1,
            totalPages,
            nextText: UI_PAGINATION_CONSTANT.DEFAULT_BUTTON_TEXT.NEXT,
            previousText: UI_PAGINATION_CONSTANT.DEFAULT_BUTTON_TEXT.PREVIOUS,
            showingItemsOfTotalString: UI_PAGINATION_CONSTANT.SHOWING_ITEMS_TOTAL_STRING.replace(
                '{firstItem}',
                firstItem
            )
                .replace('{lastItem}', lastItem)
                .replace('{totalItems}', totalItems)
        };
    }

    public getUpdatedPaginationViewModel(viewModel: any, direction: string) {
        let currentPage = viewModel.currentPage;

        switch (direction) {
            case UI_PAGINATION_CONSTANT.DIRECTIONS.PREVIOUS:
                if (viewModel.currentPage > 1) {
                    currentPage--;
                }
                break;
            case UI_PAGINATION_CONSTANT.DIRECTIONS.NEXT:
                if (viewModel.currentPage < viewModel.totalPages) {
                    currentPage++;
                }
                break;
        }

        return {
            ...viewModel,
            currentPage
        };
    }

    public getInitialPositionOfflinePagination(currentPage, totalItemsPerPage) {
        return (currentPage - 1) * totalItemsPerPage;
    }

    public getFinalPositionOfflinePagination(currentPage, totalItemsPerPage, totalNumberOfItems) {
        return currentPage * totalItemsPerPage < totalNumberOfItems
            ? currentPage * totalItemsPerPage
            : totalNumberOfItems;
    }

    public getNextLinkOfflinePagination(currentPage, totalNumberOfPages) {
        return currentPage !== totalNumberOfPages ? UI_PAGINATION_CONSTANT.DIRECTIONS.NEXT : null;
    }

    public getPreviousLinkOfflinePagination(currentPage) {
        return currentPage === 1 ? null : UI_PAGINATION_CONSTANT.DIRECTIONS.PREVIOUS;
    }
}
