import { TestBed } from '@angular/core/testing';

import { UiPaginationViewModelHelper } from './ui-pagination.view-model.helper';
import { UI_PAGINATION_CONSTANT } from './ui-pagination.constant';
describe('UiPaginationViewModelHelper', () => {
    let testedService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [UiPaginationViewModelHelper]
        }).compileComponents();
    });

    beforeEach(() => {
        testedService = TestBed.get(UiPaginationViewModelHelper);
    });

    it('should create', () => {
        expect(testedService).toBeTruthy();
    });

    describe('#getGenericPaginationViewModel', () => {
        describe('when invoked', () => {
            describe('and total pages is smaller than or equal to 1', () => {
                it('should return canShowPagination false', () => {
                    const viewModel = testedService.getGenericPaginationViewModel(10, '', '', 1, 1, 1);
                    expect(viewModel.canShowPagination).toBeFalsy();
                });
            });

            describe('and total pages is greater than 1', () => {
                it('should return canShowPagination true', () => {
                    const viewModel = testedService.getGenericPaginationViewModel(10, '', '', 2, 2, 1);
                    expect(viewModel.canShowPagination).toBeTruthy();
                });
            });

            describe('and previousApiUrl is an empty string', () => {
                it('should return isPreviousDisabled true', () => {
                    const viewModel = testedService.getGenericPaginationViewModel(10, '', '', 1, 1, 1);
                    expect(viewModel.isPreviousDisabled).toBeTruthy();
                });
            });
            describe('and previousApiUrl is a valid string', () => {
                it('should return isPreviousDisabled false', () => {
                    const viewModel = testedService.getGenericPaginationViewModel(10, 'testString', '', 1, 1, 1);
                    expect(viewModel.isPreviousDisabled).toBeFalsy();
                });
            });
            describe('and nextApiUrl is an empty string', () => {
                it('should return isNextDisabled true', () => {
                    const viewModel = testedService.getGenericPaginationViewModel(10, '', '', 1, 1, 1);
                    expect(viewModel.isNextDisabled).toBeTruthy();
                });
            });
            describe('and nextApiUrl is a valid string', () => {
                it('should return isNextDisabled false', () => {
                    const viewModel = testedService.getGenericPaginationViewModel(10, '', 'testString', 1, 1, 1);
                    expect(viewModel.isNextDisabled).toBeFalsy();
                });
            });
        });
    });

    describe('#getUpdatedPaginationViewModel', () => {
        describe('when invoked', () => {
            describe('and direction is next', () => {
                it('should increment current page', () => {
                    let viewModel = testedService.getGenericPaginationViewModel(10, '', '', 1, 10, 100);
                    viewModel = testedService.getUpdatedPaginationViewModel(
                        viewModel,
                        UI_PAGINATION_CONSTANT.DIRECTIONS.NEXT
                    );
                    expect(viewModel.currentPage).toEqual(2);
                });
            });
            describe('and currentPage is 1 and direction is previous', () => {
                it('should return the same value', () => {
                    let viewModel = testedService.getGenericPaginationViewModel(10, '', '', 1, 10, 100);
                    viewModel = testedService.getUpdatedPaginationViewModel(
                        viewModel,
                        UI_PAGINATION_CONSTANT.DIRECTIONS.PREVIOUS
                    );
                    expect(viewModel.currentPage).toEqual(1);
                });
            });
            describe('and currentPage is greater than the total and pagination is next', () => {
                it('should return the same value', () => {
                    let viewModel = testedService.getGenericPaginationViewModel(10, '', '', 11, 10, 100);
                    viewModel = testedService.getUpdatedPaginationViewModel(
                        viewModel,
                        UI_PAGINATION_CONSTANT.DIRECTIONS.NEXT
                    );
                    expect(viewModel.currentPage).toEqual(11);
                });
            });
        });
        describe('and direction is previous', () => {
            it('should decrement current page', () => {
                let viewModel = testedService.getGenericPaginationViewModel(10, '', '', 2, 10, 100);
                viewModel = testedService.getUpdatedPaginationViewModel(
                    viewModel,
                    UI_PAGINATION_CONSTANT.DIRECTIONS.PREVIOUS
                );
                expect(viewModel.currentPage).toEqual(1);
            });
        });
    });

    describe('#getInitialPositionOfflinePagination', () => {
        describe('when invoked', () => {
            describe('and totalItemsPerPage is 10 and currentPage is 2', () => {
                it('should return 10', () => {
                    expect(testedService.getInitialPositionOfflinePagination(2, 10)).toEqual(10);
                });
            });
        });
    });

    describe('#getFinalPositionOfflinePagination', () => {
        describe('when invoked', () => {
            describe('and totalItemsPerPage is 10, currentPage is 1, and totalNumberOfItems is 20', () => {
                it('should return 10', () => {
                    expect(testedService.getFinalPositionOfflinePagination(1, 10, 20)).toEqual(10);
                });
            });

            describe('and totalItemsPerPage is 10, currentPage is 2, and totalNumberOfItems is 20', () => {
                it('should return 20', () => {
                    expect(testedService.getFinalPositionOfflinePagination(2, 10, 20)).toEqual(20);
                });
            });

            describe('and totalItemsPerPage is 10, currentPage is 2, and totalNumberOfItems is 16', () => {
                it('should return 16', () => {
                    expect(testedService.getFinalPositionOfflinePagination(2, 10, 16)).toEqual(16);
                });
            });
        });
    });

    describe('#getNextLinkOfflinePagination', () => {
        describe('when invoked', () => {
            describe('and totalNumberOfPages is 10 and currentPage is 2', () => {
                it('should return NEXT', () => {
                    expect(testedService.getNextLinkOfflinePagination(2, 10)).toEqual(
                        UI_PAGINATION_CONSTANT.DIRECTIONS.NEXT
                    );
                });
            });

            describe('and totalNumberOfPages is 10 and currentPage is 10', () => {
                it('should return null', () => {
                    expect(testedService.getNextLinkOfflinePagination(10, 10)).toEqual(null);
                });
            });
        });
    });

    describe('#getPreviousLinkOfflinePagination', () => {
        describe('when invoked', () => {
            describe('and currentPage is 2', () => {
                it('should return PREVIOUS', () => {
                    expect(testedService.getPreviousLinkOfflinePagination(2)).toEqual(
                        UI_PAGINATION_CONSTANT.DIRECTIONS.PREVIOUS
                    );
                });
            });

            describe('and currentPage is 1', () => {
                it('should return null', () => {
                    expect(testedService.getPreviousLinkOfflinePagination(1)).toEqual(null);
                });
            });
        });
    });
});
