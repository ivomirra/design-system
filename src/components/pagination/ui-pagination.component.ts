import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PaginationViewModelInterface } from './ui-pagination.interface';
import { UI_PAGINATION_CONSTANT } from './ui-pagination.constant';

@Component({
    selector: 'ui-pagination',
    templateUrl: './ui-pagination.component.html',
    providers: []
})
export class UiPaginationComponent {
    constructor() {}

    public paginationDirections = UI_PAGINATION_CONSTANT.DIRECTIONS;

    @Input()
    public viewModel: PaginationViewModelInterface = {
        currentPage: 0,
        totalPages: 0,
        isPreviousDisabled: true,
        isNextDisabled: true,
        nextText: UI_PAGINATION_CONSTANT.DEFAULT_BUTTON_TEXT.NEXT,
        previousText: UI_PAGINATION_CONSTANT.DEFAULT_BUTTON_TEXT.PREVIOUS,
        canShowPagination: false,
        showingItemsOfTotalString: ''
    };

    @Output() public onPageLinkClicked = new EventEmitter();

    public onPageLinkClick(pageDirection) {
        this.onPageLinkClicked.emit(pageDirection);
    }
}
