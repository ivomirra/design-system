export interface PaginationViewModelInterface {
    currentPage: number;
    totalPages: number;
    isPreviousDisabled: boolean;
    isNextDisabled: boolean;
    nextText: string;
    previousText: string;
    canShowPagination: boolean;
    showingItemsOfTotalString: string;
}
