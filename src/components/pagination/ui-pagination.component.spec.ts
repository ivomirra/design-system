import { TestBed } from '@angular/core/testing';
import { UiPaginationComponent } from './ui-pagination.component';
import { UI_PAGINATION_CONSTANT } from './ui-pagination.constant';

describe('UiPaginationComponent', () => {
    let testedComponent;
    let fixture;
    let compiledComponent;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [UiPaginationComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiPaginationComponent);
        testedComponent = fixture.debugElement.componentInstance;
        compiledComponent = fixture.debugElement.nativeElement;
        spyOn(testedComponent.onPageLinkClicked, 'emit');

        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    describe('#onPageLinkClick', () => {
        describe('when invoked', () => {
            describe('with a certain direction', () => {
                it('should emit the event with the same direction', () => {
                    testedComponent.onPageLinkClick(UI_PAGINATION_CONSTANT.DIRECTIONS.NEXT);
                    expect(testedComponent.onPageLinkClicked.emit).toHaveBeenCalledWith(
                        UI_PAGINATION_CONSTANT.DIRECTIONS.NEXT
                    );
                    testedComponent.onPageLinkClick(UI_PAGINATION_CONSTANT.DIRECTIONS.PREVIOUS);
                    expect(testedComponent.onPageLinkClicked.emit).toHaveBeenCalledWith(
                        UI_PAGINATION_CONSTANT.DIRECTIONS.PREVIOUS
                    );
                });
            });
        });
    });
});
