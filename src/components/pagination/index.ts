import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UiPaginationComponent } from './ui-pagination.component';
import { UiPaginationViewModelHelper } from './ui-pagination.view-model.helper';

@NgModule({
    imports: [CommonModule],
    declarations: [UiPaginationComponent],
    providers: [UiPaginationViewModelHelper],
    exports: [UiPaginationComponent]
})
export class UiPaginationModule {}
