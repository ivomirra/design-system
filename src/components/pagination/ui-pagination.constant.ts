export const UI_PAGINATION_CONSTANT = {
    DIRECTIONS: {
        PREVIOUS: 'previous',
        NEXT: 'next'
    },
    DEFAULT_BUTTON_TEXT: {
        PREVIOUS: 'Back',
        NEXT: 'Next'
    },
    SHOWING_ITEMS_TOTAL_STRING: 'Showing {firstItem} - {lastItem} of {totalItems}'
};
