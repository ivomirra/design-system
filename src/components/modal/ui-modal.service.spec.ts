import { NgModule, Component, Injector } from '@angular/core';
import { Subject } from 'rxjs';
import { Overlay, OverlayConfig } from '@angular/cdk/overlay';
import { TestBed } from '@angular/core/testing';
import { UiModalService } from './ui-modal.service';
import { ComponentPortal } from '@angular/cdk/portal';

describe('UiModalService', () => {
    @Component({
        selector: 'mock-component',
        template: '<div></div>'
    })
    class MockComponent {}

    @NgModule({
        declarations: [MockComponent],
        entryComponents: [MockComponent]
    })
    class MockDialogModule {}

    const OverlaySpy = jasmine.createSpyObj('OverlaySpy', [
        'position',
        'global',
        'centerHorizontally',
        'centerVertically',
        'scrollStrategies',
        'create'
    ]);

    let subjectSpy;
    let attachSpy;
    let backdropClickSpy;
    let disposeSpy;
    let blockSpy;
    let backDropCallCallbackSpy;
    let testedService;

    beforeEach(() => {
        subjectSpy = new Subject();
        attachSpy = jasmine.createSpy('attach');
        backdropClickSpy = jasmine.createSpy('backdropClick').and.returnValue(subjectSpy);
        disposeSpy = jasmine.createSpy('dispose');
        blockSpy = jasmine.createSpy('block');
        backDropCallCallbackSpy = jasmine.createSpy('backDropCallCallback');

        OverlaySpy.position.and.returnValue(OverlaySpy);
        OverlaySpy.global.and.returnValue(OverlaySpy);
        OverlaySpy.centerHorizontally.and.returnValue(OverlaySpy);
        OverlaySpy.centerVertically.and.returnValue(OverlaySpy);
        OverlaySpy.create.and.returnValue({
            attach: attachSpy,
            backdropClick: backdropClickSpy,
            dispose: disposeSpy
        });

        OverlaySpy.scrollStrategies = {
            block: blockSpy
        };

        TestBed.configureTestingModule({
            providers: [
                UiModalService,
                Overlay,
                {
                    provide: Overlay,
                    useValue: OverlaySpy
                }
            ],
            imports: [MockDialogModule]
        }).compileComponents();

        testedService = TestBed.get(UiModalService);
    });

    describe('#openModal', () => {
        describe('when invoked', () => {
            let result;
            const componentData = {
                dummyInfo: 'dummyData'
            };

            beforeEach(() => {
                result = testedService.openModal(MockComponent, [MockComponent], componentData);
            });

            it('should get the modal position config', () => {
                expect(OverlaySpy.position).toHaveBeenCalled();
                expect(OverlaySpy.global).toHaveBeenCalled();
                expect(OverlaySpy.centerHorizontally).toHaveBeenCalled();
                expect(OverlaySpy.centerVertically).toHaveBeenCalled();
            });

            it('should create the module with an overlay config', () => {
                expect(OverlaySpy.create).toHaveBeenCalledWith(
                    new OverlayConfig({
                        hasBackdrop: true,
                        scrollStrategy: OverlaySpy.scrollStrategies.block(),
                        positionStrategy: OverlaySpy
                    })
                );
            });

            it('should attach the defined component to the modal', () => {
                expect(attachSpy).toHaveBeenCalledWith(
                    new ComponentPortal(
                        MockComponent,
                        null,
                        Injector.create({
                            providers: [
                                {
                                    provide: 'MODAL_DATA',
                                    useValue: {
                                        ...componentData,
                                        childComponents: [MockComponent]
                                    }
                                }
                            ]
                        })
                    )
                );
            });

            it('should return an afterClose method', () => {
                expect(result).toEqual({
                    afterClose: jasmine.any(Function)
                });
            });

            describe('and backdropClick subject is processed', () => {
                beforeEach(() => {
                    subjectSpy.next({});
                });

                it('should invoke dispose overlayref method', () => {
                    expect(disposeSpy).toHaveBeenCalled();
                });

                describe('and a backDropCallCallback is provided', () => {
                    beforeEach(() => {
                        testedService.openModal(MockComponent, [MockComponent], componentData, backDropCallCallbackSpy);
                        subjectSpy.next({});
                    });

                    it('should invoke backDropCallCallback', () => {
                        expect(backDropCallCallbackSpy).toHaveBeenCalled();
                    });
                });
            });
        });
    });

    describe('#close', () => {
        describe('when invoked', () => {
            let expectedResult;
            beforeEach(() => {
                expectedResult = testedService.openModal(MockComponent);
            });

            it('should receive subscription notification when modal is closed', (done) => {
                expectedResult.afterClose().subscribe((result) => {
                    expect(result).toEqual('closedData');
                    done();
                });

                testedService.close('closedData');
            });
        });
    });
});
