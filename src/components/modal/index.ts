import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OverlayModule } from '@angular/cdk/overlay';

import { UiModalService } from './ui-modal.service';
import { UiModalComponent } from './ui-modal.component';

@NgModule({
    imports: [OverlayModule, CommonModule],
    entryComponents: [UiModalComponent],
    declarations: [UiModalComponent],
    providers: [UiModalService],
    exports: [UiModalComponent]
})
export class UiModalModule {}
