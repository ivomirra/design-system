import { Subject } from 'rxjs';
import { Injectable, Injector } from '@angular/core';
import { Overlay, OverlayConfig } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';

@Injectable()
export class UiModalService {
    private modalSubject;
    private overlayRef;

    constructor(private overlay: Overlay) {}

    private disposeModal(context?) {
        this.overlayRef.dispose();
        this.modalSubject.next(context || null);
        this.modalSubject.unsubscribe();
    }

    public openModal(componentParent, childComponents: Array<any>, modalData = {}, backDropCallCallback?) {
        const positionStrategy = this.overlay
            .position()
            .global()
            .centerHorizontally()
            .centerVertically();

        this.modalSubject = new Subject<any>();

        /** Creates overlay */
        this.overlayRef = this.overlay.create(
            new OverlayConfig({
                hasBackdrop: true,
                scrollStrategy: this.overlay.scrollStrategies.block(),
                positionStrategy
            })
        );

        /** Appends component to overlay */
        this.overlayRef.attach(
            new ComponentPortal(
                componentParent,
                null,
                Injector.create({
                    providers: [
                        {
                            provide: 'MODAL_DATA',
                            useValue: {
                                ...modalData,
                                childComponents
                            }
                        }
                    ]
                })
            )
        );

        /** Appends subscribes overlay close */
        this.overlayRef.backdropClick().subscribe(() => {
            if (backDropCallCallback) {
                backDropCallCallback();
                return;
            }

            this.disposeModal();
        });

        return {
            afterClose: () => {
                return this.modalSubject.asObservable();
            }
        };
    }

    public close(data?) {
        this.disposeModal(data);
    }
}
