import { Component, Inject, OnInit, ViewContainerRef, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { UiModalService } from './ui-modal.service';

@Component({
    selector: 'ui-modal',
    templateUrl: './ui-modal.component.html'
})
export class UiModalComponent implements OnInit {
    public viewModel: {
        title: string;
    } = {
        title: this.componentData.title
    };

    @ViewChild('childrenContainer', { read: ViewContainerRef, static: true })
    private viewContainerRef: ViewContainerRef;

    constructor(
        private modal: UiModalService,
        private componentFactoryResolver: ComponentFactoryResolver,
        @Inject('MODAL_DATA') private componentData
    ) {}

    public ngOnInit() {
        this.componentData.childComponents.forEach((component) => {
            const componentCreated = this.viewContainerRef.createComponent(
                this.componentFactoryResolver.resolveComponentFactory(component)
            );
            componentCreated.changeDetectorRef.detectChanges();
        });
    }

    public closeModal() {
        this.modal.close();
    }
}
