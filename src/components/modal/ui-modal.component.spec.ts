import { TestBed } from '@angular/core/testing';
import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';

import { UiModalComponent } from './ui-modal.component';
import { UiModalService } from './ui-modal.service';

describe('UiModalComponent', () => {
    @Component({
        selector: 'mock-component',
        template: '<div></div>'
    })
    class MockComponent {}
    let fixture;
    let testedComponent;

    const detectChangesSpy = jasmine.createSpy('detectChangesSpy');

    const ViewContainerRefSpy = jasmine.createSpyObj('ViewContainerRefSpy', ['createComponent']);
    ViewContainerRefSpy.createComponent.and.returnValue({
        changeDetectorRef: {
            detectChanges: detectChangesSpy
        }
    });

    const ComponentFactoryResolverSpy = jasmine.createSpyObj('ComponentFactoryResolverSpy', [
        'resolveComponentFactory'
    ]);
    const UiModalServiceSpy = jasmine.createSpyObj('UiModalServiceSpy', ['close']);

    const getTestingModuleConfiguration = (childComponents: Array<any>) => {
        return {
            declarations: [UiModalComponent],
            providers: [
                {
                    provide: UiModalService,
                    useValue: UiModalServiceSpy
                },
                {
                    provide: ComponentFactoryResolver,
                    useValue: ComponentFactoryResolverSpy
                },
                {
                    provide: 'MODAL_DATA',
                    useValue: {
                        title: 'test title',
                        childComponents
                    }
                }
            ]
        };
    };

    describe('#ngOnInit', () => {
        describe('when invoked with one component', () => {
            beforeEach(() => {
                TestBed.configureTestingModule(getTestingModuleConfiguration([MockComponent])).compileComponents();
                fixture = TestBed.createComponent(UiModalComponent);
                testedComponent = fixture.debugElement.componentInstance;
                testedComponent.viewContainerRef = ViewContainerRefSpy;
                fixture.detectChanges();
            });
            it('should call componentFactoryResolver.resolveComponentFactory() once', () => {
                expect(ComponentFactoryResolverSpy.resolveComponentFactory).toHaveBeenCalled();
            });

            it('should call viewContainerRef.createComponent() once', () => {
                expect(ViewContainerRefSpy.createComponent).toHaveBeenCalled();
            });

            it('should call componentCreated.changeDetectorRef.detectChanges() once', () => {
                expect(detectChangesSpy).toHaveBeenCalled();
            });
        });

        describe('when invoked with two components', () => {
            beforeEach(() => {
                TestBed.configureTestingModule(
                    getTestingModuleConfiguration([MockComponent, MockComponent])
                ).compileComponents();
                fixture = TestBed.createComponent(UiModalComponent);
                testedComponent = fixture.debugElement.componentInstance;
                testedComponent.viewContainerRef = ViewContainerRefSpy;

                ComponentFactoryResolverSpy.resolveComponentFactory.calls.reset();
                ViewContainerRefSpy.createComponent.calls.reset();
                detectChangesSpy.calls.reset();

                fixture.detectChanges();
            });
            it('should call componentFactoryResolver.resolveComponentFactory() twice', () => {
                expect(ComponentFactoryResolverSpy.resolveComponentFactory.calls.count()).toBe(2);
            });

            it('should call viewContainerRef.createComponent() twice', () => {
                expect(ViewContainerRefSpy.createComponent.calls.count()).toBe(2);
            });

            it('should call componentCreated.changeDetectorRef.detectChanges() once', () => {
                expect(detectChangesSpy.calls.count()).toBe(2);
            });
        });
    });

    describe('#closeModal', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                TestBed.configureTestingModule(getTestingModuleConfiguration([MockComponent])).compileComponents();
                fixture = TestBed.createComponent(UiModalComponent);
                testedComponent = fixture.debugElement.componentInstance;

                testedComponent.viewContainerRef = ViewContainerRefSpy;

                testedComponent.closeModal();
            });
            it('should call modal.close() once', () => {
                expect(UiModalServiceSpy.close).toHaveBeenCalled();
            });
        });
    });
});
