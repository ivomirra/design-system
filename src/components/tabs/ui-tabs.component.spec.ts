import { TestBed } from '@angular/core/testing';
import { UiTabsComponent } from './ui-tabs.component';

describe('UiSpinnerComponent', () => {
    let fixture;
    let tabsComponent;
    let compiledComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [UiTabsComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiTabsComponent);
        tabsComponent = fixture.debugElement.componentInstance;
        compiledComponent = fixture.debugElement.nativeElement;
        spyOn(tabsComponent.onTabChanged, 'emit');

        tabsComponent.viewModel = {};
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(tabsComponent).toBeTruthy();
    });

    describe('on initialization', () => {
        describe('when selectedIndex is not provided', () => {
            it('should set selectedIndex as 0', () => {
                tabsComponent.ngOnInit();

                expect(tabsComponent.viewModel.selectedIndex).toEqual(0);
            });
        });

        describe('when selectedIndex is provided', () => {
            it('should set selectedIndex as 0', () => {
                tabsComponent.viewModel.selectedIndex = 3;
                tabsComponent.ngOnInit();

                expect(tabsComponent.viewModel.selectedIndex).toEqual(3);
            });
        });
    });

    describe('#onTabClicked', () => {
        describe('when selectedTab is disabled', () => {
            const selectedTab = {
                disabled: true
            };
            const selectedTabIndex = 4;

            beforeEach(() => {
                tabsComponent.viewModel.selectedIndex = 3;
                fixture.detectChanges();
            });

            it(`shouldn't update the current selectedIndex`, () => {
                tabsComponent.onTabClicked(selectedTab, selectedTabIndex);

                expect(tabsComponent.viewModel.selectedIndex).toEqual(3);
            });

            it(`shouldn't trigger callback event`, () => {
                tabsComponent.onTabClicked(selectedTab, selectedTabIndex);

                expect(tabsComponent.onTabChanged.emit).not.toHaveBeenCalled();
            });
        });

        describe(`when selectedTab isn't disabled`, () => {
            const selectedTab = {
                text: 'Selected Tab'
            };
            const selectedTabIndex = 4;

            beforeEach(() => {
                tabsComponent.viewModel.selectedIndex = 3;
                fixture.detectChanges();
            });

            it(`should update the current selectedIndex`, () => {
                tabsComponent.onTabClicked(selectedTab, selectedTabIndex);

                expect(tabsComponent.viewModel.selectedIndex).toEqual(4);
            });

            it(`should trigger callback event`, () => {
                tabsComponent.onTabClicked(selectedTab, selectedTabIndex);

                expect(tabsComponent.onTabChanged.emit).toHaveBeenCalledWith({
                    selectedTab,
                    selectedTabIndex
                });
            });
        });
    });
});
