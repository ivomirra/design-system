import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
    selector: 'ui-tabs',
    templateUrl: './ui-tabs.component.html'
})
export class UiTabsComponent implements OnInit {
    /** Property that defines the viewModel to be rendered by ui-tabs **/
    @Input()
    public viewModel: {
        items: object[];
        selectedIndex: number;
    };

    /** Property that sould be used for binding a callback to receive the tab clicked **/
    @Output()
    public onTabChanged: EventEmitter<{
        selectedTab: object;
        selectedTabIndex: number;
    }> = new EventEmitter();

    constructor() {}

    public ngOnInit() {
        this.viewModel.selectedIndex = this.viewModel.selectedIndex || 0;
    }

    /**
     * Method used to update the selected tab. This method also triggers
     * the eventEmitter for onTabChanged callback that the parent may have
     * configured.
     * selectedTab Current tab clicked
     * selectedTabIndex Current tab index clicked
     */
    public onTabClicked(selectedTab: any, selectedTabIndex: number): void {
        if (selectedTab.disabled) {
            return;
        }

        this.onTabChanged.emit({
            selectedTab,
            selectedTabIndex
        });

        this.viewModel.selectedIndex = selectedTabIndex;
    }
}
