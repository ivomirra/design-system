import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';

import { MaterialHelpersModule } from './helpers/index';
import { UiModalModule } from './modal/index';
import { UiToggleButtonsModule } from './toggle-buttons/index';
import { UiTabsModule } from './tabs/index';
import { UiFilterTagsModule } from './filter-tags/index';
import { UiFilterModule } from './filter/index';
import { UiSpinnerModule } from './forms/spinner/index';
import { UiInputSearchModule } from './forms/search/index';
import { UiTooltipsModule } from './messaging/tooltips/index';
import { UiButtonActionsModule } from './button-actions/index';
import { UiPopupMessageModule } from './popup-message/index';
import { UiAutoCompleteModule } from './auto-complete/index';
import { UiDateInputModule } from './forms/date-input/index';
import { UiProgressBarModule } from './progress-bar/index';
import { UiUploadFileModule } from './forms/upload-files/index';
import { UiPaginationModule } from './pagination/index';
import { UiStepperModule } from './stepper/index';
import { UiCurrencyInputModule } from './forms/currency-input/index';
import { UiCopyInputModule } from './forms/copy-input/index';
import { UiCheckboxIndeterminateModule } from './forms/checkbox-indeterminate/index';
import { TooltipModule } from './messaging/tooltip/index';
import { UiWeightUnitsDropdownModule } from './forms/weight-units-dropdown/index';
import { UiCurrenciesDropdownModule } from './forms/currency-dropdown/index';
import { SearchPipeModule } from './pipes/index';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        OverlayModule,
        MaterialHelpersModule,
        UiModalModule,
        UiToggleButtonsModule,
        UiTabsModule,
        UiFilterTagsModule,
        UiFilterModule,
        UiSpinnerModule,
        UiInputSearchModule,
        UiTooltipsModule,
        UiButtonActionsModule,
        UiPopupMessageModule,
        UiAutoCompleteModule,
        UiDateInputModule,
        UiProgressBarModule,
        UiPaginationModule,
        UiUploadFileModule,
        UiStepperModule,
        UiCurrencyInputModule,
        UiCopyInputModule,
        UiCheckboxIndeterminateModule,
        TooltipModule,
        UiWeightUnitsDropdownModule,
        UiCurrenciesDropdownModule,
        SearchPipeModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        OverlayModule,
        MaterialHelpersModule,
        UiModalModule,
        UiToggleButtonsModule,
        UiTabsModule,
        UiFilterTagsModule,
        UiFilterModule,
        UiSpinnerModule,
        UiInputSearchModule,
        UiTooltipsModule,
        UiButtonActionsModule,
        UiPopupMessageModule,
        UiAutoCompleteModule,
        UiDateInputModule,
        UiProgressBarModule,
        UiPaginationModule,
        UiUploadFileModule,
        UiStepperModule,
        UiCurrencyInputModule,
        UiCopyInputModule,
        UiCheckboxIndeterminateModule,
        TooltipModule,
        UiWeightUnitsDropdownModule,
        UiCurrenciesDropdownModule,
        SearchPipeModule,
        ReactiveFormsModule
    ]
})
export class HuubMaterialLibModule {}
