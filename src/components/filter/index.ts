import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialHelpersModule } from '../helpers/index';

import { UiFilterComponent } from './ui-filter.component';

@NgModule({
    imports: [CommonModule, MaterialHelpersModule],
    declarations: [UiFilterComponent],
    exports: [UiFilterComponent]
})
export class UiFilterModule {}
