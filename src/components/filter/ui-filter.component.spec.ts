import { TestBed } from '@angular/core/testing';
import { UiFilterComponent } from './ui-filter.component';
import { MaterialHelper } from '../helpers/material.helpers';

describe('UiFilterComponent', () => {
    const MaterialHelperSpy = jasmine.createSpyObj('MaterialHelperSpy', ['getAbsolutePositionRenderConfig', 'merge']);
    let fixture;
    let testedComponent;

    beforeEach(() => {
        MaterialHelperSpy.getAbsolutePositionRenderConfig.and.returnValue({
            canRenderToLeft: true,
            canRenderToRight: true
        });

        MaterialHelperSpy.merge.and.callFake((value) => value);

        TestBed.configureTestingModule({
            declarations: [UiFilterComponent],
            providers: [
                MaterialHelper,
                {
                    provide: MaterialHelper,
                    useValue: MaterialHelperSpy
                }
            ]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiFilterComponent);
        testedComponent = fixture.debugElement.componentInstance;

        testedComponent.viewModel = {};

        spyOn(testedComponent.onSaveAction, 'emit');
        spyOn(testedComponent, 'toggleOptionContainerVisibility').and.callThrough();

        spyOn(testedComponent.element.nativeElement, 'contains');

        fixture.detectChanges();
    });

    afterEach(() => {
        testedComponent.toggleOptionContainerVisibility.calls.reset();
    });

    it('should create', () => {
        expect(testedComponent).toBeTruthy();
    });

    describe('#onDomClicked', () => {
        describe('when invoked', () => {
            describe("and the native element doens't contain the event target", () => {
                it("shouldn't call toggleOptionContainerVisibility", () => {
                    testedComponent.element.nativeElement.contains.and.returnValue(false);

                    testedComponent.onDomClicked({
                        target: true
                    });

                    expect(testedComponent.toggleOptionContainerVisibility).not.toHaveBeenCalled();
                });

                describe('and optionContainerIsVisible is set as true', () => {
                    it('should call toggleOptionContainerVisibility', () => {
                        testedComponent.optionContainerIsVisible = true;
                        testedComponent.element.nativeElement.contains.and.returnValue(false);

                        testedComponent.onDomClicked({
                            target: true
                        });

                        expect(testedComponent.toggleOptionContainerVisibility).toHaveBeenCalled();
                    });
                });
            });

            describe('and the native element does contain the event target', () => {
                it("shouldn't call toggleOptionContainerVisibility", () => {
                    testedComponent.element.nativeElement.contains.and.returnValue(true);

                    testedComponent.onDomClicked({
                        target: true
                    });

                    expect(testedComponent.toggleOptionContainerVisibility).not.toHaveBeenCalled();
                });
            });
        });
    });

    describe('#trackByFn', () => {
        describe('when invoked', () => {
            it('should return the same value as sent by parameter', () => {
                const result = testedComponent.trackByFn(5);

                expect(result).toEqual(5);
            });
        });
    });

    describe('#shouldShowSaveButton', () => {
        describe('when invoked with undefined selectedOptionParent', () => {
            it('should return false', () => {
                testedComponent.selectedOptionParent = undefined;
                const result = testedComponent.shouldShowSaveButton();

                expect(result).toEqual(false);
            });
        });

        describe('when invoked with selectedOptionParent as object', () => {
            describe('and the property value is not present', () => {
                it('should return false', () => {
                    testedComponent.selectedOptionParent = {};
                    const result = testedComponent.shouldShowSaveButton();

                    expect(result).toEqual(false);
                });

                describe('but selectedOptions has items', () => {
                    it('should return true', () => {
                        testedComponent.selectedOptionParent = {};
                        testedComponent.selectedOptions.length = 1;

                        const result = testedComponent.shouldShowSaveButton();

                        expect(result).toEqual(true);
                    });
                });
            });

            describe('and the property value is not present', () => {
                it('should return true', () => {
                    testedComponent.selectedOptionParent = {
                        value: ''
                    };
                    const result = testedComponent.shouldShowSaveButton();

                    expect(result).toEqual(true);
                });
            });
        });
    });

    describe('#toggleOptionContainerVisibility', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                testedComponent.toggleOptionContainerVisibility();
            });

            it('should have no selected options', () => {
                expect(testedComponent.selectedOptions.length).toEqual(0);
            });

            it('should have no parent selected options', () => {
                expect(testedComponent.selectedOptionParent).toEqual({});
            });

            it('should set alignLeft property with getAbsolutePositionRenderConfig returned value', () => {
                expect(testedComponent.alignLeft).toEqual(true);
            });

            it('should set alignRight property with getAbsolutePositionRenderConfig returned value', () => {
                expect(testedComponent.alignRight).toEqual(true);
            });

            describe('and optionContainerIsVisible is set to false', () => {
                it('should set optionContainerIsVisible to true', () => {
                    testedComponent.optionContainerIsVisible = false;

                    testedComponent.toggleOptionContainerVisibility();

                    expect(testedComponent.optionContainerIsVisible).toEqual(true);
                });
            });

            describe('and optionContainerIsVisible is set to true', () => {
                it('should set optionContainerIsVisible to false', () => {
                    testedComponent.optionContainerIsVisible = true;

                    testedComponent.toggleOptionContainerVisibility();

                    expect(testedComponent.optionContainerIsVisible).toEqual(false);
                });
            });
        });
    });

    describe('#onSaveButtonClick', () => {
        describe('when invoked', () => {
            it('should emit changes', () => {
                testedComponent.onSaveButtonClick();

                expect(testedComponent.onSaveAction.emit).toHaveBeenCalledWith({
                    selectedOption: undefined,
                    selectedOptionParent: undefined
                });
            });

            it('should call toggleOptionContainerVisibility method', () => {
                testedComponent.onSaveButtonClick();

                expect(testedComponent.toggleOptionContainerVisibility).toHaveBeenCalled();
            });
        });

        describe('when theres a selected value in selectedOption', () => {
            it('should find that element and emit', () => {
                testedComponent.selectedOptions = [
                    {
                        text: 'test'
                    },
                    {
                        text: 'test2',
                        selected: true
                    }
                ];

                testedComponent.onSaveButtonClick();

                expect(testedComponent.onSaveAction.emit).toHaveBeenCalledWith({
                    selectedOption: {
                        text: 'test2',
                        selected: true
                    },
                    selectedOptionParent: undefined
                });
            });
        });

        describe(`when selectedOption doesn't contains selected value`, () => {
            it('should select the first array item', () => {
                testedComponent.selectedOptions = [
                    {
                        text: 'test'
                    },
                    {
                        text: 'test2'
                    }
                ];

                testedComponent.onSaveButtonClick();

                expect(testedComponent.onSaveAction.emit).toHaveBeenCalledWith({
                    selectedOption: {
                        text: 'test'
                    },
                    selectedOptionParent: undefined
                });
            });
        });
    });

    describe('#onSelectChange', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                const optionIndex = 1;

                testedComponent.viewModel.selectOptions = {
                    type1: [
                        {
                            value: '1-1',
                            text: 'item-1-1'
                        },
                        {
                            value: '1-2',
                            text: 'item-1-2'
                        }
                    ],
                    type2: [
                        {
                            value: '2-1',
                            text: 'item-2-1'
                        }
                    ]
                };

                testedComponent.viewModel.selectBox = [
                    {
                        value: '1',
                        text: 'item-1',
                        selected: false,
                        type: 'type1'
                    },
                    {
                        value: '2',
                        text: 'item-2',
                        selected: false,
                        type: 'type2'
                    },
                    {
                        value: '3',
                        text: 'item-3',
                        selected: false,
                        type: 'type3'
                    }
                ];

                testedComponent.onSelectChange(testedComponent.viewModel.selectBox, optionIndex);
            });

            it('should have the option index provided marked as selected', () => {
                expect(testedComponent.viewModel.selectBox).toEqual([
                    {
                        value: '1',
                        text: 'item-1',
                        selected: false,
                        type: 'type1'
                    },
                    {
                        value: '2',
                        text: 'item-2',
                        selected: true,
                        type: 'type2'
                    },
                    {
                        value: '3',
                        text: 'item-3',
                        selected: false,
                        type: 'type3'
                    }
                ]);
            });

            describe('and selected item type match any existing type', () => {
                it('should have the option index provided in selectedOptions', () => {
                    expect(testedComponent.selectedOptions).toEqual([
                        {
                            value: '2-1',
                            text: 'item-2-1'
                        }
                    ]);
                });

                it('should have the parent option index provided in selectedOptionsParent', () => {
                    expect(testedComponent.selectedOptionParent).toEqual({
                        value: '2',
                        text: 'item-2',
                        selected: true,
                        type: 'type2'
                    });
                });
            });

            describe('and selected item type does not match any existing type', () => {
                it('should have the option index provided in selectedOptions', () => {
                    testedComponent.onSelectChange(testedComponent.viewModel.selectBox, 2);

                    expect(testedComponent.selectedOptions).toEqual([]);
                });
            });
        });
    });

    describe('#onSecondarySelectChange', () => {
        describe('when invoked', () => {
            const optionIndex = 0;
            let options;

            beforeEach(() => {
                options = [
                    {
                        value: '1',
                        text: 'item-1',
                        selected: false
                    },
                    {
                        value: '2',
                        text: 'item-2',
                        selected: false
                    }
                ];

                testedComponent.onSecondarySelectChange(options, optionIndex);
            });

            it('should have the option index provided marked as selected', () => {
                expect(options).toEqual([
                    {
                        value: '1',
                        text: 'item-1',
                        selected: true
                    },
                    {
                        value: '2',
                        text: 'item-2',
                        selected: false
                    }
                ]);
            });
        });
    });
});
