import { Component, Input, Output, EventEmitter, ElementRef, HostListener } from '@angular/core';
import { MaterialHelper } from '../helpers/material.helpers';

export interface OptionsInterface {
    value: string;
    text: string;
    selected?: boolean;
    type?: string;
}

@Component({
    selector: 'ui-filter',
    templateUrl: './ui-filter.component.html'
})
export class UiFilterComponent {
    private readonly ELEMENT_SIZE = 350;
    private selectedOptionParent;
    public alignRight = false;
    public alignLeft = false;
    public selectedOptions = [];
    public optionContainerIsVisible = false;

    @Input()
    public viewModel: {
        filterText: string;
        buttonText: string;
        selectBox: [OptionsInterface];
        selectOptions: any;
    };

    /** Property that should be used for binding a callback to receive the save action **/
    @Output()
    public onSaveAction: EventEmitter<{
        selectedOption: any;
        selectedOptionParent: any;
    }> = new EventEmitter();

    constructor(private element: ElementRef, private helper: MaterialHelper) {}

    public trackByFn(index, item): number {
        return index;
    }

    @HostListener('document:click', ['$event'])
    public onDomClicked(event) {
        if (!this.element.nativeElement.contains(event.target) && this.optionContainerIsVisible) {
            this.toggleOptionContainerVisibility();
        }
    }

    public toggleOptionContainerVisibility(): void {
        const absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(
            this.element.nativeElement.getBoundingClientRect().left,
            this.ELEMENT_SIZE
        );

        this.optionContainerIsVisible = !this.optionContainerIsVisible;

        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
        this.selectedOptions.length = 0;
        this.selectedOptionParent = {};
    }

    public onSaveButtonClick(): void {
        this.onSaveAction.emit({
            selectedOptionParent: this.selectedOptionParent,
            selectedOption: this.selectedOptions.find((item) => item.selected === true) || this.selectedOptions[0]
        });

        this.toggleOptionContainerVisibility();
    }

    private setSelectedOption(options, optionIndex) {
        options.forEach((item) => {
            item.selected = false;
        });

        options[optionIndex].selected = true;
    }

    public onSelectChange(options: any, indexOption: number): void {
        this.setSelectedOption(options, indexOption);

        this.selectedOptions = [];
        this.selectedOptionParent = this.helper.merge(this.viewModel.selectBox[indexOption]);

        if (this.viewModel.selectOptions[this.viewModel.selectBox[indexOption].type]) {
            this.selectedOptions = new Array().concat(
                this.viewModel.selectOptions[this.viewModel.selectBox[indexOption].type].map((value) => {
                    return this.helper.merge(value);
                })
            );
        }
    }

    public shouldShowSaveButton() {
        return !!(
            this.selectedOptionParent &&
            (this.selectedOptionParent.hasOwnProperty('value') || this.selectedOptions.length > 0)
        );
    }

    public onSecondarySelectChange(options: any, indexOption: number): void {
        this.setSelectedOption(options, indexOption);
    }
}
