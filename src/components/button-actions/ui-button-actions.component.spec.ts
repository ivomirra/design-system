import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { UiButtonActionsComponent } from './ui-button-actions.component';
import { MaterialHelper } from '../helpers/material.helpers';

describe('UiButtonActionsComponent', () => {
    const MaterialHelperSpy = jasmine.createSpyObj('MaterialHelperSpy', ['getAbsolutePositionRenderConfig']);
    let fixture: any;
    let testedComponent: any;

    beforeEach(() => {
        MaterialHelperSpy.getAbsolutePositionRenderConfig.and.returnValue({
            canRenderToLeft: true,
            canRenderToRight: true
        });

        TestBed.configureTestingModule({
            imports: [FormsModule],
            declarations: [UiButtonActionsComponent],
            providers: [
                MaterialHelper,
                {
                    provide: MaterialHelper,
                    useValue: MaterialHelperSpy
                }
            ]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiButtonActionsComponent);
        testedComponent = fixture.debugElement.componentInstance;

        testedComponent.viewModel = {};

        fixture.detectChanges();
    });

    describe('#showOptionContainerVisibility', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                testedComponent.optionContainerIsVisible = false;
                testedComponent.showOptionContainerVisibility();
            });

            it('should get the absolute position config', () => {
                expect(MaterialHelperSpy.getAbsolutePositionRenderConfig).toHaveBeenCalledWith(0, 200);
            });

            it('should set optionContainerIsVisible as true', () => {
                expect(testedComponent.optionContainerIsVisible).toEqual(true);
            });

            it('should set align properties as returned by getAbsolutePositionRenderConfig method', () => {
                expect(testedComponent.alignLeft).toEqual(true);
                expect(testedComponent.alignRight).toEqual(true);
            });
        });
    });

    describe('#hideOptionContainerVisibility', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                testedComponent.optionContainerIsVisible = true;
                testedComponent.hideOptionContainerVisibility();
            });

            it('should get the absolute position config', () => {
                expect(testedComponent.optionContainerIsVisible).toEqual(false);
            });
        });
    });

    describe('#onLinkClicked', () => {
        describe('when invoked', () => {
            const callbackSpy = jasmine.createSpy('callbackSpy');

            afterEach(() => {
                callbackSpy.calls.reset();
            });

            describe('with a defined link', () => {
                beforeEach(() => {
                    testedComponent.onLinkClicked({
                        text: 'Link Example'
                    });
                });

                it("shouldn't call the selected link callback", () => {
                    expect(callbackSpy).not.toHaveBeenCalled();
                });

                describe('and with a callback defined', () => {
                    beforeEach(() => {
                        testedComponent.onLinkClicked({
                            text: 'Link Example',
                            callback: callbackSpy
                        });
                    });

                    it('should call the selected link callback', () => {
                        expect(callbackSpy).toHaveBeenCalledWith({
                            text: 'Link Example',
                            callback: callbackSpy
                        });
                    });
                });
            });

            describe('without a defined link', () => {
                beforeEach(() => {
                    testedComponent.onLinkClicked({});
                });

                it("shouldn't call the selected link callback", () => {
                    expect(callbackSpy).not.toHaveBeenCalled();
                });
            });
        });
    });
});
