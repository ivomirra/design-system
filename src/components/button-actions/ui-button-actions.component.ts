import { Component, Input, ElementRef } from '@angular/core';
import { MaterialHelper } from '../helpers/material.helpers';

@Component({
    selector: 'ui-button-actions',
    templateUrl: './ui-button-actions.component.html'
})
export class UiButtonActionsComponent {
    private DEFAULT_TYPE = 'tertiary';
    private ELEMENT_SIZE = 200;
    public alignRight = false;
    public alignLeft = false;

    @Input()
    public viewModel: {
        items: [
            {
                text: string;
                callback?: Function;
            }
        ];
    };

    @Input() public type = this.DEFAULT_TYPE;

    public optionContainerIsVisible = false;

    constructor(private element: ElementRef, private helper: MaterialHelper) {}

    public showOptionContainerVisibility(): void {
        const absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(
            this.element.nativeElement.getBoundingClientRect().left,
            this.ELEMENT_SIZE
        );

        this.optionContainerIsVisible = true;
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
    }

    public hideOptionContainerVisibility(): void {
        this.optionContainerIsVisible = false;
    }

    public onLinkClicked(link) {
        if (!link.callback || typeof link.callback !== 'function') {
            return;
        }

        link.callback(link);
    }
}
