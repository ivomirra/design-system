import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialHelpersModule } from '../helpers/index';

import { UiButtonActionsComponent } from './ui-button-actions.component';

@NgModule({
    imports: [CommonModule, MaterialHelpersModule],
    declarations: [UiButtonActionsComponent],
    exports: [UiButtonActionsComponent]
})
export class UiButtonActionsModule {}
