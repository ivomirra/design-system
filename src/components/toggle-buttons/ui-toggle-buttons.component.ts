import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
    selector: 'ui-toggle-buttons',
    templateUrl: './ui-toggle-buttons.component.html'
})
export class UiToggleButtonsComponent implements OnInit {
    /** Property that defines the viewModel to be rendered by ui-toggle-buttons **/
    @Input()
    public viewModel: {
        items: object[];
        selectedIndex: number;
    };

    /** Property that sould be used for binding a callback to receive the
     button change event **/
    @Output()
    public onButtonChanged: EventEmitter<{
        toggleButtonSelected: object;
        toggleButtonIndexSelected: number;
    }> = new EventEmitter();

    constructor() {}

    public ngOnInit() {
        this.viewModel.selectedIndex = this.viewModel.selectedIndex || 0;
    }

    /**
     * Method binded for the button toggle change action.
     * toggleButtonIndex Current tab index clicked
     */
    public onToggleButtonClicked(toggleButtonIndex: number): void {
        if (!this.viewModel.items[toggleButtonIndex]) {
            return;
        }

        this.viewModel.selectedIndex = toggleButtonIndex;

        this.onButtonChanged.emit({
            toggleButtonSelected: this.viewModel.items[toggleButtonIndex],
            toggleButtonIndexSelected: toggleButtonIndex
        });
    }
}
