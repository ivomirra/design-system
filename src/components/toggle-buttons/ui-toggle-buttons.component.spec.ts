import { TestBed } from '@angular/core/testing';
import { UiToggleButtonsComponent } from './ui-toggle-buttons.component';

describe('UiToggleButtonsComponent', () => {
    let fixture;
    let testedComponent;
    let compiledComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [UiToggleButtonsComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiToggleButtonsComponent);
        testedComponent = fixture.debugElement.componentInstance;
        compiledComponent = fixture.debugElement.nativeElement;

        testedComponent.viewModel = {
            selectedIndex: 1
        };

        spyOn(testedComponent.onButtonChanged, 'emit');

        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    describe('#ngOnInit', () => {
        describe('when invoked', () => {
            describe('and the selectedIndex is provided', () => {
                it('should keep the same value', () => {
                    testedComponent.ngOnInit();

                    expect(testedComponent.viewModel.selectedIndex).toEqual(1);
                });
            });

            describe('and the selectedIndex is not provided', () => {
                it('should keep the same selectedIndex state', () => {
                    testedComponent.viewModel = {};

                    testedComponent.ngOnInit();

                    expect(testedComponent.viewModel.selectedIndex).toEqual(0);
                });
            });
        });
    });

    describe('#onToggleButtonClicked', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                testedComponent.viewModel = {
                    selectedIndex: 0,
                    items: [
                        {
                            text: 'Example-1'
                        },
                        {
                            text: 'Example-2'
                        }
                    ]
                };
            });

            describe("and the index provided doesn't match one of the items", () => {
                beforeEach(() => {
                    testedComponent.onToggleButtonClicked(3);
                });

                it("shouldn't update the selectedIndex", () => {
                    expect(testedComponent.viewModel.selectedIndex).toEqual(0);
                });

                it("shouldn't emit any changes", () => {
                    expect(testedComponent.onButtonChanged.emit).not.toHaveBeenCalled();
                });
            });

            describe('and the index provided matches one of the items', () => {
                beforeEach(() => {
                    testedComponent.onToggleButtonClicked(1);
                });

                it('should update the selectedIndex', () => {
                    expect(testedComponent.viewModel.selectedIndex).toEqual(1);
                });

                it('should emit any changes', () => {
                    expect(testedComponent.onButtonChanged.emit).toHaveBeenCalledWith({
                        toggleButtonSelected: {
                            text: 'Example-2'
                        },
                        toggleButtonIndexSelected: 1
                    });
                });
            });
        });
    });
});
