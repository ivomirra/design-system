import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UiToggleButtonsComponent } from './ui-toggle-buttons.component';

@NgModule({
    imports: [CommonModule],
    declarations: [UiToggleButtonsComponent],
    exports: [UiToggleButtonsComponent]
})
export class UiToggleButtonsModule {}
