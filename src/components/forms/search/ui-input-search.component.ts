import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'ui-input-search',
    templateUrl: './ui-input-search.component.html'
})
export class UiInputSearchComponent {
    @Input()
    public viewModel: {
        /** Property that defines the input placeholder **/
        placeholder: string;
        /** Property that defines the input value **/
        value: string;
    } = {
        placeholder: '',
        value: ''
    };

    /** Property that sould be used for binding a callback to receive the
     input submission **/
    @Output() public onSubmit: EventEmitter<string> = new EventEmitter();

    /** Property that sould be used for binding a callback to receive the
     input change state **/
    @Output() public onChange: EventEmitter<string> = new EventEmitter();

    constructor() {}

    /**
     * Method binded to the form submit event.
     */
    public onSubmitForm(): void {
        this.onSubmit.emit(this.viewModel.value);
    }

    /**
     * Method binded to the input change event.
     */
    public onInputChange(): void {
        this.onChange.emit(this.viewModel.value);
    }

    public onClearIconClicked(): void {
        this.viewModel.value = '';

        this.onSubmitForm();
    }
}
