import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { UiInputSearchComponent } from './ui-input-search.component';

describe('UiInputSearchComponent', () => {
    let fixture;
    let searchComponent;
    let compiledComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [UiInputSearchComponent],
            imports: [FormsModule]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiInputSearchComponent);
        searchComponent = fixture.debugElement.componentInstance;
        compiledComponent = fixture.debugElement.nativeElement;
    });

    it('should create the component', () => {
        expect(searchComponent).toBeTruthy();
    });

    describe('#onSubmitForm', () => {
        describe('when a value is provided', () => {
            it('should submit the value', () => {
                searchComponent.viewModel.value = 'Test';

                searchComponent.onSubmit.subscribe((value) => {
                    expect(value).toEqual('Test');
                });

                searchComponent.onSubmitForm();
            });
        });
    });

    describe('#onClearIconClicked', () => {
        describe('when invoked', () => {
            it('should should reset the viewModel value and emit onSubmit event', () => {
                searchComponent.viewModel.value = 'Test';

                searchComponent.onSubmit.subscribe((value) => {
                    expect(value).toEqual('');
                });

                searchComponent.onClearIconClicked();
            });
        });
    });

    describe('#onInputChange', () => {
        describe('when a value is defined', () => {
            it('should emit onChange event with defined viewModel value', () => {
                searchComponent.viewModel.value = '1';

                searchComponent.onChange.subscribe((value) => {
                    expect(value).toEqual('1');
                });

                searchComponent.onInputChange();
            });
        });
    });
});
