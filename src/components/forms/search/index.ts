import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { UiInputSearchComponent } from './ui-input-search.component';

@NgModule({
    imports: [CommonModule, FormsModule],
    declarations: [UiInputSearchComponent],
    exports: [UiInputSearchComponent]
})
export class UiInputSearchModule {}
