import { Component, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
    selector: 'ui-upload-file',
    templateUrl: './ui-upload-file.component.html'
})
export class UiUploadFileComponent {
    @Input()
    public viewModel: {
        isUploading?: boolean;
        isUploaded?: boolean;
        fileName?: string;
        fileUploadPercentage?: number | string;
    } = {};

    @Output() private onFileSelected = new EventEmitter();
    @Output() private onFileRemoved = new EventEmitter();
    @Output() private onFileUploadCanceled = new EventEmitter();
    @Output() private onFileViewClicked = new EventEmitter();

    @ViewChild('uploadInput', { read: ElementRef, static: false })
    private uploadInput: ElementRef<HTMLElement>;

    constructor() {}

    public componentActionsModel = {
        isBeingDragged: false
    };

    private handleSelectedFile(selectedFile) {
        this.viewModel.fileName = selectedFile.name;
        this.viewModel.fileUploadPercentage = 0;
        this.viewModel.isUploading = true;
    }

    public onDragOver($event) {
        $event.preventDefault();

        this.componentActionsModel.isBeingDragged = true;
    }

    public onDragLeave($event) {
        $event.preventDefault();

        this.componentActionsModel.isBeingDragged = false;
    }

    public onDragDrop($event) {
        $event.preventDefault();

        this.componentActionsModel.isBeingDragged = false;

        const selectedFile = $event.dataTransfer.files[0];

        this.handleSelectedFile(selectedFile);

        this.onFileSelected.emit({
            selectedFile,
            viewModel: this.viewModel
        });
    }

    public onChange($event) {
        $event.preventDefault();

        const selectedFile = $event.target.files[0];

        this.handleSelectedFile(selectedFile);

        this.onFileSelected.emit({
            selectedFile,
            viewModel: this.viewModel
        });
    }

    public onRemoveFileClicked($event) {
        $event.preventDefault();

        this.onFileRemoved.emit({
            viewModel: this.viewModel
        });
    }

    public onUploadCancelClicked() {
        this.viewModel.isUploading = false;
        this.viewModel.fileUploadPercentage = 0;

        this.onFileUploadCanceled.emit({
            viewModel: this.viewModel
        });
    }

    public onReplaceDocumentClicked($event) {
        $event.preventDefault();

        this.uploadInput.nativeElement.click();
    }

    public onViewFileClicked($event) {
        $event.preventDefault();

        this.onFileViewClicked.emit({
            viewModel: this.viewModel
        });
    }
}
