import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { UiUploadFileComponent } from './ui-upload-file.component';
import { UiProgressBarComponent } from '../../progress-bar/ui-progress-bar.component';

describe('UiUploadFileComponent', () => {
    let fixture;
    let testedComponent;
    let compiledComponent;
    const $event = {
        preventDefault: () => {},
        target: {
            files: [
                {
                    name: 'File Mock'
                }
            ]
        },
        dataTransfer: {
            files: [
                {
                    name: 'File Mock'
                }
            ]
        }
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [UiUploadFileComponent, UiProgressBarComponent],
            imports: [FormsModule]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiUploadFileComponent);
        testedComponent = fixture.debugElement.componentInstance;
        compiledComponent = fixture.debugElement.nativeElement;

        testedComponent.viewContainerRef = {
            element: {
                nativeElement: {
                    click: () => {}
                }
            }
        };

        testedComponent.uploadInput = {
            nativeElement: {
                click: () => {}
            }
        };

        spyOn(testedComponent.onFileSelected, 'emit');
        spyOn(testedComponent.onFileRemoved, 'emit');
        spyOn(testedComponent.onFileUploadCanceled, 'emit');
        spyOn(testedComponent.onFileViewClicked, 'emit');
        spyOn(testedComponent.viewContainerRef.element.nativeElement, 'click').and.callThrough();
        spyOn(testedComponent.uploadInput.nativeElement, 'click').and.callThrough();

        spyOn(testedComponent, 'handleSelectedFile').and.callThrough();
    });

    afterEach(() => {
        testedComponent.onFileSelected.emit.calls.reset();
        testedComponent.onFileRemoved.emit.calls.reset();
        testedComponent.onFileUploadCanceled.emit.calls.reset();
        testedComponent.onFileViewClicked.emit.calls.reset();
        testedComponent.handleSelectedFile.calls.reset();
        testedComponent.viewContainerRef.element.nativeElement.click.calls.reset();
    });

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    describe('#onDragOver', () => {
        describe('when invoked', () => {
            it('should set isBeingDragged as true', () => {
                testedComponent.componentActionsModel.isBeingDragged = false;

                testedComponent.onDragOver($event);

                expect(testedComponent.componentActionsModel.isBeingDragged).toBeTruthy();
            });
        });
    });

    describe('#onDragLeave', () => {
        describe('when invoked', () => {
            it('should set isBeingDragged as false', () => {
                testedComponent.componentActionsModel.isBeingDragged = true;

                testedComponent.onDragLeave($event);

                expect(testedComponent.componentActionsModel.isBeingDragged).toBeFalsy();
            });
        });
    });

    describe('#onDragDrop', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                testedComponent.componentActionsModel.isBeingDragged = true;

                testedComponent.onDragDrop($event);
            });

            it('should set isBeingDragged as false', () => {
                expect(testedComponent.componentActionsModel.isBeingDragged).toBeFalsy();
            });

            it('should call handleSelectedFile method', () => {
                expect(testedComponent.handleSelectedFile).toHaveBeenCalledWith({
                    name: 'File Mock'
                });
            });

            it('should emit onFileSelected event', () => {
                expect(testedComponent.onFileSelected.emit).toHaveBeenCalledWith({
                    selectedFile: {
                        name: 'File Mock'
                    },
                    viewModel: {
                        fileName: 'File Mock',
                        fileUploadPercentage: 0,
                        isUploading: true
                    }
                });
            });
        });
    });

    describe('#onChange', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                testedComponent.onChange($event);
            });

            it('should call handleSelectedFile method', () => {
                expect(testedComponent.handleSelectedFile).toHaveBeenCalledWith({
                    name: 'File Mock'
                });
            });

            it('should emit onFileSelected event', () => {
                expect(testedComponent.onFileSelected.emit).toHaveBeenCalledWith({
                    selectedFile: {
                        name: 'File Mock'
                    },
                    viewModel: {
                        fileName: 'File Mock',
                        fileUploadPercentage: 0,
                        isUploading: true
                    }
                });
            });
        });
    });

    describe('#onRemoveFileClicked', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                testedComponent.onRemoveFileClicked($event);
            });

            it('should emit onFileRemoved event', () => {
                expect(testedComponent.onFileRemoved.emit).toHaveBeenCalledWith({
                    viewModel: {}
                });
            });
        });
    });

    describe('#onUploadCancelClicked', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                testedComponent.onUploadCancelClicked();
            });

            it('should set isUploading as false', () => {
                expect(testedComponent.viewModel.isUploading).toBeFalsy();
            });

            it('should set fileUploadPercentage as 0', () => {
                expect(testedComponent.viewModel.fileUploadPercentage).toBe(0);
            });

            it('should emit onFileUploadCanceled event', () => {
                expect(testedComponent.onFileUploadCanceled.emit).toHaveBeenCalledWith({
                    viewModel: {
                        isUploading: false,
                        fileUploadPercentage: 0
                    }
                });
            });
        });
    });

    describe('#onReplaceDocumentClicked', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                testedComponent.onReplaceDocumentClicked($event);
            });

            it('should call click from the native element', () => {
                expect(testedComponent.uploadInput.nativeElement.click).toHaveBeenCalled();
            });
        });
    });

    describe('#onViewFileClicked', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                testedComponent.onViewFileClicked($event);
            });

            it('should emit onFileViewClicked event', () => {
                expect(testedComponent.onFileViewClicked.emit).toHaveBeenCalledWith({
                    viewModel: {}
                });
            });
        });
    });
});
