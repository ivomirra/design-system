import { TestBed } from '@angular/core/testing';

import { HttpEventType } from '@angular/common/http';
import { UiUploadFileEventsHelper } from './ui-upload-file.events.helper';

describe('MaterialHelper', () => {
    let testedService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [UiUploadFileEventsHelper]
        }).compileComponents();
    });

    beforeEach(() => {
        testedService = TestBed.get(UiUploadFileEventsHelper);
    });

    it('should create', () => {
        expect(testedService).toBeTruthy();
    });

    describe('#getAbstractedEventsDataModel', () => {
        describe('when invoked', () => {
            describe('and the HttpEventType is sent', () => {
                it('should get the correct model data', () => {
                    const result = testedService.getAbstractedEventsDataModel({
                        type: HttpEventType.Sent
                    });

                    expect(result).toEqual({
                        isUploaded: false,
                        isUploading: false,
                        fileUploadPercentage: 0
                    });
                });
            });

            describe('and the HttpEventType is UploadProgress', () => {
                it('should get the correct model data', () => {
                    const result = testedService.getAbstractedEventsDataModel({
                        type: HttpEventType.UploadProgress,
                        loaded: 123,
                        total: 500
                    });

                    expect(result).toEqual({
                        isUploaded: false,
                        isUploading: true,
                        fileUploadPercentage: 25
                    });
                });
            });

            describe('and the HttpEventType is Response', () => {
                it('should get the correct model data', () => {
                    const result = testedService.getAbstractedEventsDataModel({
                        type: HttpEventType.Response,
                        loaded: 123,
                        total: 500
                    });

                    expect(result).toEqual({
                        type: HttpEventType.Response,
                        loaded: 123,
                        total: 500,
                        isUploaded: true,
                        isUploading: false,
                        fileUploadPercentage: 100
                    });
                });
            });
        });
    });
});
