import { Injectable } from '@angular/core';
import { HttpEventType } from '@angular/common/http';

@Injectable()
export class UiUploadFileEventsHelper {
    constructor() {}

    public getAbstractedEventsDataModel(event: any): any {
        switch (event.type) {
            case HttpEventType.Sent:
                return {
                    isUploaded: false,
                    isUploading: false,
                    fileUploadPercentage: 0
                };

            case HttpEventType.UploadProgress:
                return {
                    isUploaded: false,
                    isUploading: true,
                    fileUploadPercentage: Math.round(100 * event.loaded / event.total)
                };

            case HttpEventType.Response:
                return {
                    ...event,
                    isUploaded: true,
                    isUploading: false,
                    fileUploadPercentage: 100
                };
        }
    }
}
