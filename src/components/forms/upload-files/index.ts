import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { UiUploadFileComponent } from './ui-upload-file.component';
import { UiUploadFileEventsHelper } from './ui-upload-file.events.helper';
import { UiProgressBarModule } from '../../progress-bar/index';

@NgModule({
    imports: [CommonModule, FormsModule, UiProgressBarModule],
    providers: [UiUploadFileEventsHelper],
    declarations: [UiUploadFileComponent],
    exports: [UiUploadFileComponent]
})
export class UiUploadFileModule {}
