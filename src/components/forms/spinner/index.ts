import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { UiSpinnerComponent } from './ui-spinner.component';

@NgModule({
    imports: [CommonModule, FormsModule],
    declarations: [UiSpinnerComponent],
    exports: [UiSpinnerComponent]
})
export class UiSpinnerModule {}
