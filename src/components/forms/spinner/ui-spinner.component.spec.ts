import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { UiSpinnerComponent } from './ui-spinner.component';

describe('UiSpinnerComponent', () => {
    let fixture;
    let spinnerComponent;
    let compiledComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [UiSpinnerComponent],
            imports: [FormsModule]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiSpinnerComponent);
        spinnerComponent = fixture.debugElement.componentInstance;
        compiledComponent = fixture.debugElement.nativeElement;
        spinnerComponent.allowNegatives = true;
    });

    it('should create the component', () => {
        expect(spinnerComponent).toBeTruthy();
    });

    describe('when a invalid value is provided', () => {
        it('should throw an error', () => {
            expect(() => {
                spinnerComponent.ngOnInit();
            }).toThrowError(TypeError, 'The value defined should be a number');
        });
    });

    describe('when a valid value is provided', () => {
        beforeEach(() => {
            spinnerComponent.value = 1;
            fixture.detectChanges();
        });

        it(`shouldn't throw and error`, () => {
            expect(() => {
                spinnerComponent.ngOnInit();
            }).not.toThrowError();
        });

        describe(`and hasError isn't provided`, () => {
            it('should render input without error class', () => {
                expect(compiledComponent.querySelector('.ui-spinner__input').className).not.toContain(
                    'ui-spinner__input--error'
                );
            });
        });

        describe(`and disabled isn't provided`, () => {
            it('should render elements not disabled', () => {
                fixture.detectChanges();

                expect(compiledComponent.querySelector('.ui-spinner__input').getAttribute('disabled')).toEqual(null);

                expect(compiledComponent.querySelector('.ui-spinner__button-less').getAttribute('disabled')).toEqual(
                    null
                );

                expect(compiledComponent.querySelector('.ui-spinner__button-plus').getAttribute('disabled')).toEqual(
                    null
                );
            });
        });

        describe(`and hasError is provided as true`, () => {
            it('should render input with error class', () => {
                spinnerComponent.hasError = true;

                fixture.detectChanges();
                expect(compiledComponent.querySelector('.ui-spinner__input').className).toContain(
                    'ui-spinner__input--error'
                );
            });
        });

        describe(`and disabled is provided as true`, () => {
            it('should render elements disabled', () => {
                spinnerComponent.disabled = true;

                fixture.detectChanges();
                expect(compiledComponent.querySelector('.ui-spinner__input').getAttribute('disabled')).toEqual('true');

                expect(compiledComponent.querySelector('.ui-spinner__button-less').getAttribute('disabled')).toEqual(
                    'true'
                );

                expect(compiledComponent.querySelector('.ui-spinner__button-plus').getAttribute('disabled')).toEqual(
                    'true'
                );
            });
        });

        describe('#onPlusButtonPressed', () => {
            describe('when executed', () => {
                it('should increment 1 to the value defined', () => {
                    expect(spinnerComponent.value).toEqual(1);

                    spinnerComponent.onPlusButtonPressed();
                    fixture.detectChanges();

                    expect(spinnerComponent.value).toEqual(2);
                });

                it('should emit onValueChanged', () => {
                    let valueByDefault = 1;

                    spinnerComponent.onValueChanged.subscribe((value) => {
                        valueByDefault += 1;

                        expect(value).toEqual(valueByDefault);
                    });

                    spinnerComponent.onPlusButtonPressed();

                    spinnerComponent.onPlusButtonPressed();

                    spinnerComponent.onPlusButtonPressed();

                    fixture.detectChanges();

                    expect(spinnerComponent.value).toEqual(4);
                });
            });
        });

        describe('#onLessButtonPressed', () => {
            describe('when executed', () => {
                it('should decrement 1 to the value defined', () => {
                    expect(spinnerComponent.value).toEqual(1);

                    spinnerComponent.onLessButtonPressed();
                    fixture.detectChanges();

                    expect(spinnerComponent.value).toEqual(0);
                });

                it('should emit onValueChanged', () => {
                    let valueByDefault = 1;

                    spinnerComponent.onValueChanged.subscribe((value) => {
                        valueByDefault -= 1;

                        expect(value).toEqual(valueByDefault);
                    });

                    spinnerComponent.onLessButtonPressed();

                    spinnerComponent.onLessButtonPressed();

                    spinnerComponent.onLessButtonPressed();

                    fixture.detectChanges();

                    expect(spinnerComponent.value).toEqual(-2);
                });

                describe('and the allowNegatives is defined as true', () => {
                    it('should set value as 0 when the value reach negative', () => {
                        let valueByDefault = 1;
                        spinnerComponent.allowNegatives = false;

                        spinnerComponent.onValueChanged.subscribe((value) => {
                            valueByDefault -= 1;

                            expect(value).toEqual(valueByDefault);
                        });

                        spinnerComponent.onLessButtonPressed();

                        spinnerComponent.onLessButtonPressed();

                        spinnerComponent.onLessButtonPressed();

                        fixture.detectChanges();

                        expect(spinnerComponent.value).toEqual(0);
                    });
                });
            });
        });

        describe('#onKeyUp', () => {
            describe('when executed', () => {
                it('should emit onValueChanged', () => {
                    spyOn(spinnerComponent.onValueChanged, 'emit');

                    spinnerComponent.onKeyUp();
                    fixture.detectChanges();

                    expect(spinnerComponent.onValueChanged.emit).toHaveBeenCalled();
                });
            });
        });
    });
});
