import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
    selector: 'ui-spinner',
    templateUrl: './ui-spinner.component.html'
})
export class UiSpinnerComponent implements OnInit {
    /** Property that defines the input value **/
    @Input() public allowNegatives = true;

    /** Property that defines the input value **/
    @Input() public value: number;

    /** Property that defines if the input has any error, when a true is provided the
     input has a diferent styling beahvior **/
    @Input() public hasError: boolean;

    /** Property that sets all the component as disabled **/
    @Input() public disabled: boolean;

    /** Property that sould be used for binding a callback to receive the current input value
     everytime it changes **/
    @Output() public onValueChanged: EventEmitter<any> = new EventEmitter();

    constructor() {}

    public ngOnInit() {
        if (isNaN(this.value)) {
            throw new TypeError('The value defined should be a number');
        }
    }

    public onKeyUp() {
        this.onValueChanged.emit(this.value);
    }

    public onPlusButtonPressed(): void {
        this.value += 1;

        this.onValueChanged.emit(this.value);
    }

    public onLessButtonPressed(): void {
        this.value -= 1;

        if (!this.allowNegatives && this.value < 0) {
            this.value = 0;
            return;
        }

        this.onValueChanged.emit(this.value);
    }
}
