import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UiCheckboxIndeterminateComponent } from './ui-checkbox-indeterminate.component';

@NgModule({
    imports: [CommonModule, FormsModule],
    declarations: [UiCheckboxIndeterminateComponent],
    exports: [UiCheckboxIndeterminateComponent]
})
export class UiCheckboxIndeterminateModule {}
