import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { UiCheckboxIndeterminateComponent } from './ui-checkbox-indeterminate.component';

describe('UiCheckboxIndeterminateComponent', () => {
    let fixture;
    let checkbockIndeterminateComponent;
    let compiledComponent;
    const CopySpy = jasmine.createSpy('copyToClipboard');
    const EventInputSpy = {
        target: {
            blur: jasmine.createSpy('blur')
        }
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [UiCheckboxIndeterminateComponent],
            imports: [FormsModule, CommonModule]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiCheckboxIndeterminateComponent);
        checkbockIndeterminateComponent = fixture.debugElement.componentInstance;
        compiledComponent = fixture.debugElement.nativeElement;
    });

    it('should create the component', () => {
        expect(checkbockIndeterminateComponent).toBeTruthy();
    });

    describe('#ngOnInit', () => {
        describe('when invoked', () => {
            describe('and the status is not defined', () => {
                it('should use the default value', () => {
                    checkbockIndeterminateComponent.ngOnInit();
                    expect(checkbockIndeterminateComponent.status).toBe('unchecked');
                });
            });

            describe('and the status is not valid', () => {
                it('should use the default value', () => {
                    checkbockIndeterminateComponent.status = 'this is not a valid status';
                    checkbockIndeterminateComponent.ngOnInit();
                    expect(checkbockIndeterminateComponent.status).toBe('unchecked');
                });
            });

            describe('and the status is defined', () => {
                it('should use that value', () => {
                    checkbockIndeterminateComponent.status = 'checked';
                    checkbockIndeterminateComponent.ngOnInit();
                    expect(checkbockIndeterminateComponent.status).toBe('checked');
                });
            });

            describe('and the disabled is not set as true', () => {
                it('should use keep it as false', () => {
                    checkbockIndeterminateComponent.ngOnInit();
                    expect(checkbockIndeterminateComponent.disabled).toBeFalsy();
                });
            });

            describe('and the disabled is set as true', () => {
                it('should use keep it as true', () => {
                    checkbockIndeterminateComponent.disabled = true;
                    checkbockIndeterminateComponent.ngOnInit();
                    expect(checkbockIndeterminateComponent.disabled).toBeTruthy();
                });
            });
        });
    });

    describe('#onCheckboxClick', () => {
        describe('when the checkbox is clicked', () => {
            it('should emit onChange event with the new status', () => {
                checkbockIndeterminateComponent.status = 'checked';

                checkbockIndeterminateComponent.onChange.subscribe((value) => {
                    expect(value).toEqual('unchecked');
                });

                checkbockIndeterminateComponent.onCheckboxClick();
            });

            it('should call the update status function', () => {
                checkbockIndeterminateComponent.getNextStatus = jasmine.createSpy('getNextStatus');
                checkbockIndeterminateComponent.onCheckboxClick();
                expect(checkbockIndeterminateComponent.getNextStatus).toHaveBeenCalledTimes(1);
            });
        });
    });

    describe('#getNextStatus', () => {
        describe('when invoked', () => {
            it('should update the status', () => {
                expect(checkbockIndeterminateComponent.getNextStatus('checked')).toEqual('unchecked');
                expect(checkbockIndeterminateComponent.getNextStatus('unchecked')).toEqual('indeterminate');
                expect(checkbockIndeterminateComponent.getNextStatus('indeterminate')).toEqual('checked');
            });
        });
    });
});
