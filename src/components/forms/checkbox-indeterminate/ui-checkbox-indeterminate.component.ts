import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
    selector: 'ui-checkbox-indeterminate',
    templateUrl: './ui-checkbox-indeterminate.component.html'
})
export class UiCheckboxIndeterminateComponent implements OnInit {
    @Input() public status?: string;

    @Input() public disabled?: boolean;

    @Output() public onChange: EventEmitter<any> = new EventEmitter();

    public statusValues = {
        unchecked: 'unchecked',
        indeterminate: 'indeterminate',
        checked: 'checked'
    };

    constructor() {}

    private getNextStatus(currentStatus) {
        switch (currentStatus) {
            case this.statusValues.unchecked:
                return this.statusValues.indeterminate;
            case this.statusValues.indeterminate:
                return this.statusValues.checked;
            case this.statusValues.checked:
            default:
                return this.statusValues.unchecked;
        }
    }

    public ngOnInit() {
        this.status = this.statusValues[this.status] || this.statusValues.unchecked;
    }

    public onCheckboxClick(): void {
        this.status = this.getNextStatus(this.status);

        this.onChange.emit(this.status);
    }
}
