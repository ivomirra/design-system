import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { CurrenciesListHelper } from 'huub-utils/lib';

@Component({
    selector: 'ui-currency-input',
    providers: [CurrenciesListHelper],
    templateUrl: './ui-currency-input.component.html'
})
export class UiCurrencyInputComponent implements OnInit {
    @Input() public currency?: string;
    @Input() public language?: string;

    @Input() public value?: string;

    @Input() public placeholder?: string;

    @Input() public disabled?: boolean;

    @Output() public onChange: EventEmitter<object> = new EventEmitter();

    public isDropdownOpened = false;
    public currencySymbol = '';
    public currenciesList = this.currenciesListHelper.getCurrenciesList();
    constructor(private currenciesListHelper: CurrenciesListHelper) {}

    public ngOnInit() {
        this.currencySymbol = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).symbol;
        this.disabled = this.disabled !== undefined ? this.disabled : false;
    }

    public onInputChange(): void {
        this.onChange.emit({ currency: this.currency, value: this.value });
    }

    public openCloseDropdown() {
        if (!this.disabled) {
            this.isDropdownOpened = !this.isDropdownOpened;
        }
    }

    public changeCurrency(isoCode) {
        this.currency = isoCode;
        this.onChange.emit({ currency: this.currency, value: this.value });
        this.currencySymbol = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).symbol;
        this.openCloseDropdown();
    }
}
