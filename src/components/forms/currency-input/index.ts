import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CurrenciesListHelperModule } from 'huub-utils/lib';
import { UiCurrencyInputComponent } from './ui-currency-input.component';

@NgModule({
    imports: [CommonModule, FormsModule, CurrenciesListHelperModule],
    declarations: [UiCurrencyInputComponent],
    exports: [UiCurrencyInputComponent]
})
export class UiCurrencyInputModule {}
