import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { UiCurrencyInputComponent } from './ui-currency-input.component';
import { CurrenciesListHelperModule } from 'huub-utils/lib';

describe('UiCurrencyInputComponent', () => {
    let fixture;
    let currencyComponent;
    let compiledComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [UiCurrencyInputComponent],
            imports: [FormsModule, CurrenciesListHelperModule]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiCurrencyInputComponent);
        currencyComponent = fixture.debugElement.componentInstance;
        compiledComponent = fixture.debugElement.nativeElement;
    });

    it('should create the component', () => {
        expect(currencyComponent).toBeTruthy();
    });

    describe('#ngOnInit', () => {
        describe('when invoked', () => {
            it('should convert an iso code to a char', () => {
                currencyComponent.currency = 'GBP';
                currencyComponent.ngOnInit();
                expect(currencyComponent.currencySymbol).toBe('£');
            });
        });

        describe('and disabled is not defined', () => {
            it('should use the default value', () => {
                currencyComponent.currency = 'GBP';
                currencyComponent.ngOnInit();
                expect(currencyComponent.disabled).toEqual(false);
            });
        });

        describe('and disabled is defined', () => {
            it('should use that value', () => {
                currencyComponent.currency = 'GBP';
                currencyComponent.disabled = true;
                currencyComponent.ngOnInit();
                expect(currencyComponent.disabled).toEqual(true);
            });
        });
    });

    describe('#onInputChange', () => {
        describe('when a value is defined', () => {
            it('should emit onChange event with defined value', () => {
                currencyComponent.value = '1';
                currencyComponent.currency = 'EUR';
                currencyComponent.onChange.subscribe((value) => {
                    expect(value.currency).toEqual('EUR');
                    expect(value.value).toEqual('1');
                });

                currencyComponent.onInputChange();
            });
        });
    });

    describe('#openCloseDropdown', () => {
        describe('when invoked', () => {
            it('should change the isDropdownOpened status', () => {
                currencyComponent.openCloseDropdown();
                expect(currencyComponent.isDropdownOpened).toBeTruthy();
                currencyComponent.openCloseDropdown();
                expect(currencyComponent.isDropdownOpened).toBeFalsy();
            });
            describe('and the input is disabled', () => {
                it('should not change the isDropdownOpened status', () => {
                    currencyComponent.disabled = true;
                    currencyComponent.isDropdownOpened = false;
                    currencyComponent.openCloseDropdown();
                    expect(currencyComponent.isDropdownOpened).toBeFalsy();
                });
            });
        });
    });

    describe('#changeCurrency', () => {
        describe('when invoked', () => {
            it('should change the currency', () => {
                currencyComponent.currency = 'EUR';
                currencyComponent.changeCurrency('GBP');
                expect(currencyComponent.currency).toBe('GBP');
            });

            it('should call openCloseDropdown', () => {
                const openCloseDropdownSpy = jasmine.createSpy('openCloseDropdown');
                currencyComponent.openCloseDropdown = openCloseDropdownSpy;
                currencyComponent.changeCurrency('GBP');
                expect(openCloseDropdownSpy).toHaveBeenCalled();
            });
        });
    });
});
