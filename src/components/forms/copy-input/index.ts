import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CopyToClipboardHelperModule } from 'huub-utils/lib';
import { UiCopyInputComponent } from './ui-copy-input.component';
import { UiCurrencyInputModule } from './../currency-input/index';
import { UiTooltipsModule } from './../../messaging/tooltips/index';

@NgModule({
    imports: [CommonModule, FormsModule, CopyToClipboardHelperModule, UiCurrencyInputModule, UiTooltipsModule],
    declarations: [UiCopyInputComponent],
    exports: [UiCopyInputComponent]
})
export class UiCopyInputModule {}
