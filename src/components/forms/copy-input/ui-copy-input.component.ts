import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { UI_COPY_INPUT_CONSTANT } from './ui-copy-input.constant';
import { CopyToClipboardHelper } from 'huub-utils/lib';

@Component({
    selector: 'ui-copy-input',
    providers: [CopyToClipboardHelper],
    templateUrl: './ui-copy-input.component.html'
})
export class UiCopyInputComponent implements OnInit {
    @Input() public value?: string;

    @Input() public placeholder?: string;

    @Input() public label?: any;

    @Input() public type?: string;

    @Input() public currency?: string;

    @Input() public viewModel?: any;

    @Input() public disabled?: boolean;

    @Input() public editable?: boolean;

    @Output() public onChange: EventEmitter<any> = new EventEmitter();

    @Output() public onSubmit: EventEmitter<any> = new EventEmitter();

    public isTooltipActive = false;
    public isCopied = false;
    public isEditing = false;
    public isTypeCurrency = false;
    public field = {
        left: '0',
        top: '0'
    };

    public tooltipViewModel = {
        text: 'Tooltip test example.',
        icon: 'info',
        color: 'blue-grey-dark'
    };

    private backupValue = '';
    private currencyValue: any;

    constructor(private copyToClipboardHelper: CopyToClipboardHelper) {}

    public ngOnInit() {
        this.viewModel = this.viewModel ? this.viewModel : {};
        this.type = this.type ? this.type : 'text';
        this.isTypeCurrency = this.type === 'currency';
        this.editable = this.editable !== undefined ? this.editable : true;
        this.disabled = this.disabled !== undefined ? this.disabled : false;
        this.viewModel.editText = this.viewModel.editText ? this.viewModel.editText : UI_COPY_INPUT_CONSTANT.EDIT_TEXT;
        this.label = this.label && typeof this.label === 'string' ? { value: this.label } : this.label;
        this.viewModel.tooltipTextBeforeCopy = this.viewModel.tooltipTextBeforeCopy
            ? this.viewModel.tooltipTextBeforeCopy
            : UI_COPY_INPUT_CONSTANT.TOOLTIP_TEXT_BEFORE_COPY;
        this.viewModel.tooltipTextAfterCopy = this.viewModel.tooltipTextAfterCopy
            ? this.viewModel.tooltipTextAfterCopy
            : UI_COPY_INPUT_CONSTANT.TOOLTIP_TEXT_AFTER_COPY;
        if (this.type === 'currency') {
            this.currencyValue = {
                currency: this.currency,
                value: this.value
            };
        }
    }

    public onInputChange(currencyValue?): void {
        this.currencyValue = currencyValue;
        const value = this.type === 'currency' ? this.currencyValue : this.value;
        this.onChange.emit(value);
    }

    public copy(event) {
        if (event && !this.isEditing) {
            event.target.blur();
            const value =
                this.type === 'currency' ? `${this.currencyValue.value} ${this.currencyValue.currency}` : this.value;
            this.copyToClipboardHelper.copyToClipboard(value);
            this.isCopied = true;
        }
    }

    public edit() {
        this.isEditing = true;
        this.backupValue = this.value;
    }

    public discard() {
        this.isEditing = false;
        this.value = this.backupValue;
        this.backupValue = '';
    }

    public save() {
        const value = this.type === 'currency' ? this.currencyValue : this.value;
        this.onSubmit.emit(value);
        this.isEditing = false;
        this.backupValue = '';
    }

    public showTooltip(event?) {
        if (event) {
            this.field.left = `${event.pageX}px`;
            this.field.top = `${event.pageY}px`;
            this.isTooltipActive = true;
        } else {
            this.isTooltipActive = false;
            this.isCopied = false;
        }
    }
}
