import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CopyToClipboardHelperModule } from 'huub-utils/lib';

import { UiCopyInputComponent } from './ui-copy-input.component';

import { UiCurrencyInputModule } from './../currency-input';

import { UiTooltipsModule } from './../../messaging/tooltips';

import { UI_COPY_INPUT_CONSTANT } from './ui-copy-input.constant';

describe('UiCopyInputComponent', () => {
    let fixture;
    let copyInputComponent;
    let compiledComponent;
    const CopySpy = jasmine.createSpy('copyToClipboard');
    const EventInputSpy = {
        target: {
            blur: jasmine.createSpy('blur')
        }
    };

    const EventMouseMock = {
        pageX: 15,
        pageY: 16
    };
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [UiCopyInputComponent],
            imports: [FormsModule, CommonModule, CopyToClipboardHelperModule, UiCurrencyInputModule, UiTooltipsModule]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiCopyInputComponent);
        copyInputComponent = fixture.debugElement.componentInstance;
        compiledComponent = fixture.debugElement.nativeElement;
    });

    it('should create the component', () => {
        expect(copyInputComponent).toBeTruthy();
    });

    describe('#ngOnInit', () => {
        describe('when invoked', () => {
            describe('and the viewModel is not defined', () => {
                it('should use the default values', () => {
                    copyInputComponent.ngOnInit();
                    expect(copyInputComponent.viewModel.editText).toEqual(UI_COPY_INPUT_CONSTANT.EDIT_TEXT);
                    expect(copyInputComponent.viewModel.tooltipTextBeforeCopy).toEqual(
                        UI_COPY_INPUT_CONSTANT.TOOLTIP_TEXT_BEFORE_COPY
                    );
                    expect(copyInputComponent.viewModel.tooltipTextAfterCopy).toEqual(
                        UI_COPY_INPUT_CONSTANT.TOOLTIP_TEXT_AFTER_COPY
                    );
                });
            });

            describe('and the viewModel is defined', () => {
                it('should use the viewModel values', () => {
                    const uiCopyViewModel = {
                        editText: 'Editar',
                        tooltipTextBeforeCopy: 'Clique para copiar o texto',
                        tooltipTextAfterCopy: 'Texto copiado com sucesso'
                    };

                    copyInputComponent.viewModel = uiCopyViewModel;
                    copyInputComponent.ngOnInit();
                    expect(copyInputComponent.viewModel.editText).toEqual(uiCopyViewModel.editText);
                    expect(copyInputComponent.viewModel.tooltipTextBeforeCopy).toEqual(
                        uiCopyViewModel.tooltipTextBeforeCopy
                    );
                    expect(copyInputComponent.viewModel.tooltipTextAfterCopy).toEqual(
                        uiCopyViewModel.tooltipTextAfterCopy
                    );
                });
            });

            describe('and the type is not defined', () => {
                it('should use the default value', () => {
                    copyInputComponent.ngOnInit();
                    expect(copyInputComponent.type).toEqual('text');
                });
            });

            describe('and the type is defined', () => {
                it('should use that value', () => {
                    copyInputComponent.type = 'currency';
                    copyInputComponent.ngOnInit();
                    expect(copyInputComponent.type).toEqual('currency');
                });
            });

            describe('and disabled is not defined', () => {
                it('should use the default value', () => {
                    copyInputComponent.ngOnInit();
                    expect(copyInputComponent.disabled).toEqual(false);
                });
            });

            describe('and disabled is defined', () => {
                it('should use that value', () => {
                    copyInputComponent.disabled = true;
                    copyInputComponent.ngOnInit();
                    expect(copyInputComponent.disabled).toEqual(true);
                });
            });

            describe('and editable is not defined', () => {
                it('should use the default value', () => {
                    copyInputComponent.ngOnInit();
                    expect(copyInputComponent.editable).toEqual(true);
                });
            });

            describe('and editable is defined', () => {
                it('should use that value', () => {
                    copyInputComponent.editable = false;
                    copyInputComponent.ngOnInit();
                    expect(copyInputComponent.editable).toEqual(false);
                });
            });

            describe('and label is a string', () => {
                it('should use that value', () => {
                    copyInputComponent.label = 'test';
                    copyInputComponent.ngOnInit();
                    expect(copyInputComponent.label).toEqual({ value: 'test' });
                });
            });

            describe('and label is an object', () => {
                it('should use that value', () => {
                    copyInputComponent.label = { value: 'label' };
                    copyInputComponent.ngOnInit();
                    expect(copyInputComponent.label).toEqual({ value: 'label' });
                });
            });
        });
    });

    describe('#onInputChange', () => {
        describe('when a value is defined', () => {
            it('should emit onChange event with defined value', () => {
                copyInputComponent.value = '1';

                copyInputComponent.onChange.subscribe((value) => {
                    expect(value).toEqual('1');
                });

                copyInputComponent.onInputChange();
            });
            describe('and the type is currency', () => {
                it('should emit onChange event with defined object', () => {
                    copyInputComponent.type = 'currency';
                    copyInputComponent.currency = 'eur';
                    copyInputComponent.value = '1';

                    copyInputComponent.onChange.subscribe((value) => {
                        expect(value).toEqual({ currency: 'usd', value: '2' });
                    });
                    copyInputComponent.onInputChange({ currency: 'usd', value: '2' });
                });
            });
        });
    });

    describe('#copy', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                CopySpy.calls.reset();
                EventInputSpy.target.blur.calls.reset();
                copyInputComponent.copyToClipboardHelper.copyToClipboard = CopySpy;
            });
            it('should call function to copy to clipboard', () => {
                copyInputComponent.value = 'test copy clipboard';
                copyInputComponent.copy(EventInputSpy);
                expect(CopySpy).toHaveBeenCalled();
            });
            it('should call target blur', () => {
                copyInputComponent.value = 'test copy clipboard';
                copyInputComponent.copy(EventInputSpy);
                expect(EventInputSpy.target.blur).toHaveBeenCalled();
            });
            it('should set the flag isCopied to true', () => {
                copyInputComponent.isCopied = false;
                copyInputComponent.copy(EventInputSpy);
                expect(copyInputComponent.isCopied).toBeTruthy();
            });
            describe('and the type is currency', () => {
                it('should call function to copy to clipboard', () => {
                    copyInputComponent.type = 'currency';
                    copyInputComponent.currency = 'eur';
                    copyInputComponent.value = '1';
                    copyInputComponent.ngOnInit();
                    copyInputComponent.copy(EventInputSpy);
                    expect(CopySpy).toHaveBeenCalled();
                });
            });
        });

        describe('when invoked without event', () => {
            beforeEach(() => {
                CopySpy.calls.reset();
                EventInputSpy.target.blur.calls.reset();
            });
            it('should not call the function to copy to clipboard', () => {
                copyInputComponent.value = 'test copy clipboard';
                copyInputComponent.copyToClipboardHelper.copyToClipboard = CopySpy;
                copyInputComponent.copy();
                expect(CopySpy.calls.count()).toBe(0);
            });
            it('should not call target blur', () => {
                copyInputComponent.value = 'test copy clipboard';
                copyInputComponent.copy();
                expect(EventInputSpy.target.blur.calls.count()).toBe(0);
            });
            it('should not set the flag isCopied to true', () => {
                copyInputComponent.isCopied = false;
                copyInputComponent.copy();
                expect(copyInputComponent.isCopied).toBeFalsy();
            });
        });
    });

    describe('#edit', () => {
        describe('when invoked', () => {
            it('should set the isEditing flag true', () => {
                copyInputComponent.isEditing = false;
                copyInputComponent.edit();
                expect(copyInputComponent.isEditing).toBeTruthy();
            });

            it('should set the backupValue equal to the input value', () => {
                copyInputComponent.value = 'test copy clipboard';
                copyInputComponent.backupValue = '';
                copyInputComponent.edit();

                expect(copyInputComponent.backupValue).toEqual('test copy clipboard');
            });
        });
    });

    describe('#discard', () => {
        describe('when invoked', () => {
            it('should set the isEditing flag false', () => {
                copyInputComponent.isEditing = true;
                copyInputComponent.discard();
                expect(copyInputComponent.isEditing).toBeFalsy();
            });

            it('should set the value equal to the backup value', () => {
                copyInputComponent.value = '';
                copyInputComponent.backupValue = 'test copy clipboard';
                copyInputComponent.discard();
                expect(copyInputComponent.value).toEqual('test copy clipboard');
            });

            it('should reset the backup to an empty string', () => {
                copyInputComponent.backupValue = 'test copy clipboard';
                copyInputComponent.discard();
                expect(copyInputComponent.backupValue).toEqual('');
            });
        });
    });

    describe('#save', () => {
        describe('when invoked', () => {
            it('should set the isEditing flag false', () => {
                copyInputComponent.isEditing = true;
                copyInputComponent.save();
                expect(copyInputComponent.isEditing).toBeFalsy();
            });
            it('should reset the backup to an empty string', () => {
                copyInputComponent.backupValue = 'test copy clipboard';
                copyInputComponent.save();
                expect(copyInputComponent.backupValue).toEqual('');
            });
            it('should emit onSubmit event with defined value', () => {
                copyInputComponent.value = '1';

                copyInputComponent.onSubmit.subscribe((value) => {
                    expect(value).toEqual('1');
                });

                copyInputComponent.save();
            });

            describe('and the type is currency', () => {
                it('should emit onSubmit event with defined value', () => {
                    copyInputComponent.type = 'currency';
                    copyInputComponent.currency = 'eur';
                    copyInputComponent.value = '1';
                    copyInputComponent.ngOnInit();
                    copyInputComponent.onSubmit.subscribe((value) => {
                        expect(value).toEqual({ currency: 'eur', value: '1' });
                    });

                    copyInputComponent.save();
                });
            });
        });
    });

    describe('#showTooltip', () => {
        describe('when invoked', () => {
            describe('with the event argument', () => {
                it('should set the isTooltipActive flag true', () => {
                    copyInputComponent.isTooltipActive = false;
                    copyInputComponent.showTooltip(EventMouseMock);
                    expect(copyInputComponent.isTooltipActive).toBeTruthy();
                });
                it('should udpate the field.left and field.top values', () => {
                    copyInputComponent.showTooltip(EventMouseMock);
                    expect(copyInputComponent.field.top).toEqual(`${EventMouseMock.pageY}px`);
                    expect(copyInputComponent.field.left).toEqual(`${EventMouseMock.pageX}px`);
                });
            });
            describe('with the event without argument', () => {
                it('should set the isTooltipActive flag false', () => {
                    copyInputComponent.isTooltipActive = true;
                    copyInputComponent.showTooltip();
                    expect(copyInputComponent.isTooltipActive).toBeFalsy();
                });
                it('should set the isCopied flag to false', () => {
                    copyInputComponent.isCopied = true;
                    copyInputComponent.showTooltip();
                    expect(copyInputComponent.isCopied).toBeFalsy();
                });
            });
        });
    });
});
