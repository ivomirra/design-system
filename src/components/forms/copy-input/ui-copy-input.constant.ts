export const UI_COPY_INPUT_CONSTANT = {
    EDIT_TEXT: 'Edit',
    TOOLTIP_TEXT_BEFORE_COPY: 'Click to copy',
    TOOLTIP_TEXT_AFTER_COPY: 'Copied to clipboard!'
};
