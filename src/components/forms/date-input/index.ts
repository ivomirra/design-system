import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialHelpersModule } from '../../helpers/index';

import { UiDateInputComponent } from './ui-date-input.component';

@NgModule({
    imports: [CommonModule, FormsModule, MaterialHelpersModule],
    declarations: [UiDateInputComponent],
    exports: [UiDateInputComponent]
})
export class UiDateInputModule {}
