import { Component, Input, OnInit, Output, EventEmitter, ElementRef, HostListener } from '@angular/core';
import { MaterialHelper } from '../../helpers/material.helpers';

import * as moment_ from 'moment-mini-ts';

const moment = moment_;

const DEFAULT_CALENDAR_WIDTH = 327;
const COMPONENT_CONFIG = {
    WEEKEND_ISO_CODES: [7, 6]
};

@Component({
    selector: 'ui-date-input',
    templateUrl: './ui-date-input.component.html'
})
export class UiDateInputComponent implements OnInit {
    @Input() public disable: boolean;
    @Input() public placeholder = '';
    @Input() public monthNames: any;
    @Input() public weekDays: any;
    @Input() public bottomLabel?: string;
    @Input() public disablePastDates = false;
    @Input() public disableWeekendDays = false;
    @Input() private dateFormat = '';
    @Input() private value: string;
    @Input() private datesWithBullets = [];
    @Input() private datesDisabled = [];
    @Input() public isInvalidDate = false;

    @Output() public onDateSelected: EventEmitter<string> = new EventEmitter();

    private monthIndexPosition: number;
    public year: number;
    public month: number;
    public daysOfMonth: any = [];
    public selectedDate = '';
    public calendarIsVisible = false;

    public alignRight = false;
    public alignLeft = false;

    constructor(private helper: MaterialHelper, private element: ElementRef) {}

    private hideCalendar() {
        this.calendarIsVisible = false;
    }

    @HostListener('document:click', ['$event'])
    public onDomClicked(event) {
        if (!this.element.nativeElement.contains(event.target) && this.calendarIsVisible) {
            this.hideCalendar();
        }
    }

    private formatDate(clickedDay) {
        const selectedDate = new Date(this.year, this.monthIndexPosition, clickedDay);
        return moment(selectedDate).format(this.dateFormat);
    }

    private emitChanges() {
        this.onDateSelected.emit(this.selectedDate);
    }

    private getDateWithBulletsIndexOfByDayNumber(dayNumber) {
        const dateToBeCompared = this.formatDate(dayNumber);

        return this.datesWithBullets.findIndex((dateConfig) => {
            return dateToBeCompared === moment(dateConfig.date).format(this.dateFormat);
        });
    }

    private buildCalendarDays() {
        // because date = 0, you should pass the month instead month index
        const numberOfDays = new Date(this.year, this.month, 0).getDate();

        const weekDayStartingMonth = new Date(this.year + '-' + this.month + '-01').getDay();

        let daysToShow = [];

        this.daysOfMonth = [];

        for (let w = 1; w <= weekDayStartingMonth; w++) {
            daysToShow.push('');
        }

        for (let i = 1; i <= numberOfDays; i++) {
            if (daysToShow.length < 7) {
                daysToShow.push(i);
            } else {
                this.daysOfMonth.push(daysToShow);
                daysToShow = [i];
            }

            if (i === numberOfDays) {
                this.daysOfMonth.push(daysToShow);
            }
        }
    }

    private checkIfDateIsValid() {
        this.isInvalidDate = false;

        if (!moment(this.selectedDate, this.dateFormat, true).isValid()) {
            this.isInvalidDate = true;
            return false;
        }

        return true;
    }

    private calculateCurrentDate() {
        let currentDate: Date | any = new Date();

        if (this.selectedDate !== '') {
            currentDate = moment(this.selectedDate, this.dateFormat, true);
        }

        this.year = currentDate.getFullYear ? currentDate.getFullYear() : currentDate.year();
        this.month = currentDate.getMonth ? currentDate.getMonth() + 1 : currentDate.month() + 1;
        this.monthIndexPosition = currentDate.getMonth ? currentDate.getMonth() : currentDate.month();
    }

    public ngOnInit() {
        this.dateFormat = this.dateFormat.toUpperCase();
        this.selectedDate = this.value ? moment(this.value).format(this.dateFormat) : '';

        this.calculateCurrentDate();
        this.buildCalendarDays();
    }

    public subtractMonth() {
        this.month = this.month - 1;
        this.monthIndexPosition = this.monthIndexPosition - 1;

        if (this.month <= 0) {
            this.year = this.year - 1;
            this.month = 12;
            this.monthIndexPosition = 11;
        }

        this.buildCalendarDays();
    }

    public addMonth() {
        this.month = this.month + 1;
        this.monthIndexPosition = this.monthIndexPosition + 1;

        if (this.month >= 13) {
            this.year = this.year + 1;
            this.month = 1;
            this.monthIndexPosition = 0;
        }

        this.buildCalendarDays();
    }

    public onInputChanged() {
        if (!this.checkIfDateIsValid()) {
            return;
        }

        this.calculateCurrentDate();
        this.buildCalendarDays();
        this.emitChanges();
    }

    public onSelectedDate(clickedDay) {
        this.selectedDate = this.formatDate(clickedDay);

        this.checkIfDateIsValid();
        this.calendarToggleVisibility();
        this.emitChanges();
    }

    public isSelectedDate(dayNumber) {
        if (this.selectedDate === '') {
            return false;
        }

        return this.selectedDate === this.formatDate(dayNumber);
    }

    public dateHasBullet(dayNumber): boolean {
        return this.getDateWithBulletsIndexOfByDayNumber(dayNumber) > -1;
    }

    public getBulletTooltip(dayNumber): string {
        const indexFound = this.getDateWithBulletsIndexOfByDayNumber(dayNumber);

        return indexFound > -1 ? this.datesWithBullets[indexFound].tooltip : '';
    }

    public isDateDisabled(dayNumber): boolean {
        const dateToBeCompared = this.formatDate(dayNumber);
        const momentDateToBeCompared = new Date(this.year, this.monthIndexPosition, dayNumber);

        if (
            this.disableWeekendDays &&
            COMPONENT_CONFIG.WEEKEND_ISO_CODES.indexOf(moment(momentDateToBeCompared).isoWeekday()) > -1
        ) {
            return true;
        }

        if (this.disablePastDates && moment(momentDateToBeCompared).isBefore(new Date())) {
            return true;
        }

        return (
            this.datesDisabled.findIndex((date) => {
                return dateToBeCompared === moment(date).format(this.dateFormat);
            }) > -1
        );
    }

    public calendarToggleVisibility() {
        const calendarWith =
            this.element.nativeElement.firstElementChild.clientWidth < DEFAULT_CALENDAR_WIDTH
                ? DEFAULT_CALENDAR_WIDTH
                : this.element.nativeElement.firstElementChild.clientWidth;

        const absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(
            this.element.nativeElement.getBoundingClientRect().left,
            calendarWith
        );

        this.calculateCurrentDate();
        this.buildCalendarDays();

        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;

        this.calendarIsVisible = !this.calendarIsVisible;
    }
}
