import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { UiDateInputComponent } from './ui-date-input.component';
import { MaterialHelper } from '../../helpers/material.helpers';

describe('UiDateInputComponent', () => {
    const MaterialHelperSpy = jasmine.createSpyObj('MaterialHelperSpy', ['getAbsolutePositionRenderConfig', 'merge']);
    const currentDate = new Date('2018-01-01');

    let fixture;
    let testedComponent;
    let compiledComponent;

    beforeEach(() => {
        MaterialHelperSpy.getAbsolutePositionRenderConfig.and.returnValue({
            canRenderToLeft: true,
            canRenderToRight: true
        });

        MaterialHelperSpy.merge.and.callFake((value) => value);

        TestBed.configureTestingModule({
            declarations: [UiDateInputComponent],
            providers: [
                MaterialHelper,
                {
                    provide: MaterialHelper,
                    useValue: MaterialHelperSpy
                }
            ],
            imports: [FormsModule]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiDateInputComponent);
        testedComponent = fixture.debugElement.componentInstance;
        compiledComponent = fixture.debugElement.nativeElement;

        spyOn(testedComponent, 'hideCalendar').and.callThrough();

        spyOn(testedComponent.element.nativeElement, 'contains');

        spyOn(testedComponent.onDateSelected, 'emit');
    });

    beforeEach(() => {
        jasmine.clock().mockDate(currentDate);
    });

    afterEach(() => {
        MaterialHelperSpy.getAbsolutePositionRenderConfig.calls.reset();
    });

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    describe('#onDomClicked', () => {
        describe('when invoked', () => {
            describe("and the native element doens't contain the event target", () => {
                it("shouldn't call hideCalendar", () => {
                    testedComponent.element.nativeElement.contains.and.returnValue(false);

                    testedComponent.onDomClicked({
                        target: true
                    });

                    expect(testedComponent.hideCalendar).not.toHaveBeenCalled();
                });

                describe('and calendarIsVisible is set as true', () => {
                    it('should call hideCalendar', () => {
                        testedComponent.calendarIsVisible = true;
                        testedComponent.element.nativeElement.contains.and.returnValue(false);

                        testedComponent.onDomClicked({
                            target: true
                        });

                        expect(testedComponent.hideCalendar).toHaveBeenCalled();
                    });
                });
            });

            describe('and the native element does contain the event target', () => {
                it("shouldn't call hideCalendar", () => {
                    testedComponent.element.nativeElement.contains.and.returnValue(true);

                    testedComponent.onDomClicked({
                        target: true
                    });

                    expect(testedComponent.hideCalendar).not.toHaveBeenCalled();
                });
            });
        });
    });

    describe('when a compiling the component', () => {
        const dayOfMonthDataStructureMock = [
            ['', 1, 2, 3, 4, 5, 6],
            [7, 8, 9, 10, 11, 12, 13],
            [14, 15, 16, 17, 18, 19, 20],
            [21, 22, 23, 24, 25, 26, 27],
            [28, 29, 30, 31]
        ];

        beforeEach(() => {
            testedComponent.ngOnInit();
            fixture.detectChanges();
        });

        it('should set current year', () => {
            expect(testedComponent.year).toEqual(currentDate.getFullYear());
        });

        it('should set current month', () => {
            expect(testedComponent.month).toEqual(currentDate.getMonth() + 1);
        });

        it('should build calendar data structure', () => {
            expect(testedComponent.daysOfMonth).toEqual(dayOfMonthDataStructureMock);
        });

        describe('and a value is provided', () => {
            beforeEach(() => {
                testedComponent.value = currentDate;
                testedComponent.dateFormat = 'dd-mm-yyyy';
                testedComponent.ngOnInit();
            });

            it('should set selectet date with the provided date format', () => {
                expect(testedComponent.selectedDate).toEqual('01-01-2018');
            });
        });
    });

    describe('#subtractMonth', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                const currentDateLocal = new Date('2018-03-01');

                jasmine.clock().mockDate(currentDateLocal);
                testedComponent.ngOnInit();
                fixture.detectChanges();
            });

            it('should decrease one month to the current month date', () => {
                expect(testedComponent.month).toEqual(3);

                testedComponent.subtractMonth();

                expect(testedComponent.month).toEqual(2);
            });

            describe('and when the current month is january', () => {
                it('should move to december', () => {
                    expect(testedComponent.month).toEqual(3);

                    testedComponent.subtractMonth();
                    testedComponent.subtractMonth();
                    testedComponent.subtractMonth();

                    expect(testedComponent.month).toEqual(12);
                });

                it('should decrease one year', () => {
                    expect(testedComponent.year).toEqual(2018);

                    testedComponent.subtractMonth();
                    testedComponent.subtractMonth();
                    testedComponent.subtractMonth();

                    expect(testedComponent.year).toEqual(2017);
                });
            });
        });
    });

    describe('#addMonth', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                const currentDateLocal = new Date('2018-11-01');

                jasmine.clock().mockDate(currentDateLocal);
                testedComponent.ngOnInit();
                fixture.detectChanges();
            });

            it('should increase one month to the current month date', () => {
                expect(testedComponent.month).toEqual(11);

                testedComponent.addMonth();

                expect(testedComponent.month).toEqual(12);
            });

            describe('and when the current month is december', () => {
                it('should move to january', () => {
                    expect(testedComponent.month).toEqual(11);

                    testedComponent.addMonth();
                    testedComponent.addMonth();

                    expect(testedComponent.month).toEqual(1);
                });

                it('should increase one year', () => {
                    expect(testedComponent.year).toEqual(2018);

                    testedComponent.addMonth();
                    testedComponent.addMonth();

                    expect(testedComponent.year).toEqual(2019);
                });
            });
        });
    });

    describe('#onInputChanged', () => {
        describe('when invoked', () => {
            describe('and the selected date is valid', () => {
                beforeEach(() => {
                    testedComponent.selectedDate = '2018-01-01';
                    testedComponent.dateFormat = 'YYYY-MM-DD';
                    testedComponent.onInputChanged();
                    fixture.detectChanges();
                });

                it('should emit the selected date', () => {
                    expect(testedComponent.onDateSelected.emit).toHaveBeenCalledWith('2018-01-01');
                });

                it(`should mantain isInvalidDate false state`, () => {
                    expect(testedComponent.isInvalidDate).toEqual(false);
                });
            });

            describe('and the selected date is invalid', () => {
                beforeEach(() => {
                    testedComponent.selectedDate = '2018-01-';
                    testedComponent.dateFormat = 'YYYY-MM-DD';
                    testedComponent.onInputChanged();
                    fixture.detectChanges();
                });

                it(`shouldn't emit the selected date`, () => {
                    expect(testedComponent.onDateSelected.emit).not.toHaveBeenCalled();
                });

                it(`should set isInvalidDate as true`, () => {
                    expect(testedComponent.isInvalidDate).toEqual(true);
                });
            });
        });
    });

    describe('#onSelectedDate', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                testedComponent.dateFormat = 'YYYY-MM-DD';
                testedComponent.calendarIsVisible = true;
                testedComponent.isInvalidDate = true;
                testedComponent.ngOnInit();
            });

            it('should set the new date according the day provided', () => {
                testedComponent.onSelectedDate('3');
                expect(testedComponent.selectedDate).toEqual('2018-01-03');
            });

            it('should set the isInvalidDate flag to the correct state', () => {
                testedComponent.onSelectedDate('3');
                expect(testedComponent.isInvalidDate).toEqual(false);
            });

            it('should toggle calendar visibility', () => {
                testedComponent.onSelectedDate('3');
                expect(testedComponent.calendarIsVisible).toEqual(false);
            });

            it('should emit the new selected date', () => {
                testedComponent.onSelectedDate('3');
                expect(testedComponent.onDateSelected.emit).toHaveBeenCalledWith('2018-01-03');
            });
        });
    });

    describe('#isSelectedDate', () => {
        describe('when invoked', () => {
            describe('and the selected date is empty', () => {
                it('should return false', () => {
                    const result = testedComponent.isSelectedDate();

                    expect(result).toEqual(false);
                });
            });

            describe('and the selected date has values', () => {
                beforeEach(() => {
                    testedComponent.value = currentDate;
                    testedComponent.dateFormat = 'yyyy-mm-dd';
                    testedComponent.ngOnInit();
                });

                describe('and the selected date is equal to the day provided', () => {
                    it('should return true', () => {
                        const result = testedComponent.isSelectedDate('1');

                        expect(result).toEqual(true);
                    });
                });

                describe('and the selected date is different to the day provided', () => {
                    it('should return false', () => {
                        const result = testedComponent.isSelectedDate('2');

                        expect(result).toEqual(false);
                    });
                });
            });
        });
    });

    describe('#dateHasBullet', () => {
        beforeEach(() => {
            testedComponent.year = 2018;
            testedComponent.monthIndexPosition = 11;

            testedComponent.datesWithBullets = [
                {
                    date: new Date(2018, 11, 2)
                }
            ];
        });

        describe('when invoked with a date present in datesWithBullets', () => {
            it('should return true', () => {
                const result = testedComponent.dateHasBullet('2');

                expect(result).toEqual(true);
            });
        });

        describe('when invoked with a date not present in datesWithBullets', () => {
            it('should return false', () => {
                const result = testedComponent.dateHasBullet('3');

                expect(result).toEqual(false);
            });
        });
    });

    describe('#getBulletTooltip', () => {
        beforeEach(() => {
            testedComponent.year = 2018;
            testedComponent.monthIndexPosition = 11;

            testedComponent.datesWithBullets = [
                {
                    date: new Date(2018, 11, 2),
                    tooltip: 'Test'
                }
            ];
        });

        describe('when invoked with a date present in datesWithBullets', () => {
            it('should return the tooltip text', () => {
                const result = testedComponent.getBulletTooltip('2');

                expect(result).toEqual('Test');
            });
        });

        describe('when invoked with a date not present in datesWithBullets', () => {
            it('should return empty string', () => {
                const result = testedComponent.getBulletTooltip('3');

                expect(result).toEqual('');
            });
        });
    });

    describe('#isDateDisabled', () => {
        beforeEach(() => {
            const currentDateLocal = new Date('2018-12-04');

            jasmine.clock().mockDate(currentDateLocal);

            testedComponent.year = 2018;
            testedComponent.monthIndexPosition = 11;
            testedComponent.disablePastDates = false;
            testedComponent.disableWeekendDays = false;
            testedComponent.disablePastDates = false;
            testedComponent.datesDisabled = [new Date(2018, 11, 15)];
        });

        describe('when disableWeekendDays is set as true', () => {
            beforeEach(() => {
                testedComponent.disableWeekendDays = true;
                testedComponent.disablePastDates = false;
            });

            describe('and the selected day is a weekend day', () => {
                it('should return true', () => {
                    const result = testedComponent.isDateDisabled('2');

                    expect(result).toEqual(true);
                });
            });

            describe('and the selected day is a weekend day', () => {
                it('should return false', () => {
                    const result = testedComponent.isDateDisabled('3');

                    expect(result).toEqual(false);
                });
            });
        });

        describe('when disablePastDates is set as true', () => {
            beforeEach(() => {
                testedComponent.disableWeekendDays = false;
                testedComponent.disablePastDates = true;
            });

            describe('and the selected date is previous than the current one', () => {
                it('should return true', () => {
                    const result = testedComponent.isDateDisabled('3');

                    expect(result).toEqual(true);
                });
            });

            describe('and the selected date is after than the current one', () => {
                describe('and the selected date that is present in the datesDisabled list', () => {
                    it('should return true', () => {
                        const result = testedComponent.isDateDisabled('15');

                        expect(result).toEqual(true);
                    });
                });

                describe('and the selected date that is not present in the datesDisabled list', () => {
                    it('should return true', () => {
                        const result = testedComponent.isDateDisabled('16');

                        expect(result).toEqual(false);
                    });
                });
            });
        });
    });

    describe('#hideCalendar', () => {
        describe('when invoked', () => {
            it('should set calendarIsVisible as false', () => {
                testedComponent.calendarIsVisible = true;

                testedComponent.hideCalendar();

                expect(testedComponent.calendarIsVisible).toEqual(false);
            });
        });
    });

    describe('#calendarToggleVisibility', () => {
        describe('when invoked', () => {
            it('should set calendarIsVisible as the oposite that was defined', () => {
                testedComponent.calendarIsVisible = true;

                testedComponent.calendarToggleVisibility();

                expect(testedComponent.calendarIsVisible).toEqual(false);

                testedComponent.calendarToggleVisibility();

                expect(testedComponent.calendarIsVisible).toEqual(true);
            });

            it('should set alignLeft according with getAbsolutePositionRenderConfig result', () => {
                testedComponent.alignLeft = false;

                testedComponent.calendarToggleVisibility();

                expect(testedComponent.alignLeft).toEqual(true);
            });

            it('should set alignRight according with getAbsolutePositionRenderConfig result', () => {
                testedComponent.alignRight = false;

                testedComponent.calendarToggleVisibility();

                expect(testedComponent.alignRight).toEqual(true);
            });

            describe('when clientWidth is less than DEFAULT_CALENDAR_WIDTH', () => {
                it('should call getAbsolutePositionRenderConfig with DEFAULT_CALENDAR_WIDTH', () => {
                    testedComponent.element.nativeElement.firstElementChild.style.width = '200px';
                    testedComponent.calendarToggleVisibility();

                    expect(MaterialHelperSpy.getAbsolutePositionRenderConfig).toHaveBeenCalledWith(0, 327);
                });
            });

            describe('when clientWidth is higher than DEFAULT_CALENDAR_WIDTH', () => {
                it('should call getAbsolutePositionRenderConfig with clientWidth', () => {
                    testedComponent.element.nativeElement.firstElementChild.style.width = '400px';
                    testedComponent.calendarToggleVisibility();

                    expect(MaterialHelperSpy.getAbsolutePositionRenderConfig).toHaveBeenCalledWith(0, 400);
                });
            });

            it('should calculate the current date', () => {
                testedComponent.year = undefined;
                testedComponent.month = undefined;
                testedComponent.monthIndexPosition = undefined;

                testedComponent.selectedDate = '2018-01-01';
                testedComponent.dateFormat = 'YYYY-MM-DD';

                testedComponent.calendarToggleVisibility();

                expect(testedComponent.year).toEqual(2018);
                expect(testedComponent.month).toEqual(1);
                expect(testedComponent.monthIndexPosition).toEqual(0);
            });

            it('should build calendar days', () => {
                testedComponent.selectedDate = '2018-01-01';
                testedComponent.dateFormat = 'YYYY-MM-DD';
                testedComponent.daysOfMonth = [];

                testedComponent.calendarToggleVisibility();

                expect(testedComponent.daysOfMonth).toEqual([
                    ['', 1, 2, 3, 4, 5, 6],
                    [7, 8, 9, 10, 11, 12, 13],
                    [14, 15, 16, 17, 18, 19, 20],
                    [21, 22, 23, 24, 25, 26, 27],
                    [28, 29, 30, 31]
                ]);
            });
        });
    });
});
