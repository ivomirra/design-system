import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CurrenciesListHelper } from 'huub-utils/lib';
import { UI_CURRENCIES_DROPDOWN_CONSTANTS } from './ui-currencies-dropdown.constant';

@Component({
    selector: 'ui-currencies-dropdown',
    templateUrl: './ui-currencies-dropdown.component.html'
})
export class UiCurrenciesDropdownComponent implements OnInit {
    @Input() public currency?: string;

    @Input() public language?: string;

    @Input() public value?: string;

    @Input() public placeholder?: string;

    @Input() public disabled?: boolean;

    @Output() public onChange: EventEmitter<object> = new EventEmitter();

    public inputSearchViewModel = {
        placeholder: UI_CURRENCIES_DROPDOWN_CONSTANTS.SEARCH_CURRENCY,
        value: ''
    };

    public isDropdownOpened = false;
    public currencySymbol = '';

    public popularCurrencyTitle = UI_CURRENCIES_DROPDOWN_CONSTANTS.POPULAR_CURRENCIES;
    public allCurrencyTitle = UI_CURRENCIES_DROPDOWN_CONSTANTS.ALL_CURRENCIES;

    public currenciesList = this.currenciesListHelper.getCurrenciesList();
    public popularCurrenciesList = this.currenciesListHelper.getPopularCurrenciesList();

    constructor(private currenciesListHelper: CurrenciesListHelper) {}

    public ngOnInit() {
        this.currencySymbol = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).symbol;
        this.disabled = this.disabled !== undefined ? this.disabled : false;
    }

    public onInputChange(): void {
        this.onChange.emit({ currency: this.currency, value: this.value });
    }

    public openCloseDropdown() {
        if (!this.disabled) {
            this.isDropdownOpened = !this.isDropdownOpened;
        }
    }

    public changeCurrency(isoCode: string) {
        this.currency = isoCode;
        this.currencySymbol = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).symbol;
        this.value = this.currenciesListHelper.getCurrencyByIso3Code(this.currency).name;
        this.onChange.emit({ currency: this.currency, value: this.value });
        this.openCloseDropdown();
        this.onClearSearchInput();
    }

    public onClearSearchInput() {
        this.inputSearchViewModel.value = '';
    }
}
