import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UiCurrenciesDropdownComponent } from './ui-currencies-dropdown.component';
import { CurrenciesListHelperModule } from 'huub-utils/lib';
import { UiInputSearchModule } from '../../forms/search/index';
import { SearchPipeModule } from '../../pipes/index';

@NgModule({
    imports: [CommonModule, FormsModule, CurrenciesListHelperModule, UiInputSearchModule, SearchPipeModule],
    declarations: [UiCurrenciesDropdownComponent],
    exports: [UiCurrenciesDropdownComponent]
})
export class UiCurrenciesDropdownModule {}
