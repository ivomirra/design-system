export const UI_CURRENCIES_DROPDOWN_CONSTANTS = {
    POPULAR_CURRENCIES: 'Popular currencies',
    ALL_CURRENCIES: 'All currencies',
    SEARCH_CURRENCY: 'Type a currency'
};
