export const UI_WEIGHT_UNITS_DROPDOWN_CONSTANTS = {
    POPULAR_WEIGHT_UNITS: 'Popular weight units',
    ALL_WEIGHT_UNITS: 'All weight units',
    SEARCH_WEIGHT_UNIT: 'Search for weight unit'
};
