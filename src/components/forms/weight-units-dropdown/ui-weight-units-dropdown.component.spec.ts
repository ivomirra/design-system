import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { UiWeightUnitsDropdownComponent } from './ui-weight-units-dropdown.component';
import { WeightUnitsListHelper } from 'huub-utils/lib';
import { UiInputSearchComponent } from '../search/ui-input-search.component';
import { SearchPipe } from '../../pipes/search-pipe.pipe';

describe('UiCurrencyInputComponent', () => {
    let fixture;
    let weightUnitsComponent;
    let compiledComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [UiWeightUnitsDropdownComponent, UiInputSearchComponent, SearchPipe],
            imports: [FormsModule],
            providers: [{ provide: WeightUnitsListHelper, useClass: WeightUnitsListHelper }]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiWeightUnitsDropdownComponent);
        weightUnitsComponent = fixture.debugElement.componentInstance;
        compiledComponent = fixture.debugElement.nativeElement;
    });

    it('should create the component', () => {
        expect(weightUnitsComponent).toBeTruthy();
    });

    describe('#ngOnInit', () => {
        it('should set default weight unit by symbol', () => {
            weightUnitsComponent.weightUnitSymbol = 'kg';
            weightUnitsComponent.ngOnInit();
            expect(weightUnitsComponent.weightUnitName).toBe('kilogram');
        });
    });

    describe('#onInputChange', () => {
        describe('when a value is defined', () => {
            it('should emit onChange event with defined value', () => {
                const expectedUnit = 'kg';
                weightUnitsComponent.weightUnitSymbol = expectedUnit;

                weightUnitsComponent.onChange.subscribe((weightUnit) => {
                    expect(weightUnit.weightUnitSymbol).toEqual(expectedUnit);
                });

                weightUnitsComponent.onInputChange();
            });
        });
    });

    describe('#openCloseDropdown', () => {
        describe('when invoked', () => {
            it('should change the isDropdownOpened status', () => {
                weightUnitsComponent.openCloseDropdown();
                expect(weightUnitsComponent.isDropdownOpened).toBeTruthy();
                weightUnitsComponent.openCloseDropdown();
                expect(weightUnitsComponent.isDropdownOpened).toBeFalsy();
            });
        });
    });

    describe('#changeWeight', () => {
        describe('when invoked', () => {
            it('should change the currency', () => {
                const expectedUnit = 'kg';
                weightUnitsComponent.weightUnitSymbol = expectedUnit;
                weightUnitsComponent.changeWeight(expectedUnit);
                expect(weightUnitsComponent.weightUnitSymbol).toBe(expectedUnit);
            });

            it('should call openCloseDropdown', () => {
                const expectedUnit = 'kg';
                const openCloseDropdownSpy = jasmine.createSpy('openCloseDropdown');
                weightUnitsComponent.openCloseDropdown = openCloseDropdownSpy;
                weightUnitsComponent.changeWeight(expectedUnit);
                expect(openCloseDropdownSpy).toHaveBeenCalled();
            });
        });
    });

    describe('#onClearSearchInput', () => {
        describe('when invoked', () => {
            it('should change the inputSearchViewModel value', () => {
                weightUnitsComponent.inputSearchViewModel.value = '1';
                weightUnitsComponent.onClearSearchInput();
                expect(weightUnitsComponent.inputSearchViewModel.value).toBe('');
            });
        });
    });
});
