import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UiWeightUnitsDropdownComponent } from './ui-weight-units-dropdown.component';
import { WeightUnitsListHelperModule } from 'huub-utils/lib';
import { UiInputSearchModule } from '../../forms/search/index';
import { SearchPipeModule } from '../../pipes/index';

@NgModule({
    imports: [CommonModule, FormsModule, WeightUnitsListHelperModule, UiInputSearchModule, SearchPipeModule],
    declarations: [UiWeightUnitsDropdownComponent],
    exports: [UiWeightUnitsDropdownComponent]
})
export class UiWeightUnitsDropdownModule {}
