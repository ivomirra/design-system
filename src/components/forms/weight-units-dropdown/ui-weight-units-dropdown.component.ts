import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { WeightUnitsListHelper } from 'huub-utils/lib';
import { UI_WEIGHT_UNITS_DROPDOWN_CONSTANTS } from './ui-weight-units-dropdown.constant';

@Component({
    selector: 'ui-weight-units-dropdown',
    templateUrl: './ui-weight-units-dropdown.component.html'
})
export class UiWeightUnitsDropdownComponent implements OnInit {
    @Input() public weightUnitSymbol?: string;

    public weightUnitName?: string;

    public inputSearchViewModel = {
        placeholder: UI_WEIGHT_UNITS_DROPDOWN_CONSTANTS.SEARCH_WEIGHT_UNIT,
        value: ''
    };

    @Output() public onChange: EventEmitter<object> = new EventEmitter();

    public isDropdownOpened = false;
    public weightUnitsList = this.weightUnitsHelper.getWeightUnitListOrderByName();
    public popularWeightUnitsList = this.weightUnitsHelper.getPopularWeightUnitsListOrderByName();

    public popularWeightUnitsTitle = UI_WEIGHT_UNITS_DROPDOWN_CONSTANTS.POPULAR_WEIGHT_UNITS;
    public allWeightUnitsTitle = UI_WEIGHT_UNITS_DROPDOWN_CONSTANTS.ALL_WEIGHT_UNITS;

    constructor(private weightUnitsHelper: WeightUnitsListHelper) {}

    public ngOnInit() {
        this.weightUnitName = this.weightUnitsHelper.getWeightUnitBySymbol(this.weightUnitSymbol).name;
    }

    public onInputChange(): void {
        this.onChange.emit({ weightUnitSymbol: this.weightUnitSymbol });
    }

    public openCloseDropdown() {
        this.isDropdownOpened = !this.isDropdownOpened;
        this.weightUnitsList = this.weightUnitsHelper.getWeightUnitListOrderByName();
    }

    public changeWeight(symbol) {
        this.weightUnitSymbol = symbol;
        this.weightUnitName = this.weightUnitsHelper.getWeightUnitBySymbol(this.weightUnitSymbol).name;
        this.onChange.emit({ weightUnitSymbol: this.weightUnitSymbol });
        this.openCloseDropdown();
        this.onClearSearchInput();
    }

    public onClearSearchInput() {
        this.inputSearchViewModel.value = '';
    }
}
