import { TestBed, ComponentFixture, async, inject } from '@angular/core/testing';
import { Component, NgModule, ViewChild, ViewContainerRef } from '@angular/core';
import { TooltipDirective } from './tooltip.directive';
import { Overlay, OverlayContainer, OverlayModule } from '@angular/cdk/overlay';
import { CdkPortal, ComponentPortal, PortalModule, TemplatePortal } from '@angular/cdk/portal';
import { By } from '@angular/platform-browser';
import { TooltipComponent } from './tooltip.component';

describe('Tooltip Directive', () => {
    let overlay: Overlay;
    let componentPortal: ComponentPortal<TooltipComponent>;
    let templatePortal: TemplatePortal;
    let overlayContainerElement: HTMLElement;
    let overlayContainer: OverlayContainer;
    let viewContainerFixture: ComponentFixture<TestWithTemplatePortalsComponent>;
    let elem;

    beforeEach(
        async(() => {
            TestBed.configureTestingModule({
                imports: [OverlayModule, PortalModule, TooltipTestModule]
            }).compileComponents();
        })
    );

    beforeEach(
        inject([Overlay, OverlayContainer], (o: Overlay, oc: OverlayContainer) => {
            overlay = o;
            overlayContainer = oc;
            overlayContainerElement = oc.getContainerElement();

            const fixture = TestBed.createComponent(TestWithTemplatePortalsComponent);
            fixture.detectChanges();
            templatePortal = fixture.componentInstance.templatePortal;
            componentPortal = new ComponentPortal(TooltipComponent, fixture.componentInstance.viewContainerRef);
            viewContainerFixture = fixture;

            elem = viewContainerFixture.debugElement.query(By.css('.testme'));
            elem.triggerEventHandler('mouseenter', null);
            viewContainerFixture.detectChanges();
        })
    );

    afterEach(() => {
        overlayContainer.ngOnDestroy();
        elem.triggerEventHandler('mouseleave', null);
    });

    describe('should', () => {
        it('have attached the component to the overlay', () => {
            expect(overlayContainerElement.textContent).toBe('It works');
        });

        it('detach the component from the overlay', () => {
            elem.triggerEventHandler('mouseleave', null);

            expect(overlayContainerElement.textContent).toBe('');
        });
    });
});

@Component({
    template: `
      <ng-template cdk-portal></ng-template>
      <div class="testme" overlayTooltip="It works"></div>
  `
})
class TestWithTemplatePortalsComponent {
    @ViewChild(CdkPortal, { static: false })
    templatePortal: CdkPortal;

    constructor(public viewContainerRef: ViewContainerRef) {}
}

const TEST_COMPONENTS = [TooltipComponent, TestWithTemplatePortalsComponent];
@NgModule({
    imports: [OverlayModule, PortalModule],
    exports: [...TEST_COMPONENTS, TooltipDirective],
    declarations: [...TEST_COMPONENTS, TooltipDirective],
    entryComponents: TEST_COMPONENTS
})
class TooltipTestModule {}
