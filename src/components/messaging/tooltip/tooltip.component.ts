import { Component, Input } from '@angular/core';

@Component({
    selector: 'overlay-tooltip',
    templateUrl: './tooltip.component.html'
})
export class TooltipComponent {
    @Input() text = '';
}
