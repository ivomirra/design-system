import { ComponentRef, Directive, ElementRef, HostListener, Input, OnInit } from '@angular/core';
import { Overlay, OverlayPositionBuilder, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';

import { TooltipComponent } from './tooltip.component';

import { TOOLTIP_CONFIG } from './tooltip.config';

@Directive({ selector: '[overlayTooltip]' })
export class TooltipDirective implements OnInit {
    @Input('overlayTooltip') text = '';

    private overlayRef: OverlayRef;
    private componentPortal: ComponentPortal<TooltipComponent>;

    constructor(
        private overlay: Overlay,
        private overlayPositionBuilder: OverlayPositionBuilder,
        private elementRef: ElementRef
    ) {}

    public ngOnInit() {
        const positionStrategy = this.overlayPositionBuilder.flexibleConnectedTo(this.elementRef).withPositions([
            {
                originX: TOOLTIP_CONFIG.originX,
                originY: TOOLTIP_CONFIG.originY,
                overlayX: TOOLTIP_CONFIG.overlayX,
                overlayY: TOOLTIP_CONFIG.overlayY,
                offsetY: TOOLTIP_CONFIG.offsetY
            }
        ]);

        this.overlayRef = this.overlay.create({ positionStrategy });
        this.componentPortal = new ComponentPortal(TooltipComponent);
    }

    @HostListener('mouseenter')
    public show() {
        const tooltipRef: ComponentRef<TooltipComponent> = this.overlayRef.attach(this.componentPortal);
        tooltipRef.instance.text = this.text;
    }

    @HostListener('mouseleave')
    public hide() {
        this.overlayRef.detach();
    }
}
