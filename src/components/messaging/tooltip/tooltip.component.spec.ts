import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TooltipComponent } from './tooltip.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('TooltipComponent', () => {
    let fixture: ComponentFixture<TooltipComponent>;
    let component: TooltipComponent;
    let elem: DebugElement;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [TooltipComponent]
        });

        fixture = TestBed.createComponent(TooltipComponent);
        component = fixture.componentInstance;
        elem = fixture.debugElement.query(By.css('.tooltip'));
    });

    describe('should have', () => {
        it('text defined', () => {
            const text = 'qwerty';

            component.text = text;
            fixture.detectChanges();

            expect(elem.nativeElement.textContent).toBe(text);
        });
    });
});
