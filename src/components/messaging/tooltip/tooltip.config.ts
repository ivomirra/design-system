export const TOOLTIP_CONFIG: any = {
    originX: 'center',
    originY: 'bottom',
    overlayX: 'center',
    overlayY: 'bottom',
    offsetY: 24
};
