import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialHelpersModule } from '../../helpers/index';

import { UiTooltipsComponent } from './ui-tooltips.component';

@NgModule({
    imports: [CommonModule, MaterialHelpersModule],
    declarations: [UiTooltipsComponent],
    exports: [UiTooltipsComponent]
})
export class UiTooltipsModule {}
