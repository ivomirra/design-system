import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { UiTooltipsComponent } from './ui-tooltips.component';
import { MaterialHelper } from '../../helpers/material.helpers';

describe('UiTooltipsComponent', () => {
    const MaterialHelperSpy = jasmine.createSpyObj('MaterialHelperSpy', ['getAbsolutePositionRenderConfig']);
    let fixture;
    let testedComponent;
    let compiledComponent;

    beforeEach(() => {
        MaterialHelperSpy.getAbsolutePositionRenderConfig.and.returnValue({
            canRenderToLeft: true,
            canRenderToRight: true
        });

        TestBed.configureTestingModule({
            imports: [FormsModule],
            declarations: [UiTooltipsComponent],
            providers: [
                MaterialHelper,
                {
                    provide: MaterialHelper,
                    useValue: MaterialHelperSpy
                }
            ]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiTooltipsComponent);
        testedComponent = fixture.debugElement.componentInstance;
        compiledComponent = fixture.debugElement.nativeElement;

        spyOn(testedComponent.element.nativeElement, 'querySelector').and.callFake(() => {
            return {
                offsetWidth: 10
            };
        });

        testedComponent.viewModel = {};

        fixture.detectChanges();
    });

    describe('#ngOnInit', function() {
        describe('when invoked', function() {
            describe('and icon is not defined', function() {
                it('should set icon to default value', function() {
                    testedComponent.ngOnInit();

                    expect(testedComponent.viewModel.icon).toEqual('info');
                });
            });

            describe('and color is not defined', function() {
                it('should set color to default value', function() {
                    testedComponent.ngOnInit();

                    expect(testedComponent.viewModel.color).toEqual('blue');
                });
            });

            describe('and both icon and color are defined', function() {
                beforeEach(() => {
                    testedComponent.viewModel = {
                        color: 'green',
                        icon: 'explode'
                    };

                    testedComponent.ngOnInit();
                });

                it('should set icon to given value', function() {
                    expect(testedComponent.viewModel.icon).toEqual('explode');
                });

                it('should set color to given value', function() {
                    expect(testedComponent.viewModel.color).toEqual('green');
                });
            });
        });
    });

    describe('#onTooltipShow', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                testedComponent.alignLeft = false;
                testedComponent.alignRight = false;
                testedComponent.onTooltipShow();
            });

            it('should get the absolute position config', () => {
                expect(MaterialHelperSpy.getAbsolutePositionRenderConfig).toHaveBeenCalledWith(0, 10);
            });

            it('should set align properties as returned by getAbsolutePositionRenderConfig method', () => {
                expect(testedComponent.alignLeft).toEqual(true);
                expect(testedComponent.alignRight).toEqual(true);
            });
        });
    });
});
