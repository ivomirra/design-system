import { Component, Input, ElementRef, OnInit } from '@angular/core';
import { MaterialHelper } from '../../helpers/material.helpers';
import { UI_TOOLTIP_CONFIG } from './ui-tooltips.config';

@Component({
    selector: 'ui-tooltips',
    templateUrl: './ui-tooltips.component.html'
})
export class UiTooltipsComponent implements OnInit {
    @Input()
    public viewModel: {
        title?: string;
        text?: string;
        icon?: string;
        color?: string;
        size?: string;
    };

    public alignLeft = false;
    public alignRight = false;

    constructor(private element: ElementRef, private helper: MaterialHelper) {}

    public ngOnInit() {
        this.viewModel.icon = !!this.viewModel.icon ? this.viewModel.icon : UI_TOOLTIP_CONFIG.defaultIcon;
        this.viewModel.color = !!this.viewModel.color ? this.viewModel.color : UI_TOOLTIP_CONFIG.defaultColor;
    }

    public onTooltipShow() {
        const absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(
            this.element.nativeElement.getBoundingClientRect().left,
            this.element.nativeElement.querySelector('.ui-tooltip__container').offsetWidth
        );

        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
    }
}
