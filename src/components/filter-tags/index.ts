import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UiFilterTagsComponent } from './ui-filter-tags.component';

@NgModule({
    imports: [CommonModule],
    declarations: [UiFilterTagsComponent],
    exports: [UiFilterTagsComponent]
})
export class UiFilterTagsModule {}
