import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'ui-filter-tags',
    templateUrl: './ui-filter-tags.component.html'
})
export class UiFilterTagsComponent {
    /** Property that defines the viewModel to be rendered by ui-filter-tags **/
    @Input()
    public viewModel: {
        items: object[];
    };

    /** Property that should be used for binding a callback to receive the
     input submission **/
    @Output()
    public onLabelRemoved: EventEmitter<{
        removedLabel: object;
        removedIndex: number;
        items: object[];
    }> = new EventEmitter();

    constructor() {}

    /**
     * Method bound to the form submit event.
     */
    public onLabelRemovedClick(labelIndex: number): void {
        if (!this.viewModel.items[labelIndex]) {
            return;
        }

        const removedLabel = this.viewModel.items[labelIndex];

        this.viewModel.items.splice(labelIndex, 1);

        this.onLabelRemoved.emit({
            removedLabel,
            removedIndex: labelIndex,
            items: this.viewModel.items
        });
    }
}
