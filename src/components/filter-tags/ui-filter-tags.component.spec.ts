import { TestBed } from '@angular/core/testing';
import { UiFilterTagsComponent } from './ui-filter-tags.component';

describe('UiFilterTagsComponent', () => {
    let fixture;
    let testedComponent;
    let compiledComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [UiFilterTagsComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiFilterTagsComponent);
        testedComponent = fixture.debugElement.componentInstance;
        compiledComponent = fixture.debugElement.nativeElement;

        testedComponent.viewModel = {};

        spyOn(testedComponent.onLabelRemoved, 'emit');

        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    describe('#onLabelRemovedClick', () => {
        describe('when invoked', () => {
            describe('and labelIndex is out of bounds', () => {
                beforeEach(() => {
                    testedComponent.viewModel.items = [
                        {
                            text: 'item-1'
                        },
                        {
                            text: 'item-2'
                        }
                    ];
                    testedComponent.onLabelRemovedClick(2);
                });

                it('should not emit changes', () => {
                    expect(testedComponent.onLabelRemoved.emit).not.toHaveBeenCalled();
                });

                it('should not delete an item', () => {
                    expect(testedComponent.viewModel.items.length).toEqual(2);
                });
            });

            describe('and labelIndex is inside bounds', () => {
                beforeEach(() => {
                    testedComponent.viewModel.items = [
                        {
                            text: 'item-1'
                        },
                        {
                            text: 'item-2'
                        }
                    ];
                    testedComponent.onLabelRemovedClick(0);
                });

                it('should emit changes', () => {
                    expect(testedComponent.onLabelRemoved.emit).toHaveBeenCalledWith({
                        removedLabel: { text: 'item-1' },
                        removedIndex: 0,
                        items: [
                            {
                                text: 'item-2'
                            }
                        ]
                    });
                });

                it('should delete an item', () => {
                    expect(testedComponent.viewModel.items.length).toEqual(1);
                });
            });
        });
    });
});
