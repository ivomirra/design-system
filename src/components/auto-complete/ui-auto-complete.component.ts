import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'ui-auto-complete',
    templateUrl: './ui-auto-complete.component.html'
})
export class UiAutoCompleteComponent implements OnInit {
    public searchString = '';
    public selectedOptions: any = [];
    public processedOptionsList: any = [];
    public showOptionsList = false;
    public filterTagsViewModel: {
        items: any[];
    } = {
        items: []
    };

    @Input() private optionsList: any[] = [];
    @Input() private multiple = true;
    @Input() private defaultValues: any[] = [];
    @Input() public placeholder: string;
    @Input() public hasError = false;
    @Input() public disabled = false;

    @Output()
    public onItemsChanged: EventEmitter<{
        selectedItems: any[];
    }> = new EventEmitter();

    constructor() {}

    private emitUpdates() {
        this.onItemsChanged.emit({
            selectedItems: this.selectedOptions
        });
    }

    public ngOnInit() {
        this.processedOptionsList = this.optionsList;
        this.selectedOptions = this.defaultValues;

        if (this.multiple) {
            this.filterTagsViewModel.items = new Array().concat(this.selectedOptions);
        }

        if (!this.multiple) {
            this.searchString = this.defaultValues.length === 1 ? this.defaultValues[0].text : '';
        }
    }

    public showOptionsMenu() {
        this.showOptionsList = true;
    }

    public hideOptionsMenu() {
        this.showOptionsList = false;
    }

    public searchValue(searchString) {
        this.processedOptionsList = this.optionsList.filter((item) => {
            return item.text.toLowerCase().includes(searchString.toLowerCase());
        });
    }

    public clearSearch() {
        this.searchString = '';

        if (!this.multiple) {
            this.selectedOptions.length = 0;
            this.emitUpdates();
        }
    }

    public onFilterTagRemoved(filterData) {
        this.selectedOptions.splice(filterData.removedIndex, 1);

        this.emitUpdates();
    }

    public onOptionSelected(optionSelected) {
        this.hideOptionsMenu();

        if (this.multiple) {
            this.clearSearch();
            this.ngOnInit();
            this.selectedOptions.push(optionSelected);
            this.filterTagsViewModel.items.push(optionSelected);
        } else {
            this.searchString = optionSelected.text;
            this.selectedOptions[0] = optionSelected;
        }

        this.emitUpdates();
    }
}
