import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UiFilterTagsModule } from '../filter-tags/index';

import { UiAutoCompleteComponent } from './ui-auto-complete.component';

@NgModule({
    imports: [CommonModule, FormsModule, UiFilterTagsModule],
    declarations: [UiAutoCompleteComponent],
    exports: [UiAutoCompleteComponent]
})
export class UiAutoCompleteModule {}
