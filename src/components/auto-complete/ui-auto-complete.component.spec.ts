import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { UiAutoCompleteComponent } from './ui-auto-complete.component';
import { UiFilterTagsComponent } from '../filter-tags/ui-filter-tags.component';

describe('UiAutoCompleteComponent', () => {
    const optionsList = [{ text: 'Afghanistan' }, { text: 'Albania' }];
    const defaultValues = [
        {
            text: 'Example'
        }
    ];
    let fixture;
    let testedComponent;
    let compiledComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule],
            declarations: [UiAutoCompleteComponent, UiFilterTagsComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(UiAutoCompleteComponent);
        testedComponent = fixture.debugElement.componentInstance;
        compiledComponent = fixture.debugElement.nativeElement;

        testedComponent.viewModel = {};

        spyOn(testedComponent.onItemsChanged, 'emit');
        spyOn(testedComponent, 'clearSearch').and.callThrough();
        spyOn(testedComponent, 'ngOnInit').and.callThrough();

        fixture.detectChanges();
    });

    afterEach(() => {
        testedComponent.onItemsChanged.emit.calls.reset();
    });

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    describe('when initializing the component', () => {
        beforeEach(() => {
            testedComponent.optionsList = optionsList;
            testedComponent.defaultValues = defaultValues;

            testedComponent.ngOnInit();
        });

        it('should define processedOptionsList with options list', () => {
            expect(testedComponent.processedOptionsList).toEqual(optionsList);
        });

        it('should set filterTagsViewModel with default values', () => {
            expect(testedComponent.filterTagsViewModel).toEqual({
                items: defaultValues
            });
        });

        describe('and the multiple options is defined as false', () => {
            beforeEach(() => {
                testedComponent.multiple = false;
                testedComponent.ngOnInit();
            });

            it('should define selectedOptions with default values', () => {
                expect(testedComponent.defaultValues).toEqual(defaultValues);
            });

            describe('and defaultValues has one position', () => {
                it('should define searchString with the first position in default values', () => {
                    expect(testedComponent.searchString).toEqual(defaultValues[0].text);
                });
            });

            describe('and defaultValues has more than one position', () => {
                it('should define searchString as empty string', () => {
                    testedComponent.defaultValues = [].concat(defaultValues, defaultValues);
                    testedComponent.ngOnInit();

                    expect(testedComponent.searchString).toEqual('');
                });
            });
        });
    });

    describe('#showOptionsMenu', () => {
        describe('when invoked', () => {
            it('should showOptionsList as true', () => {
                testedComponent.showOptionsList = false;

                testedComponent.showOptionsMenu();

                expect(testedComponent.showOptionsList).toEqual(true);
            });
        });
    });

    describe('#hideOptionsMenu', () => {
        describe('when invoked', () => {
            it('should showOptionsList as true', () => {
                testedComponent.showOptionsList = true;

                testedComponent.hideOptionsMenu();

                expect(testedComponent.showOptionsList).toEqual(false);
            });
        });
    });

    describe('#searchValue', () => {
        beforeEach(() => {
            testedComponent.optionsList = optionsList;
            testedComponent.defaultValues = defaultValues;

            testedComponent.ngOnInit();
        });

        describe('when invoked with a search string that matches any value', () => {
            it('should set processedOptionsList with the data filtered', () => {
                testedComponent.searchValue('Afghanistan');

                expect(testedComponent.processedOptionsList).toEqual([
                    {
                        text: 'Afghanistan'
                    }
                ]);
            });
        });

        describe("when invoked with a search string that doesn't match any value", () => {
            it('should set processedOptionsList with an empty array', () => {
                testedComponent.searchValue('Portugal');

                expect(testedComponent.processedOptionsList).toEqual([]);
            });
        });
    });

    describe('#clearSearch', () => {
        beforeEach(() => {
            testedComponent.searchString = 'Test';
            testedComponent.selectedOptions = [1, 2];
        });

        describe('when invoked', () => {
            it('should set searchString as empty array', () => {
                testedComponent.clearSearch();

                expect(testedComponent.searchString).toEqual('');
            });

            it(`shouldn't set selectedOptions as empty array`, () => {
                testedComponent.clearSearch();

                expect(testedComponent.selectedOptions.length).toEqual(2);
            });

            it(`shouldn't emit changes`, () => {
                testedComponent.clearSearch();

                expect(testedComponent.onItemsChanged.emit).not.toHaveBeenCalled();
            });

            describe(`and isn't multiple`, () => {
                beforeEach(() => {
                    testedComponent.multiple = false;
                    testedComponent.clearSearch();
                });

                it(`should set selectedOptions as empty array`, () => {
                    testedComponent.clearSearch();

                    expect(testedComponent.selectedOptions.length).toEqual(0);
                });

                it(`should emit changes`, () => {
                    testedComponent.clearSearch();

                    expect(testedComponent.onItemsChanged.emit).toHaveBeenCalledWith({
                        selectedItems: []
                    });
                });
            });
        });
    });

    describe('#onFilterTagRemoved', () => {
        describe('when invoked with filterData to be removed', () => {
            beforeEach(() => {
                testedComponent.selectedOptions = [1, 2, 3];
                testedComponent.onFilterTagRemoved({ selectedIndex: 1 });
            });

            it('should remove the selected index', () => {
                expect(testedComponent.selectedOptions).toEqual([2, 3]);
            });

            it('should emit updated values', () => {
                expect(testedComponent.onItemsChanged.emit).toHaveBeenCalledWith({
                    selectedItems: [2, 3]
                });
            });
        });
    });

    describe('#onOptionSelected', () => {
        describe('when invoked', () => {
            beforeEach(() => {
                testedComponent.showOptionsList = true;
                testedComponent.defaultValues = [1];
                testedComponent.onOptionSelected({
                    text: 'Example'
                });
            });

            it('should set showOptionsList as false', () => {
                expect(testedComponent.showOptionsList).toEqual(false);
            });

            it('should emit updates', () => {
                expect(testedComponent.onItemsChanged.emit).toHaveBeenCalledWith({
                    selectedItems: [
                        1,
                        {
                            text: 'Example'
                        }
                    ]
                });
            });

            it('should call #clearSearch method', () => {
                expect(testedComponent.clearSearch).toHaveBeenCalled();
            });

            it('should call #ngOnInit method', () => {
                expect(testedComponent.ngOnInit).toHaveBeenCalled();
            });

            it('should add the mock to selectedOptions', () => {
                expect(testedComponent.selectedOptions).toEqual([
                    1,
                    {
                        text: 'Example'
                    }
                ]);
            });

            it('should add the mock to filterTagsViewModel items', () => {
                expect(testedComponent.filterTagsViewModel.items).toEqual([
                    1,
                    {
                        text: 'Example'
                    }
                ]);
            });

            describe(`and the flag multiple is setted as false`, () => {
                beforeEach(() => {
                    testedComponent.multiple = false;
                    testedComponent.selectedOptions = [
                        {
                            text: 'Example2'
                        }
                    ];
                    testedComponent.onOptionSelected({
                        text: 'Example'
                    });
                });

                it('should set searchString as the selected option', () => {
                    expect(testedComponent.searchString).toEqual('Example');
                });

                it('should set the first position of selectedOptions as the selected option', () => {
                    expect(testedComponent.selectedOptions).toEqual([
                        {
                            text: 'Example'
                        }
                    ]);
                });
            });
        });
    });
});
