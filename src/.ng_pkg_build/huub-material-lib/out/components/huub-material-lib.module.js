/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';
import { MaterialHelper } from './helpers/material.helpers';
import { UiModalService } from './modal/ui-modal.service';
import { UiToggleButtonsComponent } from './toggle-buttons/ui-toggle-buttons.component';
import { UiTabsComponent } from './tabs/ui-tabs.component';
import { UiMenuBarComponent } from './menu-bar/ui-menu-bar.component';
import { UiFilterTagsComponent } from './filter-tags/ui-filter-tags.component';
import { UiFilterComponent } from './filter/ui-filter.component';
import { UiActivityTimelineComponent } from './activity-timeline/ui-activity-timeline.component';
import { UiSpinnerComponent } from './forms/spinner/ui-spinner.component';
import { UiInputSearchComponent } from './forms/search/ui-input-search.component';
import { UiTablesComponent } from './tables/ui-tables.component';
import { UiTooltipsComponent } from './messaging/tooltips/ui-tooltips.component';
import { UiTablesPlaceholderComponent } from './tables/ui-tables-placeholder.component';
import { UiButtonActionsComponent } from './button-actions/ui-button-actions.component';
import { UiLoaderComponent } from './loaders/ui-loader.component';
import { UiPopupMessageComponent } from './popup-message/ui-popup-message.component';
import { UiAutoCompleteComponent } from './auto-complete/ui-auto-complete.component';
import { UiDateInputComponent } from './forms/date-input/ui-date-input.component';
import { UiUploadImageComponent } from './forms/upload-image/ui-upload-image.component';
export class HuubMaterialLibModule {
}
HuubMaterialLibModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, FormsModule, OverlayModule],
                declarations: [
                    UiToggleButtonsComponent,
                    UiTabsComponent,
                    UiMenuBarComponent,
                    UiFilterTagsComponent,
                    UiFilterComponent,
                    UiActivityTimelineComponent,
                    UiSpinnerComponent,
                    UiInputSearchComponent,
                    UiTablesComponent,
                    UiButtonActionsComponent,
                    UiTooltipsComponent,
                    UiLoaderComponent,
                    UiTablesPlaceholderComponent,
                    UiPopupMessageComponent,
                    UiAutoCompleteComponent,
                    UiDateInputComponent,
                    UiUploadImageComponent
                ],
                exports: [
                    UiToggleButtonsComponent,
                    UiTabsComponent,
                    UiMenuBarComponent,
                    UiFilterTagsComponent,
                    UiFilterComponent,
                    UiActivityTimelineComponent,
                    UiSpinnerComponent,
                    UiInputSearchComponent,
                    UiTablesComponent,
                    UiButtonActionsComponent,
                    UiTooltipsComponent,
                    UiLoaderComponent,
                    UiTablesPlaceholderComponent,
                    UiPopupMessageComponent,
                    UiAutoCompleteComponent,
                    UiDateInputComponent,
                    UiUploadImageComponent
                ],
                providers: [MaterialHelper, UiModalService]
            },] },
];
/** @nocollapse */
HuubMaterialLibModule.ctorParameters = () => [];
function HuubMaterialLibModule_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    HuubMaterialLibModule.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    HuubMaterialLibModule.ctorParameters;
}
//# sourceMappingURL=huub-material-lib.module.js.map