/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { find } from 'lodash';
import { MaterialHelper } from '../helpers/material.helpers';
/**
 * @record
 */
export function OptionsInterface() { }
function OptionsInterface_tsickle_Closure_declarations() {
    /** @type {?} */
    OptionsInterface.prototype.value;
    /** @type {?} */
    OptionsInterface.prototype.text;
    /** @type {?|undefined} */
    OptionsInterface.prototype.selected;
    /** @type {?|undefined} */
    OptionsInterface.prototype.type;
}
export class UiFilterComponent {
    /**
     * @param {?} element
     * @param {?} helper
     */
    constructor(element, helper) {
        this.element = element;
        this.helper = helper;
        this.ELEMENT_SIZE = 350;
        this.alignRight = false;
        this.alignLeft = false;
        this.selectedOptions = [];
        this.optionContainerIsVisible = false;
        /**
         * Property that sould be used for binding a callback to receive the save action *
         */
        this.onSaveAction = new EventEmitter();
    }
    /**
     * @param {?} index
     * @param {?} item
     * @return {?}
     */
    trackByFn(index, item) {
        return index;
    }
    /**
     * @return {?}
     */
    toogleOptionContainerVisibility() {
        const /** @type {?} */ absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, this.ELEMENT_SIZE);
        this.optionContainerIsVisible = !this.optionContainerIsVisible;
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
        this.selectedOptions.length = 0;
    }
    /**
     * @return {?}
     */
    onSaveButtonClick() {
        this.onSaveAction.emit({
            selectedOption: find(this.selectedOptions, { selected: true }) ||
                this.selectedOptions[0]
        });
    }
    /**
     * @param {?} options
     * @param {?} optionIndex
     * @return {?}
     */
    setSelectedOption(options, optionIndex) {
        options.forEach(item => {
            item.selected = false;
        });
        options[optionIndex].selected = true;
    }
    /**
     * @param {?} options
     * @param {?} indexOption
     * @return {?}
     */
    onSelectChange(options, indexOption) {
        this.setSelectedOption(options, indexOption);
        this.selectedOptions =
            this.viewModel.selectOptions[this.viewModel.selectBox[indexOption].type] || [];
    }
    /**
     * @param {?} options
     * @param {?} indexOption
     * @return {?}
     */
    onSecondarySelectChange(options, indexOption) {
        this.setSelectedOption(options, indexOption);
    }
}
UiFilterComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-filter',
                template: `<button class="ui-filter__container">
    <i class="material-icons ui-filter__icon ui-filter__icon__tune"
       (click)="toogleOptionContainerVisibility()">tune</i>
    <span class="ui-filter__text"
          (click)="toogleOptionContainerVisibility()">{{viewModel.filterText}}</span>
    <i class="material-icons ui-filter__icon"
       (click)="toogleOptionContainerVisibility()">keyboard_arrow_down</i>

    <div class="ui-filter__options-container"
        [ngClass]="{
            'ui-filter__options-container--visible': optionContainerIsVisible,
            'ui-filter__options-container--right': alignRight,
            'ui-filter__options-container--left': alignLeft
        }"
        (mouseleave)="toogleOptionContainerVisibility()">
        <ng-container *ngIf="optionContainerIsVisible">
            <select class="ui-filter__select"
                    (change)="onSelectChange(viewModel.selectBox, $event.target.value)">
                <option *ngFor="let option of viewModel.selectBox; let index = index; trackBy: trackByFn"
                        value="{{index}}">{{option.text}}</option>
            </select>

            <select *ngIf="selectedOptions.length > 0"
                    class="ui-filter__select"
                    (change)="onSecondarySelectChange(selectedOptions, $event.target.value)">
                <option *ngFor="let option of selectedOptions; let index = index; trackBy: trackByFn"
                        value="{{index}}">{{option.text}}</option>
            </select>
        </ng-container>

        <button type="secondary" class="ui-filter__button"
                (click)="onSaveButtonClick()">{{viewModel.buttonText}}</button>
    </div>
</button>
`
            },] },
];
/** @nocollapse */
UiFilterComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: MaterialHelper, },
];
UiFilterComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onSaveAction": [{ type: Output },],
};
function UiFilterComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiFilterComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiFilterComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    UiFilterComponent.propDecorators;
    /** @type {?} */
    UiFilterComponent.prototype.ELEMENT_SIZE;
    /** @type {?} */
    UiFilterComponent.prototype.alignRight;
    /** @type {?} */
    UiFilterComponent.prototype.alignLeft;
    /** @type {?} */
    UiFilterComponent.prototype.selectedOptions;
    /** @type {?} */
    UiFilterComponent.prototype.optionContainerIsVisible;
    /** @type {?} */
    UiFilterComponent.prototype.viewModel;
    /**
     * Property that sould be used for binding a callback to receive the save action *
     * @type {?}
     */
    UiFilterComponent.prototype.onSaveAction;
    /** @type {?} */
    UiFilterComponent.prototype.element;
    /** @type {?} */
    UiFilterComponent.prototype.helper;
}
//# sourceMappingURL=ui-filter.component.js.map