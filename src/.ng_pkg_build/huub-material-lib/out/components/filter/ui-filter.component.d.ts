import { EventEmitter, ElementRef } from '@angular/core';
import { MaterialHelper } from '../helpers/material.helpers';
export interface OptionsInterface {
    value: string;
    text: string;
    selected?: boolean;
    type?: string;
}
export declare class UiFilterComponent {
    private element;
    private helper;
    private ELEMENT_SIZE;
    alignRight: boolean;
    alignLeft: boolean;
    selectedOptions: any[];
    optionContainerIsVisible: boolean;
    viewModel: {
        filterText: string;
        buttonText: string;
        selectBox: [OptionsInterface];
        selectOptions: any;
    };
    /** Property that sould be used for binding a callback to receive the save action **/
    onSaveAction: EventEmitter<{
        selectedOption: any;
    }>;
    constructor(element: ElementRef, helper: MaterialHelper);
    trackByFn(index: any, item: any): number;
    toogleOptionContainerVisibility(): void;
    onSaveButtonClick(): void;
    private setSelectedOption(options, optionIndex);
    onSelectChange(options: any, indexOption: number): void;
    onSecondarySelectChange(options: any, indexOption: number): void;
}
