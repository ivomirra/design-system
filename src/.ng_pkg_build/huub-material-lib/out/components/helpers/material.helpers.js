/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
export class MaterialHelper {
    constructor() { }
    /**
     * @param {?} elementOffSet
     * @param {?} elementSize
     * @return {?}
     */
    getAbsolutePositionRenderConfig(elementOffSet, elementSize) {
        return {
            canRenderToRight: window.innerWidth > elementOffSet + elementSize,
            canRenderToLeft: elementOffSet - elementSize > 0
        };
    }
}
MaterialHelper.decorators = [
    { type: Injectable },
];
/** @nocollapse */
MaterialHelper.ctorParameters = () => [];
function MaterialHelper_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    MaterialHelper.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    MaterialHelper.ctorParameters;
}
//# sourceMappingURL=material.helpers.js.map