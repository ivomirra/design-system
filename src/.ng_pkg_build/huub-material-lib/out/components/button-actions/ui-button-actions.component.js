/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, ElementRef } from '@angular/core';
import { MaterialHelper } from '../helpers/material.helpers';
export class UiButtonActionsComponent {
    /**
     * @param {?} element
     * @param {?} helper
     */
    constructor(element, helper) {
        this.element = element;
        this.helper = helper;
        this.ELEMENT_SIZE = 200;
        this.alignRight = false;
        this.alignLeft = false;
        this.type = 'flat';
        this.optionContainerIsVisible = false;
    }
    /**
     * @return {?}
     */
    showOptionContainerVisibility() {
        const /** @type {?} */ absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, this.ELEMENT_SIZE);
        this.optionContainerIsVisible = true;
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
    }
    /**
     * @return {?}
     */
    hideOptionContainerVisibility() {
        this.optionContainerIsVisible = false;
    }
    /**
     * @param {?} link
     * @return {?}
     */
    onLinkClicked(link) {
        if (!link.callback || typeof link.callback !== 'function') {
            return;
        }
        link.callback(link);
    }
}
UiButtonActionsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-button-actions',
                template: `<div class="ui-button-actions"
    (mouseleave)="hideOptionContainerVisibility()">
    <button [attr.type]="type"
            role="icon"
            (click)="showOptionContainerVisibility()"></button>
    <div class="ui-button-actions__actions-list"
        [ngClass]="{
            'ui-button-actions__actions-list--visible': optionContainerIsVisible,
            'ui-button-actions__actions-list--right': alignRight,
            'ui-button-actions__actions-list--left': alignLeft
        }"
        (mouseleave)="hideOptionContainerVisibility()">
            <a *ngFor="let link of viewModel.items"
                class="ui-button-actions__actions-list-link"
                (click)="onLinkClicked(link)">
                {{link.text}}
            </a>
    </div>
</div>
`
            },] },
];
/** @nocollapse */
UiButtonActionsComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: MaterialHelper, },
];
UiButtonActionsComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "type": [{ type: Input },],
};
function UiButtonActionsComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiButtonActionsComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiButtonActionsComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    UiButtonActionsComponent.propDecorators;
    /** @type {?} */
    UiButtonActionsComponent.prototype.ELEMENT_SIZE;
    /** @type {?} */
    UiButtonActionsComponent.prototype.alignRight;
    /** @type {?} */
    UiButtonActionsComponent.prototype.alignLeft;
    /** @type {?} */
    UiButtonActionsComponent.prototype.viewModel;
    /** @type {?} */
    UiButtonActionsComponent.prototype.type;
    /** @type {?} */
    UiButtonActionsComponent.prototype.optionContainerIsVisible;
    /** @type {?} */
    UiButtonActionsComponent.prototype.element;
    /** @type {?} */
    UiButtonActionsComponent.prototype.helper;
}
//# sourceMappingURL=ui-button-actions.component.js.map