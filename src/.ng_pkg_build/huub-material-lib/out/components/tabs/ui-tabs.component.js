/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
export class UiTabsComponent {
    constructor() {
        /**
         * Property that sould be used for binding a callback to receive the tab clicked *
         */
        this.onTabChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.viewModel.selectedIndex = this.viewModel.selectedIndex || 0;
    }
    /**
     * Method used to update the selected tab. This method also triggers
     * the eventEmitter for onTabChanged callback that the parent may have
     * configured.
     * selectedTab Current tab clicked
     * selectedTabIndex Current tab index clicked
     * @param {?} selectedTab
     * @param {?} selectedTabIndex
     * @return {?}
     */
    onTabClicked(selectedTab, selectedTabIndex) {
        if (selectedTab.disabled) {
            return;
        }
        this.onTabChanged.emit({
            selectedTab,
            selectedTabIndex
        });
        this.viewModel.selectedIndex = selectedTabIndex;
    }
}
UiTabsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-tabs',
                template: `<nav class="ui-tabs__nav">
    <ul class="ui-tabs__nav-items">
        <li *ngFor="let item of viewModel.items; let itemIndex = index;"
            [ngClass]="{
                'ui-tabs__nav-item': !(itemIndex === viewModel.selectedIndex) && !item.disabled,
                'ui-tabs__nav-item--active': itemIndex === viewModel.selectedIndex,
                'ui-tabs__nav-item--disabled': item.disabled
            }">
            <a (click)="onTabClicked(item, itemIndex)" class="ui-tabs__nav-link">
                {{item.text}}
            </a>
        </li>
    </ul>
</nav>
`
            },] },
];
/** @nocollapse */
UiTabsComponent.ctorParameters = () => [];
UiTabsComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onTabChanged": [{ type: Output },],
};
function UiTabsComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiTabsComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiTabsComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    UiTabsComponent.propDecorators;
    /**
     * Property that defines the viewModel to be rendered by ui-tabs *
     * @type {?}
     */
    UiTabsComponent.prototype.viewModel;
    /**
     * Property that sould be used for binding a callback to receive the tab clicked *
     * @type {?}
     */
    UiTabsComponent.prototype.onTabChanged;
}
//# sourceMappingURL=ui-tabs.component.js.map