/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component } from '@angular/core';
export class UiTablesPlaceholderComponent {
    constructor() { }
}
UiTablesPlaceholderComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-tables-placeholder',
                template: `<section class="ui-table-placeholder hb-animation-mask">
    <div class="ui-table-placeholder__header">
        <div class="ui-table-placeholder__header-column"></div>
        <div class="ui-table-placeholder__header-column"></div>
        <div class="ui-table-placeholder__header-column"></div>
        <div class="ui-table-placeholder__header-column"></div>
    </div>

    <div class="ui-table-placeholder__body"></div>
    <div class="ui-table-placeholder__body"></div>
    <div class="ui-table-placeholder__body"></div>
    <div class="ui-table-placeholder__body"></div>
</section>
`
            },] },
];
/** @nocollapse */
UiTablesPlaceholderComponent.ctorParameters = () => [];
function UiTablesPlaceholderComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiTablesPlaceholderComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiTablesPlaceholderComponent.ctorParameters;
}
//# sourceMappingURL=ui-tables-placeholder.component.js.map