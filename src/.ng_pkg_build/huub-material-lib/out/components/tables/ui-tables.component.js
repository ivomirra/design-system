/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { sortBy } from 'lodash';
export class UiTablesComponent {
    constructor() { }
    /**
     * @param {?} headerItemIndex
     * @return {?}
     */
    sortBodyTable(headerItemIndex) {
        this.viewModel.tableBody = sortBy(this.viewModel.tableBody, o => o[headerItemIndex].text.toLowerCase() ||
            o[headerItemIndex].value);
    }
    /**
     * @param {?} headerItemIndex
     * @return {?}
     */
    resetAllTableSort(headerItemIndex) {
        this.viewModel.tableHeader.forEach((column, index) => {
            if (headerItemIndex === index) {
                return;
            }
            column.sortBy = false;
        });
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.viewModel.tableHeader.forEach((column, index) => {
            if (column.sortBy) {
                this.sortBodyTable(index);
            }
        });
    }
    /**
     * @param {?} column
     * @return {?}
     */
    onColumnClicked(column) {
        if (!column.callback || typeof column.callback !== 'function') {
            return;
        }
        column.callback(column);
    }
    /**
     * @param {?} headerItemIndex
     * @return {?}
     */
    onSortButtonClick(headerItemIndex) {
        this.sortBodyTable(headerItemIndex);
        this.viewModel.tableHeader[headerItemIndex].sortBy = !this.viewModel
            .tableHeader[headerItemIndex].sortBy;
        if (!this.viewModel.tableHeader[headerItemIndex].sortBy) {
            this.viewModel.tableBody.reverse();
        }
        this.resetAllTableSort(headerItemIndex);
    }
}
UiTablesComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-tables',
                template: `<table class="ui-table__table">
    <thead>
       <tr class="ui-table__tr">
            <th class="ui-table__th"
                *ngFor="let column of viewModel.tableHeader; let columnIndex = index;">
                <button *ngIf="column.text && column.text !== ''"
                        (click)="onSortButtonClick(columnIndex)"
                        class="ui-table__order-container">
                    <span>{{column.text}}</span>
                    <i class="material-icons ui-table__order-icon"
                        [ngClass]="{
                            'ui-table__order-icon--rotate': column.sortBy
                        }">keyboard_arrow_up</i>
                </button>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr class="ui-table__tr ui-table__row" *ngFor="let row of viewModel.tableBody">
            <td class="ui-table__td"
                *ngFor="let column of viewModel.tableHeader; let i = index"
                [ngSwitch]="row[i].type">
                    <input *ngSwitchCase="'checkbox'"
                            type="checkbox"
                            class="material-icons"
                            (click)="onColumnClicked(row[i])"
                            [ngModel]="row[i].value">
                    <span *ngSwitchCase="'string'"
                           (click)="onColumnClicked(row[i])"
                          [ngClass]="{
                                'ui-table__text-bold': row[i].textBold
                           }">{{row[i].text}}</span>
                    <a *ngSwitchCase="'link'"
                        (click)="onColumnClicked(row[i])"
                        class="ui-table__td-link">{{row[i].text}}</a>
                    <button *ngSwitchCase="'button'"
                            type="secondary"
                            [attr.color]="row[i].color"
                            (click)="onColumnClicked(row[i])"
                            >{{row[i].text}}</button>
                    <ui-button-actions *ngSwitchCase="'button-actions'"
                                        [viewModel]="row[i].viewModel"></ui-button-actions>
                    <span *ngSwitchCase="'status'"
                         class="ui-table__status"
                         (click)="onColumnClicked(row[i])"
                         [ngClass]="{
                           'ui-table__status--active': row[i].value}"></span>
            </td>
        </tr>
    </tbody>
</table>

`
            },] },
];
/** @nocollapse */
UiTablesComponent.ctorParameters = () => [];
UiTablesComponent.propDecorators = {
    "viewModel": [{ type: Input },],
};
function UiTablesComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiTablesComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiTablesComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    UiTablesComponent.propDecorators;
    /** @type {?} */
    UiTablesComponent.prototype.viewModel;
}
//# sourceMappingURL=ui-tables.component.js.map