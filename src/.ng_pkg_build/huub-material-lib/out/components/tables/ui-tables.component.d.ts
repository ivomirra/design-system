import { OnInit } from '@angular/core';
export declare class UiTablesComponent implements OnInit {
    viewModel: {
        tableHeader: [{
            sortBy?: boolean;
            text?: string;
        }];
        tableBody: [[{
            type: string;
            color?: string;
            value?: any;
            text?: string;
            callback?: Function;
            link?: string;
        }]];
    };
    constructor();
    private sortBodyTable(headerItemIndex);
    private resetAllTableSort(headerItemIndex);
    ngOnInit(): void;
    onColumnClicked(column: any): void;
    onSortButtonClick(headerItemIndex: any): void;
}
