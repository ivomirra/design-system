/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
export class UiFilterTagsComponent {
    constructor() {
        /**
         * Property that sould be used for binding a callback to receive the
         * input submission *
         */
        this.onLabelRemoved = new EventEmitter();
    }
    /**
     * Method binded to the form submit event.
     * @param {?} labelIndex
     * @return {?}
     */
    onLabelRemovedClick(labelIndex) {
        if (!this.viewModel.items[labelIndex]) {
            return;
        }
        const /** @type {?} */ removedLabel = this.viewModel.items[labelIndex];
        this.viewModel.items.splice(labelIndex, 1);
        this.onLabelRemoved.emit({
            removedLabel,
            removedIndex: labelIndex,
            items: this.viewModel.items
        });
    }
}
UiFilterTagsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-filter-tags',
                template: `<label *ngFor="let label of viewModel.items; let labelIndex = index;"
        class="ui-filter-tags__label">
        {{label.text}}
        <i class="material-icons ui-filter-tags__icon"
            (click)="onLabelRemovedClick(labelIndex)">clear</i>
</label>
`
            },] },
];
/** @nocollapse */
UiFilterTagsComponent.ctorParameters = () => [];
UiFilterTagsComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onLabelRemoved": [{ type: Output },],
};
function UiFilterTagsComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiFilterTagsComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiFilterTagsComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    UiFilterTagsComponent.propDecorators;
    /**
     * Property that defines the viewModel to be rendered by ui-filter-tags *
     * @type {?}
     */
    UiFilterTagsComponent.prototype.viewModel;
    /**
     * Property that sould be used for binding a callback to receive the
     * input submission *
     * @type {?}
     */
    UiFilterTagsComponent.prototype.onLabelRemoved;
}
//# sourceMappingURL=ui-filter-tags.component.js.map