/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component } from '@angular/core';
export class UiLoaderComponent {
    constructor() { }
}
UiLoaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-loader',
                template: `<div class="ui-spinner__out">
    <div class="ui-spinner__in"></div>
</div>
`
            },] },
];
/** @nocollapse */
UiLoaderComponent.ctorParameters = () => [];
function UiLoaderComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiLoaderComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiLoaderComponent.ctorParameters;
}
//# sourceMappingURL=ui-loader.component.js.map