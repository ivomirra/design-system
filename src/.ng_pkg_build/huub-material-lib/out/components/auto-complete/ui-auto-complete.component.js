/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { filter, extend } from 'lodash';
export class UiAutoCompleteComponent {
    constructor() {
        this.searchString = '';
        this.selectedOptions = [];
        this.processedOptionsList = [];
        this.showOptionsList = false;
        this.filterTagsViewModel = {
            items: []
        };
        this.optionsList = [];
        this.multiple = true;
        this.defaultValues = [];
        this.hasError = false;
        this.disabled = false;
        this.onItemsChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.processedOptionsList = this.optionsList;
        this.selectedOptions = this.defaultValues;
        if (this.multiple) {
            this.filterTagsViewModel.items = extend([], this.selectedOptions);
        }
        if (!this.multiple) {
            this.searchString =
                this.defaultValues.length === 1
                    ? this.defaultValues[0].text
                    : '';
        }
    }
    /**
     * @return {?}
     */
    showOptionsMenu() {
        this.showOptionsList = true;
    }
    /**
     * @return {?}
     */
    hideOptionsMenu() {
        this.showOptionsList = false;
    }
    /**
     * @param {?} searchString
     * @return {?}
     */
    searchValue(searchString) {
        this.processedOptionsList = filter(this.optionsList, function (item) {
            return item.text.toLowerCase().includes(searchString.toLowerCase());
        });
    }
    /**
     * @return {?}
     */
    clearSearch() {
        this.searchString = '';
        if (!this.multiple) {
            this.selectedOptions.length = 0;
            this.emitUpdates();
        }
    }
    /**
     * @return {?}
     */
    emitUpdates() {
        this.onItemsChanged.emit({
            selectedItems: this.selectedOptions
        });
    }
    /**
     * @param {?} filterData
     * @return {?}
     */
    onFilterTagRemoved(filterData) {
        this.selectedOptions.splice(filterData.removedIndex, 1);
        this.emitUpdates();
    }
    /**
     * @param {?} optionSelected
     * @return {?}
     */
    onOptionSelected(optionSelected) {
        this.hideOptionsMenu();
        if (this.multiple) {
            this.clearSearch();
            this.ngOnInit();
            this.selectedOptions.push(optionSelected);
            this.filterTagsViewModel.items.push(optionSelected);
        }
        else {
            this.searchString = optionSelected.text;
            this.selectedOptions[0] = optionSelected;
        }
        this.emitUpdates();
    }
}
UiAutoCompleteComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-auto-complete',
                template: `<div class="ui-autocomplete"
    (mouseleave)="hideOptionsMenu()">
    <input type="text"
           class="ui-autocomplete__textbox"
           [attr.placeholder]="placeholder"
           (keyup)="searchValue(searchString)"
           (focus)="showOptionsMenu()"
           [(ngModel)]="searchString"
           [ngClass]="{'form-field__input--error': hasError}"
           [disabled]="disabled">

    <button *ngIf="searchString !== ''"
            class="ui-autocomplete__clear"
            (click)="clearSearch()">
        <i class="material-icons">close</i>
    </button>
    <ui-filter-tags *ngIf="selectedOptions.length > 0"
                    class="ui-autocomplete__filter-tags"
                    (onLabelRemoved)="onFilterTagRemoved($event)"
                    [viewModel]="filterTagsViewModel"></ui-filter-tags>
    <div class="ui-autocomplete__selectbox"
         [ngClass]="{'ui-autocomplete__selectbox--visible': showOptionsList}">
        <div class="ui-autocomplete__option"
             *ngFor="let option of processedOptionsList"
             (click)="onOptionSelected(option)">{{option.text}}</div>
    </div>
</div>
`
            },] },
];
/** @nocollapse */
UiAutoCompleteComponent.ctorParameters = () => [];
UiAutoCompleteComponent.propDecorators = {
    "optionsList": [{ type: Input },],
    "multiple": [{ type: Input },],
    "defaultValues": [{ type: Input },],
    "placeholder": [{ type: Input },],
    "hasError": [{ type: Input },],
    "disabled": [{ type: Input },],
    "onItemsChanged": [{ type: Output },],
};
function UiAutoCompleteComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiAutoCompleteComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiAutoCompleteComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    UiAutoCompleteComponent.propDecorators;
    /** @type {?} */
    UiAutoCompleteComponent.prototype.searchString;
    /** @type {?} */
    UiAutoCompleteComponent.prototype.selectedOptions;
    /** @type {?} */
    UiAutoCompleteComponent.prototype.processedOptionsList;
    /** @type {?} */
    UiAutoCompleteComponent.prototype.showOptionsList;
    /** @type {?} */
    UiAutoCompleteComponent.prototype.filterTagsViewModel;
    /** @type {?} */
    UiAutoCompleteComponent.prototype.optionsList;
    /** @type {?} */
    UiAutoCompleteComponent.prototype.multiple;
    /** @type {?} */
    UiAutoCompleteComponent.prototype.defaultValues;
    /** @type {?} */
    UiAutoCompleteComponent.prototype.placeholder;
    /** @type {?} */
    UiAutoCompleteComponent.prototype.hasError;
    /** @type {?} */
    UiAutoCompleteComponent.prototype.disabled;
    /** @type {?} */
    UiAutoCompleteComponent.prototype.onItemsChanged;
}
//# sourceMappingURL=ui-auto-complete.component.js.map