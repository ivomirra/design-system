import { OnInit, EventEmitter } from '@angular/core';
export declare class UiAutoCompleteComponent implements OnInit {
    searchString: string;
    selectedOptions: any;
    processedOptionsList: any;
    showOptionsList: boolean;
    filterTagsViewModel: {
        items: any[];
    };
    private optionsList;
    private multiple;
    private defaultValues;
    placeholder: string;
    hasError: boolean;
    disabled: boolean;
    onItemsChanged: EventEmitter<{
        selectedItems: any[];
    }>;
    constructor();
    ngOnInit(): void;
    showOptionsMenu(): void;
    hideOptionsMenu(): void;
    searchValue(searchString: any): void;
    clearSearch(): void;
    emitUpdates(): void;
    onFilterTagRemoved(filterData: any): void;
    onOptionSelected(optionSelected: any): void;
}
