/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class UiPopupMessageComponent {
    constructor() { }
    /**
     * @return {?}
     */
    popupVisibility() {
        this.viewModel.isVisible = !this.viewModel.isVisible;
    }
    /**
     * @param {?} option
     * @return {?}
     */
    onButtonClicked(option) {
        if (!option.callback || typeof option.callback !== 'function') {
            return;
        }
        option.callback(option);
        this.popupVisibility();
    }
}
UiPopupMessageComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-popup-message',
                template: `<div class="ui-popup" *ngIf="viewModel.isVisible">
    <div class="ui-popup__overlay ui-popup__overlay--visible"
         (click)="popupVisibility()">
    </div>
    <div class="ui-popup__content">
        <div class="ui-popup__header">
            <h2 class="ui-popup__header-title">{{viewModel.title}}</h2>
        </div>
        <div class="ui-popup__body">
            {{viewModel.message}}
        </div>
        <div class="ui-popup__footer">
            <button *ngIf="viewModel.cancel.button_text!== ''"
                    type="flat"
                    class="ui-popup__cancel"
                    (click)="onButtonClicked(viewModel.cancel)">
                {{viewModel.cancel.button_text}}
            </button>
            <button  *ngIf="viewModel.submit.button_text!== ''"
                     type="secondary"
                     (click)="onButtonClicked(viewModel.submit)">
                {{viewModel.submit.button_text}}
            </button>
        </div>
    </div>
</div>
`
            },] },
];
/** @nocollapse */
UiPopupMessageComponent.ctorParameters = () => [];
UiPopupMessageComponent.propDecorators = {
    "viewModel": [{ type: Input },],
};
function UiPopupMessageComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiPopupMessageComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiPopupMessageComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    UiPopupMessageComponent.propDecorators;
    /** @type {?} */
    UiPopupMessageComponent.prototype.viewModel;
}
//# sourceMappingURL=ui-popup-message.component.js.map