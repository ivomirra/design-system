import { EventEmitter } from '@angular/core';
export declare class UiUploadImageComponent {
    constructor();
    viewModel: any;
    onFileChanged: EventEmitter<{
        selectedFile: any[];
    }>;
    emitUpload(uploadedFile: any): void;
}
