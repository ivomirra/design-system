/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
export class UiUploadImageComponent {
    constructor() {
        this.viewModel = {
            src: ''
        };
        this.onFileChanged = new EventEmitter();
    }
    /**
     * @param {?} uploadedFile
     * @return {?}
     */
    emitUpload(uploadedFile) {
        this.onFileChanged.emit({
            selectedFile: uploadedFile
        });
    }
}
UiUploadImageComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-upload-image',
                template: ` <div class="ui-upload-image__content__containers--upload">
    <div class="ui-upload-image__image-placeholder">
        <input type="file"
               class="ui-upload-image__image-uploader"
               (change)="emitUpload($event.target.files[0])">
        <i class="material-icons ui-upload-image__icon-upload"
           [ngClass]="{
                'ui-upload-image__icon-upload--image-selected': viewModel.src && viewModel.src !== ''
            }">file_upload</i>
        <img class="ui-upload-image__image"
             [attr.src]="viewModel.src">
    </div>
</div>
`
            },] },
];
/** @nocollapse */
UiUploadImageComponent.ctorParameters = () => [];
UiUploadImageComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onFileChanged": [{ type: Output },],
};
function UiUploadImageComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiUploadImageComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiUploadImageComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    UiUploadImageComponent.propDecorators;
    /** @type {?} */
    UiUploadImageComponent.prototype.viewModel;
    /** @type {?} */
    UiUploadImageComponent.prototype.onFileChanged;
}
//# sourceMappingURL=ui-upload-image.component.js.map