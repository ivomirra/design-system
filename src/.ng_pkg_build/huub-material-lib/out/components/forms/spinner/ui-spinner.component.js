/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
export class UiSpinnerComponent {
    constructor() {
        /**
         * Property that defines the input value *
         */
        this.allowNegatives = true;
        /**
         * Property that sould be used for binding a callback to receive the current input value
         * everytime it changes *
         */
        this.onValueChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (isNaN(this.value)) {
            throw new TypeError('The value defined should be a number');
        }
    }
    /**
     * @return {?}
     */
    onKeyUp() {
        this.onValueChanged.emit(this.value);
    }
    /**
     * @return {?}
     */
    onPlusButtonPressed() {
        this.value += 1;
        this.onValueChanged.emit(this.value);
    }
    /**
     * @return {?}
     */
    onLessButtonPressed() {
        this.value -= 1;
        if (!this.allowNegatives && this.value < 0) {
            this.value = 0;
            return;
        }
        this.onValueChanged.emit(this.value);
    }
}
UiSpinnerComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-spinner',
                template: `<input class="ui-spinner__input"
       #ctrl="ngModel"
       [(ngModel)]="value"
       [ngClass]="{'ui-spinner__input--error': hasError}"
       (keyup)="onKeyUp()"
       type="number"
       [attr.disabled]="disabled">
<button class="material-icons ui-spinner__button ui-spinner__button-less"
        (click)="onLessButtonPressed()"
        [attr.disabled]="disabled"></button>
<button class="material-icons ui-spinner__button ui-spinner__button-plus"
        (click)="onPlusButtonPressed()"
        [attr.disabled]="disabled"></button>
`
            },] },
];
/** @nocollapse */
UiSpinnerComponent.ctorParameters = () => [];
UiSpinnerComponent.propDecorators = {
    "allowNegatives": [{ type: Input },],
    "value": [{ type: Input },],
    "hasError": [{ type: Input },],
    "disabled": [{ type: Input },],
    "onValueChanged": [{ type: Output },],
};
function UiSpinnerComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiSpinnerComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiSpinnerComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    UiSpinnerComponent.propDecorators;
    /**
     * Property that defines the input value *
     * @type {?}
     */
    UiSpinnerComponent.prototype.allowNegatives;
    /**
     * Property that defines the input value *
     * @type {?}
     */
    UiSpinnerComponent.prototype.value;
    /**
     * Property that defines if the input has any error, when a true is provided the
     * input has a diferent styling beahvior *
     * @type {?}
     */
    UiSpinnerComponent.prototype.hasError;
    /**
     * Property that sets all the component as disabled *
     * @type {?}
     */
    UiSpinnerComponent.prototype.disabled;
    /**
     * Property that sould be used for binding a callback to receive the current input value
     * everytime it changes *
     * @type {?}
     */
    UiSpinnerComponent.prototype.onValueChanged;
}
//# sourceMappingURL=ui-spinner.component.js.map