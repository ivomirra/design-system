import { EventEmitter } from '@angular/core';
export declare class UiInputSearchComponent {
    viewModel: {
        /** Property that defines the input placeholder **/
        placeholder: string;
        /** Property that defines the input value **/
        value: string;
    };
    /** Property that sould be used for binding a callback to receive the
     input submission **/
    onSubmit: EventEmitter<string>;
    constructor();
    /**
     * Method binded to the form submit event.
     */
    onSubmitForm(): void;
}
