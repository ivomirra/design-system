/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
export class UiInputSearchComponent {
    constructor() {
        this.viewModel = {
            placeholder: '',
            value: ''
        };
        /**
         * Property that sould be used for binding a callback to receive the
         * input submission *
         */
        this.onSubmit = new EventEmitter();
    }
    /**
     * Method binded to the form submit event.
     * @return {?}
     */
    onSubmitForm() {
        this.onSubmit.emit(this.viewModel.value);
    }
}
UiInputSearchComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-input-search',
                template: `<form (submit)="onSubmitForm()" class="ui-input-search__form">
    <input type="text"
       class="ui-input-search"
       [attr.placeholder]="viewModel.placeholder"
       [(ngModel)]="viewModel.value"
       [ngModelOptions]="{standalone: true}">
    <i class="material-icons ui-input-search__icon"
        *ngIf="!viewModel.value || viewModel.value == ''">search</i>
</form>
`
            },] },
];
/** @nocollapse */
UiInputSearchComponent.ctorParameters = () => [];
UiInputSearchComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onSubmit": [{ type: Output },],
};
function UiInputSearchComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiInputSearchComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiInputSearchComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    UiInputSearchComponent.propDecorators;
    /** @type {?} */
    UiInputSearchComponent.prototype.viewModel;
    /**
     * Property that sould be used for binding a callback to receive the
     * input submission *
     * @type {?}
     */
    UiInputSearchComponent.prototype.onSubmit;
}
//# sourceMappingURL=ui-input-search.component.js.map