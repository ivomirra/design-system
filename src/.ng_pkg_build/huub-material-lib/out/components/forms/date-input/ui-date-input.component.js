/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as moment_ from 'moment';
const /** @type {?} */ moment = moment_;
export class UiDateInputComponent {
    constructor() {
        this.placeholder = '';
        this.dateFormat = '';
        this.onDateSelected = new EventEmitter();
        this.daysToShow = [];
        this.daysOfMonth = [];
        this.calendarIsVisible = false;
        this.isInvalidDate = false;
    }
    /**
     * @param {?} clickedDay
     * @return {?}
     */
    formatDate(clickedDay) {
        const /** @type {?} */ selectedDate = new Date(this.year, this.month, clickedDay);
        return moment(selectedDate).format(this.dateFormat);
    }
    /**
     * @return {?}
     */
    emitChanges() {
        this.onDateSelected.emit(this.selectedDate);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.dateFormat = this.dateFormat.toUpperCase();
        this.selectedDate = this.value
            ? moment(this.dateFormat).format(this.value)
            : '';
        this.currentDate = new Date();
        this.year = this.currentDate.getFullYear();
        this.month = this.currentDate.getMonth() + 1;
        this.buildCalendarDays();
    }
    /**
     * @return {?}
     */
    buildCalendarDays() {
        const /** @type {?} */ numberOfdays = new Date(this.year, this.month, 0).getDate();
        this.daysOfMonth = [];
        this.daysToShow = [];
        this.weekDayStartingMonth = new Date(this.year + '-' + this.month + '-01').getDay();
        for (let /** @type {?} */ w = 1; w <= this.weekDayStartingMonth; w++) {
            this.daysToShow.push('');
        }
        for (let /** @type {?} */ i = 1; i <= numberOfdays; i++) {
            if (this.daysToShow.length < 7) {
                this.daysToShow.push(i);
            }
            else {
                this.daysOfMonth.push(this.daysToShow);
                this.daysToShow = [i];
            }
            if (i === numberOfdays) {
                this.daysOfMonth.push(this.daysToShow);
            }
        }
    }
    /**
     * @return {?}
     */
    subtractMonth() {
        this.month = this.month - 1;
        if (this.month <= 0) {
            this.year = this.year - 1;
            this.month = 12;
        }
        this.buildCalendarDays();
    }
    /**
     * @return {?}
     */
    addMonth() {
        this.month = this.month + 1;
        if (this.month >= 13) {
            this.year = this.year + 1;
            this.month = 1;
        }
        this.buildCalendarDays();
    }
    /**
     * @return {?}
     */
    onInputChanged() {
        this.isInvalidDate = false;
        if (!moment(this.selectedDate, this.dateFormat, true).isValid()) {
            this.isInvalidDate = true;
            return;
        }
        this.emitChanges();
    }
    /**
     * @param {?} clickedDay
     * @return {?}
     */
    onSelectedDate(clickedDay) {
        this.selectedDate = this.formatDate(clickedDay);
        this.calendarToggleVisibility();
        this.emitChanges();
    }
    /**
     * @param {?} dayNumber
     * @return {?}
     */
    isSelectedDate(dayNumber) {
        if (this.selectedDate === '') {
            return false;
        }
        return this.selectedDate === this.formatDate(dayNumber);
    }
    /**
     * @return {?}
     */
    hideCalendar() {
        this.calendarIsVisible = false;
    }
    /**
     * @return {?}
     */
    calendarToggleVisibility() {
        this.calendarIsVisible = !this.calendarIsVisible;
    }
}
UiDateInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-date-input',
                template: `<div class="ui-date-picker"
     (mouseleave)="hideCalendar()">
    <input class="ui-date-picker__input"
           type="text"
           placeholder="{{placeholder}}"
           [disabled]="disable"
           [(ngModel)]="selectedDate"
           (keyup)="onInputChanged()"
           [ngClass]="{
                'form-field__input--error': isInvalidDate
            }">
    <button class="ui-date-picker__button"
            (click)="calendarToggleVisibility()"
            [disabled]="disable">
        <i class="material-icons">today</i>
    </button>

    <div class="ui-date-picker__calendar"
        *ngIf="calendarIsVisible">

        <div class="ui-date-picker__hideborders"
             (click)="calendarToggleVisibility()">
        </div>

        <div class="ui-date-picker__month-year">
            <button (click)="subtractMonth()">
                <i class="material-icons ui-date-picker__backward">
                    keyboard_arrow_left</i>
            </button>
            <div class="ui-date-picker__actual-month-year">
                {{monthNames[month-1]}} {{year}}
            </div>
            <button (click)="addMonth()">
                <i class="material-icons ui-date-picker__forward">
                    keyboard_arrow_right</i>
            </button>
        </div>

        <div class="ui-date-picker__days-list-container">
            <table class="ui-date-picker__days-list">
                <thead class="ui-date-picker__week-days">
                    <tr>
                        <td *ngFor="let weekday of weekDays">
                            {{weekday}}
                        </td>
                    </tr>
                </thead>
                <tbody class="ui-date-picker__days">
                    <tr class="ui-date-picker__days-line"
                        *ngFor="let dayOfMonthSet of daysOfMonth; let i = index">
                       <td *ngFor="let dayNumber of daysOfMonth[i]">
                            <button class="ui-date-picker__day"
                                 *ngIf="dayNumber !== ''"
                                 [ngClass]="{
                                    'ui-date-picker__day--selected': isSelectedDate(dayNumber)
                                  }"
                                 (click)="onSelectedDate(dayNumber)">
                             {{dayNumber}}
                            </button>
                       </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


`
            },] },
];
/** @nocollapse */
UiDateInputComponent.ctorParameters = () => [];
UiDateInputComponent.propDecorators = {
    "disable": [{ type: Input },],
    "value": [{ type: Input },],
    "placeholder": [{ type: Input },],
    "dateFormat": [{ type: Input },],
    "monthNames": [{ type: Input },],
    "weekDays": [{ type: Input },],
    "onDateSelected": [{ type: Output },],
};
function UiDateInputComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiDateInputComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiDateInputComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    UiDateInputComponent.propDecorators;
    /** @type {?} */
    UiDateInputComponent.prototype.disable;
    /** @type {?} */
    UiDateInputComponent.prototype.value;
    /** @type {?} */
    UiDateInputComponent.prototype.placeholder;
    /** @type {?} */
    UiDateInputComponent.prototype.dateFormat;
    /** @type {?} */
    UiDateInputComponent.prototype.monthNames;
    /** @type {?} */
    UiDateInputComponent.prototype.weekDays;
    /** @type {?} */
    UiDateInputComponent.prototype.onDateSelected;
    /** @type {?} */
    UiDateInputComponent.prototype.currentDate;
    /** @type {?} */
    UiDateInputComponent.prototype.weekDayStartingMonth;
    /** @type {?} */
    UiDateInputComponent.prototype.daysToShow;
    /** @type {?} */
    UiDateInputComponent.prototype.year;
    /** @type {?} */
    UiDateInputComponent.prototype.month;
    /** @type {?} */
    UiDateInputComponent.prototype.daysOfMonth;
    /** @type {?} */
    UiDateInputComponent.prototype.selectedDate;
    /** @type {?} */
    UiDateInputComponent.prototype.calendarIsVisible;
    /** @type {?} */
    UiDateInputComponent.prototype.isInvalidDate;
}
//# sourceMappingURL=ui-date-input.component.js.map