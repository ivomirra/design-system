/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Overlay, OverlayConfig } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
export class UiModalService {
    /**
     * @param {?} overlay
     */
    constructor(overlay) {
        this.overlay = overlay;
    }
    /**
     * @param {?=} context
     * @return {?}
     */
    disposeModal(context) {
        this.overlayRef.dispose();
        this.modalSubject.next(context || null);
        this.modalSubject.unsubscribe();
    }
    /**
     * @param {?} component
     * @param {?=} backDropCallCallback
     * @return {?}
     */
    openModal(component, backDropCallCallback) {
        const /** @type {?} */ positionStrategy = this.overlay
            .position()
            .global()
            .centerHorizontally()
            .centerVertically();
        this.modalSubject = new Subject();
        /** Creates overlay */
        this.overlayRef = this.overlay.create(new OverlayConfig({
            hasBackdrop: true,
            scrollStrategy: this.overlay.scrollStrategies.block(),
            positionStrategy
        }));
        /** Appends component to overlay */
        this.overlayRef.attach(new ComponentPortal(component, null, null));
        /** Appends subscribes overlay close */
        this.overlayRef.backdropClick().subscribe(() => {
            if (backDropCallCallback) {
                backDropCallCallback();
                return;
            }
            this.disposeModal();
        });
        return {
            afterClose: () => {
                return this.modalSubject.asObservable();
            }
        };
    }
    /**
     * @param {?} data
     * @return {?}
     */
    close(data) {
        this.disposeModal(data);
    }
}
UiModalService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
UiModalService.ctorParameters = () => [
    { type: Overlay, },
];
function UiModalService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiModalService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiModalService.ctorParameters;
    /** @type {?} */
    UiModalService.prototype.modalSubject;
    /** @type {?} */
    UiModalService.prototype.overlayRef;
    /** @type {?} */
    UiModalService.prototype.overlay;
}
//# sourceMappingURL=ui-modal.service.js.map