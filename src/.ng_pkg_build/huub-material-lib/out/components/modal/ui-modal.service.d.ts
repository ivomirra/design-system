import { Overlay } from '@angular/cdk/overlay';
export declare class UiModalService {
    private overlay;
    private modalSubject;
    private overlayRef;
    constructor(overlay: Overlay);
    private disposeModal(context?);
    openModal(component: any, backDropCallCallback?: any): {
        afterClose: () => any;
    };
    close(data: any): void;
}
