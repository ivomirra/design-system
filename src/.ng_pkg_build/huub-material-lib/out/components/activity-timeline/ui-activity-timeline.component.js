/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class UiActivityTimelineComponent {
    constructor() { }
}
UiActivityTimelineComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-activity-timeline',
                template: `<div *ngFor="let statusItem of viewModel.statusList; let statusIndex = index;"
     class="ui-activity-timeline__group-status">
    <div class="ui-activity-timeline__status-date">{{statusItem.date}}</div>
    <div class="material-icons ui-activity-timeline__status-bar"
         [ngClass]="{
            'ui-activity-timeline__status-bar--open': statusItem.open,
            'ui-activity-timeline__status-bar--close': !statusItem.open,
            'ui-activity-timeline__status-bar--last': (statusIndex === 0)
         }"></div>
    <div class="ui-activity-timeline__status-name">{{statusItem.name}}</div>
</div>
`
            },] },
];
/** @nocollapse */
UiActivityTimelineComponent.ctorParameters = () => [];
UiActivityTimelineComponent.propDecorators = {
    "viewModel": [{ type: Input },],
};
function UiActivityTimelineComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiActivityTimelineComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiActivityTimelineComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    UiActivityTimelineComponent.propDecorators;
    /**
     * Property that defines the viewModel to be rendered by ui-activity-timeline *
     * @type {?}
     */
    UiActivityTimelineComponent.prototype.viewModel;
}
//# sourceMappingURL=ui-activity-timeline.component.js.map