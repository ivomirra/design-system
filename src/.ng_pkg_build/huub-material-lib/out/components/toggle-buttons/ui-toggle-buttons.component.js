/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
export class UiToggleButtonsComponent {
    constructor() {
        /**
         * Property that sould be used for binding a callback to receive the
         * button change event *
         */
        this.onButtonChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.viewModel.selectedIndex = this.viewModel.selectedIndex || 0;
    }
    /**
     * Method binded for the button toggle change action.
     * toggleButtonIndex Current tab index clicked
     * @param {?} toggleButtonIndex
     * @return {?}
     */
    onToggleButtonClicked(toggleButtonIndex) {
        if (!this.viewModel.items[toggleButtonIndex]) {
            return;
        }
        this.viewModel.selectedIndex = toggleButtonIndex;
        this.onButtonChanged.emit({
            toggleButtonSelected: this.viewModel.items[toggleButtonIndex],
            toggleButtonIndexSelected: toggleButtonIndex
        });
    }
}
UiToggleButtonsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-toggle-buttons',
                template: `<nav class="ui-toggle-buttons__nav">
    <button *ngFor="let toggleButton of viewModel.items; let toggleIndex = index;"
            [ngClass]="{
                'ui-toggle-buttons__button': !(viewModel.selectedIndex === toggleIndex),
                'ui-toggle-buttons__button--active': viewModel.selectedIndex === toggleIndex
            }"
            [attr.disabled]="toggleButton.disabled"
            (click)="onToggleButtonClicked(toggleIndex)">{{toggleButton.text}}</button>
</nav>
`
            },] },
];
/** @nocollapse */
UiToggleButtonsComponent.ctorParameters = () => [];
UiToggleButtonsComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onButtonChanged": [{ type: Output },],
};
function UiToggleButtonsComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiToggleButtonsComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiToggleButtonsComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    UiToggleButtonsComponent.propDecorators;
    /**
     * Property that defines the viewModel to be rendered by ui-toggle-buttons *
     * @type {?}
     */
    UiToggleButtonsComponent.prototype.viewModel;
    /**
     * Property that sould be used for binding a callback to receive the
     * button change event *
     * @type {?}
     */
    UiToggleButtonsComponent.prototype.onButtonChanged;
}
//# sourceMappingURL=ui-toggle-buttons.component.js.map