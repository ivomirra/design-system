/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, ElementRef } from '@angular/core';
import { MaterialHelper } from '../../helpers/material.helpers';
export class UiTooltipsComponent {
    /**
     * @param {?} element
     * @param {?} helper
     */
    constructor(element, helper) {
        this.element = element;
        this.helper = helper;
        this.ELEMENT_SIZE = 200;
        this.alignLeft = false;
        this.alignRight = false;
    }
    /**
     * @return {?}
     */
    onTooltipShow() {
        const /** @type {?} */ absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, this.ELEMENT_SIZE);
        if (absoluteRenderConfig.canRenderToRight &&
            absoluteRenderConfig.canRenderToLeft) {
            return;
        }
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
    }
}
UiTooltipsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-tooltips',
                template: `<div class="ui-tooltip" (mouseover)="onTooltipShow()">
    <i class="material-icons ui-tooltip__icon">info</i>
    <div class="ui-tooltip__arrow"></div>
    <div class="ui-tooltip__container"
        [ngClass]="{
            'ui-tooltip__container--left': alignLeft,
            'ui-tooltip__container--right': alignRight,
            'ui-tooltip__container--center': alignLeft && alignRight
        }">
        <div class="ui-tooltip__title">
            {{viewModel.title}}
        </div>
        <div class="ui-tooltip__message">
            {{viewModel.text}}
        </div>
    </div>
</div>
`
            },] },
];
/** @nocollapse */
UiTooltipsComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: MaterialHelper, },
];
UiTooltipsComponent.propDecorators = {
    "viewModel": [{ type: Input },],
};
function UiTooltipsComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiTooltipsComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiTooltipsComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    UiTooltipsComponent.propDecorators;
    /** @type {?} */
    UiTooltipsComponent.prototype.viewModel;
    /** @type {?} */
    UiTooltipsComponent.prototype.ELEMENT_SIZE;
    /** @type {?} */
    UiTooltipsComponent.prototype.alignLeft;
    /** @type {?} */
    UiTooltipsComponent.prototype.alignRight;
    /** @type {?} */
    UiTooltipsComponent.prototype.element;
    /** @type {?} */
    UiTooltipsComponent.prototype.helper;
}
//# sourceMappingURL=ui-tooltips.component.js.map