import { ElementRef } from '@angular/core';
import { MaterialHelper } from '../../helpers/material.helpers';
export declare class UiTooltipsComponent {
    private element;
    private helper;
    viewModel: {
        title?: string;
        text?: string;
    };
    private ELEMENT_SIZE;
    alignLeft: boolean;
    alignRight: boolean;
    constructor(element: ElementRef, helper: MaterialHelper);
    onTooltipShow(): void;
}
