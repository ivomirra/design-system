/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
export class UiMenuBarComponent {
    constructor() {
        /**
         * Property that sould be used for binding a callback to receive the nav item click *
         */
        this.onNavItemClicked = new EventEmitter();
        /**
         * Property that sould be used for binding a callback to receive the user icons click *
         */
        this.onUserIconsClicked = new EventEmitter();
        /**
         * Property that sould be used for binding a callback to receive the user action click *
         */
        this.onUserActionsClicked = new EventEmitter();
    }
    /**
     * @param {?} selectedItem
     * @param {?} selectedItemIndex
     * @return {?}
     */
    onMenuItemClicked(selectedItem, selectedItemIndex) {
        if (selectedItem.disabled) {
            return;
        }
        this.viewModel.navigation.selectedIndex = selectedItemIndex;
        this.onNavItemClicked.emit({
            selectedItem,
            selectedItemIndex
        });
    }
    /**
     * @param {?} icon
     * @param {?} iconsIndex
     * @return {?}
     */
    onIconsClicked(icon, iconsIndex) {
        this.onUserIconsClicked.emit({
            icon,
            iconsIndex
        });
    }
    /**
     * @param {?} action
     * @param {?} actionIndex
     * @return {?}
     */
    onActionsClicked(action, actionIndex) {
        this.onUserActionsClicked.emit({
            action,
            actionIndex
        });
    }
}
UiMenuBarComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-menu-bar',
                template: `<header class="ui-menu-bar__container">
    <div class="ui-menu-bar__logo">
        <img *ngIf="viewModel.logo"
             class="ui-menu-bar__logo-image"
             [attr.src]="viewModel.logo">
        <span *ngIf="!viewModel.logo"
              class="material-icons ui-menu-bar__logo--error">error</span>
    </div>
    <nav class="ui-menu-bar__nav">
        <a *ngFor="let menuItem of viewModel.navigation.items; let menuItemIndex = index;"
            (click)="onMenuItemClicked(menuItem, menuItemIndex)"
            class="ui-menu-bar__nav-item"
            [ngClass]="{
                'ui-menu-bar__nav-item--active': viewModel.navigation.selectedIndex === menuItemIndex,
                'ui-menu-bar__nav-item--disabled': menuItem.disabled
            }">{{menuItem.text}}</a>
    </nav>
    <div class="ui-menu-bar__user-data">
        <span *ngFor="let icon of viewModel.userData.icons; let iconsIndex = index;"
              (click)="onIconsClicked(icon, iconsIndex)">
            <i class="material-icons">{{icon.text}}</i>
        </span>
        <img *ngIf="viewModel.userData.photo"
             [attr.src]="viewModel.userData.photo"
             class="ui-menu-bar__photo">
        <span *ngIf="!viewModel.userData.photo"
              class="material-icons ui-menu-bar__photo--error">account_circle</span>
        <div class="ui-menu-bar__user-identification">
            <span>
                {{viewModel.userData.translatedText}},
                <strong>{{viewModel.userData.name}}</strong>
            </span>
            <i class="material-icons ui-menu-bar__user-identification-arrow">keyboard_arrow_down</i>
            <nav *ngIf="viewModel.userData.listActions.length > 0"
                 class="ui-menu-bar__user-identification-nav">
                <a *ngFor="let action of viewModel.userData.listActions; let actionIndex = index;"
                    (click)="onActionsClicked(action, actionIndex)"
                    class="ui-menu-bar__user-identification-nav-item">{{action.text}}</a>
            </nav>
        </div>
    </div>
</header>
`
            },] },
];
/** @nocollapse */
UiMenuBarComponent.ctorParameters = () => [];
UiMenuBarComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onNavItemClicked": [{ type: Output },],
    "onUserIconsClicked": [{ type: Output },],
    "onUserActionsClicked": [{ type: Output },],
};
function UiMenuBarComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UiMenuBarComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UiMenuBarComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    UiMenuBarComponent.propDecorators;
    /**
     * Property that defines the viewModel to be rendered by ui-menu-bar *
     * @type {?}
     */
    UiMenuBarComponent.prototype.viewModel;
    /**
     * Property that sould be used for binding a callback to receive the nav item click *
     * @type {?}
     */
    UiMenuBarComponent.prototype.onNavItemClicked;
    /**
     * Property that sould be used for binding a callback to receive the user icons click *
     * @type {?}
     */
    UiMenuBarComponent.prototype.onUserIconsClicked;
    /**
     * Property that sould be used for binding a callback to receive the user action click *
     * @type {?}
     */
    UiMenuBarComponent.prototype.onUserActionsClicked;
}
//# sourceMappingURL=ui-menu-bar.component.js.map