import { EventEmitter } from '@angular/core';
export declare class UiMenuBarComponent {
    /** Property that defines the viewModel to be rendered by ui-menu-bar **/
    viewModel: {
        navigation: {
            items: object[];
            selectedIndex: number;
        };
        logo: string;
        userData: {
            listActions: object[];
            icons: object[];
            photo: string;
            translatedText: string;
            name: string;
        };
    };
    /** Property that sould be used for binding a callback to receive the nav item click **/
    onNavItemClicked: EventEmitter<{
        selectedItem: object;
        selectedItemIndex: number;
    }>;
    /** Property that sould be used for binding a callback to receive the user icons click **/
    onUserIconsClicked: EventEmitter<{
        icon: object;
        iconsIndex: number;
    }>;
    /** Property that sould be used for binding a callback to receive the user action click **/
    onUserActionsClicked: EventEmitter<{
        action: object;
        actionIndex: number;
    }>;
    constructor();
    onMenuItemClicked(selectedItem: any, selectedItemIndex: number): void;
    onIconsClicked(icon: object, iconsIndex: number): void;
    onActionsClicked(action: object, actionIndex: number): void;
}
