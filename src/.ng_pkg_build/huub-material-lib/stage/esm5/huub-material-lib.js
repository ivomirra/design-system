import { Injectable, Component, Input, Output, EventEmitter, ElementRef, NgModule } from '@angular/core';
import { Subject } from 'rxjs';
import { Overlay, OverlayConfig, OverlayModule } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { find, sortBy, filter, extend } from 'lodash';
import * as moment_ from 'moment';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

var MaterialHelper = /** @class */ (function () {
    function MaterialHelper() {
    }
    MaterialHelper.prototype.getAbsolutePositionRenderConfig = function (elementOffSet, elementSize) {
        return {
            canRenderToRight: window.innerWidth > elementOffSet + elementSize,
            canRenderToLeft: elementOffSet - elementSize > 0
        };
    };
    return MaterialHelper;
}());
MaterialHelper.decorators = [
    { type: Injectable },
];
MaterialHelper.ctorParameters = function () { return []; };
var UiModalService = /** @class */ (function () {
    function UiModalService(overlay) {
        this.overlay = overlay;
    }
    UiModalService.prototype.disposeModal = function (context) {
        this.overlayRef.dispose();
        this.modalSubject.next(context || null);
        this.modalSubject.unsubscribe();
    };
    UiModalService.prototype.openModal = function (component, backDropCallCallback) {
        var _this = this;
        var positionStrategy = this.overlay
            .position()
            .global()
            .centerHorizontally()
            .centerVertically();
        this.modalSubject = new Subject();
        this.overlayRef = this.overlay.create(new OverlayConfig({
            hasBackdrop: true,
            scrollStrategy: this.overlay.scrollStrategies.block(),
            positionStrategy: positionStrategy
        }));
        this.overlayRef.attach(new ComponentPortal(component, null, null));
        this.overlayRef.backdropClick().subscribe(function () {
            if (backDropCallCallback) {
                backDropCallCallback();
                return;
            }
            _this.disposeModal();
        });
        return {
            afterClose: function () {
                return _this.modalSubject.asObservable();
            }
        };
    };
    UiModalService.prototype.close = function (data) {
        this.disposeModal(data);
    };
    return UiModalService;
}());
UiModalService.decorators = [
    { type: Injectable },
];
UiModalService.ctorParameters = function () { return [
    { type: Overlay, },
]; };
var UiToggleButtonsComponent = /** @class */ (function () {
    function UiToggleButtonsComponent() {
        this.onButtonChanged = new EventEmitter();
    }
    UiToggleButtonsComponent.prototype.ngOnInit = function () {
        this.viewModel.selectedIndex = this.viewModel.selectedIndex || 0;
    };
    UiToggleButtonsComponent.prototype.onToggleButtonClicked = function (toggleButtonIndex) {
        if (!this.viewModel.items[toggleButtonIndex]) {
            return;
        }
        this.viewModel.selectedIndex = toggleButtonIndex;
        this.onButtonChanged.emit({
            toggleButtonSelected: this.viewModel.items[toggleButtonIndex],
            toggleButtonIndexSelected: toggleButtonIndex
        });
    };
    return UiToggleButtonsComponent;
}());
UiToggleButtonsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-toggle-buttons',
                template: "<nav class=\"ui-toggle-buttons__nav\">\n    <button *ngFor=\"let toggleButton of viewModel.items; let toggleIndex = index;\"\n            [ngClass]=\"{\n                'ui-toggle-buttons__button': !(viewModel.selectedIndex === toggleIndex),\n                'ui-toggle-buttons__button--active': viewModel.selectedIndex === toggleIndex\n            }\"\n            [attr.disabled]=\"toggleButton.disabled\"\n            (click)=\"onToggleButtonClicked(toggleIndex)\">{{toggleButton.text}}</button>\n</nav>\n"
            },] },
];
UiToggleButtonsComponent.ctorParameters = function () { return []; };
UiToggleButtonsComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onButtonChanged": [{ type: Output },],
};
var UiTabsComponent = /** @class */ (function () {
    function UiTabsComponent() {
        this.onTabChanged = new EventEmitter();
    }
    UiTabsComponent.prototype.ngOnInit = function () {
        this.viewModel.selectedIndex = this.viewModel.selectedIndex || 0;
    };
    UiTabsComponent.prototype.onTabClicked = function (selectedTab, selectedTabIndex) {
        if (selectedTab.disabled) {
            return;
        }
        this.onTabChanged.emit({
            selectedTab: selectedTab,
            selectedTabIndex: selectedTabIndex
        });
        this.viewModel.selectedIndex = selectedTabIndex;
    };
    return UiTabsComponent;
}());
UiTabsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-tabs',
                template: "<nav class=\"ui-tabs__nav\">\n    <ul class=\"ui-tabs__nav-items\">\n        <li *ngFor=\"let item of viewModel.items; let itemIndex = index;\"\n            [ngClass]=\"{\n                'ui-tabs__nav-item': !(itemIndex === viewModel.selectedIndex) && !item.disabled,\n                'ui-tabs__nav-item--active': itemIndex === viewModel.selectedIndex,\n                'ui-tabs__nav-item--disabled': item.disabled\n            }\">\n            <a (click)=\"onTabClicked(item, itemIndex)\" class=\"ui-tabs__nav-link\">\n                {{item.text}}\n            </a>\n        </li>\n    </ul>\n</nav>\n"
            },] },
];
UiTabsComponent.ctorParameters = function () { return []; };
UiTabsComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onTabChanged": [{ type: Output },],
};
var UiMenuBarComponent = /** @class */ (function () {
    function UiMenuBarComponent() {
        this.onNavItemClicked = new EventEmitter();
        this.onUserIconsClicked = new EventEmitter();
        this.onUserActionsClicked = new EventEmitter();
    }
    UiMenuBarComponent.prototype.onMenuItemClicked = function (selectedItem, selectedItemIndex) {
        if (selectedItem.disabled) {
            return;
        }
        this.viewModel.navigation.selectedIndex = selectedItemIndex;
        this.onNavItemClicked.emit({
            selectedItem: selectedItem,
            selectedItemIndex: selectedItemIndex
        });
    };
    UiMenuBarComponent.prototype.onIconsClicked = function (icon, iconsIndex) {
        this.onUserIconsClicked.emit({
            icon: icon,
            iconsIndex: iconsIndex
        });
    };
    UiMenuBarComponent.prototype.onActionsClicked = function (action, actionIndex) {
        this.onUserActionsClicked.emit({
            action: action,
            actionIndex: actionIndex
        });
    };
    return UiMenuBarComponent;
}());
UiMenuBarComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-menu-bar',
                template: "<header class=\"ui-menu-bar__container\">\n    <div class=\"ui-menu-bar__logo\">\n        <img *ngIf=\"viewModel.logo\"\n             class=\"ui-menu-bar__logo-image\"\n             [attr.src]=\"viewModel.logo\">\n        <span *ngIf=\"!viewModel.logo\"\n              class=\"material-icons ui-menu-bar__logo--error\">error</span>\n    </div>\n    <nav class=\"ui-menu-bar__nav\">\n        <a *ngFor=\"let menuItem of viewModel.navigation.items; let menuItemIndex = index;\"\n            (click)=\"onMenuItemClicked(menuItem, menuItemIndex)\"\n            class=\"ui-menu-bar__nav-item\"\n            [ngClass]=\"{\n                'ui-menu-bar__nav-item--active': viewModel.navigation.selectedIndex === menuItemIndex,\n                'ui-menu-bar__nav-item--disabled': menuItem.disabled\n            }\">{{menuItem.text}}</a>\n    </nav>\n    <div class=\"ui-menu-bar__user-data\">\n        <span *ngFor=\"let icon of viewModel.userData.icons; let iconsIndex = index;\"\n              (click)=\"onIconsClicked(icon, iconsIndex)\">\n            <i class=\"material-icons\">{{icon.text}}</i>\n        </span>\n        <img *ngIf=\"viewModel.userData.photo\"\n             [attr.src]=\"viewModel.userData.photo\"\n             class=\"ui-menu-bar__photo\">\n        <span *ngIf=\"!viewModel.userData.photo\"\n              class=\"material-icons ui-menu-bar__photo--error\">account_circle</span>\n        <div class=\"ui-menu-bar__user-identification\">\n            <span>\n                {{viewModel.userData.translatedText}},\n                <strong>{{viewModel.userData.name}}</strong>\n            </span>\n            <i class=\"material-icons ui-menu-bar__user-identification-arrow\">keyboard_arrow_down</i>\n            <nav *ngIf=\"viewModel.userData.listActions.length > 0\"\n                 class=\"ui-menu-bar__user-identification-nav\">\n                <a *ngFor=\"let action of viewModel.userData.listActions; let actionIndex = index;\"\n                    (click)=\"onActionsClicked(action, actionIndex)\"\n                    class=\"ui-menu-bar__user-identification-nav-item\">{{action.text}}</a>\n            </nav>\n        </div>\n    </div>\n</header>\n"
            },] },
];
UiMenuBarComponent.ctorParameters = function () { return []; };
UiMenuBarComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onNavItemClicked": [{ type: Output },],
    "onUserIconsClicked": [{ type: Output },],
    "onUserActionsClicked": [{ type: Output },],
};
var UiFilterTagsComponent = /** @class */ (function () {
    function UiFilterTagsComponent() {
        this.onLabelRemoved = new EventEmitter();
    }
    UiFilterTagsComponent.prototype.onLabelRemovedClick = function (labelIndex) {
        if (!this.viewModel.items[labelIndex]) {
            return;
        }
        var removedLabel = this.viewModel.items[labelIndex];
        this.viewModel.items.splice(labelIndex, 1);
        this.onLabelRemoved.emit({
            removedLabel: removedLabel,
            removedIndex: labelIndex,
            items: this.viewModel.items
        });
    };
    return UiFilterTagsComponent;
}());
UiFilterTagsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-filter-tags',
                template: "<label *ngFor=\"let label of viewModel.items; let labelIndex = index;\"\n        class=\"ui-filter-tags__label\">\n        {{label.text}}\n        <i class=\"material-icons ui-filter-tags__icon\"\n            (click)=\"onLabelRemovedClick(labelIndex)\">clear</i>\n</label>\n"
            },] },
];
UiFilterTagsComponent.ctorParameters = function () { return []; };
UiFilterTagsComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onLabelRemoved": [{ type: Output },],
};
var UiFilterComponent = /** @class */ (function () {
    function UiFilterComponent(element, helper) {
        this.element = element;
        this.helper = helper;
        this.ELEMENT_SIZE = 350;
        this.alignRight = false;
        this.alignLeft = false;
        this.selectedOptions = [];
        this.optionContainerIsVisible = false;
        this.onSaveAction = new EventEmitter();
    }
    UiFilterComponent.prototype.trackByFn = function (index, item) {
        return index;
    };
    UiFilterComponent.prototype.toogleOptionContainerVisibility = function () {
        var absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, this.ELEMENT_SIZE);
        this.optionContainerIsVisible = !this.optionContainerIsVisible;
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
        this.selectedOptions.length = 0;
    };
    UiFilterComponent.prototype.onSaveButtonClick = function () {
        this.onSaveAction.emit({
            selectedOption: find(this.selectedOptions, { selected: true }) ||
                this.selectedOptions[0]
        });
    };
    UiFilterComponent.prototype.setSelectedOption = function (options, optionIndex) {
        options.forEach(function (item) {
            item.selected = false;
        });
        options[optionIndex].selected = true;
    };
    UiFilterComponent.prototype.onSelectChange = function (options, indexOption) {
        this.setSelectedOption(options, indexOption);
        this.selectedOptions =
            this.viewModel.selectOptions[this.viewModel.selectBox[indexOption].type] || [];
    };
    UiFilterComponent.prototype.onSecondarySelectChange = function (options, indexOption) {
        this.setSelectedOption(options, indexOption);
    };
    return UiFilterComponent;
}());
UiFilterComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-filter',
                template: "<button class=\"ui-filter__container\">\n    <i class=\"material-icons ui-filter__icon ui-filter__icon__tune\"\n       (click)=\"toogleOptionContainerVisibility()\">tune</i>\n    <span class=\"ui-filter__text\"\n          (click)=\"toogleOptionContainerVisibility()\">{{viewModel.filterText}}</span>\n    <i class=\"material-icons ui-filter__icon\"\n       (click)=\"toogleOptionContainerVisibility()\">keyboard_arrow_down</i>\n\n    <div class=\"ui-filter__options-container\"\n        [ngClass]=\"{\n            'ui-filter__options-container--visible': optionContainerIsVisible,\n            'ui-filter__options-container--right': alignRight,\n            'ui-filter__options-container--left': alignLeft\n        }\"\n        (mouseleave)=\"toogleOptionContainerVisibility()\">\n        <ng-container *ngIf=\"optionContainerIsVisible\">\n            <select class=\"ui-filter__select\"\n                    (change)=\"onSelectChange(viewModel.selectBox, $event.target.value)\">\n                <option *ngFor=\"let option of viewModel.selectBox; let index = index; trackBy: trackByFn\"\n                        value=\"{{index}}\">{{option.text}}</option>\n            </select>\n\n            <select *ngIf=\"selectedOptions.length > 0\"\n                    class=\"ui-filter__select\"\n                    (change)=\"onSecondarySelectChange(selectedOptions, $event.target.value)\">\n                <option *ngFor=\"let option of selectedOptions; let index = index; trackBy: trackByFn\"\n                        value=\"{{index}}\">{{option.text}}</option>\n            </select>\n        </ng-container>\n\n        <button type=\"secondary\" class=\"ui-filter__button\"\n                (click)=\"onSaveButtonClick()\">{{viewModel.buttonText}}</button>\n    </div>\n</button>\n"
            },] },
];
UiFilterComponent.ctorParameters = function () { return [
    { type: ElementRef, },
    { type: MaterialHelper, },
]; };
UiFilterComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onSaveAction": [{ type: Output },],
};
var UiActivityTimelineComponent = /** @class */ (function () {
    function UiActivityTimelineComponent() {
    }
    return UiActivityTimelineComponent;
}());
UiActivityTimelineComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-activity-timeline',
                template: "<div *ngFor=\"let statusItem of viewModel.statusList; let statusIndex = index;\"\n     class=\"ui-activity-timeline__group-status\">\n    <div class=\"ui-activity-timeline__status-date\">{{statusItem.date}}</div>\n    <div class=\"material-icons ui-activity-timeline__status-bar\"\n         [ngClass]=\"{\n            'ui-activity-timeline__status-bar--open': statusItem.open,\n            'ui-activity-timeline__status-bar--close': !statusItem.open,\n            'ui-activity-timeline__status-bar--last': (statusIndex === 0)\n         }\"></div>\n    <div class=\"ui-activity-timeline__status-name\">{{statusItem.name}}</div>\n</div>\n"
            },] },
];
UiActivityTimelineComponent.ctorParameters = function () { return []; };
UiActivityTimelineComponent.propDecorators = {
    "viewModel": [{ type: Input },],
};
var UiSpinnerComponent = /** @class */ (function () {
    function UiSpinnerComponent() {
        this.allowNegatives = true;
        this.onValueChanged = new EventEmitter();
    }
    UiSpinnerComponent.prototype.ngOnInit = function () {
        if (isNaN(this.value)) {
            throw new TypeError('The value defined should be a number');
        }
    };
    UiSpinnerComponent.prototype.onKeyUp = function () {
        this.onValueChanged.emit(this.value);
    };
    UiSpinnerComponent.prototype.onPlusButtonPressed = function () {
        this.value += 1;
        this.onValueChanged.emit(this.value);
    };
    UiSpinnerComponent.prototype.onLessButtonPressed = function () {
        this.value -= 1;
        if (!this.allowNegatives && this.value < 0) {
            this.value = 0;
            return;
        }
        this.onValueChanged.emit(this.value);
    };
    return UiSpinnerComponent;
}());
UiSpinnerComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-spinner',
                template: "<input class=\"ui-spinner__input\"\n       #ctrl=\"ngModel\"\n       [(ngModel)]=\"value\"\n       [ngClass]=\"{'ui-spinner__input--error': hasError}\"\n       (keyup)=\"onKeyUp()\"\n       type=\"number\"\n       [attr.disabled]=\"disabled\">\n<button class=\"material-icons ui-spinner__button ui-spinner__button-less\"\n        (click)=\"onLessButtonPressed()\"\n        [attr.disabled]=\"disabled\"></button>\n<button class=\"material-icons ui-spinner__button ui-spinner__button-plus\"\n        (click)=\"onPlusButtonPressed()\"\n        [attr.disabled]=\"disabled\"></button>\n"
            },] },
];
UiSpinnerComponent.ctorParameters = function () { return []; };
UiSpinnerComponent.propDecorators = {
    "allowNegatives": [{ type: Input },],
    "value": [{ type: Input },],
    "hasError": [{ type: Input },],
    "disabled": [{ type: Input },],
    "onValueChanged": [{ type: Output },],
};
var UiInputSearchComponent = /** @class */ (function () {
    function UiInputSearchComponent() {
        this.viewModel = {
            placeholder: '',
            value: ''
        };
        this.onSubmit = new EventEmitter();
    }
    UiInputSearchComponent.prototype.onSubmitForm = function () {
        this.onSubmit.emit(this.viewModel.value);
    };
    return UiInputSearchComponent;
}());
UiInputSearchComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-input-search',
                template: "<form (submit)=\"onSubmitForm()\" class=\"ui-input-search__form\">\n    <input type=\"text\"\n       class=\"ui-input-search\"\n       [attr.placeholder]=\"viewModel.placeholder\"\n       [(ngModel)]=\"viewModel.value\"\n       [ngModelOptions]=\"{standalone: true}\">\n    <i class=\"material-icons ui-input-search__icon\"\n        *ngIf=\"!viewModel.value || viewModel.value == ''\">search</i>\n</form>\n"
            },] },
];
UiInputSearchComponent.ctorParameters = function () { return []; };
UiInputSearchComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onSubmit": [{ type: Output },],
};
var UiTablesComponent = /** @class */ (function () {
    function UiTablesComponent() {
    }
    UiTablesComponent.prototype.sortBodyTable = function (headerItemIndex) {
        this.viewModel.tableBody = sortBy(this.viewModel.tableBody, function (o) { return o[headerItemIndex].text.toLowerCase() ||
            o[headerItemIndex].value; });
    };
    UiTablesComponent.prototype.resetAllTableSort = function (headerItemIndex) {
        this.viewModel.tableHeader.forEach(function (column, index) {
            if (headerItemIndex === index) {
                return;
            }
            column.sortBy = false;
        });
    };
    UiTablesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.viewModel.tableHeader.forEach(function (column, index) {
            if (column.sortBy) {
                _this.sortBodyTable(index);
            }
        });
    };
    UiTablesComponent.prototype.onColumnClicked = function (column) {
        if (!column.callback || typeof column.callback !== 'function') {
            return;
        }
        column.callback(column);
    };
    UiTablesComponent.prototype.onSortButtonClick = function (headerItemIndex) {
        this.sortBodyTable(headerItemIndex);
        this.viewModel.tableHeader[headerItemIndex].sortBy = !this.viewModel
            .tableHeader[headerItemIndex].sortBy;
        if (!this.viewModel.tableHeader[headerItemIndex].sortBy) {
            this.viewModel.tableBody.reverse();
        }
        this.resetAllTableSort(headerItemIndex);
    };
    return UiTablesComponent;
}());
UiTablesComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-tables',
                template: "<table class=\"ui-table__table\">\n    <thead>\n       <tr class=\"ui-table__tr\">\n            <th class=\"ui-table__th\"\n                *ngFor=\"let column of viewModel.tableHeader; let columnIndex = index;\">\n                <button *ngIf=\"column.text && column.text !== ''\"\n                        (click)=\"onSortButtonClick(columnIndex)\"\n                        class=\"ui-table__order-container\">\n                    <span>{{column.text}}</span>\n                    <i class=\"material-icons ui-table__order-icon\"\n                        [ngClass]=\"{\n                            'ui-table__order-icon--rotate': column.sortBy\n                        }\">keyboard_arrow_up</i>\n                </button>\n            </th>\n        </tr>\n    </thead>\n    <tbody>\n        <tr class=\"ui-table__tr ui-table__row\" *ngFor=\"let row of viewModel.tableBody\">\n            <td class=\"ui-table__td\"\n                *ngFor=\"let column of viewModel.tableHeader; let i = index\"\n                [ngSwitch]=\"row[i].type\">\n                    <input *ngSwitchCase=\"'checkbox'\"\n                            type=\"checkbox\"\n                            class=\"material-icons\"\n                            (click)=\"onColumnClicked(row[i])\"\n                            [ngModel]=\"row[i].value\">\n                    <span *ngSwitchCase=\"'string'\"\n                           (click)=\"onColumnClicked(row[i])\"\n                          [ngClass]=\"{\n                                'ui-table__text-bold': row[i].textBold\n                           }\">{{row[i].text}}</span>\n                    <a *ngSwitchCase=\"'link'\"\n                        (click)=\"onColumnClicked(row[i])\"\n                        class=\"ui-table__td-link\">{{row[i].text}}</a>\n                    <button *ngSwitchCase=\"'button'\"\n                            type=\"secondary\"\n                            [attr.color]=\"row[i].color\"\n                            (click)=\"onColumnClicked(row[i])\"\n                            >{{row[i].text}}</button>\n                    <ui-button-actions *ngSwitchCase=\"'button-actions'\"\n                                        [viewModel]=\"row[i].viewModel\"></ui-button-actions>\n                    <span *ngSwitchCase=\"'status'\"\n                         class=\"ui-table__status\"\n                         (click)=\"onColumnClicked(row[i])\"\n                         [ngClass]=\"{\n                           'ui-table__status--active': row[i].value}\"></span>\n            </td>\n        </tr>\n    </tbody>\n</table>\n\n"
            },] },
];
UiTablesComponent.ctorParameters = function () { return []; };
UiTablesComponent.propDecorators = {
    "viewModel": [{ type: Input },],
};
var UiTooltipsComponent = /** @class */ (function () {
    function UiTooltipsComponent(element, helper) {
        this.element = element;
        this.helper = helper;
        this.ELEMENT_SIZE = 200;
        this.alignLeft = false;
        this.alignRight = false;
    }
    UiTooltipsComponent.prototype.onTooltipShow = function () {
        var absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, this.ELEMENT_SIZE);
        if (absoluteRenderConfig.canRenderToRight &&
            absoluteRenderConfig.canRenderToLeft) {
            return;
        }
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
    };
    return UiTooltipsComponent;
}());
UiTooltipsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-tooltips',
                template: "<div class=\"ui-tooltip\" (mouseover)=\"onTooltipShow()\">\n    <i class=\"material-icons ui-tooltip__icon\">info</i>\n    <div class=\"ui-tooltip__arrow\"></div>\n    <div class=\"ui-tooltip__container\"\n        [ngClass]=\"{\n            'ui-tooltip__container--left': alignLeft,\n            'ui-tooltip__container--right': alignRight,\n            'ui-tooltip__container--center': alignLeft && alignRight\n        }\">\n        <div class=\"ui-tooltip__title\">\n            {{viewModel.title}}\n        </div>\n        <div class=\"ui-tooltip__message\">\n            {{viewModel.text}}\n        </div>\n    </div>\n</div>\n"
            },] },
];
UiTooltipsComponent.ctorParameters = function () { return [
    { type: ElementRef, },
    { type: MaterialHelper, },
]; };
UiTooltipsComponent.propDecorators = {
    "viewModel": [{ type: Input },],
};
var UiTablesPlaceholderComponent = /** @class */ (function () {
    function UiTablesPlaceholderComponent() {
    }
    return UiTablesPlaceholderComponent;
}());
UiTablesPlaceholderComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-tables-placeholder',
                template: "<section class=\"ui-table-placeholder hb-animation-mask\">\n    <div class=\"ui-table-placeholder__header\">\n        <div class=\"ui-table-placeholder__header-column\"></div>\n        <div class=\"ui-table-placeholder__header-column\"></div>\n        <div class=\"ui-table-placeholder__header-column\"></div>\n        <div class=\"ui-table-placeholder__header-column\"></div>\n    </div>\n\n    <div class=\"ui-table-placeholder__body\"></div>\n    <div class=\"ui-table-placeholder__body\"></div>\n    <div class=\"ui-table-placeholder__body\"></div>\n    <div class=\"ui-table-placeholder__body\"></div>\n</section>\n"
            },] },
];
UiTablesPlaceholderComponent.ctorParameters = function () { return []; };
var UiButtonActionsComponent = /** @class */ (function () {
    function UiButtonActionsComponent(element, helper) {
        this.element = element;
        this.helper = helper;
        this.ELEMENT_SIZE = 200;
        this.alignRight = false;
        this.alignLeft = false;
        this.type = 'flat';
        this.optionContainerIsVisible = false;
    }
    UiButtonActionsComponent.prototype.showOptionContainerVisibility = function () {
        var absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, this.ELEMENT_SIZE);
        this.optionContainerIsVisible = true;
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
    };
    UiButtonActionsComponent.prototype.hideOptionContainerVisibility = function () {
        this.optionContainerIsVisible = false;
    };
    UiButtonActionsComponent.prototype.onLinkClicked = function (link) {
        if (!link.callback || typeof link.callback !== 'function') {
            return;
        }
        link.callback(link);
    };
    return UiButtonActionsComponent;
}());
UiButtonActionsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-button-actions',
                template: "<div class=\"ui-button-actions\"\n    (mouseleave)=\"hideOptionContainerVisibility()\">\n    <button [attr.type]=\"type\"\n            role=\"icon\"\n            (click)=\"showOptionContainerVisibility()\"></button>\n    <div class=\"ui-button-actions__actions-list\"\n        [ngClass]=\"{\n            'ui-button-actions__actions-list--visible': optionContainerIsVisible,\n            'ui-button-actions__actions-list--right': alignRight,\n            'ui-button-actions__actions-list--left': alignLeft\n        }\"\n        (mouseleave)=\"hideOptionContainerVisibility()\">\n            <a *ngFor=\"let link of viewModel.items\"\n                class=\"ui-button-actions__actions-list-link\"\n                (click)=\"onLinkClicked(link)\">\n                {{link.text}}\n            </a>\n    </div>\n</div>\n"
            },] },
];
UiButtonActionsComponent.ctorParameters = function () { return [
    { type: ElementRef, },
    { type: MaterialHelper, },
]; };
UiButtonActionsComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "type": [{ type: Input },],
};
var UiLoaderComponent = /** @class */ (function () {
    function UiLoaderComponent() {
    }
    return UiLoaderComponent;
}());
UiLoaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-loader',
                template: "<div class=\"ui-spinner__out\">\n    <div class=\"ui-spinner__in\"></div>\n</div>\n"
            },] },
];
UiLoaderComponent.ctorParameters = function () { return []; };
var UiPopupMessageComponent = /** @class */ (function () {
    function UiPopupMessageComponent() {
    }
    UiPopupMessageComponent.prototype.popupVisibility = function () {
        this.viewModel.isVisible = !this.viewModel.isVisible;
    };
    UiPopupMessageComponent.prototype.onButtonClicked = function (option) {
        if (!option.callback || typeof option.callback !== 'function') {
            return;
        }
        option.callback(option);
        this.popupVisibility();
    };
    return UiPopupMessageComponent;
}());
UiPopupMessageComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-popup-message',
                template: "<div class=\"ui-popup\" *ngIf=\"viewModel.isVisible\">\n    <div class=\"ui-popup__overlay ui-popup__overlay--visible\"\n         (click)=\"popupVisibility()\">\n    </div>\n    <div class=\"ui-popup__content\">\n        <div class=\"ui-popup__header\">\n            <h2 class=\"ui-popup__header-title\">{{viewModel.title}}</h2>\n        </div>\n        <div class=\"ui-popup__body\">\n            {{viewModel.message}}\n        </div>\n        <div class=\"ui-popup__footer\">\n            <button *ngIf=\"viewModel.cancel.button_text!== ''\"\n                    type=\"flat\"\n                    class=\"ui-popup__cancel\"\n                    (click)=\"onButtonClicked(viewModel.cancel)\">\n                {{viewModel.cancel.button_text}}\n            </button>\n            <button  *ngIf=\"viewModel.submit.button_text!== ''\"\n                     type=\"secondary\"\n                     (click)=\"onButtonClicked(viewModel.submit)\">\n                {{viewModel.submit.button_text}}\n            </button>\n        </div>\n    </div>\n</div>\n"
            },] },
];
UiPopupMessageComponent.ctorParameters = function () { return []; };
UiPopupMessageComponent.propDecorators = {
    "viewModel": [{ type: Input },],
};
var UiAutoCompleteComponent = /** @class */ (function () {
    function UiAutoCompleteComponent() {
        this.searchString = '';
        this.selectedOptions = [];
        this.processedOptionsList = [];
        this.showOptionsList = false;
        this.filterTagsViewModel = {
            items: []
        };
        this.optionsList = [];
        this.multiple = true;
        this.defaultValues = [];
        this.hasError = false;
        this.disabled = false;
        this.onItemsChanged = new EventEmitter();
    }
    UiAutoCompleteComponent.prototype.ngOnInit = function () {
        this.processedOptionsList = this.optionsList;
        this.selectedOptions = this.defaultValues;
        if (this.multiple) {
            this.filterTagsViewModel.items = extend([], this.selectedOptions);
        }
        if (!this.multiple) {
            this.searchString =
                this.defaultValues.length === 1
                    ? this.defaultValues[0].text
                    : '';
        }
    };
    UiAutoCompleteComponent.prototype.showOptionsMenu = function () {
        this.showOptionsList = true;
    };
    UiAutoCompleteComponent.prototype.hideOptionsMenu = function () {
        this.showOptionsList = false;
    };
    UiAutoCompleteComponent.prototype.searchValue = function (searchString) {
        this.processedOptionsList = filter(this.optionsList, function (item) {
            return item.text.toLowerCase().includes(searchString.toLowerCase());
        });
    };
    UiAutoCompleteComponent.prototype.clearSearch = function () {
        this.searchString = '';
        if (!this.multiple) {
            this.selectedOptions.length = 0;
            this.emitUpdates();
        }
    };
    UiAutoCompleteComponent.prototype.emitUpdates = function () {
        this.onItemsChanged.emit({
            selectedItems: this.selectedOptions
        });
    };
    UiAutoCompleteComponent.prototype.onFilterTagRemoved = function (filterData) {
        this.selectedOptions.splice(filterData.removedIndex, 1);
        this.emitUpdates();
    };
    UiAutoCompleteComponent.prototype.onOptionSelected = function (optionSelected) {
        this.hideOptionsMenu();
        if (this.multiple) {
            this.clearSearch();
            this.ngOnInit();
            this.selectedOptions.push(optionSelected);
            this.filterTagsViewModel.items.push(optionSelected);
        }
        else {
            this.searchString = optionSelected.text;
            this.selectedOptions[0] = optionSelected;
        }
        this.emitUpdates();
    };
    return UiAutoCompleteComponent;
}());
UiAutoCompleteComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-auto-complete',
                template: "<div class=\"ui-autocomplete\"\n    (mouseleave)=\"hideOptionsMenu()\">\n    <input type=\"text\"\n           class=\"ui-autocomplete__textbox\"\n           [attr.placeholder]=\"placeholder\"\n           (keyup)=\"searchValue(searchString)\"\n           (focus)=\"showOptionsMenu()\"\n           [(ngModel)]=\"searchString\"\n           [ngClass]=\"{'form-field__input--error': hasError}\"\n           [disabled]=\"disabled\">\n\n    <button *ngIf=\"searchString !== ''\"\n            class=\"ui-autocomplete__clear\"\n            (click)=\"clearSearch()\">\n        <i class=\"material-icons\">close</i>\n    </button>\n    <ui-filter-tags *ngIf=\"selectedOptions.length > 0\"\n                    class=\"ui-autocomplete__filter-tags\"\n                    (onLabelRemoved)=\"onFilterTagRemoved($event)\"\n                    [viewModel]=\"filterTagsViewModel\"></ui-filter-tags>\n    <div class=\"ui-autocomplete__selectbox\"\n         [ngClass]=\"{'ui-autocomplete__selectbox--visible': showOptionsList}\">\n        <div class=\"ui-autocomplete__option\"\n             *ngFor=\"let option of processedOptionsList\"\n             (click)=\"onOptionSelected(option)\">{{option.text}}</div>\n    </div>\n</div>\n"
            },] },
];
UiAutoCompleteComponent.ctorParameters = function () { return []; };
UiAutoCompleteComponent.propDecorators = {
    "optionsList": [{ type: Input },],
    "multiple": [{ type: Input },],
    "defaultValues": [{ type: Input },],
    "placeholder": [{ type: Input },],
    "hasError": [{ type: Input },],
    "disabled": [{ type: Input },],
    "onItemsChanged": [{ type: Output },],
};
var moment = moment_;
var UiDateInputComponent = /** @class */ (function () {
    function UiDateInputComponent() {
        this.placeholder = '';
        this.dateFormat = '';
        this.onDateSelected = new EventEmitter();
        this.daysToShow = [];
        this.daysOfMonth = [];
        this.calendarIsVisible = false;
        this.isInvalidDate = false;
    }
    UiDateInputComponent.prototype.formatDate = function (clickedDay) {
        var selectedDate = new Date(this.year, this.month, clickedDay);
        return moment(selectedDate).format(this.dateFormat);
    };
    UiDateInputComponent.prototype.emitChanges = function () {
        this.onDateSelected.emit(this.selectedDate);
    };
    UiDateInputComponent.prototype.ngOnInit = function () {
        this.dateFormat = this.dateFormat.toUpperCase();
        this.selectedDate = this.value
            ? moment(this.dateFormat).format(this.value)
            : '';
        this.currentDate = new Date();
        this.year = this.currentDate.getFullYear();
        this.month = this.currentDate.getMonth() + 1;
        this.buildCalendarDays();
    };
    UiDateInputComponent.prototype.buildCalendarDays = function () {
        var numberOfdays = new Date(this.year, this.month, 0).getDate();
        this.daysOfMonth = [];
        this.daysToShow = [];
        this.weekDayStartingMonth = new Date(this.year + '-' + this.month + '-01').getDay();
        for (var w = 1; w <= this.weekDayStartingMonth; w++) {
            this.daysToShow.push('');
        }
        for (var i = 1; i <= numberOfdays; i++) {
            if (this.daysToShow.length < 7) {
                this.daysToShow.push(i);
            }
            else {
                this.daysOfMonth.push(this.daysToShow);
                this.daysToShow = [i];
            }
            if (i === numberOfdays) {
                this.daysOfMonth.push(this.daysToShow);
            }
        }
    };
    UiDateInputComponent.prototype.subtractMonth = function () {
        this.month = this.month - 1;
        if (this.month <= 0) {
            this.year = this.year - 1;
            this.month = 12;
        }
        this.buildCalendarDays();
    };
    UiDateInputComponent.prototype.addMonth = function () {
        this.month = this.month + 1;
        if (this.month >= 13) {
            this.year = this.year + 1;
            this.month = 1;
        }
        this.buildCalendarDays();
    };
    UiDateInputComponent.prototype.onInputChanged = function () {
        this.isInvalidDate = false;
        if (!moment(this.selectedDate, this.dateFormat, true).isValid()) {
            this.isInvalidDate = true;
            return;
        }
        this.emitChanges();
    };
    UiDateInputComponent.prototype.onSelectedDate = function (clickedDay) {
        this.selectedDate = this.formatDate(clickedDay);
        this.calendarToggleVisibility();
        this.emitChanges();
    };
    UiDateInputComponent.prototype.isSelectedDate = function (dayNumber) {
        if (this.selectedDate === '') {
            return false;
        }
        return this.selectedDate === this.formatDate(dayNumber);
    };
    UiDateInputComponent.prototype.hideCalendar = function () {
        this.calendarIsVisible = false;
    };
    UiDateInputComponent.prototype.calendarToggleVisibility = function () {
        this.calendarIsVisible = !this.calendarIsVisible;
    };
    return UiDateInputComponent;
}());
UiDateInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-date-input',
                template: "<div class=\"ui-date-picker\"\n     (mouseleave)=\"hideCalendar()\">\n    <input class=\"ui-date-picker__input\"\n           type=\"text\"\n           placeholder=\"{{placeholder}}\"\n           [disabled]=\"disable\"\n           [(ngModel)]=\"selectedDate\"\n           (keyup)=\"onInputChanged()\"\n           [ngClass]=\"{\n                'form-field__input--error': isInvalidDate\n            }\">\n    <button class=\"ui-date-picker__button\"\n            (click)=\"calendarToggleVisibility()\"\n            [disabled]=\"disable\">\n        <i class=\"material-icons\">today</i>\n    </button>\n\n    <div class=\"ui-date-picker__calendar\"\n        *ngIf=\"calendarIsVisible\">\n\n        <div class=\"ui-date-picker__hideborders\"\n             (click)=\"calendarToggleVisibility()\">\n        </div>\n\n        <div class=\"ui-date-picker__month-year\">\n            <button (click)=\"subtractMonth()\">\n                <i class=\"material-icons ui-date-picker__backward\">\n                    keyboard_arrow_left</i>\n            </button>\n            <div class=\"ui-date-picker__actual-month-year\">\n                {{monthNames[month-1]}} {{year}}\n            </div>\n            <button (click)=\"addMonth()\">\n                <i class=\"material-icons ui-date-picker__forward\">\n                    keyboard_arrow_right</i>\n            </button>\n        </div>\n\n        <div class=\"ui-date-picker__days-list-container\">\n            <table class=\"ui-date-picker__days-list\">\n                <thead class=\"ui-date-picker__week-days\">\n                    <tr>\n                        <td *ngFor=\"let weekday of weekDays\">\n                            {{weekday}}\n                        </td>\n                    </tr>\n                </thead>\n                <tbody class=\"ui-date-picker__days\">\n                    <tr class=\"ui-date-picker__days-line\"\n                        *ngFor=\"let dayOfMonthSet of daysOfMonth; let i = index\">\n                       <td *ngFor=\"let dayNumber of daysOfMonth[i]\">\n                            <button class=\"ui-date-picker__day\"\n                                 *ngIf=\"dayNumber !== ''\"\n                                 [ngClass]=\"{\n                                    'ui-date-picker__day--selected': isSelectedDate(dayNumber)\n                                  }\"\n                                 (click)=\"onSelectedDate(dayNumber)\">\n                             {{dayNumber}}\n                            </button>\n                       </td>\n                    </tr>\n                </tbody>\n            </table>\n        </div>\n    </div>\n</div>\n\n\n"
            },] },
];
UiDateInputComponent.ctorParameters = function () { return []; };
UiDateInputComponent.propDecorators = {
    "disable": [{ type: Input },],
    "value": [{ type: Input },],
    "placeholder": [{ type: Input },],
    "dateFormat": [{ type: Input },],
    "monthNames": [{ type: Input },],
    "weekDays": [{ type: Input },],
    "onDateSelected": [{ type: Output },],
};
var UiUploadImageComponent = /** @class */ (function () {
    function UiUploadImageComponent() {
        this.viewModel = {
            src: ''
        };
        this.onFileChanged = new EventEmitter();
    }
    UiUploadImageComponent.prototype.emitUpload = function (uploadedFile) {
        this.onFileChanged.emit({
            selectedFile: uploadedFile
        });
    };
    return UiUploadImageComponent;
}());
UiUploadImageComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-upload-image',
                template: " <div class=\"ui-upload-image__content__containers--upload\">\n    <div class=\"ui-upload-image__image-placeholder\">\n        <input type=\"file\"\n               class=\"ui-upload-image__image-uploader\"\n               (change)=\"emitUpload($event.target.files[0])\">\n        <i class=\"material-icons ui-upload-image__icon-upload\"\n           [ngClass]=\"{\n                'ui-upload-image__icon-upload--image-selected': viewModel.src && viewModel.src !== ''\n            }\">file_upload</i>\n        <img class=\"ui-upload-image__image\"\n             [attr.src]=\"viewModel.src\">\n    </div>\n</div>\n"
            },] },
];
UiUploadImageComponent.ctorParameters = function () { return []; };
UiUploadImageComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onFileChanged": [{ type: Output },],
};
var HuubMaterialLibModule = /** @class */ (function () {
    function HuubMaterialLibModule() {
    }
    return HuubMaterialLibModule;
}());
HuubMaterialLibModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, FormsModule, OverlayModule],
                declarations: [
                    UiToggleButtonsComponent,
                    UiTabsComponent,
                    UiMenuBarComponent,
                    UiFilterTagsComponent,
                    UiFilterComponent,
                    UiActivityTimelineComponent,
                    UiSpinnerComponent,
                    UiInputSearchComponent,
                    UiTablesComponent,
                    UiButtonActionsComponent,
                    UiTooltipsComponent,
                    UiLoaderComponent,
                    UiTablesPlaceholderComponent,
                    UiPopupMessageComponent,
                    UiAutoCompleteComponent,
                    UiDateInputComponent,
                    UiUploadImageComponent
                ],
                exports: [
                    UiToggleButtonsComponent,
                    UiTabsComponent,
                    UiMenuBarComponent,
                    UiFilterTagsComponent,
                    UiFilterComponent,
                    UiActivityTimelineComponent,
                    UiSpinnerComponent,
                    UiInputSearchComponent,
                    UiTablesComponent,
                    UiButtonActionsComponent,
                    UiTooltipsComponent,
                    UiLoaderComponent,
                    UiTablesPlaceholderComponent,
                    UiPopupMessageComponent,
                    UiAutoCompleteComponent,
                    UiDateInputComponent,
                    UiUploadImageComponent
                ],
                providers: [MaterialHelper, UiModalService]
            },] },
];
HuubMaterialLibModule.ctorParameters = function () { return []; };

export { HuubMaterialLibModule, UiActivityTimelineComponent as ɵg, UiAutoCompleteComponent as ɵp, UiButtonActionsComponent as ɵk, UiFilterTagsComponent as ɵd, UiFilterComponent as ɵe, UiDateInputComponent as ɵq, UiInputSearchComponent as ɵi, UiSpinnerComponent as ɵh, UiUploadImageComponent as ɵr, MaterialHelper as ɵf, UiLoaderComponent as ɵm, UiMenuBarComponent as ɵc, UiTooltipsComponent as ɵl, UiModalService as ɵs, UiPopupMessageComponent as ɵo, UiTablesPlaceholderComponent as ɵn, UiTablesComponent as ɵj, UiTabsComponent as ɵb, UiToggleButtonsComponent as ɵa };
//# sourceMappingURL=huub-material-lib.js.map
