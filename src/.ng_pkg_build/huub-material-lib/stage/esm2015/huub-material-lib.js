import { Injectable, Component, Input, Output, EventEmitter, ElementRef, NgModule } from '@angular/core';
import { Subject } from 'rxjs';
import { Overlay, OverlayConfig, OverlayModule } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { find, sortBy, filter, extend } from 'lodash';
import * as moment_ from 'moment';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MaterialHelper {
    constructor() { }
    /**
     * @param {?} elementOffSet
     * @param {?} elementSize
     * @return {?}
     */
    getAbsolutePositionRenderConfig(elementOffSet, elementSize) {
        return {
            canRenderToRight: window.innerWidth > elementOffSet + elementSize,
            canRenderToLeft: elementOffSet - elementSize > 0
        };
    }
}
MaterialHelper.decorators = [
    { type: Injectable },
];
/** @nocollapse */
MaterialHelper.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiModalService {
    /**
     * @param {?} overlay
     */
    constructor(overlay) {
        this.overlay = overlay;
    }
    /**
     * @param {?=} context
     * @return {?}
     */
    disposeModal(context) {
        this.overlayRef.dispose();
        this.modalSubject.next(context || null);
        this.modalSubject.unsubscribe();
    }
    /**
     * @param {?} component
     * @param {?=} backDropCallCallback
     * @return {?}
     */
    openModal(component, backDropCallCallback) {
        const /** @type {?} */ positionStrategy = this.overlay
            .position()
            .global()
            .centerHorizontally()
            .centerVertically();
        this.modalSubject = new Subject();
        /** Creates overlay */
        this.overlayRef = this.overlay.create(new OverlayConfig({
            hasBackdrop: true,
            scrollStrategy: this.overlay.scrollStrategies.block(),
            positionStrategy
        }));
        /** Appends component to overlay */
        this.overlayRef.attach(new ComponentPortal(component, null, null));
        /** Appends subscribes overlay close */
        this.overlayRef.backdropClick().subscribe(() => {
            if (backDropCallCallback) {
                backDropCallCallback();
                return;
            }
            this.disposeModal();
        });
        return {
            afterClose: () => {
                return this.modalSubject.asObservable();
            }
        };
    }
    /**
     * @param {?} data
     * @return {?}
     */
    close(data) {
        this.disposeModal(data);
    }
}
UiModalService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
UiModalService.ctorParameters = () => [
    { type: Overlay, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiToggleButtonsComponent {
    constructor() {
        /**
         * Property that sould be used for binding a callback to receive the
         * button change event *
         */
        this.onButtonChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.viewModel.selectedIndex = this.viewModel.selectedIndex || 0;
    }
    /**
     * Method binded for the button toggle change action.
     * toggleButtonIndex Current tab index clicked
     * @param {?} toggleButtonIndex
     * @return {?}
     */
    onToggleButtonClicked(toggleButtonIndex) {
        if (!this.viewModel.items[toggleButtonIndex]) {
            return;
        }
        this.viewModel.selectedIndex = toggleButtonIndex;
        this.onButtonChanged.emit({
            toggleButtonSelected: this.viewModel.items[toggleButtonIndex],
            toggleButtonIndexSelected: toggleButtonIndex
        });
    }
}
UiToggleButtonsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-toggle-buttons',
                template: `<nav class="ui-toggle-buttons__nav">
    <button *ngFor="let toggleButton of viewModel.items; let toggleIndex = index;"
            [ngClass]="{
                'ui-toggle-buttons__button': !(viewModel.selectedIndex === toggleIndex),
                'ui-toggle-buttons__button--active': viewModel.selectedIndex === toggleIndex
            }"
            [attr.disabled]="toggleButton.disabled"
            (click)="onToggleButtonClicked(toggleIndex)">{{toggleButton.text}}</button>
</nav>
`
            },] },
];
/** @nocollapse */
UiToggleButtonsComponent.ctorParameters = () => [];
UiToggleButtonsComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onButtonChanged": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiTabsComponent {
    constructor() {
        /**
         * Property that sould be used for binding a callback to receive the tab clicked *
         */
        this.onTabChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.viewModel.selectedIndex = this.viewModel.selectedIndex || 0;
    }
    /**
     * Method used to update the selected tab. This method also triggers
     * the eventEmitter for onTabChanged callback that the parent may have
     * configured.
     * selectedTab Current tab clicked
     * selectedTabIndex Current tab index clicked
     * @param {?} selectedTab
     * @param {?} selectedTabIndex
     * @return {?}
     */
    onTabClicked(selectedTab, selectedTabIndex) {
        if (selectedTab.disabled) {
            return;
        }
        this.onTabChanged.emit({
            selectedTab,
            selectedTabIndex
        });
        this.viewModel.selectedIndex = selectedTabIndex;
    }
}
UiTabsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-tabs',
                template: `<nav class="ui-tabs__nav">
    <ul class="ui-tabs__nav-items">
        <li *ngFor="let item of viewModel.items; let itemIndex = index;"
            [ngClass]="{
                'ui-tabs__nav-item': !(itemIndex === viewModel.selectedIndex) && !item.disabled,
                'ui-tabs__nav-item--active': itemIndex === viewModel.selectedIndex,
                'ui-tabs__nav-item--disabled': item.disabled
            }">
            <a (click)="onTabClicked(item, itemIndex)" class="ui-tabs__nav-link">
                {{item.text}}
            </a>
        </li>
    </ul>
</nav>
`
            },] },
];
/** @nocollapse */
UiTabsComponent.ctorParameters = () => [];
UiTabsComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onTabChanged": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiMenuBarComponent {
    constructor() {
        /**
         * Property that sould be used for binding a callback to receive the nav item click *
         */
        this.onNavItemClicked = new EventEmitter();
        /**
         * Property that sould be used for binding a callback to receive the user icons click *
         */
        this.onUserIconsClicked = new EventEmitter();
        /**
         * Property that sould be used for binding a callback to receive the user action click *
         */
        this.onUserActionsClicked = new EventEmitter();
    }
    /**
     * @param {?} selectedItem
     * @param {?} selectedItemIndex
     * @return {?}
     */
    onMenuItemClicked(selectedItem, selectedItemIndex) {
        if (selectedItem.disabled) {
            return;
        }
        this.viewModel.navigation.selectedIndex = selectedItemIndex;
        this.onNavItemClicked.emit({
            selectedItem,
            selectedItemIndex
        });
    }
    /**
     * @param {?} icon
     * @param {?} iconsIndex
     * @return {?}
     */
    onIconsClicked(icon, iconsIndex) {
        this.onUserIconsClicked.emit({
            icon,
            iconsIndex
        });
    }
    /**
     * @param {?} action
     * @param {?} actionIndex
     * @return {?}
     */
    onActionsClicked(action, actionIndex) {
        this.onUserActionsClicked.emit({
            action,
            actionIndex
        });
    }
}
UiMenuBarComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-menu-bar',
                template: `<header class="ui-menu-bar__container">
    <div class="ui-menu-bar__logo">
        <img *ngIf="viewModel.logo"
             class="ui-menu-bar__logo-image"
             [attr.src]="viewModel.logo">
        <span *ngIf="!viewModel.logo"
              class="material-icons ui-menu-bar__logo--error">error</span>
    </div>
    <nav class="ui-menu-bar__nav">
        <a *ngFor="let menuItem of viewModel.navigation.items; let menuItemIndex = index;"
            (click)="onMenuItemClicked(menuItem, menuItemIndex)"
            class="ui-menu-bar__nav-item"
            [ngClass]="{
                'ui-menu-bar__nav-item--active': viewModel.navigation.selectedIndex === menuItemIndex,
                'ui-menu-bar__nav-item--disabled': menuItem.disabled
            }">{{menuItem.text}}</a>
    </nav>
    <div class="ui-menu-bar__user-data">
        <span *ngFor="let icon of viewModel.userData.icons; let iconsIndex = index;"
              (click)="onIconsClicked(icon, iconsIndex)">
            <i class="material-icons">{{icon.text}}</i>
        </span>
        <img *ngIf="viewModel.userData.photo"
             [attr.src]="viewModel.userData.photo"
             class="ui-menu-bar__photo">
        <span *ngIf="!viewModel.userData.photo"
              class="material-icons ui-menu-bar__photo--error">account_circle</span>
        <div class="ui-menu-bar__user-identification">
            <span>
                {{viewModel.userData.translatedText}},
                <strong>{{viewModel.userData.name}}</strong>
            </span>
            <i class="material-icons ui-menu-bar__user-identification-arrow">keyboard_arrow_down</i>
            <nav *ngIf="viewModel.userData.listActions.length > 0"
                 class="ui-menu-bar__user-identification-nav">
                <a *ngFor="let action of viewModel.userData.listActions; let actionIndex = index;"
                    (click)="onActionsClicked(action, actionIndex)"
                    class="ui-menu-bar__user-identification-nav-item">{{action.text}}</a>
            </nav>
        </div>
    </div>
</header>
`
            },] },
];
/** @nocollapse */
UiMenuBarComponent.ctorParameters = () => [];
UiMenuBarComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onNavItemClicked": [{ type: Output },],
    "onUserIconsClicked": [{ type: Output },],
    "onUserActionsClicked": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiFilterTagsComponent {
    constructor() {
        /**
         * Property that sould be used for binding a callback to receive the
         * input submission *
         */
        this.onLabelRemoved = new EventEmitter();
    }
    /**
     * Method binded to the form submit event.
     * @param {?} labelIndex
     * @return {?}
     */
    onLabelRemovedClick(labelIndex) {
        if (!this.viewModel.items[labelIndex]) {
            return;
        }
        const /** @type {?} */ removedLabel = this.viewModel.items[labelIndex];
        this.viewModel.items.splice(labelIndex, 1);
        this.onLabelRemoved.emit({
            removedLabel,
            removedIndex: labelIndex,
            items: this.viewModel.items
        });
    }
}
UiFilterTagsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-filter-tags',
                template: `<label *ngFor="let label of viewModel.items; let labelIndex = index;"
        class="ui-filter-tags__label">
        {{label.text}}
        <i class="material-icons ui-filter-tags__icon"
            (click)="onLabelRemovedClick(labelIndex)">clear</i>
</label>
`
            },] },
];
/** @nocollapse */
UiFilterTagsComponent.ctorParameters = () => [];
UiFilterTagsComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onLabelRemoved": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @record
 */

class UiFilterComponent {
    /**
     * @param {?} element
     * @param {?} helper
     */
    constructor(element, helper) {
        this.element = element;
        this.helper = helper;
        this.ELEMENT_SIZE = 350;
        this.alignRight = false;
        this.alignLeft = false;
        this.selectedOptions = [];
        this.optionContainerIsVisible = false;
        /**
         * Property that sould be used for binding a callback to receive the save action *
         */
        this.onSaveAction = new EventEmitter();
    }
    /**
     * @param {?} index
     * @param {?} item
     * @return {?}
     */
    trackByFn(index, item) {
        return index;
    }
    /**
     * @return {?}
     */
    toogleOptionContainerVisibility() {
        const /** @type {?} */ absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, this.ELEMENT_SIZE);
        this.optionContainerIsVisible = !this.optionContainerIsVisible;
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
        this.selectedOptions.length = 0;
    }
    /**
     * @return {?}
     */
    onSaveButtonClick() {
        this.onSaveAction.emit({
            selectedOption: find(this.selectedOptions, { selected: true }) ||
                this.selectedOptions[0]
        });
    }
    /**
     * @param {?} options
     * @param {?} optionIndex
     * @return {?}
     */
    setSelectedOption(options, optionIndex) {
        options.forEach(item => {
            item.selected = false;
        });
        options[optionIndex].selected = true;
    }
    /**
     * @param {?} options
     * @param {?} indexOption
     * @return {?}
     */
    onSelectChange(options, indexOption) {
        this.setSelectedOption(options, indexOption);
        this.selectedOptions =
            this.viewModel.selectOptions[this.viewModel.selectBox[indexOption].type] || [];
    }
    /**
     * @param {?} options
     * @param {?} indexOption
     * @return {?}
     */
    onSecondarySelectChange(options, indexOption) {
        this.setSelectedOption(options, indexOption);
    }
}
UiFilterComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-filter',
                template: `<button class="ui-filter__container">
    <i class="material-icons ui-filter__icon ui-filter__icon__tune"
       (click)="toogleOptionContainerVisibility()">tune</i>
    <span class="ui-filter__text"
          (click)="toogleOptionContainerVisibility()">{{viewModel.filterText}}</span>
    <i class="material-icons ui-filter__icon"
       (click)="toogleOptionContainerVisibility()">keyboard_arrow_down</i>

    <div class="ui-filter__options-container"
        [ngClass]="{
            'ui-filter__options-container--visible': optionContainerIsVisible,
            'ui-filter__options-container--right': alignRight,
            'ui-filter__options-container--left': alignLeft
        }"
        (mouseleave)="toogleOptionContainerVisibility()">
        <ng-container *ngIf="optionContainerIsVisible">
            <select class="ui-filter__select"
                    (change)="onSelectChange(viewModel.selectBox, $event.target.value)">
                <option *ngFor="let option of viewModel.selectBox; let index = index; trackBy: trackByFn"
                        value="{{index}}">{{option.text}}</option>
            </select>

            <select *ngIf="selectedOptions.length > 0"
                    class="ui-filter__select"
                    (change)="onSecondarySelectChange(selectedOptions, $event.target.value)">
                <option *ngFor="let option of selectedOptions; let index = index; trackBy: trackByFn"
                        value="{{index}}">{{option.text}}</option>
            </select>
        </ng-container>

        <button type="secondary" class="ui-filter__button"
                (click)="onSaveButtonClick()">{{viewModel.buttonText}}</button>
    </div>
</button>
`
            },] },
];
/** @nocollapse */
UiFilterComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: MaterialHelper, },
];
UiFilterComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onSaveAction": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiActivityTimelineComponent {
    constructor() { }
}
UiActivityTimelineComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-activity-timeline',
                template: `<div *ngFor="let statusItem of viewModel.statusList; let statusIndex = index;"
     class="ui-activity-timeline__group-status">
    <div class="ui-activity-timeline__status-date">{{statusItem.date}}</div>
    <div class="material-icons ui-activity-timeline__status-bar"
         [ngClass]="{
            'ui-activity-timeline__status-bar--open': statusItem.open,
            'ui-activity-timeline__status-bar--close': !statusItem.open,
            'ui-activity-timeline__status-bar--last': (statusIndex === 0)
         }"></div>
    <div class="ui-activity-timeline__status-name">{{statusItem.name}}</div>
</div>
`
            },] },
];
/** @nocollapse */
UiActivityTimelineComponent.ctorParameters = () => [];
UiActivityTimelineComponent.propDecorators = {
    "viewModel": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiSpinnerComponent {
    constructor() {
        /**
         * Property that defines the input value *
         */
        this.allowNegatives = true;
        /**
         * Property that sould be used for binding a callback to receive the current input value
         * everytime it changes *
         */
        this.onValueChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (isNaN(this.value)) {
            throw new TypeError('The value defined should be a number');
        }
    }
    /**
     * @return {?}
     */
    onKeyUp() {
        this.onValueChanged.emit(this.value);
    }
    /**
     * @return {?}
     */
    onPlusButtonPressed() {
        this.value += 1;
        this.onValueChanged.emit(this.value);
    }
    /**
     * @return {?}
     */
    onLessButtonPressed() {
        this.value -= 1;
        if (!this.allowNegatives && this.value < 0) {
            this.value = 0;
            return;
        }
        this.onValueChanged.emit(this.value);
    }
}
UiSpinnerComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-spinner',
                template: `<input class="ui-spinner__input"
       #ctrl="ngModel"
       [(ngModel)]="value"
       [ngClass]="{'ui-spinner__input--error': hasError}"
       (keyup)="onKeyUp()"
       type="number"
       [attr.disabled]="disabled">
<button class="material-icons ui-spinner__button ui-spinner__button-less"
        (click)="onLessButtonPressed()"
        [attr.disabled]="disabled"></button>
<button class="material-icons ui-spinner__button ui-spinner__button-plus"
        (click)="onPlusButtonPressed()"
        [attr.disabled]="disabled"></button>
`
            },] },
];
/** @nocollapse */
UiSpinnerComponent.ctorParameters = () => [];
UiSpinnerComponent.propDecorators = {
    "allowNegatives": [{ type: Input },],
    "value": [{ type: Input },],
    "hasError": [{ type: Input },],
    "disabled": [{ type: Input },],
    "onValueChanged": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiInputSearchComponent {
    constructor() {
        this.viewModel = {
            placeholder: '',
            value: ''
        };
        /**
         * Property that sould be used for binding a callback to receive the
         * input submission *
         */
        this.onSubmit = new EventEmitter();
    }
    /**
     * Method binded to the form submit event.
     * @return {?}
     */
    onSubmitForm() {
        this.onSubmit.emit(this.viewModel.value);
    }
}
UiInputSearchComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-input-search',
                template: `<form (submit)="onSubmitForm()" class="ui-input-search__form">
    <input type="text"
       class="ui-input-search"
       [attr.placeholder]="viewModel.placeholder"
       [(ngModel)]="viewModel.value"
       [ngModelOptions]="{standalone: true}">
    <i class="material-icons ui-input-search__icon"
        *ngIf="!viewModel.value || viewModel.value == ''">search</i>
</form>
`
            },] },
];
/** @nocollapse */
UiInputSearchComponent.ctorParameters = () => [];
UiInputSearchComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onSubmit": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiTablesComponent {
    constructor() { }
    /**
     * @param {?} headerItemIndex
     * @return {?}
     */
    sortBodyTable(headerItemIndex) {
        this.viewModel.tableBody = sortBy(this.viewModel.tableBody, o => o[headerItemIndex].text.toLowerCase() ||
            o[headerItemIndex].value);
    }
    /**
     * @param {?} headerItemIndex
     * @return {?}
     */
    resetAllTableSort(headerItemIndex) {
        this.viewModel.tableHeader.forEach((column, index) => {
            if (headerItemIndex === index) {
                return;
            }
            column.sortBy = false;
        });
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.viewModel.tableHeader.forEach((column, index) => {
            if (column.sortBy) {
                this.sortBodyTable(index);
            }
        });
    }
    /**
     * @param {?} column
     * @return {?}
     */
    onColumnClicked(column) {
        if (!column.callback || typeof column.callback !== 'function') {
            return;
        }
        column.callback(column);
    }
    /**
     * @param {?} headerItemIndex
     * @return {?}
     */
    onSortButtonClick(headerItemIndex) {
        this.sortBodyTable(headerItemIndex);
        this.viewModel.tableHeader[headerItemIndex].sortBy = !this.viewModel
            .tableHeader[headerItemIndex].sortBy;
        if (!this.viewModel.tableHeader[headerItemIndex].sortBy) {
            this.viewModel.tableBody.reverse();
        }
        this.resetAllTableSort(headerItemIndex);
    }
}
UiTablesComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-tables',
                template: `<table class="ui-table__table">
    <thead>
       <tr class="ui-table__tr">
            <th class="ui-table__th"
                *ngFor="let column of viewModel.tableHeader; let columnIndex = index;">
                <button *ngIf="column.text && column.text !== ''"
                        (click)="onSortButtonClick(columnIndex)"
                        class="ui-table__order-container">
                    <span>{{column.text}}</span>
                    <i class="material-icons ui-table__order-icon"
                        [ngClass]="{
                            'ui-table__order-icon--rotate': column.sortBy
                        }">keyboard_arrow_up</i>
                </button>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr class="ui-table__tr ui-table__row" *ngFor="let row of viewModel.tableBody">
            <td class="ui-table__td"
                *ngFor="let column of viewModel.tableHeader; let i = index"
                [ngSwitch]="row[i].type">
                    <input *ngSwitchCase="'checkbox'"
                            type="checkbox"
                            class="material-icons"
                            (click)="onColumnClicked(row[i])"
                            [ngModel]="row[i].value">
                    <span *ngSwitchCase="'string'"
                           (click)="onColumnClicked(row[i])"
                          [ngClass]="{
                                'ui-table__text-bold': row[i].textBold
                           }">{{row[i].text}}</span>
                    <a *ngSwitchCase="'link'"
                        (click)="onColumnClicked(row[i])"
                        class="ui-table__td-link">{{row[i].text}}</a>
                    <button *ngSwitchCase="'button'"
                            type="secondary"
                            [attr.color]="row[i].color"
                            (click)="onColumnClicked(row[i])"
                            >{{row[i].text}}</button>
                    <ui-button-actions *ngSwitchCase="'button-actions'"
                                        [viewModel]="row[i].viewModel"></ui-button-actions>
                    <span *ngSwitchCase="'status'"
                         class="ui-table__status"
                         (click)="onColumnClicked(row[i])"
                         [ngClass]="{
                           'ui-table__status--active': row[i].value}"></span>
            </td>
        </tr>
    </tbody>
</table>

`
            },] },
];
/** @nocollapse */
UiTablesComponent.ctorParameters = () => [];
UiTablesComponent.propDecorators = {
    "viewModel": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiTooltipsComponent {
    /**
     * @param {?} element
     * @param {?} helper
     */
    constructor(element, helper) {
        this.element = element;
        this.helper = helper;
        this.ELEMENT_SIZE = 200;
        this.alignLeft = false;
        this.alignRight = false;
    }
    /**
     * @return {?}
     */
    onTooltipShow() {
        const /** @type {?} */ absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, this.ELEMENT_SIZE);
        if (absoluteRenderConfig.canRenderToRight &&
            absoluteRenderConfig.canRenderToLeft) {
            return;
        }
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
    }
}
UiTooltipsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-tooltips',
                template: `<div class="ui-tooltip" (mouseover)="onTooltipShow()">
    <i class="material-icons ui-tooltip__icon">info</i>
    <div class="ui-tooltip__arrow"></div>
    <div class="ui-tooltip__container"
        [ngClass]="{
            'ui-tooltip__container--left': alignLeft,
            'ui-tooltip__container--right': alignRight,
            'ui-tooltip__container--center': alignLeft && alignRight
        }">
        <div class="ui-tooltip__title">
            {{viewModel.title}}
        </div>
        <div class="ui-tooltip__message">
            {{viewModel.text}}
        </div>
    </div>
</div>
`
            },] },
];
/** @nocollapse */
UiTooltipsComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: MaterialHelper, },
];
UiTooltipsComponent.propDecorators = {
    "viewModel": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiTablesPlaceholderComponent {
    constructor() { }
}
UiTablesPlaceholderComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-tables-placeholder',
                template: `<section class="ui-table-placeholder hb-animation-mask">
    <div class="ui-table-placeholder__header">
        <div class="ui-table-placeholder__header-column"></div>
        <div class="ui-table-placeholder__header-column"></div>
        <div class="ui-table-placeholder__header-column"></div>
        <div class="ui-table-placeholder__header-column"></div>
    </div>

    <div class="ui-table-placeholder__body"></div>
    <div class="ui-table-placeholder__body"></div>
    <div class="ui-table-placeholder__body"></div>
    <div class="ui-table-placeholder__body"></div>
</section>
`
            },] },
];
/** @nocollapse */
UiTablesPlaceholderComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiButtonActionsComponent {
    /**
     * @param {?} element
     * @param {?} helper
     */
    constructor(element, helper) {
        this.element = element;
        this.helper = helper;
        this.ELEMENT_SIZE = 200;
        this.alignRight = false;
        this.alignLeft = false;
        this.type = 'flat';
        this.optionContainerIsVisible = false;
    }
    /**
     * @return {?}
     */
    showOptionContainerVisibility() {
        const /** @type {?} */ absoluteRenderConfig = this.helper.getAbsolutePositionRenderConfig(this.element.nativeElement.getBoundingClientRect().left, this.ELEMENT_SIZE);
        this.optionContainerIsVisible = true;
        this.alignLeft = absoluteRenderConfig.canRenderToLeft;
        this.alignRight = absoluteRenderConfig.canRenderToRight;
    }
    /**
     * @return {?}
     */
    hideOptionContainerVisibility() {
        this.optionContainerIsVisible = false;
    }
    /**
     * @param {?} link
     * @return {?}
     */
    onLinkClicked(link) {
        if (!link.callback || typeof link.callback !== 'function') {
            return;
        }
        link.callback(link);
    }
}
UiButtonActionsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-button-actions',
                template: `<div class="ui-button-actions"
    (mouseleave)="hideOptionContainerVisibility()">
    <button [attr.type]="type"
            role="icon"
            (click)="showOptionContainerVisibility()"></button>
    <div class="ui-button-actions__actions-list"
        [ngClass]="{
            'ui-button-actions__actions-list--visible': optionContainerIsVisible,
            'ui-button-actions__actions-list--right': alignRight,
            'ui-button-actions__actions-list--left': alignLeft
        }"
        (mouseleave)="hideOptionContainerVisibility()">
            <a *ngFor="let link of viewModel.items"
                class="ui-button-actions__actions-list-link"
                (click)="onLinkClicked(link)">
                {{link.text}}
            </a>
    </div>
</div>
`
            },] },
];
/** @nocollapse */
UiButtonActionsComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: MaterialHelper, },
];
UiButtonActionsComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "type": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiLoaderComponent {
    constructor() { }
}
UiLoaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-loader',
                template: `<div class="ui-spinner__out">
    <div class="ui-spinner__in"></div>
</div>
`
            },] },
];
/** @nocollapse */
UiLoaderComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiPopupMessageComponent {
    constructor() { }
    /**
     * @return {?}
     */
    popupVisibility() {
        this.viewModel.isVisible = !this.viewModel.isVisible;
    }
    /**
     * @param {?} option
     * @return {?}
     */
    onButtonClicked(option) {
        if (!option.callback || typeof option.callback !== 'function') {
            return;
        }
        option.callback(option);
        this.popupVisibility();
    }
}
UiPopupMessageComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-popup-message',
                template: `<div class="ui-popup" *ngIf="viewModel.isVisible">
    <div class="ui-popup__overlay ui-popup__overlay--visible"
         (click)="popupVisibility()">
    </div>
    <div class="ui-popup__content">
        <div class="ui-popup__header">
            <h2 class="ui-popup__header-title">{{viewModel.title}}</h2>
        </div>
        <div class="ui-popup__body">
            {{viewModel.message}}
        </div>
        <div class="ui-popup__footer">
            <button *ngIf="viewModel.cancel.button_text!== ''"
                    type="flat"
                    class="ui-popup__cancel"
                    (click)="onButtonClicked(viewModel.cancel)">
                {{viewModel.cancel.button_text}}
            </button>
            <button  *ngIf="viewModel.submit.button_text!== ''"
                     type="secondary"
                     (click)="onButtonClicked(viewModel.submit)">
                {{viewModel.submit.button_text}}
            </button>
        </div>
    </div>
</div>
`
            },] },
];
/** @nocollapse */
UiPopupMessageComponent.ctorParameters = () => [];
UiPopupMessageComponent.propDecorators = {
    "viewModel": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiAutoCompleteComponent {
    constructor() {
        this.searchString = '';
        this.selectedOptions = [];
        this.processedOptionsList = [];
        this.showOptionsList = false;
        this.filterTagsViewModel = {
            items: []
        };
        this.optionsList = [];
        this.multiple = true;
        this.defaultValues = [];
        this.hasError = false;
        this.disabled = false;
        this.onItemsChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.processedOptionsList = this.optionsList;
        this.selectedOptions = this.defaultValues;
        if (this.multiple) {
            this.filterTagsViewModel.items = extend([], this.selectedOptions);
        }
        if (!this.multiple) {
            this.searchString =
                this.defaultValues.length === 1
                    ? this.defaultValues[0].text
                    : '';
        }
    }
    /**
     * @return {?}
     */
    showOptionsMenu() {
        this.showOptionsList = true;
    }
    /**
     * @return {?}
     */
    hideOptionsMenu() {
        this.showOptionsList = false;
    }
    /**
     * @param {?} searchString
     * @return {?}
     */
    searchValue(searchString) {
        this.processedOptionsList = filter(this.optionsList, function (item) {
            return item.text.toLowerCase().includes(searchString.toLowerCase());
        });
    }
    /**
     * @return {?}
     */
    clearSearch() {
        this.searchString = '';
        if (!this.multiple) {
            this.selectedOptions.length = 0;
            this.emitUpdates();
        }
    }
    /**
     * @return {?}
     */
    emitUpdates() {
        this.onItemsChanged.emit({
            selectedItems: this.selectedOptions
        });
    }
    /**
     * @param {?} filterData
     * @return {?}
     */
    onFilterTagRemoved(filterData) {
        this.selectedOptions.splice(filterData.removedIndex, 1);
        this.emitUpdates();
    }
    /**
     * @param {?} optionSelected
     * @return {?}
     */
    onOptionSelected(optionSelected) {
        this.hideOptionsMenu();
        if (this.multiple) {
            this.clearSearch();
            this.ngOnInit();
            this.selectedOptions.push(optionSelected);
            this.filterTagsViewModel.items.push(optionSelected);
        }
        else {
            this.searchString = optionSelected.text;
            this.selectedOptions[0] = optionSelected;
        }
        this.emitUpdates();
    }
}
UiAutoCompleteComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-auto-complete',
                template: `<div class="ui-autocomplete"
    (mouseleave)="hideOptionsMenu()">
    <input type="text"
           class="ui-autocomplete__textbox"
           [attr.placeholder]="placeholder"
           (keyup)="searchValue(searchString)"
           (focus)="showOptionsMenu()"
           [(ngModel)]="searchString"
           [ngClass]="{'form-field__input--error': hasError}"
           [disabled]="disabled">

    <button *ngIf="searchString !== ''"
            class="ui-autocomplete__clear"
            (click)="clearSearch()">
        <i class="material-icons">close</i>
    </button>
    <ui-filter-tags *ngIf="selectedOptions.length > 0"
                    class="ui-autocomplete__filter-tags"
                    (onLabelRemoved)="onFilterTagRemoved($event)"
                    [viewModel]="filterTagsViewModel"></ui-filter-tags>
    <div class="ui-autocomplete__selectbox"
         [ngClass]="{'ui-autocomplete__selectbox--visible': showOptionsList}">
        <div class="ui-autocomplete__option"
             *ngFor="let option of processedOptionsList"
             (click)="onOptionSelected(option)">{{option.text}}</div>
    </div>
</div>
`
            },] },
];
/** @nocollapse */
UiAutoCompleteComponent.ctorParameters = () => [];
UiAutoCompleteComponent.propDecorators = {
    "optionsList": [{ type: Input },],
    "multiple": [{ type: Input },],
    "defaultValues": [{ type: Input },],
    "placeholder": [{ type: Input },],
    "hasError": [{ type: Input },],
    "disabled": [{ type: Input },],
    "onItemsChanged": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const moment = moment_;
class UiDateInputComponent {
    constructor() {
        this.placeholder = '';
        this.dateFormat = '';
        this.onDateSelected = new EventEmitter();
        this.daysToShow = [];
        this.daysOfMonth = [];
        this.calendarIsVisible = false;
        this.isInvalidDate = false;
    }
    /**
     * @param {?} clickedDay
     * @return {?}
     */
    formatDate(clickedDay) {
        const /** @type {?} */ selectedDate = new Date(this.year, this.month, clickedDay);
        return moment(selectedDate).format(this.dateFormat);
    }
    /**
     * @return {?}
     */
    emitChanges() {
        this.onDateSelected.emit(this.selectedDate);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.dateFormat = this.dateFormat.toUpperCase();
        this.selectedDate = this.value
            ? moment(this.dateFormat).format(this.value)
            : '';
        this.currentDate = new Date();
        this.year = this.currentDate.getFullYear();
        this.month = this.currentDate.getMonth() + 1;
        this.buildCalendarDays();
    }
    /**
     * @return {?}
     */
    buildCalendarDays() {
        const /** @type {?} */ numberOfdays = new Date(this.year, this.month, 0).getDate();
        this.daysOfMonth = [];
        this.daysToShow = [];
        this.weekDayStartingMonth = new Date(this.year + '-' + this.month + '-01').getDay();
        for (let /** @type {?} */ w = 1; w <= this.weekDayStartingMonth; w++) {
            this.daysToShow.push('');
        }
        for (let /** @type {?} */ i = 1; i <= numberOfdays; i++) {
            if (this.daysToShow.length < 7) {
                this.daysToShow.push(i);
            }
            else {
                this.daysOfMonth.push(this.daysToShow);
                this.daysToShow = [i];
            }
            if (i === numberOfdays) {
                this.daysOfMonth.push(this.daysToShow);
            }
        }
    }
    /**
     * @return {?}
     */
    subtractMonth() {
        this.month = this.month - 1;
        if (this.month <= 0) {
            this.year = this.year - 1;
            this.month = 12;
        }
        this.buildCalendarDays();
    }
    /**
     * @return {?}
     */
    addMonth() {
        this.month = this.month + 1;
        if (this.month >= 13) {
            this.year = this.year + 1;
            this.month = 1;
        }
        this.buildCalendarDays();
    }
    /**
     * @return {?}
     */
    onInputChanged() {
        this.isInvalidDate = false;
        if (!moment(this.selectedDate, this.dateFormat, true).isValid()) {
            this.isInvalidDate = true;
            return;
        }
        this.emitChanges();
    }
    /**
     * @param {?} clickedDay
     * @return {?}
     */
    onSelectedDate(clickedDay) {
        this.selectedDate = this.formatDate(clickedDay);
        this.calendarToggleVisibility();
        this.emitChanges();
    }
    /**
     * @param {?} dayNumber
     * @return {?}
     */
    isSelectedDate(dayNumber) {
        if (this.selectedDate === '') {
            return false;
        }
        return this.selectedDate === this.formatDate(dayNumber);
    }
    /**
     * @return {?}
     */
    hideCalendar() {
        this.calendarIsVisible = false;
    }
    /**
     * @return {?}
     */
    calendarToggleVisibility() {
        this.calendarIsVisible = !this.calendarIsVisible;
    }
}
UiDateInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-date-input',
                template: `<div class="ui-date-picker"
     (mouseleave)="hideCalendar()">
    <input class="ui-date-picker__input"
           type="text"
           placeholder="{{placeholder}}"
           [disabled]="disable"
           [(ngModel)]="selectedDate"
           (keyup)="onInputChanged()"
           [ngClass]="{
                'form-field__input--error': isInvalidDate
            }">
    <button class="ui-date-picker__button"
            (click)="calendarToggleVisibility()"
            [disabled]="disable">
        <i class="material-icons">today</i>
    </button>

    <div class="ui-date-picker__calendar"
        *ngIf="calendarIsVisible">

        <div class="ui-date-picker__hideborders"
             (click)="calendarToggleVisibility()">
        </div>

        <div class="ui-date-picker__month-year">
            <button (click)="subtractMonth()">
                <i class="material-icons ui-date-picker__backward">
                    keyboard_arrow_left</i>
            </button>
            <div class="ui-date-picker__actual-month-year">
                {{monthNames[month-1]}} {{year}}
            </div>
            <button (click)="addMonth()">
                <i class="material-icons ui-date-picker__forward">
                    keyboard_arrow_right</i>
            </button>
        </div>

        <div class="ui-date-picker__days-list-container">
            <table class="ui-date-picker__days-list">
                <thead class="ui-date-picker__week-days">
                    <tr>
                        <td *ngFor="let weekday of weekDays">
                            {{weekday}}
                        </td>
                    </tr>
                </thead>
                <tbody class="ui-date-picker__days">
                    <tr class="ui-date-picker__days-line"
                        *ngFor="let dayOfMonthSet of daysOfMonth; let i = index">
                       <td *ngFor="let dayNumber of daysOfMonth[i]">
                            <button class="ui-date-picker__day"
                                 *ngIf="dayNumber !== ''"
                                 [ngClass]="{
                                    'ui-date-picker__day--selected': isSelectedDate(dayNumber)
                                  }"
                                 (click)="onSelectedDate(dayNumber)">
                             {{dayNumber}}
                            </button>
                       </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


`
            },] },
];
/** @nocollapse */
UiDateInputComponent.ctorParameters = () => [];
UiDateInputComponent.propDecorators = {
    "disable": [{ type: Input },],
    "value": [{ type: Input },],
    "placeholder": [{ type: Input },],
    "dateFormat": [{ type: Input },],
    "monthNames": [{ type: Input },],
    "weekDays": [{ type: Input },],
    "onDateSelected": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiUploadImageComponent {
    constructor() {
        this.viewModel = {
            src: ''
        };
        this.onFileChanged = new EventEmitter();
    }
    /**
     * @param {?} uploadedFile
     * @return {?}
     */
    emitUpload(uploadedFile) {
        this.onFileChanged.emit({
            selectedFile: uploadedFile
        });
    }
}
UiUploadImageComponent.decorators = [
    { type: Component, args: [{
                selector: 'ui-upload-image',
                template: ` <div class="ui-upload-image__content__containers--upload">
    <div class="ui-upload-image__image-placeholder">
        <input type="file"
               class="ui-upload-image__image-uploader"
               (change)="emitUpload($event.target.files[0])">
        <i class="material-icons ui-upload-image__icon-upload"
           [ngClass]="{
                'ui-upload-image__icon-upload--image-selected': viewModel.src && viewModel.src !== ''
            }">file_upload</i>
        <img class="ui-upload-image__image"
             [attr.src]="viewModel.src">
    </div>
</div>
`
            },] },
];
/** @nocollapse */
UiUploadImageComponent.ctorParameters = () => [];
UiUploadImageComponent.propDecorators = {
    "viewModel": [{ type: Input },],
    "onFileChanged": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class HuubMaterialLibModule {
}
HuubMaterialLibModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, FormsModule, OverlayModule],
                declarations: [
                    UiToggleButtonsComponent,
                    UiTabsComponent,
                    UiMenuBarComponent,
                    UiFilterTagsComponent,
                    UiFilterComponent,
                    UiActivityTimelineComponent,
                    UiSpinnerComponent,
                    UiInputSearchComponent,
                    UiTablesComponent,
                    UiButtonActionsComponent,
                    UiTooltipsComponent,
                    UiLoaderComponent,
                    UiTablesPlaceholderComponent,
                    UiPopupMessageComponent,
                    UiAutoCompleteComponent,
                    UiDateInputComponent,
                    UiUploadImageComponent
                ],
                exports: [
                    UiToggleButtonsComponent,
                    UiTabsComponent,
                    UiMenuBarComponent,
                    UiFilterTagsComponent,
                    UiFilterComponent,
                    UiActivityTimelineComponent,
                    UiSpinnerComponent,
                    UiInputSearchComponent,
                    UiTablesComponent,
                    UiButtonActionsComponent,
                    UiTooltipsComponent,
                    UiLoaderComponent,
                    UiTablesPlaceholderComponent,
                    UiPopupMessageComponent,
                    UiAutoCompleteComponent,
                    UiDateInputComponent,
                    UiUploadImageComponent
                ],
                providers: [MaterialHelper, UiModalService]
            },] },
];
/** @nocollapse */
HuubMaterialLibModule.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Generated bundle index. Do not edit.
 */

export { HuubMaterialLibModule, UiActivityTimelineComponent as ɵg, UiAutoCompleteComponent as ɵp, UiButtonActionsComponent as ɵk, UiFilterTagsComponent as ɵd, UiFilterComponent as ɵe, UiDateInputComponent as ɵq, UiInputSearchComponent as ɵi, UiSpinnerComponent as ɵh, UiUploadImageComponent as ɵr, MaterialHelper as ɵf, UiLoaderComponent as ɵm, UiMenuBarComponent as ɵc, UiTooltipsComponent as ɵl, UiModalService as ɵs, UiPopupMessageComponent as ɵo, UiTablesPlaceholderComponent as ɵn, UiTablesComponent as ɵj, UiTabsComponent as ɵb, UiToggleButtonsComponent as ɵa };
//# sourceMappingURL=huub-material-lib.js.map
