# Huub Material (Design System)

Welcome to the `huub-material` repository.
The  **Huub Material**  was built as a [Angular](https://angular.io/) library to be used in future Frontend developments.

## Project Setup

The whole project is built around [Angular-Cli](https://cli.angular.io/), to automate repetitive
tasks, both for development and deployment. Besides that, this project also uses [NPM](https://www.npmjs.com), a package
manager, to handle all dependencies for the project itself. Plugins and NPM
dependencies are all installed and managed via [npm](https://www.npmjs.org/), the [Node.js](http://nodejs.org/) package
manager. Before continuing, please download and install the latest stable **Node.js** version.

After installing the latest **Node.js** version run the following comand line to install all project dependencies:

    npm install

This could take a while, so grab yourself a cup of coffee and please be patient. If the command fails, make sure you are
not connect to a VPN and try to run it again.

**Note:** If you are on Windows you shouldn't, please quit now.

## NPM Tasks Usage

* **build**: Generates the library bundle
* **buildApp**: Builds the sandbox app into a dist folder
* **start**: Run an HTTP server with the application in ES6
* **test**: Run application unit tests (ChromeHeadless)
* **testDev**: Run application unit tests for development enviroment
* **lint**: Run lint in all directory files

## Repository Conventions

Git is a very powerful tool and can help ease the work of everyone if used properly, following certain proven
conventions and/or good practice recommendations - especially concerning commits and associated messages. More about
that below, first, let's start with the branch naming scheme.

### Branch Names

For the branch naming scheme, it's recommended to always prefix the branch name with a "folder name" to easily identify
the type of branch. This helps with branch sorting when listing all local/remote branches with the `git branch -a`
command. Also, some Git clients, if not most, provide a way to neatly organize branches by folders. The following is a
list of recommendations for "folder names":

* `backlog/<feature-name>`: Branch for a specific feature that was in development, not yet integrated with the `master`
  branch, to be integrated in the foreseeable future.
* `feature/<feature-name>`: Development branch for a specific feature (typically a user story) that is a work in
  progress.
* `fix/<fix-name>`: Development branch for a specific issue or bugfix (typically a defect) found during development or
  already in production.
* `refactor/<refactor-name>`: Development branch for refactoring a specific piece of legacy/unsustainable code.
* `spike/<spike-name>`: Branch used for testing and research work on a specific feature to be implemented in the future.

So the remote repository doesn't become a mess, certain general conventions are recommended for this project:

* Always use [spinal-case](http://stackoverflow.com/a/17820138) (or [kebab-case](http://stackoverflow.com/a/12273101))
  for branch names. All characters lower case and spaces replaced by hyphens, that is.
* Choose **meaningful and short names** for branch names so each branch is easily identified by reading the branch name.
* Please avoid naming a branch `USXXXXX` or `DEXXXXX`; unless you're having a hard time choosing a meaningful and short
  name.

### Commits

#### Commit Messages

Commit messages should be concise and easy to read; the following is a list of common good practices:

* Wrap the commit message between 50 and 72 characters whenever possible (don't forget we are required to prefix each
  commit message with `SCR: XXXXX -` or `SCR: N/A -`) and do not end it with a period (i.e., `.`).
* If more information is required, insert a single blank line after the first line and provide a detailed description in
  the following lines, breaking paragraphs where needed.
* Do not assume the code is self-evident/self-documenting. Describe _why_ a change is being made.
* Write your commit message in the imperative: _"Fix bug"_ and not _"Fixed bug"_ or _"Fixes bug"_.

#### Micro Commits

There's a few things to keep in mind and avoid when creating commits; the following is a list of common good practices:

* Do not mix whitespace and/or code style changes with functional code changes.
* Do not mix two completely unrelated functional changes.
* Do not send large new features into a single giant commit.

Think about it this way, if you write your commit message like _"Add this **and** that"_ or _"Fix bug **and** add
this"_, you are probably doing it wrong. In a nutshell, train yourself to create **micro commits** whenever possible.

To better understand the reasoning behind all these good practices described above, please read more in the following
links:

* [A Note About Git Commit Messages](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html)
* [GIT Commit Good Practice](https://wiki.openstack.org/wiki/GitCommitMessages)
* [Micro Commits](http://lucasr.org/2011/01/29/micro-commits/)

### Available components
- `ui-activity-timeline`
- `ui-auto-complete`
- `ui-button-actions`
- `ui-filter`
- `ui-filter-tags`
- `ui-date-input`
- `ui-input-search`
- `ui-spinner`
- `ui-upload-image`
- `ui-loader`
- `ui-menu-bar`
- `ui-tooltips`
- `ui-popup-message`
- `ui-tables`
- `ui-tabs`
- `ui-toggle-buttons`
- `ui-progress-bar`

### Available helpers
- `MaterialHelper`
- `UiModalService`

#### One last thing...

If you have finished your work on a certain branch and have already merged your changes to the `master` branch (after
making sure it passed all unit tests and went through the normal code review process), **please remove the branch from
the remote repository**. This also avoids certain merge issues in the future if you keep your local `master` branch
updated with the latest changes from `origin/master` and always create new branches from `origin/master` latest
revision.

> _A clean house is a happy house..._
